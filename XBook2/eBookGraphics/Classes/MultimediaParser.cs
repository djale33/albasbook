﻿using KlasimeBase;
using KlasimeBase.Interfaces;
using KlasimeBase.Log;
using KlasimeViews;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using XBook2;

namespace eBookGraphics
{

    public class MultimediaParser : BaseParser
    {
        public string user = "MultimediaParser";
        #region VARIABLES

        bool autoBaseProp;
        float rullerWidth = 40;
        public MultimediaType MultimediaType { get; private set; }
        SKBitmap bmpAudio;
        SKBitmap bmpPhotoSlide;
        SKBitmap bmpOpenWindow;
        SKBitmap bmpVideo;
        SKBitmap bmpText;
        SKBitmap bmpYouTube;
        SKBitmap bmpExternalLink;
        SKBitmap bmpFile;

        SKBitmap bmpPhotoRight;
        SKBitmap bmpPhotoLeft;
        SKBitmap bmpTextRight;
        SKBitmap bmpTextLeft;

        SKBitmap bmpAudioRight;
        SKBitmap bmpAudioLeft;
        SKBitmap bmpVideoRight;
        SKBitmap bmpVideoLeft;

        SKBitmap bmpYoutubeRight;
        SKBitmap bmpYoutubeLeft;

        SKBitmap bmpLinkRight;
        SKBitmap bmpLinkLeft;

        SKBitmap bmpFileRight;
        SKBitmap bmpFileLeft;

        SKBitmap bmpNoteAdd;
        SKBitmap bmpAudioAdd;
        SKBitmap bmpVideoAdd;
        SKBitmap bmpPhotoAdd;
        SKBitmap bmpFileAdd;
        SKBitmap bmpLinkAdd;
        SKBitmap bmpYoutubeAdd;

        SKBitmap bmpPhotoRightCustom;
        SKBitmap bmpPhotoLeftCustom;

        SKBitmap bmpTextRightCustom;
        SKBitmap bmpTextLeftCustom;

        SKBitmap bmpAudioRightCustom;
        SKBitmap bmpAudioLeftCustom;

        SKBitmap bmpVideoRightCustom;
        SKBitmap bmpVideoLeftCustom;

        SKBitmap bmpYoutubeRightCustom;
        SKBitmap bmpYoutubeLeftCustom;

        SKBitmap bmpLinkRightCustom;
        SKBitmap bmpLinkLeftCustom;

        SKBitmap bmpFileRightCustom;
        SKBitmap bmpFileLeftCustom;

        SKImageInfo imageInfo;

        SKPoint mouseLocation;

        string logText = "";
        #endregion

        #region PROPERTIES
        public SKBitmap BmpAudio { get => bmpAudio; set => bmpAudio = value; }
        public SKBitmap BmpPhotoSlide { get => bmpPhotoSlide; set => bmpPhotoSlide = value; }
        public SKBitmap BmpOpenWindow { get => bmpOpenWindow; set => bmpOpenWindow = value; }
        public SKBitmap BmpVideo { get => bmpVideo; set => bmpVideo = value; }
        public bool AutoBaseProp { get => autoBaseProp; set => autoBaseProp = value; }
        public SKBitmap BmpText { get => bmpText; set => bmpText = value; }
        public SKBitmap BmpYouTube { get => bmpYouTube; set => bmpYouTube = value; }
        public SKBitmap BmpExternalLink { get => bmpExternalLink; set => bmpExternalLink = value; }
        public SKBitmap BmpPhotoRight { get => bmpPhotoRight; set => bmpPhotoRight = value; }
        public SKBitmap BmpPhotoLeft { get => bmpPhotoLeft; set => bmpPhotoLeft = value; }
        public SKBitmap BmpTextRight { get => bmpTextRight; set => bmpTextRight = value; }
        public SKBitmap BmpTextLeft { get => bmpTextLeft; set => bmpTextLeft = value; }
        public SKBitmap BmpAudioRight { get => bmpAudioRight; set => bmpAudioRight = value; }
        public SKBitmap BmpAudioLeft { get => bmpAudioLeft; set => bmpAudioLeft = value; }
        public SKBitmap BmpVideoRight { get => bmpVideoRight; set => bmpVideoRight = value; }
        public SKBitmap BmpVideoLeft { get => bmpVideoLeft; set => bmpVideoLeft = value; }
        public SKBitmap BmpYoutubeRight { get => bmpYoutubeRight; set => bmpYoutubeRight = value; }
        public SKBitmap BmpYoutubeLeft { get => bmpYoutubeLeft; set => bmpYoutubeLeft = value; }
        public SKBitmap BmpLinkRight { get => bmpLinkRight; set => bmpLinkRight = value; }
        public SKBitmap BmpLinkLeft { get => bmpLinkLeft; set => bmpLinkLeft = value; }
        public SKBitmap BmpNoteAdd { get => bmpNoteAdd; set => bmpNoteAdd = value; }
        public SKBitmap BmpAudioAdd { get => bmpAudioAdd; set => bmpAudioAdd = value; }
        public SKBitmap BmpVideoAdd { get => bmpVideoAdd; set => bmpVideoAdd = value; }
        public SKBitmap BmpPhotoAdd { get => bmpPhotoAdd; set => bmpPhotoAdd = value; }
        public float RullerWidth { get => rullerWidth; set => rullerWidth = value; }
        public SKBitmap BmpPhotoRightCustom { get => bmpPhotoRightCustom; set => bmpPhotoRightCustom = value; }
        public SKBitmap BmpPhotoLeftCustom { get => bmpPhotoLeftCustom; set => bmpPhotoLeftCustom = value; }
        public SKBitmap BmpTextRightCustom { get => bmpTextRightCustom; set => bmpTextRightCustom = value; }
        public SKBitmap BmpTextLeftCustom { get => bmpTextLeftCustom; set => bmpTextLeftCustom = value; }
        public SKBitmap BmpAudioRightCustom { get => bmpAudioRightCustom; set => bmpAudioRightCustom = value; }
        public SKBitmap BmpAudioLeftCustom { get => bmpAudioLeftCustom; set => bmpAudioLeftCustom = value; }
        public SKBitmap BmpVideoRightCustom { get => bmpVideoRightCustom; set => bmpVideoRightCustom = value; }
        public SKBitmap BmpVideoLeftCustom { get => bmpVideoLeftCustom; set => bmpVideoLeftCustom = value; }
        public SKBitmap BmpYoutubeRightCustom { get => bmpYoutubeRightCustom; set => bmpYoutubeRightCustom = value; }
        public SKBitmap BmpYoutubeLeftCustom { get => bmpYoutubeLeftCustom; set => bmpYoutubeLeftCustom = value; }
        public SKBitmap BmpLinkRightCustom { get => bmpLinkRightCustom; set => bmpLinkRightCustom = value; }
        public SKBitmap BmpLinkLeftCustom { get => bmpLinkLeftCustom; set => bmpLinkLeftCustom = value; }
        public SKBitmap BmpFileAdd { get => bmpFileAdd; set => bmpFileAdd = value; }
        public SKBitmap BmpLinkAdd { get => bmpLinkAdd; set => bmpLinkAdd = value; }
        public SKBitmap BmpYoutubeAdd { get => bmpYoutubeAdd; set => bmpYoutubeAdd = value; }
        public SKBitmap BmpFile { get => bmpFile; set => bmpFile = value; }
        public SKBitmap BmpFileRight { get => bmpFileRight; set => bmpFileRight = value; }
        public SKBitmap BmpFileLeft { get => bmpFileLeft; set => bmpFileLeft = value; }
        public SKBitmap BmpFileRightCustom { get => bmpFileRightCustom; set => bmpFileRightCustom = value; }
        public SKBitmap BmpFileLeftCustom { get => bmpFileLeftCustom; set => bmpFileLeftCustom = value; }

        #endregion

        #region CONSTRUCTOR
        public MultimediaParser(string _user,SKPoint _location, SKCanvas _canvas, SKImageInfo _imageInfo)
        {
            user = _user;
            mouseLocation = _location;
            Canvas = _canvas;
            imageInfo = _imageInfo;
        }
        public MultimediaParser( SKCanvas _canvas)
        {
            
            Canvas = _canvas;

        }
        #endregion


        #region CustomEvents
        public delegate void MultimediaHandler(object sender, MultimediaRulerEventArgs e);
        public event MultimediaHandler OnSetMultimedia;
        private void OnFindMultimedia(object sender, MultimediaRulerEventArgs e)
        {
            if (OnSetMultimedia != null)
            {
                OnSetMultimedia(sender, e);
            }
        }
        #endregion


        #region METHODS
        public async Task<string> MultimediaItemsDrawOnPage(float fScale,bool Ruller,
            Book book,
            PageType pageType,
            bool readyForMove,
            float startXforRemove,
            float newXforRemove,
            int tabletMode = 0,
            MultimediaType multimediaTypetoAdd = MultimediaType.Unknown)
        {
            string res = "";
            #region DrawMultimedia

            if (book.HTPageAnnos.Count > 0)
            {
                PageAnno pageAnno = (PageAnno)book.HTPageAnnos[CurrentPage];
                if (pageAnno != null)
                {
                    foreach (Anno nodAnno in pageAnno.Annos)
                    {
                        //draw icon on page

                        DrawAnno drawAnno = await SetMultimediaItem(nodAnno, Ruller, fScale, pageType, readyForMove, startXforRemove, newXforRemove, tabletMode);
                        res = logText;
                        //remeber object for mouse click , to run the player
                        if (drawAnno != null)
                        {
                            try
                            {
                                if (nodAnno.AnnoId != null)
                                {
                                    if (!pageAnno.htDrawAnno.ContainsKey(nodAnno.AnnoId))
                                    {
                                        pageAnno.htDrawAnno.Add(nodAnno.AnnoId, drawAnno);
                                    }
                                    else
                                    {
                                        DrawAnno tmpAnno = (DrawAnno)pageAnno.htDrawAnno[nodAnno.AnnoId];
                                        if (tmpAnno.irectHeight != nodAnno.Location.Height)
                                        {
                                            //DbLog.Log(MethodBase.GetCurrentMethod(), "need to update !");
                                            pageAnno.htDrawAnno[nodAnno.AnnoId] = drawAnno;
                                        }
                                    }
                                }
                                else
                                {
                                    //logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(),null, "key is null");
                                }
                            }
                            catch (Exception ee)
                            {
                                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                            }
                        }
                    }


                }

            }
            #endregion


            #region Add new Multimedia item

            float irectWidth = 30, irectHeight = 30;
            float irectX = (mouseLocation.X - irectWidth / 2);
            float irectY = mouseLocation.Y - irectWidth / 2;

            SKRect rectOnPage = SKRect.Create(irectX, irectY, irectWidth, irectHeight);
            switch (multimediaTypetoAdd)
            {
                case MultimediaType.Note:
                    Canvas.DrawBitmap(BmpNoteAdd, rectOnPage, BitmapStretch.Fill);
                    break;
                case MultimediaType.File:
                    Canvas.DrawBitmap(BmpFileAdd, rectOnPage, BitmapStretch.Fill);
                    break;
                case MultimediaType.Audio:
                    Canvas.DrawBitmap(BmpAudioAdd, rectOnPage, BitmapStretch.Fill);
                    break;
                case MultimediaType.Video:
                    Canvas.DrawBitmap(BmpVideoAdd, rectOnPage, BitmapStretch.Fill);
                    break;
                case MultimediaType.Link:
                    Canvas.DrawBitmap(BmpLinkAdd, rectOnPage, BitmapStretch.Fill);
                    break;
                case MultimediaType.PhotoGalery:
                    Canvas.DrawBitmap(BmpPhotoAdd, rectOnPage, BitmapStretch.Fill);
                    break;
                case MultimediaType.YouTube:
                    Canvas.DrawBitmap(BmpYoutubeAdd, rectOnPage, BitmapStretch.Fill);
                    break;

            }
            #endregion
            return res;
        }
        public void DrawItemOnPage()
        {

        }

        public async Task<DrawAnno> SetMultimediaItem(Anno nodAnno,bool Ruller,
            float fScale,
            PageType pageType,
            bool readyForMove,
            float startXforRemove,
            float newXforRemove,
            int tabletMode = 0
            )
        {
            DrawAnno drawAnnoToRemember = null;
            try
            {

                double rectX = 0;
                double rectY = 0;
                double rectWidth = 0;
                double rectHeight = 0;
                rectX = nodAnno.Location.X;
                rectY = nodAnno.Location.Y;
                rectWidth = nodAnno.Location.Width;
                rectHeight = nodAnno.Location.Height;
                var scaleHeight = imageInfo.Height;

                #region Automatic BaseProp
                //if (AutoBaseProp)
                //    BaseProp = scaleHeight;
                #endregion

                float irectX = Base.SetInt32(rectX * BaseProp * XProp);
                //if(pageType==PageType.Left)
                //{
                //    irectX = irectX + RullerWidth;
                //}
                float irectY = Base.SetInt32(rectY * BaseProp * YProp);
                float irectWidth = Base.SetInt32(rectWidth * BaseProp * WProp * 1.4);
                float irectHeight = Base.SetInt32(rectHeight * BaseProp * HProp * 1.4);

                float irectWidthTableMode = irectWidth * 2;
                float irectHeightTableMode = irectHeight * 2;


                SKRect rectOnPage = SKRect.Create(irectX, irectY, irectWidth, irectHeight);
                if (nodAnno.Cutom)
                {
                    rectOnPage = SKRect.Create(irectX, irectY, (float)(irectWidth * 1.1), irectHeight);
                }

                SKRect rectRulerRight = SKRect.Create(imageInfo.Width, irectY, irectWidth, (float)(irectHeight / 1.5));
                SKRect rectRulerLeft = SKRect.Create(14, irectY, irectWidth, (float)(irectHeight / 1.5));

                if (nodAnno.Cutom)
                {
                    if (readyForMove == true & pageType == PageType.Right)
                    {
                        rectRulerRight = SKRect.Create(newXforRemove, irectY + 4, irectWidth, irectHeight - 8);
                    }
                    if (readyForMove == true & pageType == PageType.Left)
                    {
                        if (newXforRemove < startXforRemove)
                        {
                            if (newXforRemove != 0)
                                rectRulerLeft = SKRect.Create(newXforRemove, irectY + 4, irectWidth, irectHeight - 8);
                        }

                    }
                }

                drawAnnoToRemember = new DrawAnno(irectX, irectY, irectWidth, irectHeight);

                // the rectangle
                if (tabletMode == 1)
                {
                    rectOnPage = SKRect.Create(irectX, irectY, irectWidthTableMode, irectHeightTableMode);
                }

                // the brush (fill with blue)..todo: param
                var paint = new SKPaint
                {
                    Style = SKPaintStyle.Stroke,
                    Color = SKColors.Blue
                };
                // draw
                switch (nodAnno.Action.ActionType)
                {
                    case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenURL":
                        #region Draw on page
                        string url = nodAnno.Action.Url;

                        if (nodAnno.Cutom == false)
                        {
                            Canvas.DrawBitmap(bmpExternalLink, rectOnPage, BitmapStretch.Fill);
                        }
                        else
                        {
                            Canvas.DrawBitmap(bmpLinkAdd, rectOnPage, BitmapStretch.Fill);
                        }
                        #endregion

                        #region draw annotation
                        if (Ruller)
                        {
                            if (pageType == PageType.Right)
                                Canvas.DrawBitmap(BmpLinkRight, rectRulerRight, BitmapStretch.Fill);

                            if (pageType == PageType.Left)
                                Canvas.DrawBitmap(BmpLinkLeft, rectRulerLeft, BitmapStretch.Fill);

                            if (nodAnno.Cutom == false)
                            {
                                if (pageType == PageType.Right)
                                    Canvas.DrawBitmap(BmpLinkRight, rectRulerRight, BitmapStretch.Fill);

                                if (pageType == PageType.Left)
                                    Canvas.DrawBitmap(BmpLinkLeft, rectRulerLeft, BitmapStretch.Fill);
                            }
                            else
                            {
                                if (pageType == PageType.Right)
                                    Canvas.DrawBitmap(BmpLinkRightCustom, rectRulerRight, BitmapStretch.Fill);

                                if (pageType == PageType.Left)
                                {
                                    Canvas.DrawBitmap(BmpLinkLeftCustom, rectRulerLeft, BitmapStretch.Fill);
                                    logText = "rectRulerLeft " + rectRulerLeft.Right;
                                }
                            }
                        }
                        #endregion

                        MultimediaRulerEventArgs pe4 = new MultimediaRulerEventArgs(MultimediaType.Link, nodAnno, (int)rectOnPage.Top);
                        OnFindMultimedia(this, pe4);
                        break;
                    case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenWindow":
                        #region Draw on page
                        if (nodAnno.Cutom == false)
                        {
                            Canvas.DrawBitmap(BmpVideo, rectOnPage, BitmapStretch.Fill);
                        }
                        else
                        {
                            Canvas.DrawBitmap(BmpVideoAdd, rectOnPage, BitmapStretch.Fill);
                        }
                        #endregion

                        #region draw annotation
                        if (Ruller)
                        {
                            if (nodAnno.Cutom == false)
                            {
                                if (pageType == PageType.Right)
                                    Canvas.DrawBitmap(bmpVideoRight, rectRulerRight, BitmapStretch.Fill);

                                if (pageType == PageType.Left)
                                    Canvas.DrawBitmap(bmpVideoLeft, rectRulerLeft, BitmapStretch.Fill);
                            }
                            else
                            {
                                if (pageType == PageType.Right)
                                    Canvas.DrawBitmap(bmpVideoRightCustom, rectRulerRight, BitmapStretch.Fill);

                                if (pageType == PageType.Left)
                                {
                                    Canvas.DrawBitmap(bmpVideoLeftCustom, rectRulerLeft, BitmapStretch.Fill);
                                    logText = "rectRulerLeft " + rectRulerLeft.Right;
                                }
                            }
                        }
                        #endregion


                        MultimediaRulerEventArgs pe3 = new MultimediaRulerEventArgs(MultimediaType.Video, nodAnno, (int)rectOnPage.Top);
                        OnFindMultimedia(this, pe3);
                        break;
                    case "com.mobiano.flipbook.pageeditor.TAnnoActionShowInformation":
                        #region Draw on page
                        if (nodAnno.Cutom == false)
                        {
                            Canvas.DrawBitmap(BmpText, rectOnPage, BitmapStretch.Fill);
                        }
                        else
                        {
                            Canvas.DrawBitmap(BmpNoteAdd, rectOnPage, BitmapStretch.Fill);
                        }
                        #endregion

                        #region draw annotation
                        if (Ruller)
                        {
                            if (nodAnno.Cutom == false)
                            {
                                if (pageType == PageType.Right)
                                    Canvas.DrawBitmap(bmpTextRight, rectRulerRight, BitmapStretch.Fill);

                                if (pageType == PageType.Left)
                                    Canvas.DrawBitmap(BmpTextLeft, rectRulerLeft, BitmapStretch.Fill);
                            }
                            else
                            {
                                if (pageType == PageType.Right)
                                    Canvas.DrawBitmap(bmpTextRightCustom, rectRulerRight, BitmapStretch.Fill);

                                if (pageType == PageType.Left)
                                {
                                    Canvas.DrawBitmap(BmpTextLeftCustom, rectRulerLeft, BitmapStretch.Fill);
                                    logText = "rectRulerLeft " + rectRulerLeft.Right;
                                }
                            }
                        }
                        #endregion
                        break;
                    case "com.mobiano.flipbook.Action.TAnnoActionPlayAudio":
                        {
                            #region Draw on page
                            if (nodAnno.Cutom == false)
                            {
                                Canvas.DrawBitmap(BmpAudio, rectOnPage, BitmapStretch.Fill);
                            }
                            else
                            {
                                Canvas.DrawBitmap(BmpAudioAdd, rectOnPage, BitmapStretch.Fill);
                            }
                            #endregion

                            #region draw annotation
                            if (Ruller)
                            {
                                if (nodAnno.Cutom == false)
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(bmpAudioRight, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                        Canvas.DrawBitmap(BmpAudioLeft, rectRulerLeft, BitmapStretch.Fill);
                                }
                                else
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(bmpAudioRightCustom, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                    {
                                        Canvas.DrawBitmap(BmpAudioLeftCustom, rectRulerLeft, BitmapStretch.Fill);
                                        logText = "rectRulerLeft " + rectRulerLeft.Right;
                                    }
                                }
                            }
                            #endregion

                            MultimediaRulerEventArgs pe2 = new MultimediaRulerEventArgs(MultimediaType.Audio, nodAnno, (int)rectOnPage.Top);
                            OnFindMultimedia(this, pe2);
                            break;
                        }
                    case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayAudio":
                        {
                            #region Draw on page
                            if (nodAnno.Cutom == false)
                            {
                                Canvas.DrawBitmap(BmpAudio, rectOnPage, BitmapStretch.Fill);
                            }
                            else
                            {
                                Canvas.DrawBitmap(BmpAudioAdd, rectOnPage, BitmapStretch.Fill);
                            }
                            #endregion

                            #region draw annotation
                            if (Ruller)
                            {
                                if (nodAnno.Cutom == false)
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(bmpAudioRight, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                        Canvas.DrawBitmap(BmpAudioLeft, rectRulerLeft, BitmapStretch.Fill);
                                }
                                else
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(bmpAudioRightCustom, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                    {
                                        Canvas.DrawBitmap(BmpAudioLeftCustom, rectRulerLeft, BitmapStretch.Fill);
                                        logText = "rectRulerLeft " + rectRulerLeft.Right;
                                    }
                                }
                            }
                            #endregion

                            MultimediaRulerEventArgs pe2 = new MultimediaRulerEventArgs(MultimediaType.Audio, nodAnno, (int)rectOnPage.Top);
                            OnFindMultimedia(this, pe2);
                            break;
                        }
                    case "com.mobiano.flipbook.pageeditor.TAnnoActionPhotoSlide":
                        {
                            
                            #region Draw on page
                            if (nodAnno.Cutom == false)
                            {
                                Canvas.DrawBitmap(BmpPhotoSlide, rectOnPage, BitmapStretch.Fill);
                            }
                            else
                            {
                                Canvas.DrawBitmap(bmpPhotoAdd, rectOnPage, BitmapStretch.Fill);
                            }
                            #endregion

                            #region draw annotation
                            if (Ruller)
                            {
                                if (nodAnno.Cutom == false)
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(BmpPhotoRight, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                        Canvas.DrawBitmap(BmpPhotoLeft, rectRulerLeft, BitmapStretch.Fill);
                                }
                                else
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(BmpPhotoRightCustom, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                    {
                                        Canvas.DrawBitmap(BmpPhotoLeftCustom, rectRulerLeft, BitmapStretch.Fill);
                                        logText = "rectRulerLeft " + rectRulerLeft.Right;
                                    }
                                }
                            }
                            #endregion

                            MultimediaRulerEventArgs pe2 = new MultimediaRulerEventArgs(MultimediaType.PhotoGalery, nodAnno, (int)rectOnPage.Top);
                            pe2.BaseProp = BaseProp;
                            pe2.origY = rectY;
                            pe2.YProp = YProp;
                            OnFindMultimedia(this, pe2);
                            break;
                        }
                    case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayVideo":
                        {
                            #region Draw On Page
                            MultimediaRulerEventArgs pe2 = null;
                            SKBitmap tmpVideo = null;
                            SKBitmap tmpVideoRuler = null;


                            if (nodAnno.Action.ResourceContent == null)
                            {
                                if (nodAnno.Action.WindowType == "TYPE_YOUTUBE")
                                {
                                    pe2 = new MultimediaRulerEventArgs(MultimediaType.YouTube, nodAnno, (int)rectOnPage.Top);

                                    if (nodAnno.Cutom == false)
                                    {
                                        tmpVideo = bmpYouTube;
                                        if (pageType == PageType.Right)
                                            tmpVideoRuler = bmpYoutubeRight;

                                        if (pageType == PageType.Left)
                                            tmpVideoRuler = bmpYoutubeLeft;
                                    }
                                    else
                                    {
                                        tmpVideo = bmpYoutubeAdd;
                                        if (pageType == PageType.Right)
                                            tmpVideoRuler = bmpYoutubeRightCustom;

                                        if (pageType == PageType.Left)
                                            tmpVideoRuler = bmpYoutubeLeftCustom;
                                    }
                                }
                            }
                            else
                            {
                                pe2 = new MultimediaRulerEventArgs(MultimediaType.Video, nodAnno, (int)rectOnPage.Top);
                                tmpVideo = bmpVideo;
                                if (nodAnno.Cutom == false)
                                {
                                    tmpVideo = bmpVideo;
                                }
                                else
                                {
                                    tmpVideo = bmpVideoAdd;
                                }

                            }
                            if (tmpVideo != null)
                            {
                                Canvas.DrawBitmap(tmpVideo, rectOnPage, BitmapStretch.Fill);
                                OnFindMultimedia(this, pe2);
                            }
                            else
                            {
                                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(),null, "Video Problem");
                            }
                            #endregion

                            #region draw annotation
                            if (Ruller)
                            {
                                if (tmpVideoRuler != null)
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(tmpVideoRuler, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                        Canvas.DrawBitmap(tmpVideoRuler, rectRulerLeft, BitmapStretch.Fill);
                                }
                                else
                                {
                                    logInstance.Log(user, Severity.Low, MethodBase.GetCurrentMethod(), null, "Video Ruler Problem");
                                }
                            }
                            #endregion
                            break;
                        }
                    case "com.mobiano.flipbook.pageeditor.TAnnoActionFile":
                        {
                            #region Draw On Page
                            if (nodAnno.Cutom == false)
                            {
                                Canvas.DrawBitmap(BmpFile, rectOnPage, BitmapStretch.Fill);
                            }
                            else
                            {
                                Canvas.DrawBitmap(BmpFileAdd, rectOnPage, BitmapStretch.Fill);
                            }
                            #endregion

                            #region draw annotation
                            if (Ruller)
                            {
                                if (nodAnno.Cutom == false)
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(BmpFileRight, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                        Canvas.DrawBitmap(BmpFileLeft, rectRulerLeft, BitmapStretch.Fill);
                                }
                                else
                                {
                                    if (pageType == PageType.Right)
                                        Canvas.DrawBitmap(bmpFileRightCustom, rectRulerRight, BitmapStretch.Fill);

                                    if (pageType == PageType.Left)
                                    {
                                        Canvas.DrawBitmap(BmpFileLeftCustom, rectRulerLeft, BitmapStretch.Fill);
                                        logText = "rectRulerLeft " + rectRulerLeft.Right;
                                    }
                                }
                            }
                            #endregion

                            MultimediaRulerEventArgs pe2 = new MultimediaRulerEventArgs(MultimediaType.File, nodAnno, (int)rectOnPage.Top);
                            OnFindMultimedia(this, pe2);

                            break;
                        }
                    default:
                        string missing = nodAnno.Action.ActionType;
                        break;
                }


            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }

            return drawAnnoToRemember;

        }
     
        #endregion
    }
}
