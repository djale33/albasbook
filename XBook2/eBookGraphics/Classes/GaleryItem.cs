﻿using KlasimeBase.Log;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xamarin.Forms;

namespace eBookGraphics.Classes
{
    public class GaleryItem : BaseLog
    {
        public static string user = "program";
        #region VARIABLE
        private int iD;
        
        private string name;
        private string description;
        private GaleryType type;
        SKCanvasView skAudio;
        int sAudio = 40;
       
        SKBitmap bmpAudio;

        #endregion

        public GaleryItem(GaleryType _type, SKBitmap _bmpAudio)
        {
            type = _type;
            bmpAudio = _bmpAudio;
            SkAudio = new SKCanvasView();
            SkAudio.EnableTouchEvents = true;
            SkAudio.HorizontalOptions = LayoutOptions.FillAndExpand;
            SkAudio.VerticalOptions = LayoutOptions.FillAndExpand;
            SkAudio.PaintSurface += SkAudio_PaintSurface; 
            TapGestureRecognizer tr = new TapGestureRecognizer();
            tr.NumberOfTapsRequired = 2;
            tr.Tapped += Tr_Tapped; 
            SkAudio.GestureRecognizers.Add(tr);
            SkAudio.Touch += SkAudio_Touch;
        }

        #region EVENT
        private void SkAudio_Touch(object sender, SKTouchEventArgs e)
        {
            try
            {
                SKCanvasView canvasView = (SKCanvasView)sender;
                // Convert Xamarin.Forms point to pixels
                SKPoint pt = e.Location;
                SKPoint point =
                    new SKPoint((float)(canvasView.CanvasSize.Width * pt.X / canvasView.Width),
                                (float)(canvasView.CanvasSize.Height * pt.Y / canvasView.Height));

                switch (e.ActionType)
                {
                    case SKTouchAction.Entered:

                        //if (touchIds.Contains(e.Id))
                        //{
                        //    // bitmap.ProcessTouchEvent(args.Id, args.Type, point);

                        //    //  
                        //}
                        break;
                    case SKTouchAction.Pressed:
                        {
                            //bAudioMove = true;
                            //lblSelect.Text = "Audio Press: " ;
                            break;
                        }
                    case SKTouchAction.Moved:
                        {
                           // if (bAudioMove)
                            {
                                /*
                                //find possition on image
                                lblSelect.Text = "M: " + xAudio + "  pX: " + pt.X ;
                                int xd = xAudio - (int)pt.X ;
                                int yd = yAudio - (int)pt.Y ;
                                //todo: find exact position on icon
                                int newXAudio = (int)pt.X - (sAudio / 2);
                                if(newXAudio < xAudio)
                                {
                                    dAudio++;
                                    if(dAudio>100)
                                    {
                                        myGaleryLeftStack.Children.Remove(skAudio);
                                        bAudioMove = false;
                                        xAudio = 100;
                                        
                                    }
                                }
                                xAudio = newXAudio;
                                //Y will not changed to make horisontal move only
                                //yAudio = (int)pt.Y ;
                                canvasView.InvalidateSurface();
                                */
                            }

                            //lblSelect.Text = "Audio Move: " + bAudioMove +" " + pt.X + " " + pt.Y;
                            break;
                        }
                    case SKTouchAction.Released:
                        {
                          //  bAudioMove = false;
                            //dAudio = 0;
                            //lblSelect.Text = "Audio Relsed: ";
                            break;
                        }

                }

            }
            catch (Exception ee)
            {
               logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void Tr_Tapped(object sender, EventArgs e)
        {
            //myGaleryLeftStack.Children.Remove(skAudio);
        }

        private void SkAudio_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            try
            {
                SKImageInfo info = e.Info;
                SKSurface surface = e.Surface;
                SKCanvas canvas = surface.Canvas;
                SKRectI poss = new SKRectI(0, 0, sAudio, sAudio);
                canvas.Clear();
                canvas.DrawBitmap(BmpAudio, poss, BitmapStretch.Fill);

            }
            catch (Exception ee)
            {

               logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion

        #region PROPERTY
        public int ID { get => iD; set => iD = value; }
        public string Name { get => name; set => name = value; }
        public string Description { get => description; set => description = value; }
        public GaleryType Type { get => type; set => type = value; }
        
        public SKCanvasView SkAudio { get => skAudio; set => skAudio = value; }
        public SKBitmap BmpAudio { get => bmpAudio; set => bmpAudio = value; }
        
        #endregion

    }
}
