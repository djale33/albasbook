﻿using KlasimeBase.Log;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace eBookGraphics
{
    public class BaseParser : BaseLog
    {
        SKCanvas canvas;
        private int currentPage = 0;
        double baseProp = 2350; // 440;// 990;
        double yProp = 1.312;// 1.355;
        double xProp = 0.96;// 1.355;
        double hProp = 1.3;
        double wProp = 0.97;
        int width;

        public double BaseProp { get => baseProp; set => baseProp = value; }
        public double YProp { get => yProp; set => yProp = value; }
        public double HProp { get => hProp; set => hProp = value; }
        public double XProp { get => xProp; set => xProp = value; }
        public double WProp { get => wProp; set => wProp = value; }
        public int CurrentPage { get => currentPage; set => currentPage = value; }
        public SKCanvas Canvas { get => canvas; set => canvas = value; }
        public int Width { get => width; set => width = value; }
    }
}
