﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eBookGraphics.Classes
{
    public  enum GaleryType
    {
        Audio,
        Video,
        Flash,
        Photo,
        Text,
        DOC,
        XLS,
        PDF
    }
}
