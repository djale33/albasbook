﻿using SkiaSharp;

namespace eBookGraphics
{
    public class SearchItemText
    {
        private SKPoint searchPossition;
        private SKPoint findStartPossition;
        private SKPoint findEndPossition;
        private string findSearch;
        private float width;
        private float height;
        
        public SearchItemText(SKPoint searchPossition, SKPoint findStartPossition, 
            SKPoint findEndPossition, 
            string findWord)
        {
            this.SearchPossition = searchPossition;
            this.FindStartPossition = findStartPossition;
            this.FindEndPossition = findEndPossition;
            this.FindSearch = findWord;
            this.Width = findEndPossition.X - findStartPossition.X;
            this.Height = findEndPossition.Y - findStartPossition.Y;

            
        }

        public SKPoint SearchPossition { get => searchPossition; set => searchPossition = value; }
        public SKPoint FindStartPossition { get => findStartPossition; set => findStartPossition = value; }
        public SKPoint FindEndPossition { get => findEndPossition; set => findEndPossition = value; }
        public string FindSearch { get => findSearch; set => findSearch = value; }
        public float Width { get => width; set => width = value; }
        public float Height { get => height; set => height = value; }
        
    }
}
