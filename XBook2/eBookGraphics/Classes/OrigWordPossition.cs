﻿using Xamarin.Forms;

namespace eBookGraphics
{
    public class OrigWordPossition
    {
        private string findSearch;
        private double origX;
        private double origY;
        private double origWidth;
        private double origHeight;
        private Point origlocation;

        public OrigWordPossition(string findSearch, double origX, double origY, double origWidth, double origHeight)
        {
            this.findSearch = findSearch;
            this.origX = origX;
            this.origY = origY;
            this.origWidth = origWidth;
            this.origHeight = origHeight;
            this.origlocation = new Point(origX, origY);
        }

        public double OrigX { get => origX; set => origX = value; }
        public double OrigY { get => origY; set => origY = value; }
        public double OrigWidth { get => origWidth; set => origWidth = value; }
        public double OrigHeight { get => origHeight; set => origHeight = value; }
        public string FindSearch { get => findSearch; set => findSearch = value; }
        public Point Origlocation { get => origlocation; set => origlocation = value; }
    }
}
