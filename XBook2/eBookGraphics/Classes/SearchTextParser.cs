﻿using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using SkiaSharp.Views.Forms;
using KlasimeBase;
using System.Xml;
using static Xamarin.Forms.Internals.GIFBitmap;
using KlasimeBase.Log;
using System.Reflection;
using XBook2;
using System.Runtime.InteropServices;
using Xamarin.Forms.Shapes;
using System.Globalization;
using System.IO;
using KlasimeViews;
using System.Threading.Tasks;
using Xamarin.Essentials;
using KlasimeBase.Interfaces;

namespace eBookGraphics
{
    public class SearchTextParser : BaseParser
    {
        public string user = "SearchTextParser";
        #region VARIABLES
        float rullerSmallStep = 4;
        float rullerBigStep = 14;
        int rullerWidth = 100;
        int correctionForRuller = 0;
        private Hashtable htPageWordsOriginal = new Hashtable();
        private Hashtable htPageWords = new Hashtable();
        private Hashtable htNumberAndWords = new Hashtable();
        public ArrayList listWordPositions = new ArrayList();
        public int wordCounter = 1;
        public double ratioSize;
        public int imageWidth;
        public int hoverTextSize = 14;
        double transparencyStart = 0.8;
        double transparencyEnd = 0;
        SKPaintSurfaceEventArgs e;
        int shadowWidth = 200;
        bool bookEncoding;

        #endregion

        #region PROPERTIES
        public double RatioSize { get => ratioSize; set => ratioSize = value; }
        public int ImageWidth { get => imageWidth; set => imageWidth = value; }

        List<string> allWordsOnPage = new List<string>();
        public SKCanvas PictureBox { get => Canvas; set => Canvas = value; }
        public int RullerWidth { get => rullerWidth; set => rullerWidth = value; }
        public float RullerSmallStep { get => rullerSmallStep; set => rullerSmallStep = value; }
        public int ShadowWidth { get => shadowWidth; set => shadowWidth = value; }
        public float RullerBigStep { get => rullerBigStep; set => rullerBigStep = value; }
        public bool BookEncoding { get => bookEncoding; set => bookEncoding = value; }
        public List<string> AllWordsOnPage { get => allWordsOnPage; set => allWordsOnPage = value; }
        public Hashtable HtPageWords { get => htPageWords; set => htPageWords = value; }
        public Hashtable HtPageWordsOriginal { get => htPageWordsOriginal; set => htPageWordsOriginal = value; }
        public Hashtable HtNumberAndWords { get => htNumberAndWords; set => htNumberAndWords = value; }


        #endregion

        #region CONSTRUCTOR
        public SearchTextParser(string _user,SKPaintSurfaceEventArgs _e, bool bookEncoding)
        {
            user = _user;
            e = _e;
            BookEncoding = bookEncoding;
            Common.user = user;
            Base.user = user;
        }
        #endregion

        #region METHODS
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_myBitmap">show current page</param>
        /// <param name="fScale"></param>
        /// <param name="hoverAnno">if not null will draw hover text at uppon right corner of the multimedia icon</param>
        /// <param name="strLenKoef"></param>
        /// add shadow gradient to both pages to simulate real book
        public async Task<SKImageInfo> ShowPageEventHandler(
            bool bRuler,
            bool Shadow,
            PageType pageType,
            SKBitmap _myBitmap,
            float fScale,
            Anno hoverAnno,
            bool autoBaseProp,
            SKColor pageBackgroundColor,
            CDrawItemOnPage dip,
            float strLenKoef = 2,
            bool rulerAnno = false,
            bool ReadyForMove = false,
            List<OrigWordPossition> findItem = null,
            bool testSearch = false,
            string StrFindAndLocation = ""
            )
        {
            #region image info

            int shadowOffset = 0;
            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;


            Canvas = surface.Canvas;
            Canvas.Clear();
            if (fScale != 1)
            {
                Canvas.Scale(fScale);
            }
            #endregion

            float scale = Math.Min((float)(info.Rect.Width - rullerWidth) / (float)_myBitmap.Width, (float)info.Rect.Height / (float)_myBitmap.Height);

            var scaleWidth = (int)(_myBitmap.Width * scale);
            var scaleHeight = (int)(_myBitmap.Height * scale);

            SKImageInfo dstInfo = new SKImageInfo(scaleWidth, scaleHeight);

            try
            {
                #region Automatic BaseProp
                if (autoBaseProp)
                    BaseProp = scaleHeight;
                #endregion

                #region Page and Ruler
                //make it proportional
                correctionForRuller = 0; // _myBitmap.Info.Width - (info.Rect.Right - RullerWidth);
                //logInstance.Log(user,Severity.Trace, MethodBase.GetCurrentMethod(),null, "correctionForRuller : " + correctionForRuller.ToString());
                SKRectI rect = new SKRectI(info.Rect.Left + RullerWidth, info.Rect.Top, info.Rect.Right, info.Rect.Bottom);
                #region lines for test
                var redLine = new SKPaint
                {
                    Style = SKPaintStyle.Stroke,
                    //StrokeWidth = 2f,
                    Color = SKColors.Red

                };
                var greenLine = new SKPaint
                {
                    Style = SKPaintStyle.Stroke,
                    StrokeWidth = 2f,
                    Color = SKColors.Green

                };
                #endregion

                if (pageType == PageType.Left)
                {

                    rect = new SKRectI(0, 0, info.Rect.Width - 1, info.Rect.Height - 1);
                    //Canvas.DrawRect(rect, redLine);


                    var rectBmp = new SKRectI(rullerWidth, 0, dstInfo.Width + rullerWidth, dstInfo.Height);
                    rect = new SKRectI(0, info.Rect.Top, RullerWidth, dstInfo.Height);
                    if (bRuler)
                        DrawRuler(PageType.Left, rect, dstInfo.Width, pageBackgroundColor);
                    //rect = new SKRectI(info.Rect.Left + RullerWidth, info.Rect.Top, info.Rect.Right, info.Rect.Bottom);

                    //Canvas.DrawRect(rectBmp, greenLine);
                    Canvas.DrawBitmap(_myBitmap, rectBmp, BitmapStretch.AspectFit, BitmapAlignment.Start, BitmapAlignment.Start);


                }
                else
                {

                    rect = new SKRectI(0, 0, info.Rect.Width - 1, info.Rect.Height - 1);
                    //Canvas.DrawRect(rect, redLine);


                    var rectBmp = new SKRectI(0, 0, dstInfo.Width, dstInfo.Height);
                    rect = new SKRectI(dstInfo.Width , info.Rect.Top, dstInfo.Width, dstInfo.Height);
                    if (bRuler)
                        DrawRuler(PageType.Right, rect, dstInfo.Width, pageBackgroundColor);
                    //rect = new SKRectI(info.Rect.Left + RullerWidth, info.Rect.Top, info.Rect.Right, info.Rect.Bottom);

                    //Canvas.DrawRect(rectBmp, greenLine);
                    Canvas.DrawBitmap(_myBitmap, rectBmp, BitmapStretch.AspectFit, BitmapAlignment.Start, BitmapAlignment.Start);

                    //rect = new SKRectI(info.Rect.Right - RullerWidth, info.Rect.Top, info.Rect.Right, info.Rect.Bottom - correctionForRuller);
                    //DrawRuler(PageType.Right, rect);
                    //rect = new SKRectI(info.Rect.Left, info.Rect.Top, info.Rect.Right - RullerWidth, info.Rect.Bottom);
                    //Canvas.DrawBitmap(_myBitmap, rect, BitmapStretch.None, BitmapAlignment.Start, BitmapAlignment.Start);

                }
                #endregion

                #region hover text for Multimedia on UWP
                if (ReadyForMove == false)
                {
                    if (hoverAnno != null)
                    {
                        string hoverText = "";
                        if (hoverAnno.Hint != null)
                        {
                            hoverText = Common.GetTitle(hoverAnno.Hint.Text);
                        }
                        else
                        {
                            logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), null,"hoverAnno.Hint = null");
                        }
                        float irectX = Base.SetInt32(hoverAnno.Location.X * BaseProp * XProp);
                        float irectY = Base.SetInt32(hoverAnno.Location.Y * BaseProp * YProp);
                        float irectWidth = Base.SetInt32(hoverAnno.Location.Width * BaseProp * WProp);
                        float irectHeight = Base.SetInt32(hoverAnno.Location.Height * BaseProp * HProp);

                        SKPaint textPaint = new SKPaint
                        {
                            Color = SKColors.Chocolate,
                            TextSize = (float)(hoverTextSize)

                        };
                        // SKRect textBounds = new SKRect();
                        // textPaint.MeasureText(hoverText, ref textBounds);

                        //float strLenKoef2 = 0;
                        //this is for 1920
                        strLenKoef = Common.CalculateDeltaF(hoverText.Length);
                        //for resolution 3840

                        float textWidth = (float)(textPaint.MeasureText(hoverText) * strLenKoef);
                        //todo: find better text size


                        int offset = 10;
                        SKRect newRect = new SKRect(irectX + irectWidth - offset
                            , irectY + offset
                            , irectX + textWidth
                            , irectY - 16);

                        if (rulerAnno)
                        {
                            if (pageType == PageType.Left)
                            {
                                newRect.Left = 0;
                            }
                            else
                            {
                                newRect = new SKRect(info.Width - textWidth - offset
                             , irectY + offset
                             , info.Width - textWidth + textWidth
                             , irectY - 16);
                            }
                        }

                        using (SKPaint paint = new SKPaint())
                        {
                            paint.Style = SKPaintStyle.Fill;
                            paint.Color = new SKColor(30, 117, 183);
                            Canvas.DrawRect(newRect, paint);
                        }

                        SKPoint newPoss = new SKPoint(irectX + irectWidth, irectY);
                        if (rulerAnno)
                        {
                            if (pageType == PageType.Left)
                            {
                                newPoss.X = 10;
                            }
                            else
                            {
                                newPoss.X = info.Width - textWidth;
                            }
                        }
                        using (SKPaint paint = new SKPaint())
                        {
                            paint.Color = SKColors.White;
                            //paint.TextAlign = SKTextAlign.Center;
                            paint.TextSize = hoverTextSize;

                            Canvas.DrawText(hoverText, newPoss, paint);
                        }
                    }
                }
                else
                {
                    string mouseButtonPress = "ReadyForMove " + ReadyForMove;
                }
                #endregion

                #region SelectSearchItem
                if (findItem != null)
                {
                    foreach (OrigWordPossition oneFindItem in findItem)
                    {

                        SKPaint textPaint = new SKPaint
                        {
                            Color = SKColors.Chocolate,
                            TextSize = (float)(hoverTextSize)

                        };
                        int offset = 10;
                        float textWidth = (float)(textPaint.MeasureText(oneFindItem.FindSearch) * strLenKoef);

                        float irectX = Base.SetInt32(oneFindItem.OrigX * BaseProp * XProp);
                        float irectY = Base.SetInt32(oneFindItem.OrigY * BaseProp * YProp);
                        float irectWidth = Base.SetInt32(oneFindItem.OrigWidth * BaseProp * WProp);
                        float irectHeight = Base.SetInt32(oneFindItem.OrigHeight * BaseProp * HProp);


                        SKRect newRect = new SKRect(irectX - 5
                              , irectY
                              , irectX + irectWidth
                              , irectY + irectHeight);

                        if (pageType == PageType.Left)
                        {

                            newRect = new SKRect(irectX - 5 + RullerWidth
                              , irectY
                              , irectX + irectWidth + RullerWidth
                              , irectY + irectHeight);
                        }
                        using (SKPaint paint = new SKPaint())
                        {
                            paint.Style = SKPaintStyle.Fill;
                            paint.Color = new SKColor(237, 2, 130).WithAlpha(0x80); 

                            Canvas.DrawRect(newRect, paint);
                        }
                        //using (SKPaint paint = new SKPaint())
                        //{
                        //    SKPoint newPoss = new SKPoint(irectX, irectY + oneFindItem.Height - 2);
                        //    paint.Color = SKColors.White;
                        //    //paint.TextAlign = SKTextAlign.Center;
                        //    paint.TextSize = hoverTextSize;

                        //    Canvas.DrawText(oneFindItem.FindSearch, newPoss, paint);
                        //}
                    }
                }
                #endregion

                #region page shadow simulation like real book
                if (Shadow)
                {
                    shadowWidth = scaleWidth / 10;
                    if (pageType == PageType.Right)
                    {

                        using (SKPaint paint = new SKPaint())
                        {

                            for (int i = 0; i < ShadowWidth; i++)
                            {
                                double step = (transparencyStart - transparencyEnd) / ShadowWidth;
                                double transparency = transparencyStart - (i * step);
                                SKRect newRect = new SKRect(i - 1, 0, i, dstInfo.Height);
                                paint.Style = SKPaintStyle.Fill;
                                paint.Color = SKColors.Gray.WithAlpha((byte)(0xFF * transparency));
                                //paint.Shader = SKShader.CreateLinearGradient(
                                //new SKPoint(0, (e.Info.Height/2) ),
                                //new SKPoint(shadowWidth, (e.Info.Height / 2) ),
                                //new SKColor[] { new SKColor(169,169,169), SKColors.White },
                                //new float[] { 0, 1 },
                                //SKShaderTileMode.Clamp);

                                Canvas.DrawRect(newRect, paint);
                            }
                        }
                    }
                    if (pageType == PageType.Left)
                    {
                        using (SKPaint paint = new SKPaint())
                        {
                            for (int i = 0; i < ShadowWidth; i++)
                            {
                                int widhtStart = scaleWidth + rullerWidth - ShadowWidth;
                                int widhtEnd = scaleWidth + rullerWidth;

                                SKRect newRect = new SKRect(widhtEnd - i, 0, widhtEnd - i - 1, dstInfo.Height);
                                double step = (transparencyStart - transparencyEnd) / ShadowWidth;
                                double transparency = transparencyStart - (i * step);
                                paint.Style = SKPaintStyle.Fill;
                                paint.Color = SKColors.Gray.WithAlpha((byte)(0xFF * transparency));
                                //paint.Shader = SKShader.CreateLinearGradient(
                                //new SKPoint(e.Info.Width - shadowWidth, (e.Info.Height / 2)),
                                //new SKPoint(e.Info.Width, (e.Info.Height / 2)),
                                //new SKColor[] { SKColors.White, new SKColor(169, 169, 169) },
                                //new float[] { 0, 1 },
                                //SKShaderTileMode.Clamp);

                                Canvas.DrawRect(newRect, paint);
                            }
                        }
                    }
                }
                #endregion

                #region testSearch
                //if (testSearch)
                //{
                //    foreach (string item in AllWordsOnPage)
                //    {
                //        FillWordList(pageType, item, fScale, true, true);
                //    }
                //}
                #endregion

                #region DrawOnPage
                
                if (dip != null)
                {
                    SKPaint lpaint = new SKPaint
                    {
                        Style = SKPaintStyle.Stroke,
                        Color = SKColors.White,
                        StrokeWidth = 10,
                        StrokeCap = SKStrokeCap.Round,
                        StrokeJoin = SKStrokeJoin.Round
                    };
                    foreach (MyPenList mpl in dip.completedDrawingPaths)
                    {

                        switch (mpl.DrawLineColorEnum)
                        {
                            case DrawLineColor.LightBlue:
                                lpaint.Color = SKColors.LightBlue;

                                break;
                            case DrawLineColor.LightPink:
                                lpaint.Color = SKColors.LightPink;

                                break;
                            case DrawLineColor.LightGreen:
                                lpaint.Color = SKColors.LightGreen;

                                break;
                        }
                        lpaint.StrokeWidth = (float)mpl.MyDrawLineWidth;
                        Canvas.DrawPath(mpl.sKPath, lpaint);
                    }
                    //foreach (SKPath path in dip.inProgressPaths.Values)
                    //{
                    //    Canvas.DrawPath(path, paint);
                    //}
                    #region startDrawingLine
                    {
                        //Canvas.DrawLine(pointStartLine, pointEndLine, paint);


                        for (int i = 0; i < dip.htLines.Count; i++)
                        {
                            MyLineList ml = dip.htLines[i];
                            switch (ml.customDrawLineColor)
                            {
                                case DrawLineColor.LightBlue:
                                    lpaint.Color = SKColors.LightBlue;

                                    break;
                                case DrawLineColor.LightPink:
                                    lpaint.Color = SKColors.LightPink;

                                    break;
                                case DrawLineColor.LightGreen:
                                    lpaint.Color = SKColors.LightGreen;

                                    break;
                            }
                            lpaint.StrokeWidth = (float)ml.customDrawLineWidth;
                            Canvas.DrawLine(ml.PointStart, ml.PointEnd, lpaint);
                        }
                    }
                    #endregion
                    /*
                    #region startDrawingLine
                    if (dip.HtLines != null)
                    {
                        for (int i = 0; i < dip.HtLines.Count; i++)
                        {
                            MyLineList ml = (MyLineList)dip.HtLines[i];
                            float istartX = (float)(ml.PointStart.X * BaseProp * XProp);
                            float istartY = (float)(ml.PointStart.Y * BaseProp * YProp);
                            SKPoint pointStart = new SKPoint(istartX, istartY);
                            float iendX = (float)(ml.PointEnd.X * BaseProp * XProp);
                            float iendY = (float)(ml.PointEnd.Y * BaseProp * YProp);
                            SKPoint pointEnd = new SKPoint(iendX, iendY);
                            //if(ml.PaintLine == null)
                            {
                                SKColor myColor = SKColors.Black;
                                switch (ml.customDrawLineColor)
                                {
                                    case DrawLineColor.LightPink: myColor = SKColors.LightPink; break;
                                    case DrawLineColor.LightBlue: myColor = SKColors.LightBlue; break;
                                    case DrawLineColor.LightGreen: myColor = SKColors.LightGreen; break;
                                }
                                var myLine = new SKPaint
                                {
                                    Style = SKPaintStyle.Stroke,
                                    StrokeWidth = (float)ml.customDrawLineWidth,
                                    Color = myColor
                                };
                                ml.PaintLine = myLine;
                            }
                            Canvas.DrawLine(pointStart, pointEnd, ml.PaintLine);
                        }
                    }
                    #endregion


                    #region startDrawingPen
                    for (int i = 0; i < dip.HtDraws.Count; i++)
                    {
                        MyLineList penList = (MyLineList)dip.HtDraws[i];
                        if (penList != null)
                        {
                            for (int j = 0; j < penList.ListPoints.Count; j++)
                            {
                                if (j > 0)
                                {
                                    SKPoint pointStartPen = (SKPoint)penList.ListPoints[j - 1];
                                    SKPoint pointEndPen = (SKPoint)penList.ListPoints[j];

                                    float istartX = (float)(pointStartPen.X * BaseProp * XProp);
                                    float istartY = (float)(pointStartPen.Y * BaseProp * YProp);
                                    SKPoint pointStart = new SKPoint(istartX, istartY);
                                    float iendX = (float)(pointEndPen.X * BaseProp * XProp);
                                    float iendY = (float)(pointEndPen.Y * BaseProp * YProp);
                                    SKPoint pointEnd = new SKPoint(iendX, iendY);

                                    //if (penList.PaintLine == null)
                                    {
                                        SKColor myColor = SKColors.Black;
                                        switch (penList.customDrawLineColor)
                                        {
                                            case DrawLineColor.LightPink: myColor = SKColors.LightPink; break;
                                            case DrawLineColor.LightBlue: myColor = SKColors.LightBlue; break;
                                            case DrawLineColor.LightGreen: myColor = SKColors.LightGreen; break;
                                        }
                                        var myLine = new SKPaint
                                        {
                                            Style = SKPaintStyle.Stroke,
                                            StrokeWidth = (float)penList.customDrawLineWidth,
                                            Color = myColor
                                        };
                                        penList.PaintLine = myLine;
                                    }

                                    Canvas.DrawLine(pointStart, pointEnd, penList.PaintLine);
                                }
                            }
                        }
                    }
                    
                    #endregion
                    */
                }

                #endregion

                #region ShowSearchText
                if (StrFindAndLocation != "")
                {
                    if (AllWordsOnPage.Contains(StrFindAndLocation))
                    {
                        ShowWordSearach(pageType, StrFindAndLocation, fScale, bRuler, true);
                    }
                }
                #endregion

            }
            catch (Exception ee)
            {

                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return dstInfo;
        }
        private void DrawRuler(PageType pt, SKRectI rect, int maxWidth, SKColor pageBackgroundColor)
        {
            try
            {
                float bigStep = RullerSmallStep * RullerBigStep;
                int smallLineWidht = rullerWidth / 4;
                int bigLineWidth = rullerWidth / 2;


                if (CurrentPage >= 0)
                {
                    SKImageInfo info = e.Info;
                    SKSurface surface = e.Surface;
                    SKCanvas canvas = surface.Canvas;
                    float rightWidth = info.Width;
                    if (rightWidth > 940)
                    {
                        rightWidth = maxWidth + 20;
                    }
                    canvas.Clear();
                    //canvas.DrawBitmap(bmpRightRuller, info.Rect, BitmapStretch.Uniform);

                    var paintBackground = new SKPaint
                    {
                        Style = SKPaintStyle.StrokeAndFill,
                        Color = pageBackgroundColor,
                    };
                    var paintLine = new SKPaint
                    {
                        Style = SKPaintStyle.Stroke,
                        //StrokeWidth = 2f,
                        Color = SKColors.Gray

                    };
                    var paintLine2 = new SKPaint
                    {
                        Style = SKPaintStyle.Stroke,
                        //StrokeWidth = 3f,
                        Color = SKColors.Gray

                    };
                    canvas.DrawRect(rect, paintBackground);

                    int smallLineCounter = 0;
                    int bigLineCounter = 0;
                    for (float lineCounter = 0; lineCounter < rect.Height; lineCounter++)
                    {
                        //right
                        SKPoint point1 = new SKPoint(rightWidth - rect.Width, lineCounter);
                        SKPoint point2 = new SKPoint(rightWidth - rect.Width + smallLineWidht, lineCounter);

                        if (pt == PageType.Left)
                        {
                            point1 = new SKPoint(rect.Width - smallLineWidht, lineCounter);
                            point2 = new SKPoint(rect.Width, lineCounter);
                        }
                        if (smallLineCounter == RullerSmallStep)
                        {
                            canvas.DrawLine(point1, point2, paintLine);
                            smallLineCounter = 0;
                        }

                        //big step
                        if (bigLineCounter == bigStep)
                        {

                            //right
                            point1 = new SKPoint(rightWidth - rect.Width, lineCounter);
                            point2 = new SKPoint(rightWidth - rect.Width + bigLineWidth, lineCounter);
                            if (pt == PageType.Left)
                            {
                                point1 = new SKPoint(rect.Width - bigLineWidth, lineCounter);
                                point2 = new SKPoint(rect.Width, lineCounter);
                            }
                            canvas.DrawLine(point1, point2, paintLine2);
                            bigLineCounter = 0;
                        }
                        bigLineCounter++;
                        smallLineCounter++;
                    }
                }
                else
                {
                    //first two pages
                    SKImageInfo info = e.Info;
                    SKSurface surface = e.Surface;
                    SKCanvas canvas = surface.Canvas;

                    canvas.Clear();
                }

            }
            catch (Exception ee)
            {

                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        public void testSearchItems()
        {
            string test = "";
            foreach (string item in AllWordsOnPage)
            {
                test += item + Environment.NewLine;
            }
        }
        public void Reset()
        {
            SetPageWords(new Hashtable());
            AllWordsOnPage = new List<string>();
        }
        public Hashtable SearchItems(PageType pageType, float fScale, bool leftRulerOn, bool showAllSearch, bool searchTextActive, bool forceReset = false)
        {
            if (searchTextActive)
            {

                HtNumberAndWords = new Hashtable();
                HtPageWords = new Hashtable();
                HtPageWordsOriginal = new Hashtable();

                foreach (string item in AllWordsOnPage)
                {
                    //SelectWordPro(item);
                    //SelectWordCorporate(item);
                    //ssi.SelectWord(item, PictureBox);  

                    FillWordList(pageType, item, fScale, leftRulerOn, showAllSearch);
                }
                //lblFind.Text = htPageWords.Count.ToString();
                // testSearchItems();
            }
            return HtPageWords;
        }
        public void ShowWordSearach(PageType pageType, string inputString, float fScale, bool LeftRulerOn, bool showAllSearch)
        {
            try
            {
                ArrayList listWordPositions = new ArrayList();
                var lineSplitArray = inputString.Split("@".ToCharArray());
                if (lineSplitArray.Length == 2)
                {

                    string wordItem = lineSplitArray[0];
                    //localization!
                    //double[] cords = Array.ConvertAll(lineSplitArray[1].Split(":".ToCharArray()), Double.Parse);

                    //double[] lines = lineStrings.Select(s => double.Parse(s, CultureInfo.InvariantCulture)).ToArray();
                    double[] cords = Array.ConvertAll(lineSplitArray[1].Split(":".ToCharArray()), i => Convert.ToDouble(i, CultureInfo.InvariantCulture));

                    double rectX = 0;
                    double rectY = 0;
                    double rectWidth = 0;
                    double rectHeight = 0;
                    rectX = cords[0];
                    rectY = cords[1];
                    rectWidth = cords[0];
                    rectHeight = cords[1];
                    int rectIndex = 2;
                    while (rectIndex < 8)
                    {
                        if (rectX > cords[rectIndex])
                        {
                            rectX = cords[rectIndex];
                        }
                        if (rectY > cords[rectIndex + 1])
                        {
                            rectY = cords[rectIndex + 1];
                        }
                        if (rectWidth < cords[rectIndex])
                        {
                            rectWidth = cords[rectIndex];
                        }
                        if (rectHeight < cords[rectIndex + 1])
                        {
                            rectHeight = cords[rectIndex + 1];
                        }
                        rectIndex = rectIndex + 2;
                    }
                    rectWidth = cords[2] - cords[0];
                    rectHeight = cords[3] - cords[5];
                    //Rectangle de = new Rectangle(rectX, rectY, rectWidth, rectHeight);


                    float irectX = Base.SetInt32(rectX * BaseProp * XProp);
                    float irectY = Base.SetInt32(rectY * BaseProp * YProp);
                    float irectWidth = Base.SetInt32(rectWidth * BaseProp * WProp);
                    float irectHeight = Base.SetInt32(rectHeight * BaseProp * HProp);

                    if (pageType == PageType.Left && LeftRulerOn == true)
                    {
                        irectX += RullerWidth;
                    }

                    // the rectangle
                    var rect = SKRect.Create(irectX, irectY, irectWidth, irectHeight);
                    // the brush (fill with blue)
                    var paint = new SKPaint
                    {
                        Style = SKPaintStyle.Fill,
                        Color = SKColors.Violet.WithAlpha(0x80)

                    };
                    //test draw 
                    if (showAllSearch)
                    {
                        if (Canvas != null)
                        {
                            Canvas.DrawRect(rect, paint);
                        }
                    }

                    //rember for screen analysis
                    float irectX2 = irectX + irectWidth;
                    float irectY2 = irectY + irectHeight;


                    SKPoint cursor = new SKPoint(irectX, irectY);
                    listWordPositions.Add(cursor);


                    irectX *= fScale;
                    irectX2 *= fScale;
                    irectY *= fScale;
                    irectY2 *= fScale;
                    //for (int i = (int)irectX; i < (int)irectX2; i++)
                    //{
                    //    for (int j = (int)irectY; j < (int)irectY2; j++)
                    //    {
                    //        cursor = new SKPoint(i, j);
                    //        if (i > 0) //184
                    //        {
                    //            if (j == 196)
                    //            {
                    //                //string stop = "";
                    //            }
                    //        }
                    //        listWordPositions.Add(cursor);

                    //    }
                    //}

                    //HtNumberAndWords.Add(wordCounter, wordItem);
                    //OrigWordPossition origItem = new OrigWordPossition(wordItem, rectX, rectY, rectWidth, rectHeight);
                    //HtPageWordsOriginal.Add(wordCounter, origItem);
                    //HtPageWords.Add(wordCounter++, listWordPositions);

                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }

        }
        public void FillWordList(PageType pageType, string inputString, float fScale, bool LeftRulerOn, bool showAllSearch)
        {
            try
            {
                ArrayList listWordPositions = new ArrayList();
                var lineSplitArray = inputString.Split("@".ToCharArray());
                if (lineSplitArray.Length == 2)
                {

                    string wordItem = lineSplitArray[0];
                    //localization!
                    //double[] cords = Array.ConvertAll(lineSplitArray[1].Split(":".ToCharArray()), Double.Parse);

                    //double[] lines = lineStrings.Select(s => double.Parse(s, CultureInfo.InvariantCulture)).ToArray();
                    double[] cords = Array.ConvertAll(lineSplitArray[1].Split(":".ToCharArray()), i => Convert.ToDouble(i, CultureInfo.InvariantCulture));

                    double rectX = 0;
                    double rectY = 0;
                    double rectWidth = 0;
                    double rectHeight = 0;
                    rectX = cords[0];
                    rectY = cords[1];
                    rectWidth = cords[0];
                    rectHeight = cords[1];
                    int rectIndex = 2;
                    while (rectIndex < 8)
                    {
                        if (rectX > cords[rectIndex])
                        {
                            rectX = cords[rectIndex];
                        }
                        if (rectY > cords[rectIndex + 1])
                        {
                            rectY = cords[rectIndex + 1];
                        }
                        if (rectWidth < cords[rectIndex])
                        {
                            rectWidth = cords[rectIndex];
                        }
                        if (rectHeight < cords[rectIndex + 1])
                        {
                            rectHeight = cords[rectIndex + 1];
                        }
                        rectIndex = rectIndex + 2;
                    }
                    rectWidth = cords[2] - cords[0];
                    rectHeight = cords[3] - cords[5];
                    //Rectangle de = new Rectangle(rectX, rectY, rectWidth, rectHeight);


                    float irectX = Base.SetInt32(rectX * BaseProp * XProp);
                    float irectY = Base.SetInt32(rectY * BaseProp * YProp);
                    float irectWidth = Base.SetInt32(rectWidth * BaseProp * WProp);
                    float irectHeight = Base.SetInt32(rectHeight * BaseProp * HProp);

                    if (pageType == PageType.Left && LeftRulerOn == true)
                    {
                        irectX += RullerWidth;
                    }

                    // the rectangle
                    var rect = SKRect.Create(irectX, irectY, irectWidth, irectHeight);
                    // the brush (fill with blue)
                    var paint = new SKPaint
                    {
                        Style = SKPaintStyle.Stroke,
                        Color = SKColors.Blue
                    };
                    //test draw 
                    if (showAllSearch)
                    {
                        if (Canvas != null)
                        {
                            Canvas.DrawRect(rect, paint);
                        }
                    }

                    //rember for screen analysis
                    float irectX2 = irectX + irectWidth;
                    float irectY2 = irectY + irectHeight;


                    SKPoint cursor = new SKPoint(irectX, irectY);
                    listWordPositions.Add(cursor);


                    irectX *= fScale;
                    irectX2 *= fScale;
                    irectY *= fScale;
                    irectY2 *= fScale;
                    for (int i = (int)irectX; i < (int)irectX2; i++)
                    {
                        for (int j = (int)irectY; j < (int)irectY2; j++)
                        {
                            cursor = new SKPoint(i, j);
                            if (i > 0) //184
                            {
                                if (j == 196)
                                {
                                    //string stop = "";
                                }
                            }
                            listWordPositions.Add(cursor);

                        }
                    }

                    HtNumberAndWords.Add(wordCounter, wordItem);
                    OrigWordPossition origItem = new OrigWordPossition(wordItem, rectX, rectY, rectWidth, rectHeight);
                    HtPageWordsOriginal.Add(wordCounter, origItem);
                    HtPageWords.Add(wordCounter++, listWordPositions);

                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }

        }
        public string[] parseSearchFileForFlipCorporate(string fileName)
        {
            string[] strLines = new string[0];
            try
            {


                if (bookEncoding == true)
                {

                    byte[] source = System.IO.File.ReadAllBytes(fileName);
                    byte[] strres = Common.encodeByteArray(source);
                    string xmlstr = Encoding.UTF8.GetString(strres);
                    //strLines = xmlstr.Split(@"\n".ToCharArray());

                    //todo: remove this after test!
                    File.WriteAllText(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "WriteLines.txt", xmlstr);
                    strLines = System.IO.File.ReadAllLines(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "WriteLines.txt");
                    File.Delete(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "WriteLines.txt");

                }
                else
                {
                    strLines = System.IO.File.ReadAllLines(fileName);
                }

                foreach (var item in strLines)
                {
                    AllWordsOnPage.Add(item);
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return strLines;
        }
        public bool parseSearchFileForFlipPro(string fileName)
        {
            bool res = true;
            try
            {
                int page = 0;
                JObject searchFile = JObject.Parse(System.IO.File.ReadAllText(fileName));
                foreach (var item in searchFile)
                {
                    //page number
                    if (item.Key == "page")
                    {
                        int.TryParse(item.Value.ToString(), out page);
                    }
                    //all words in a page
                    //item.Key is keyword for possitions
                    if (item.Key == "positions")
                    {
                        //item.Value is w: possitions up to 25
                        foreach (var searchItem in item.Value)
                        {
                            //format p:[ items...]
                            string searchWord = "";
                            foreach (var wordBox in searchItem)
                            {
                                string key = ((JProperty)wordBox).Name;
                                string itemValue = ((JProperty)wordBox).Value.ToString();
                                string searchWordPossition = "@";

                                if (key == "w")
                                {
                                    searchWord = itemValue;
                                }
                                //get possition of cuurent word
                                if (key == "p")
                                {

                                    foreach (var poss in ((JProperty)wordBox).Value)
                                    {
                                        string st = poss.ToString();
                                        searchWordPossition += st + ":";

                                    }
                                    if (searchWordPossition.Length > 1)
                                    {
                                        if (searchWordPossition.EndsWith(":"))
                                        {
                                            searchWordPossition = searchWordPossition.Remove(searchWordPossition.Length - 1, 1);
                                        }
                                    }
                                    string oneWord = searchWord + ":" + searchWordPossition;
                                    AllWordsOnPage.Add(oneWord);
                                }
                            }
                        }
                    }
                    string s = item.ToString();
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                res = false;
            }
            return res;
        }

        public OrigWordPossition GetSearchItem(SKPoint Location)
        {
            OrigWordPossition res = null;
            foreach (DictionaryEntry de in HtPageWords)
            {

                ArrayList listWordPositions = (ArrayList)de.Value;
                foreach (SKPoint item in listWordPositions)
                {
                    if (item.X == (int)Location.X && item.Y == (int)Location.Y)
                    {
                        if (HtNumberAndWords.ContainsKey(de.Key))
                        {
                            if (HtPageWordsOriginal.ContainsKey(de.Key))
                            {
                                res = (OrigWordPossition)HtPageWordsOriginal[de.Key];
                            }

                            return res;
                        }
                    }
                }
            }
            return res;
        }

        public void SetPageWords(Hashtable newTable)
        {
            HtPageWords = newTable;
        }

        #endregion

    }
}
