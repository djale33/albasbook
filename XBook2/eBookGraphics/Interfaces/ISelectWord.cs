﻿using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace eBookGraphics
{
    public interface ISelectWord
    {
        void SelectWord(string inputString, SKCanvas canvas);
        void SetRatio(double newRatio);
        void SetImageWidth(int newWidth);
        void SetPageWords(Hashtable newTable);
        string GetSearchItem(SKPoint location);
    }
}
