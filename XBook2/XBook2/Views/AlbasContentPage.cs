﻿using KlasimeBase.Interfaces;
using KlasimeBase.Log;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;
using XBook2.Views.Interfaces;

namespace XBook2.Views
{
    public class AlbasContentPage : ContentPage, IBackground
    {
        #region VARIABLES
        public ApiLog logInstance = ApiLog.GetInstance();
        #endregion
        public AlbasContentPage( string user)
        {
            SetBackground( user);
        }
        public void SetBackground( string user)
        {
            try
            {

                var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
                if (mainDisplayInfo != null)
                {
                    
                    if (this.Height > this.Width)
                    {
                        //PORTRAIT
                        this.BackgroundImageSource = "portretDark.png";
                   

                    }
                    else
                    {
                        //LANDSCAPE                        
                        this.BackgroundImageSource = "landscapeDark.png";
                       
                    }
                    
                }


            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        
    }
}
