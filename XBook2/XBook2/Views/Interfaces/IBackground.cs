﻿using SharedKlasime;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace XBook2.Views.Interfaces
{
    public interface IBackground
    {
        void SetBackground(string user);
    }
}
