﻿using KlasimeBase;
using KlasimeBase.Log;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2.ViewModels;

namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DownloadBookControl2 : AlbasContentPage
    {
        #region VARIABLES
        DownloadBookControlViewModel MyViewModelInstance;
  
        AlbasBook book;
        #endregion

        #region CONSTRUCTOR
        public DownloadBookControl2(CUser _loggedUser, AlbasBook abook, BookButtonAction bookAction, bool IsSmallScreen) : base(_loggedUser.firstname + "-" + _loggedUser.lastname)
        {
          
            book = abook;

            InitializeComponent();
            MyViewModelInstance = new DownloadBookControlViewModel(_loggedUser, book, bookAction, IsSmallScreen);
            MyViewModelInstance.PercentText = "0%";
            BindingContext = MyViewModelInstance;
        }
        #endregion

        #region EVENTS
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            //start downloading

            //await myViewModel.DownloadBook();
            await MyViewModelInstance.Demo();

        }

        private void ContentPage_LayoutChanged(object sender, EventArgs e)
        {
            SetBackground(MyViewModelInstance.User);
        }
        #endregion
    }
}