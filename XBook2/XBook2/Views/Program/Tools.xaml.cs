﻿//using eBookGraphics;
using KlasimeBase.Log;
using KlasimeBase;
using SharedKlasime;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Action = System.Action;
using XBook2.Touch;
using Xamarin.Forms.Shapes;
using eBookGraphics;
using XBook2.ViewModels;

namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tools : ContentPage
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        public static string user = "program";
        #region VARIABLES
        public ToolsViewModel MainPageViewModelInstance;
        // SKBitmap bmpAudio;
        CUser loggedUser;
        
        SKCanvas Canvas;
        private SkiaSharp.SKSurface surface;
        SKBitmap myBitmap;
        bool lineDraw = false;
        bool penDraw = false;
        bool startDrawingLine = false;
        bool startDrawingPen = false;
        SKPoint pointStartLine;
        SKPoint pointEndLine;
        //SKPoint pointDrag;
        SKPoint pointStartPen;
        Hashtable htLines = new Hashtable();
        Hashtable htDraws = new Hashtable();
        List<SKPoint> listPoints = new List<SKPoint>();
        int lineCounter = 0;
        int drawCounter = 0;
        
        //bool drag = false;
        Dictionary<long, SKPath> inProgressPaths = new Dictionary<long, SKPath>();
        List<SKPath> completedDrawingPaths = new List<SKPath>();

        SKPaint paint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.White,
            StrokeWidth = 10,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        };
        #endregion

        #region CONSTRUCTOR
        public Tools(CUser muser, Book _currentBook)
        {

            //myBitmap = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.blackboard.jpg");
            MainPageViewModelInstance = new ToolsViewModel( muser,  _currentBook);
            BindingContext = MainPageViewModelInstance;
            SetBackground();


            InitializeComponent();
            loggedUser = muser;
            //bookid = mbookid;            
           
        }
        #endregion

        #region EVENTS
      


        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            try
            {

                lbPen.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_pen_free"); //"Pen";
                //btnPen.Source = ImageSource.FromResource("XBook2.media.load.orange.pen-button-up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                lblLine.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_pen_direct"); //"Line";
                //btnLine.Source = ImageSource.FromResource("XBook2.media.load.orange.line_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                lblUnselect.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_unselect"); //"Unselect";
                //btnUnselect.Source = ImageSource.FromResource("XBook2.media.load.orange.unselect_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }
            catch (Exception ee)
            {

                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void btnPen_Clicked(object sender, EventArgs e)
        {
            penDraw = true;
            lineDraw = false;
            btnPen.BackgroundColor= Color.Gray;
            btnLine.BackgroundColor = Color.FromRgb(88,89,91);
            //btnPen.Source = ImageSource.FromResource("XBook2.media.load.orange.pen-button-down.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //btnLine.Source = ImageSource.FromResource("XBook2.media.load.orange.line_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);


            startDrawingLine = false;
            skiaView.InvalidateSurface();
        }

        private void btnLine_Clicked(object sender, EventArgs e)
        {
            penDraw = false;
            btnLine.BackgroundColor = Color.Gray;
            btnPen.BackgroundColor = Color.FromRgb(88, 89, 91);

            //btnLine.Source = ImageSource.FromResource("XBook2.media.load.orange.line_down.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //btnPen.Source = ImageSource.FromResource("XBook2.media.load.orange.pen-button-up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            lineDraw = true;
            skiaView.InvalidateSurface();
        }

        private void btnUnselect_Clicked(object sender, EventArgs e)
        {
            btnLine.BackgroundColor = Color.FromRgb(88, 89, 91);
            btnPen.BackgroundColor = Color.FromRgb(88, 89, 91);

            penDraw = false;
            lineDraw = false;
            startDrawingLine = false;
            completedDrawingPaths.Clear();
            listPoints.Clear();
            htLines.Clear();
            htDraws.Clear();
            drawCounter = 0;
            lineCounter = 0;

            pointStartLine = new SKPoint(0,0);
            pointEndLine = new SKPoint(0, 0);
            SetBackground();
            skiaView.InvalidateSurface();
        }

        private void skiaView_PaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {

            SKCanvas canvas = args.Surface.Canvas;
            canvas.Clear();

            foreach (SKPath path in completedDrawingPaths)
            {
                canvas.DrawPath(path, paint);
            }

            foreach (SKPath path in inProgressPaths.Values)
            {
                canvas.DrawPath(path, paint);
            }
            #region startDrawingLine
            {
                canvas.DrawLine(pointStartLine, pointEndLine, paint);
                for (int i = 0; i < htLines.Count; i++)
                {
                    MyLineList ml = (MyLineList)htLines[i];
                    canvas.DrawLine(ml.PointStart, ml.PointEnd, paint);
                }
            }
            #endregion

        }


        #endregion

        #region METHODS
        private void SetBackground()
        {
            string resourceID = "XBook2.media.blackboard.jpg";
            var type = GetType();
            Assembly assembly = type.GetTypeInfo().Assembly;
            using (var streamReader = new StreamReader(assembly.GetManifestResourceStream(resourceID)))
            {
                var bytes = default(byte[]);
                using (var memstream = new MemoryStream())
                {
                    streamReader.BaseStream.CopyTo(memstream);
                    bytes = memstream.ToArray();

                    myBitmap = SKBitmap.Decode(bytes);
                }
            }
        }
        private void Draw(SKPaintSurfaceEventArgs e)
        {
            try
            {
                #region set paint
                SKImageInfo info = e.Info;
                SKSurface surface = e.Surface;
                Canvas = surface.Canvas;
                Canvas.Clear();
                Canvas.Flush();
                var rectBmp = new SKRectI(0, 0, info.Width, info.Height);
                if (myBitmap != null)
                    Canvas.DrawBitmap(myBitmap, rectBmp, BitmapStretch.AspectFit, BitmapAlignment.Start, BitmapAlignment.Start);

                var paintLine = new SKPaint
                {
                    Style = SKPaintStyle.Stroke,
                    StrokeWidth = 3f,
                    Color = SKColors.White

                };

                #endregion

                #region startDrawingLine
                {
                    Canvas.DrawLine(pointStartLine, pointEndLine, paintLine);
                    for (int i = 0; i < htLines.Count; i++)
                    {
                        MyLineList ml = (MyLineList)htLines[i];
                        Canvas.DrawLine(ml.PointStart, ml.PointEnd, paintLine);
                    }
                }
                #endregion

                #region startDrawingLine startDrawingPen
                {
                    //Canvas.DrawPoint(pointStartPen, paintLine);
                    //draw curent shape
                    for (int i = 0; i < listPoints.Count; i++)
                    {
                        if (i > 0)
                        {
                            pointStartPen = (SKPoint)listPoints[i - 1];
                            SKPoint pointEndPen = (SKPoint)listPoints[i];

                            Canvas.DrawLine(pointStartPen, pointEndPen, paintLine);
                        }


                    }

                    //draw older shapes
                    for (int a = 0; a < htDraws.Count; a++)
                    {
                        List<SKPoint> tmpListPoints = (List<SKPoint>)htDraws[a];

                        for (int i = 0; i < tmpListPoints.Count-1; i++)
                        {
                            if (i > 0)
                            {
                                pointStartPen = (SKPoint)tmpListPoints[i - 1];
                                SKPoint pointEndPen = (SKPoint)tmpListPoints[i];                                
                                Canvas.DrawLine(pointStartPen, pointEndPen, paintLine);
                            }
                        }
                    }
                }
                #endregion

            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        #endregion

        SKPoint ConvertToPixel(Point pt)
        {
            return new SKPoint((float)(skiaView.CanvasSize.Width * pt.X / skiaView.Width),
                              (float)(skiaView.CanvasSize.Height * (pt.Y - lblTitle.Height) / skiaView.Height));
        }
        private void OnTouchEffectAction(object sender, Touch.TouchActionEventArgs args)
        {
            switch (args.Type)
            {
                
                case TouchActionType.Pressed:
                    if (penDraw == true)
                    {
                        if (!inProgressPaths.ContainsKey(args.Id))
                        {
                            SKPath path = new SKPath();
                            path.MoveTo(ConvertToPixel(args.Location));
                            inProgressPaths.Add(args.Id, path);
                            
                        }
                        

                    }
                    if (lineDraw == true)
                    {
                        startDrawingLine = true;
                        pointStartLine = ConvertToPixel(args.Location);
                        pointEndLine = pointStartLine;
                    }
                    skiaView.InvalidateSurface();
                    break;

                case TouchActionType.Moved:
                    if (penDraw == true)
                    {
                        if (inProgressPaths.ContainsKey(args.Id))
                        {
                            SKPath path = inProgressPaths[args.Id];
                            path.LineTo(ConvertToPixel(args.Location));
                        }
                        
                    }

                    if (lineDraw == true)
                    {
                        if (startDrawingLine)
                        {
                            pointEndLine = ConvertToPixel(args.Location);
                            skiaView.InvalidateSurface();
                        }
                    }
                    skiaView.InvalidateSurface();
                    break;

                case TouchActionType.Released:
                    if (penDraw == true)
                    {
                        if (inProgressPaths.ContainsKey(args.Id))
                        {
                            completedDrawingPaths.Add(inProgressPaths[args.Id]);
                            inProgressPaths.Remove(args.Id);
                            skiaView.InvalidateSurface();
                        }
                    }

                    if (lineDraw == true)
                    {
                        if (startDrawingLine)
                        {
                            MyLineList ml = new MyLineList(pointStartLine, pointEndLine);
                            htLines.Add(lineCounter++, ml);
                            startDrawingLine = false;
                        }
                    }
                    break;

                case TouchActionType.Cancelled:
                    if (inProgressPaths.ContainsKey(args.Id))
                    {
                        inProgressPaths.Remove(args.Id);
                        skiaView.InvalidateSurface();
                    }
                    break;
            }
        }
    }

}