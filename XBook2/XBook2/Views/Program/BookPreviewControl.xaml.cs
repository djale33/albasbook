﻿using KlasimeBase;
using KlasimeBase.Log;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2;
using XBook2.ViewModels;
using XBook2.Views;


namespace KlasimeViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookPreviewControl : AlbasContentPage
    {

        #region VARIABLES
        public ApiLog logInstance = ApiLog.GetInstance();
        BookPreviewControlViewModel MyViewModelInstance;


        public AlbasBook MAlbasBook { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public bool BookExist { get; set; }
        public bool BookActivated { get; set; }
        public BookButtonAction ButtonAction { get; set; }
        #endregion

        #region Custom Event
        public delegate void BookClickHandler(object sender, BookEventArgs e);
        public event BookClickHandler OnBookClick;

        private void OnBookClickEvent(object sender, BookEventArgs e)
        {
            if (OnBookClick != null)
            {
                OnBookClick(sender, e);
            }
        }

        #endregion

        #region CONSTRUCTOR
        public BookPreviewControl(CUser _loggedUser, AlbasBook albasBook, bool exist, bool activated, BookButtonAction buttonAction, bool _IsSmallScreen) : base(_loggedUser.firstname + "-" + _loggedUser.lastname)
        {
            InitializeComponent();
           
            MyViewModelInstance = new BookPreviewControlViewModel(_loggedUser, albasBook, _IsSmallScreen);
            MyViewModelInstance.ButtonAction = buttonAction;
           
            BindingContext = MyViewModelInstance;
          
            MAlbasBook = albasBook;
            BookExist = exist;
            BookActivated = activated;
            ButtonAction= buttonAction;

            try
            {
                btnBook.Source = albasBook.medium_photo_url;

            }
            catch (Exception ee)
            {
                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        #endregion

        #region EVENTS

        private void ContentPage_LayoutChanged(object sender, EventArgs e)
        {
            SetBackgroundCustom();
        }
        #endregion

        #region VARIABLES
        public void SetBackgroundCustom()
        {
            try
            {
                var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
                if (mainDisplayInfo != null)
                {
                    if (mainDisplayInfo.Height > mainDisplayInfo.Width)
                    {
                        //PORTRAIT
                       
                        CollectionOne.Orientation = StackOrientation.Vertical;
                        if (MyViewModelInstance.IsSmallScreen == false)
                        {
                            //tablet
                            MyViewModelInstance.BookDownloadHeight = 400;
                            MyViewModelInstance.BookDownloadWidth = 300;
                        }
                        else
                        {
                            //phone
                            MyViewModelInstance.BookDownloadHeight = 300;
                            MyViewModelInstance.BookDownloadWidth = 220;
                        }
                    }
                    else
                    {
                        //LANDSCAPE   
                       
                        CollectionOne.Orientation = StackOrientation.Horizontal;
                        if (MyViewModelInstance.IsSmallScreen == false)
                        {
                            //tablet
                            MyViewModelInstance.BookDownloadHeight = 340;
                            MyViewModelInstance.BookDownloadWidth = 300;
                        }
                        else
                        {
                            //phone
                            MyViewModelInstance.BookDownloadHeight = 220;
                            MyViewModelInstance.BookDownloadWidth = 170;
                        }

                    }
                }

            }
            catch (Exception ee)
            {

                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion
    }
}