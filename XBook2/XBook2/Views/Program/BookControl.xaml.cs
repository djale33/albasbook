﻿using KlasimeBase;
using KlasimeBase.Log;
using System;
using System.Reflection;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2;

namespace XBook2.Views.Program
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookControl : ContentView
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        #region VARIABLES
        public static string user = "BookControl";
        public AlbasBook AlbasBook { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public bool BookExist { get; set; }
        public bool BookActivated { get; set; }
        #endregion
      

        #region Custom Event
        public delegate void BookClickHandler(object sender, BookEventArgs e);
        public event BookClickHandler OnBookClick;

        private void OnBookClickEvent(object sender, BookEventArgs e)
        {
            if (OnBookClick != null)
            {
                OnBookClick(sender, e);
            }
        }
        #endregion

        #region CONSTRUCTOR
        public BookControl(AlbasBook albasBook, bool exist,bool activated)
        {

            InitializeComponent();

            AlbasBook = albasBook;
            BookExist = exist;
            BookActivated = activated;

            try
            {
                lblTitle.Text = albasBook.title;
                lblID.Text = albasBook.id.ToString();
                //btnBook.Text = albasBook.id;
                btnBook.Source = albasBook.medium_photo_url;
                

                if(BookExist)
                {
                    //this.BackgroundColor= Color.White;
                    imgBookNotReady.IsVisible = false;
                    lblID.TextColor= Color.Red;
                    //imgBookExist.IsVisible = false;
                    imgDownload.IsVisible = false;
                    imgLocked.IsVisible = false;
                    imgRemove.IsVisible = true;
                    imgBookReady.IsVisible = true;
                    imgBookReady.BackgroundColor= Color.Red;    

                }
                else
                {
                    imgBookReady.IsVisible = false;
                    imgRemove.IsVisible = false;
                    imgDownload.IsVisible = true;
                    imgLocked.IsVisible = false;
                    if (BookActivated == false)
                    {
                        imgDownload.IsVisible = false;
                        imgLocked.IsVisible = true;
                    }
                }

            }
            catch (Exception ee)
            {

                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            
        }
        #endregion

        #region EVENTS

        private void imgDownload_Clicked(object sender, EventArgs e)
        {
            ButtonClick();
        }
        private async void btnRemoveBook(object sender, EventArgs e)
        {
            #region RemoveBook
            try
            {
                bool answer = await App.Current.MainPage.DisplayAlert("Alert, delete Book", "Are you sure?", "Yes", "No");
                if (answer)
                {
                    BookEventArgs b = new BookEventArgs(AlbasBook, BookButtonAction.Remove);


                    OnBookClickEvent(this, b);
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            #endregion
        }
        #endregion


        private void ButtonClick()
        {
            if (BookExist)
            {
                #region GetUpdate
                //try
                //{
                //    BookEventArgs b = new BookEventArgs(AlbasBook);
                //    b.Update = true;

                //    OnBookClickEvent(this, b);
                //}
                //catch (Exception ee)
                //{
                //    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                //}
                #endregion

                #region OpenBook
                try
                {
                    BookEventArgs b = new BookEventArgs(AlbasBook, BookButtonAction.Open);


                    OnBookClickEvent(this, b);
                }
                catch (Exception ee)
                {
                    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                }
                #endregion
            }
            else
            {
                if (BookActivated == true)
                {
                    #region DownloadBook
                    try
                    {
                        BookEventArgs b = new BookEventArgs(AlbasBook, BookButtonAction.Download);


                        OnBookClickEvent(this, b);
                    }
                    catch (Exception ee)
                    {
                        logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                    }
                    #endregion
                }
                else
                {
                    #region ActivateBook
                    try
                    {
                        BookEventArgs b = new BookEventArgs(AlbasBook, BookButtonAction.Activate);

                        OnBookClickEvent(this, b);
                    }
                    catch (Exception ee)
                    {
                        logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                    }
                    #endregion
                }
            }
        }

       
    }
}