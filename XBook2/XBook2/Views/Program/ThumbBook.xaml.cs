﻿using KlasimeBase.Log;
using KlasimeBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2;
using SkiaSharp;
using System.IO;

namespace KlasimeViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThumbBook : ContentView
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        public static string user = "program";
        #region Custom Event

        public delegate void BookClickHandler(object sender, BookThumbEventArgs e);
        public event BookClickHandler OnBookClick;
        public int PageID { get; set; }


        private void OnBookClickEvent(object sender, BookThumbEventArgs e)
        {
            if (OnBookClick != null)
            {
                OnBookClick(sender, e);
            }
        }
        #endregion
        public ThumbBook(int pageId)
        {
            InitializeComponent();
            PageID = pageId;    
        }
        public bool SetImage(SKBitmap myBitmap)
        {
            //SKBitmap myBitmap = resource;
            if (myBitmap != null)
            {
                SKData sk = myBitmap.Encode(SKEncodedImageFormat.Png, 10);
                Stream stream = sk.AsStream();
                btnBook.Source = ImageSource.FromStream(() => stream);
            }
            else
            {
                logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(),null, "Thumb image missing for page=1");
                return false;
            }
            
            return true;
        }
        public bool SetTitle(string title)
        {
            lblTitle.Text = title;
            return true;
        }
        private void btnBook_Clicked(object sender, EventArgs e)
        {
            try
            {
                BookThumbEventArgs b = new BookThumbEventArgs(PageID);

                OnBookClickEvent(this, b);
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
    }
}