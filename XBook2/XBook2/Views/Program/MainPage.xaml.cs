﻿using eBookGraphics;
using KlasimeBase;
using KlasimeBase.Log;
using KlasimeViews;
using Microsoft.AppCenter.Crashes;
using SharedKlasime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2.ViewModels;
using XBook2.Views.Multimedia;

namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        public static string user = "MainPage";

        public static double ScreenWidth { get; } = DeviceDisplay.MainDisplayInfo.Width / DeviceDisplay.MainDisplayInfo.Density;
        public static double ScreenHeight { get; } = DeviceDisplay.MainDisplayInfo.Height / DeviceDisplay.MainDisplayInfo.Density;
        public static bool IsSmallScreen { get; } = ScreenWidth <= 360;

        #region VARIABLES
        public MainPageViewModel MainPageViewModelInstance;
        CUser loggedUser;


        //int bookID = 41;// 158;
        string sep = Common.GetPathSeparator();


        int oldWidth = 0;
        private SKColor PageBackgroundColor = new SKColor(237, 238, 239);
        PageType remeberPageType;

        ImageSource is1;
        ImageSource is2;

        PageAnno currentPageAnnoLeft;
        PageAnno currentPageAnnoRight;
        ImageControl photoViewer;
        VideoControl videoPlayer;
        AudioControl audioPlayer;

        NoteControl notePreview;
        FileControl filePreview;
        LinkControl linkPreview;
        YouTubeControl youTubePreview;
        private bool leftPageFullScreen = false;
        private bool rightPageFullScreen = false;
        //GridLength leftPageColLen;

        public string drId = "";

        private SKColor drawOnPageColor = SKColors.LightPink;
        private DrawLineColor idrawOnPageColor = DrawLineColor.LightPink;
        private DrawLineWidth drawOnPagePenSize = DrawLineWidth.Thin;
        private DrawType drawOnPagePenType;


        private MultimediaType actualNewCustomMediaType;
        private bool selectTextActive = false;
        #region BITMAPS
        SKBitmap bmpAudio;
        SKBitmap bmpPhotoSlide;
        SKBitmap bmpPhotoRight;
        SKBitmap bmpPhotoLeft;
        SKBitmap bmpOpenWindow;
        SKBitmap bmpVideo;
        SKBitmap bmpText;
        SKBitmap bmpFile;

        SKBitmap bmpTextRight;
        SKBitmap bmpTextLeft;

        SKBitmap bmpAudioRight;
        SKBitmap bmpAudioLeft;

        SKBitmap bmpVideoRight;
        SKBitmap bmpVideoLeft;

        SKBitmap bmpYoutubeRight;
        SKBitmap bmpYoutubeLeft;

        SKBitmap bmpLinkRight;
        SKBitmap bmpLinkLeft;

        SKBitmap bmpFileRight;
        SKBitmap bmpFileLeft;

        SKBitmap bmpPhotoRightCustom;
        SKBitmap bmpPhotoLeftCustom;

        SKBitmap bmpTextRightCustom;
        SKBitmap bmpTextLeftCustom;

        SKBitmap bmpAudioRightCustom;
        SKBitmap bmpAudioLeftCustom;

        SKBitmap bmpVideoRightCustom;
        SKBitmap bmpVideoLeftCustom;

        SKBitmap bmpYoutubeRightCustom;
        SKBitmap bmpYoutubeLeftCustom;

        SKBitmap bmpLinkRightCustom;
        SKBitmap bmpLinkLeftCustom;

        SKBitmap bmpFileRightCustom;
        SKBitmap bmpFileLeftCustom;

        SKBitmap bmpNoteAdd;
        SKBitmap bmpAudioAdd;
        SKBitmap bmpVideoAdd;
        SKBitmap bmpPhotoAdd;
        SKBitmap bmpFileAdd;
        SKBitmap bmpLinkAdd;
        SKBitmap bmpYoutubeAdd;

        SKBitmap bmpYouTube;
        SKBitmap bmpExternalLink;
        #endregion
        #endregion

        #region CONSTRUCTOR
        public MainPage(CUser _user, int ibookId, Book _currentBook, int pageNumLeft = 0)
        {
            
            try
            {

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                loggedUser = _user;

                user = loggedUser.firstname + "-" + loggedUser.lastname;
                Common.user = user;
                Base.user = user;

                loggedUser.currentColorTempalte = ColorTemlates.Green;

            
                InitializeComponent();


                this.BackgroundColor = Color.FromRgb(65, 64, 66);
                // filterList.BackgroundColor = Color.FromRgb(16, 110, 127);

                #region load from resources

                #region MediaAddRegular
                bmpOpenWindow = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_photo.png");
                bmpVideo = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_video.png");
                bmpText = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_text.png");
                bmpYouTube = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_youtube.png");
                BmpExternalLink = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_link.png");
                BmpFile = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_file.png");

                bmpAudio = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_audio.png");
                bmpPhotoSlide = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_photo.png");

                bmpPhotoRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_photo_ruler_right.png");
                bmpPhotoLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_photo_ruler_left.png");

                bmpTextRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_text_ruler_right.png");
                bmpTextLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_text_ruler_left.png");

                bmpAudioRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_audio_ruler_right.png");
                bmpAudioLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_audio_ruler_left.png");

                bmpVideoRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_video_ruler_right.png");
                bmpVideoLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_video_ruler_left.png");

                bmpYoutubeRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_youtube_ruler_right.png");
                bmpYoutubeLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_youtube_ruler_left.png");

                bmpLinkRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_link_ruler_right.png");
                bmpLinkLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_link_ruler_left.png");

                bmpFileRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_file_ruler_right.png");
                bmpFileLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_file_ruler_left.png");
                #endregion

                #region MediaAddCustom
                bmpNoteAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_text.png");
                BmpFileAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_file.png");
                BmpLinkAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_link.png");
                bmpYoutubeAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_youtube.png");
                bmpAudioAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_audio.png");
                bmpVideoAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_video.png");
                bmpPhotoAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_photo.png");

                bmpPhotoRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_photo_ruler_right.png");
                bmpPhotoLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_photo_ruler_left.png");

                bmpTextRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_text_ruler_right.png");
                bmpTextLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_text_ruler_left.png");

                bmpAudioRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_audio_ruler_right.png");
                bmpAudioLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_audio_ruler_left.png");

                bmpVideoRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_video_ruler_right.png");
                bmpVideoLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_video_ruler_left.png");

                bmpYoutubeRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_youtube_ruler_right.png");
                bmpYoutubeLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_youtube_ruler_left.png");

                bmpLinkRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_link_ruler_right.png");
                bmpLinkLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_link_ruler_left.png");

                bmpFileRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_file_ruler_right.png");
                bmpFileLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_file_ruler_left.png");

                #endregion

                #endregion

                is1 = ImageSource.FromResource("XBook2.media.load.synchronize_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                is2 = ImageSource.FromResource("XBook2.media.load.synchronize_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);


                setLabelsVisible(true);
                setLabelText();

                cbLink.IsChecked = true;
                cbGalery.IsChecked = true;
                cbInformation.IsChecked = true;
                cbVideo.IsChecked = true;
                cbYoutube.IsChecked = true;
                cbAudio.IsChecked = true;
                cbQuiz.IsChecked = true;

                if (_currentBook != null)
                {
                    MainPageViewModelInstance = new MainPageViewModel(loggedUser, _currentBook);
                }
                else
                {
                    MainPageViewModelInstance = new MainPageViewModel(this,loggedUser, ibookId, true);
                }
                MainPageViewModelInstance.WaitOnBook = true;

                MainPageViewModelInstance.OnMultimedia += MyViewModelInstance_OnMultimedia;
                MainPageViewModelInstance.PrepareBookListAsync().Wait();
                logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "book full path:" + Common.GetLocalPath() + Common.GetPathSeparator() + MainPageViewModelInstance.BookBasePath);

                BindingContext = MainPageViewModelInstance;

                #region Settings per Device OS

                Thickness tk = new Thickness(30, 30);
                switch (Device.RuntimePlatform)
                {
                    case "Android":
                        //move that into modelview to calculate size, when have space to be FALSE
                        //when phone have small screen then TRUE
                        MainPageViewModelInstance.SinglePageMode = true;
                        logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "Device.RuntimePlatform Android SinglePageModet: " + MainPageViewModelInstance.SinglePageMode);
                        break;
                    case "iOS":
                        MainPageViewModelInstance.SinglePageMode = true;
                        logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "Device.RuntimePlatform iOS SinglePageModet: " + MainPageViewModelInstance.SinglePageMode);
                        break;

                    case "UWP":
                        //MyViewModelInstance.SinglePageMode = true;
                        logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "Device.RuntimePlatform UWP SinglePageModet: " + MainPageViewModelInstance.SinglePageMode);
                        break;
                    default:
                        logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "Device.RuntimePlatform NOT in a list: " + Device.RuntimePlatform + " SinglePageMode: " + MainPageViewModelInstance.SinglePageMode);
                        break;
                }
                //LeftMenuList.Margin = tk;
                LeftPage.SinglePageMode = MainPageViewModelInstance.SinglePageMode;
                //if (MainPageViewModelInstance.SinglePageMode == true)
                //{
                //    mainGridColPageRight.Width = 0;
                //}
                #endregion


                if (MainPageViewModelInstance.CurrentBook.Status == true)
                {
                    if (MainPageViewModelInstance.CurrentBook.Status == true)
                    {
                        #region LeftPage
                        LeftPage.user = user;
                        LeftPage.BmpAudio = bmpAudio;
                        LeftPage.BmpVideo = bmpVideo;
                        LeftPage.BmpText = bmpText;
                        LeftPage.BmpYouTube = bmpYouTube;
                        LeftPage.BmpExternalLink = BmpExternalLink;
                        LeftPage.BmpFile = BmpFile;

                        LeftPage.BmpOpenWindow = bmpOpenWindow;
                        LeftPage.BmpPhotoSlide = bmpPhotoSlide;

                        LeftPage.BmpPhotoRight = bmpPhotoRight;
                        LeftPage.BmpPhotoLeft = bmpPhotoLeft;

                        LeftPage.BmpTextRight = bmpTextRight;
                        LeftPage.BmpTextLeft = BmpTextLeft;

                        LeftPage.BmpAudioRight = BmpAudioRight;
                        LeftPage.BmpAudioLeft = BmpAudioLeft;

                        LeftPage.BmpVideoRight = bmpVideoRight;
                        LeftPage.BmpVideoLeft = BmpVideoLeft;

                        LeftPage.BmpYoutubeRight = bmpYoutubeRight;
                        LeftPage.BmpYoutubeLeft = bmpYoutubeLeft;

                        LeftPage.BmpLinkRight = bmpLinkRight;
                        LeftPage.BmpLinkLeft = bmpLinkLeft;

                        LeftPage.BmpFileRight = bmpFileRight;
                        LeftPage.BmpFileLeft = BmpFileLeft;

                        //
                        LeftPage.BmpPhotoRightCustom = bmpPhotoRightCustom;
                        LeftPage.BmpPhotoLeftCustom = bmpPhotoLeftCustom;

                        LeftPage.BmpTextRightCustom = bmpTextRightCustom;
                        LeftPage.BmpTextLeftCustom = bmpTextLeftCustom;

                        LeftPage.BmpAudioRightCustom = bmpAudioRightCustom;
                        LeftPage.BmpAudioLeftCustom = bmpAudioLeftCustom;

                        LeftPage.BmpVideoRightCustom = bmpVideoRightCustom;
                        LeftPage.BmpVideoLeftCustom = bmpVideoLeftCustom;

                        LeftPage.BmpYoutubeRightCustom = bmpYoutubeRightCustom;
                        LeftPage.BmpYoutubeLeftCustom = bmpYoutubeLeftCustom;

                        LeftPage.BmpLinkRightCustom = bmpLinkRightCustom;
                        LeftPage.BmpLinkLeftCustom = bmpLinkLeftCustom;
                        //
                        LeftPage.BmpNoteAdd = bmpNoteAdd;
                        LeftPage.BmpAudioAdd = bmpAudioAdd;
                        LeftPage.BmpVideoAdd = bmpVideoAdd;
                        LeftPage.BmpPhotoAdd = bmpPhotoAdd;
                        LeftPage.BmpFileAdd = BmpFileAdd;
                        LeftPage.BmpLinkAdd = BmpLinkAdd;
                        LeftPage.BmpYoutubeAdd = BmpYoutubeAdd;

                        //CUSTOM
                        LeftPage.BmpPhotoRightCustom = bmpPhotoRightCustom;
                        LeftPage.BmpPhotoLeftCustom = bmpPhotoLeftCustom;

                        LeftPage.BmpTextRightCustom = bmpTextRightCustom;
                        LeftPage.BmpTextLeftCustom = bmpTextLeftCustom;

                        LeftPage.BmpAudioRightCustom = bmpAudioRightCustom;
                        LeftPage.BmpAudioLeftCustom = bmpAudioLeftCustom;

                        LeftPage.BmpVideoRightCustom = bmpVideoRightCustom;
                        LeftPage.BmpVideoLeftCustom = bmpVideoLeftCustom;

                        LeftPage.BmpYoutubeRightCustom = bmpYoutubeRightCustom;
                        LeftPage.BmpYoutubeLeftCustom = bmpYoutubeLeftCustom;

                        LeftPage.BmpLinkRightCustom = bmpLinkRightCustom;
                        LeftPage.BmpLinkLeftCustom = bmpLinkLeftCustom;

                        LeftPage.BmpFileRightCustom = BmpFileRightCustom;
                        LeftPage.BmpFileLeftCustom = BmpFileLeftCustom;
                        //

                        LeftPage.BookPath = MainPageViewModelInstance.BookBasePath;
                        LeftPage.BookID = MainPageViewModelInstance.BookID;
                        LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;
                        LeftPage.Book = MainPageViewModelInstance.BookFiles;
                        LeftPage.TabletMode = MainPageViewModelInstance.TabletMode;
                        #endregion

                        #region RightPage
                        //if (MainPageViewModelInstance.SinglePageMode == false)
                        {
                            RightPage.user = user;
                            RightPage.BmpAudio = bmpAudio;
                            RightPage.BmpVideo = bmpVideo;
                            RightPage.BmpText = bmpText;
                            RightPage.BmpYouTube = bmpYouTube;
                            RightPage.BmpExternalLink = BmpExternalLink;
                            RightPage.BmpFile = BmpFile;

                            RightPage.BmpOpenWindow = bmpOpenWindow;
                            RightPage.BmpPhotoSlide = bmpPhotoSlide;
                            RightPage.BmpPhotoSlide = bmpPhotoSlide;
                            RightPage.BmpPhotoRight = bmpPhotoRight;

                            RightPage.BmpTextRight = bmpTextRight;
                            RightPage.BmpTextLeft = BmpTextLeft;

                            RightPage.BmpAudioRight = BmpAudioRight;
                            RightPage.BmpAudioLeft = BmpAudioLeft;
                            RightPage.BmpVideoRight = bmpVideoRight;
                            RightPage.BmpVideoLeft = BmpVideoLeft;

                            RightPage.BmpYoutubeRight = bmpYoutubeRight;
                            RightPage.BmpYoutubeLeft = bmpYoutubeLeft;
                            RightPage.BmpLinkRight = bmpLinkRight;
                            RightPage.BmpLinkLeft = bmpLinkLeft;

                            RightPage.BmpFileRight = bmpFileRight;
                            RightPage.BmpFileLeft = BmpFileLeft;



                            RightPage.BmpNoteAdd = bmpNoteAdd;
                            RightPage.BmpAudioAdd = bmpAudioAdd;
                            RightPage.BmpVideoAdd = bmpVideoAdd;
                            RightPage.BmpPhotoAdd = bmpPhotoAdd;
                            RightPage.BmpFileAdd = BmpFileAdd;
                            RightPage.BmpLinkAdd = BmpLinkAdd;
                            RightPage.BmpYoutubeAdd = BmpYoutubeAdd;

                            //CUSTOM
                            RightPage.BmpPhotoRightCustom = bmpPhotoRightCustom;
                            RightPage.BmpPhotoLeftCustom = bmpPhotoLeftCustom;

                            RightPage.BmpTextRightCustom = bmpTextRightCustom;
                            RightPage.BmpTextLeftCustom = bmpTextLeftCustom;

                            RightPage.BmpAudioRightCustom = bmpAudioRightCustom;
                            RightPage.BmpAudioLeftCustom = bmpAudioLeftCustom;

                            RightPage.BmpVideoRightCustom = bmpVideoRightCustom;
                            RightPage.BmpVideoLeftCustom = bmpVideoLeftCustom;

                            RightPage.BmpYoutubeRightCustom = bmpYoutubeRightCustom;
                            RightPage.BmpYoutubeLeftCustom = bmpYoutubeLeftCustom;

                            RightPage.BmpLinkRightCustom = bmpLinkRightCustom;
                            RightPage.BmpLinkLeftCustom = bmpLinkLeftCustom;

                            RightPage.BmpFileRightCustom = BmpFileRightCustom;
                            RightPage.BmpFileLeftCustom = BmpFileLeftCustom;
                            //

                            RightPage.BookPath = MainPageViewModelInstance.BookBasePath;
                            RightPage.BookID = MainPageViewModelInstance.BookID;
                            RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;
                            RightPage.Book = MainPageViewModelInstance.BookFiles;
                            RightPage.TabletMode = MainPageViewModelInstance.TabletMode;
                        }

                        #endregion

                        lblStatus.Text = MainPageViewModelInstance.CurrentBook.Description;


                    }



                    // MoveToPage();
                }

                stopWatch.Stop();
                MainPageViewModelInstance.MainPageLoadTime = stopWatch.Elapsed.Seconds;

               
            }
            catch (Exception ee)
            {

                Crashes.TrackError(ee, new Dictionary<string, string>() {
                    { "IsSubscribed", "true" }
                });
            }
        }
        public MainPage(CUser _user, int ibookId, Book _currentBook, bool _leftPageFullScreen, bool _rightPageFullScreen, int pageNumLeft)
        {

            try
            {
                leftPageFullScreen = _leftPageFullScreen;
                rightPageFullScreen = _rightPageFullScreen;

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                loggedUser = _user;

                user = loggedUser.firstname + "-" + loggedUser.lastname;
                Common.user = user;
                Base.user = user;

                loggedUser.currentColorTempalte = ColorTemlates.Green;

                //2400 1080
                //MaximizeWindowOnLoad();

                InitializeComponent();


                this.BackgroundColor = Color.FromRgb(65, 64, 66);
                // filterList.BackgroundColor = Color.FromRgb(16, 110, 127);

                #region load from resources

                #region MediaAddRegular
                bmpOpenWindow = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_photo.png");
                bmpVideo = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_video.png");
                bmpText = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_text.png");
                bmpYouTube = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_youtube.png");
                BmpExternalLink = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_link.png");
                BmpFile = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_file.png");

                bmpAudio = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_audio.png");
                bmpPhotoSlide = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_photo.png");

                bmpPhotoRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_photo_ruler_right.png");
                bmpPhotoLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_photo_ruler_left.png");

                bmpTextRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_text_ruler_right.png");
                bmpTextLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_text_ruler_left.png");

                bmpAudioRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_audio_ruler_right.png");
                bmpAudioLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_audio_ruler_left.png");

                bmpVideoRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_video_ruler_right.png");
                bmpVideoLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_video_ruler_left.png");

                bmpYoutubeRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_youtube_ruler_right.png");
                bmpYoutubeLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_youtube_ruler_left.png");

                bmpLinkRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_link_ruler_right.png");
                bmpLinkLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_link_ruler_left.png");

                bmpFileRight = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_file_ruler_right.png");
                bmpFileLeft = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.regular.annotation_file_ruler_left.png");
                #endregion

                #region MediaAddCustom
                bmpNoteAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_text.png");
                BmpFileAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_file.png");
                BmpLinkAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_link.png");
                bmpYoutubeAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_youtube.png");
                bmpAudioAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_audio.png");
                bmpVideoAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_video.png");
                bmpPhotoAdd = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.eannotation_photo.png");

                bmpPhotoRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_photo_ruler_right.png");
                bmpPhotoLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_photo_ruler_left.png");

                bmpTextRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_text_ruler_right.png");
                bmpTextLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_text_ruler_left.png");

                bmpAudioRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_audio_ruler_right.png");
                bmpAudioLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_audio_ruler_left.png");

                bmpVideoRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_video_ruler_right.png");
                bmpVideoLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_video_ruler_left.png");

                bmpYoutubeRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_youtube_ruler_right.png");
                bmpYoutubeLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_youtube_ruler_left.png");

                bmpLinkRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_link_ruler_right.png");
                bmpLinkLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_link_ruler_left.png");

                bmpFileRightCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_file_ruler_right.png");
                bmpFileLeftCustom = BitmapExtensions.LoadBitmapResource(GetType(), "XBook2.media.load.annotation.custom.annotation_file_ruler_left.png");

                #endregion

                #endregion

                //leftPageColLen = mainGridColPageLeft.Width;


                is1 = ImageSource.FromResource("XBook2.media.load.synchronize_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                is2 = ImageSource.FromResource("XBook2.media.load.synchronize_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);


                setLabelsVisible(true);
                setLabelText();

                cbLink.IsChecked = true;
                cbGalery.IsChecked = true;
                cbInformation.IsChecked = true;
                cbVideo.IsChecked = true;
                cbYoutube.IsChecked = true;
                cbAudio.IsChecked = true;
                cbQuiz.IsChecked = true;

                if (_currentBook != null)
                {
                    MainPageViewModelInstance = new MainPageViewModel(loggedUser, _currentBook);
                }
                else
                {
                    MainPageViewModelInstance = new MainPageViewModel(this,loggedUser, ibookId, true);
                }


                MainPageViewModelInstance.OnMultimedia += MyViewModelInstance_OnMultimedia;
                MainPageViewModelInstance.PrepareBookListAsync().Wait();
                logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "book full path:" + Common.GetLocalPath() + Common.GetPathSeparator() + MainPageViewModelInstance.BookBasePath);

                BindingContext = MainPageViewModelInstance;

                #region Settings per Device OS

                Thickness tk = new Thickness(30, 30);
                switch (Device.RuntimePlatform)
                {
                    case "Android":
                        //move that into modelview to calculate size, when have space to be FALSE
                        //when phone have small screen then TRUE
                        MainPageViewModelInstance.SinglePageMode = true;
                        logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "Device.RuntimePlatform Android SinglePageModet: " + MainPageViewModelInstance.SinglePageMode);
                        break;
                    case "iOS":
                        MainPageViewModelInstance.SinglePageMode = true;
                        logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "Device.RuntimePlatform iOS SinglePageModet: " + MainPageViewModelInstance.SinglePageMode);
                        break;

                    case "UWP":
                        //MyViewModelInstance.SinglePageMode = true;
                        logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "Device.RuntimePlatform UWP SinglePageModet: " + MainPageViewModelInstance.SinglePageMode);
                        break;
                    default:
                        logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "Device.RuntimePlatform NOT in a list: " + Device.RuntimePlatform + " SinglePageMode: " + MainPageViewModelInstance.SinglePageMode);
                        break;
                }
                //LeftMenuList.Margin = tk;
                LeftPage.SinglePageMode = MainPageViewModelInstance.SinglePageMode;
                //if (MainPageViewModelInstance.SinglePageMode == true)
                //{
                //    mainGridColPageRight.Width = 0;
                //}
                #endregion
                MainPageViewModelInstance.LeftPageFullScreen = leftPageFullScreen;
                MainPageViewModelInstance.RightPageFullScreen = rightPageFullScreen;
                MainPageViewModelInstance.PageNumLeft = pageNumLeft;
                MainPageViewModelInstance.PageNumRight = pageNumLeft + 1;

                if (MainPageViewModelInstance.CurrentBook.Status == true)
                {
                    if (MainPageViewModelInstance.CurrentBook.Status == true)
                    {
                        #region LeftPage
                        LeftPage.user = user;
                        LeftPage.BmpAudio = bmpAudio;
                        LeftPage.BmpVideo = bmpVideo;
                        LeftPage.BmpText = bmpText;
                        LeftPage.BmpYouTube = bmpYouTube;
                        LeftPage.BmpExternalLink = BmpExternalLink;
                        LeftPage.BmpFile = BmpFile;

                        LeftPage.BmpOpenWindow = bmpOpenWindow;
                        LeftPage.BmpPhotoSlide = bmpPhotoSlide;

                        LeftPage.BmpPhotoRight = bmpPhotoRight;
                        LeftPage.BmpPhotoLeft = bmpPhotoLeft;

                        LeftPage.BmpTextRight = bmpTextRight;
                        LeftPage.BmpTextLeft = BmpTextLeft;

                        LeftPage.BmpAudioRight = BmpAudioRight;
                        LeftPage.BmpAudioLeft = BmpAudioLeft;

                        LeftPage.BmpVideoRight = bmpVideoRight;
                        LeftPage.BmpVideoLeft = BmpVideoLeft;

                        LeftPage.BmpYoutubeRight = bmpYoutubeRight;
                        LeftPage.BmpYoutubeLeft = bmpYoutubeLeft;

                        LeftPage.BmpLinkRight = bmpLinkRight;
                        LeftPage.BmpLinkLeft = bmpLinkLeft;

                        LeftPage.BmpFileRight = bmpFileRight;
                        LeftPage.BmpFileLeft = BmpFileLeft;

                        //
                        LeftPage.BmpPhotoRightCustom = bmpPhotoRightCustom;
                        LeftPage.BmpPhotoLeftCustom = bmpPhotoLeftCustom;

                        LeftPage.BmpTextRightCustom = bmpTextRightCustom;
                        LeftPage.BmpTextLeftCustom = bmpTextLeftCustom;

                        LeftPage.BmpAudioRightCustom = bmpAudioRightCustom;
                        LeftPage.BmpAudioLeftCustom = bmpAudioLeftCustom;

                        LeftPage.BmpVideoRightCustom = bmpVideoRightCustom;
                        LeftPage.BmpVideoLeftCustom = bmpVideoLeftCustom;

                        LeftPage.BmpYoutubeRightCustom = bmpYoutubeRightCustom;
                        LeftPage.BmpYoutubeLeftCustom = bmpYoutubeLeftCustom;

                        LeftPage.BmpLinkRightCustom = bmpLinkRightCustom;
                        LeftPage.BmpLinkLeftCustom = bmpLinkLeftCustom;
                        //
                        LeftPage.BmpNoteAdd = bmpNoteAdd;
                        LeftPage.BmpAudioAdd = bmpAudioAdd;
                        LeftPage.BmpVideoAdd = bmpVideoAdd;
                        LeftPage.BmpPhotoAdd = bmpPhotoAdd;
                        LeftPage.BmpFileAdd = BmpFileAdd;
                        LeftPage.BmpLinkAdd = BmpLinkAdd;
                        LeftPage.BmpYoutubeAdd = BmpYoutubeAdd;

                        //CUSTOM
                        LeftPage.BmpPhotoRightCustom = bmpPhotoRightCustom;
                        LeftPage.BmpPhotoLeftCustom = bmpPhotoLeftCustom;

                        LeftPage.BmpTextRightCustom = bmpTextRightCustom;
                        LeftPage.BmpTextLeftCustom = bmpTextLeftCustom;

                        LeftPage.BmpAudioRightCustom = bmpAudioRightCustom;
                        LeftPage.BmpAudioLeftCustom = bmpAudioLeftCustom;

                        LeftPage.BmpVideoRightCustom = bmpVideoRightCustom;
                        LeftPage.BmpVideoLeftCustom = bmpVideoLeftCustom;

                        LeftPage.BmpYoutubeRightCustom = bmpYoutubeRightCustom;
                        LeftPage.BmpYoutubeLeftCustom = bmpYoutubeLeftCustom;

                        LeftPage.BmpLinkRightCustom = bmpLinkRightCustom;
                        LeftPage.BmpLinkLeftCustom = bmpLinkLeftCustom;

                        LeftPage.BmpFileRightCustom = BmpFileRightCustom;
                        LeftPage.BmpFileLeftCustom = BmpFileLeftCustom;
                        //

                        LeftPage.BookPath = MainPageViewModelInstance.BookBasePath;
                        LeftPage.BookID = MainPageViewModelInstance.BookID;
                        LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;
                        LeftPage.Book = MainPageViewModelInstance.BookFiles;
                        LeftPage.TabletMode = MainPageViewModelInstance.TabletMode;
                        #endregion

                        #region RightPage
                        //if (MainPageViewModelInstance.SinglePageMode == false)
                        {
                            RightPage.user = user;
                            RightPage.BmpAudio = bmpAudio;
                            RightPage.BmpVideo = bmpVideo;
                            RightPage.BmpText = bmpText;
                            RightPage.BmpYouTube = bmpYouTube;
                            RightPage.BmpExternalLink = BmpExternalLink;
                            RightPage.BmpFile = BmpFile;

                            RightPage.BmpOpenWindow = bmpOpenWindow;
                            RightPage.BmpPhotoSlide = bmpPhotoSlide;
                            RightPage.BmpPhotoSlide = bmpPhotoSlide;
                            RightPage.BmpPhotoRight = bmpPhotoRight;

                            RightPage.BmpTextRight = bmpTextRight;
                            RightPage.BmpTextLeft = BmpTextLeft;

                            RightPage.BmpAudioRight = BmpAudioRight;
                            RightPage.BmpAudioLeft = BmpAudioLeft;
                            RightPage.BmpVideoRight = bmpVideoRight;
                            RightPage.BmpVideoLeft = BmpVideoLeft;

                            RightPage.BmpYoutubeRight = bmpYoutubeRight;
                            RightPage.BmpYoutubeLeft = bmpYoutubeLeft;
                            RightPage.BmpLinkRight = bmpLinkRight;
                            RightPage.BmpLinkLeft = bmpLinkLeft;

                            RightPage.BmpFileRight = bmpFileRight;
                            RightPage.BmpFileLeft = BmpFileLeft;



                            RightPage.BmpNoteAdd = bmpNoteAdd;
                            RightPage.BmpAudioAdd = bmpAudioAdd;
                            RightPage.BmpVideoAdd = bmpVideoAdd;
                            RightPage.BmpPhotoAdd = bmpPhotoAdd;
                            RightPage.BmpFileAdd = BmpFileAdd;
                            RightPage.BmpLinkAdd = BmpLinkAdd;
                            RightPage.BmpYoutubeAdd = BmpYoutubeAdd;

                            //CUSTOM
                            RightPage.BmpPhotoRightCustom = bmpPhotoRightCustom;
                            RightPage.BmpPhotoLeftCustom = bmpPhotoLeftCustom;

                            RightPage.BmpTextRightCustom = bmpTextRightCustom;
                            RightPage.BmpTextLeftCustom = bmpTextLeftCustom;

                            RightPage.BmpAudioRightCustom = bmpAudioRightCustom;
                            RightPage.BmpAudioLeftCustom = bmpAudioLeftCustom;

                            RightPage.BmpVideoRightCustom = bmpVideoRightCustom;
                            RightPage.BmpVideoLeftCustom = bmpVideoLeftCustom;

                            RightPage.BmpYoutubeRightCustom = bmpYoutubeRightCustom;
                            RightPage.BmpYoutubeLeftCustom = bmpYoutubeLeftCustom;

                            RightPage.BmpLinkRightCustom = bmpLinkRightCustom;
                            RightPage.BmpLinkLeftCustom = bmpLinkLeftCustom;

                            RightPage.BmpFileRightCustom = BmpFileRightCustom;
                            RightPage.BmpFileLeftCustom = BmpFileLeftCustom;
                            //

                            RightPage.BookPath = MainPageViewModelInstance.BookBasePath;
                            RightPage.BookID = MainPageViewModelInstance.BookID;
                            RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;
                            RightPage.Book = MainPageViewModelInstance.BookFiles;
                            RightPage.TabletMode = MainPageViewModelInstance.TabletMode;
                        }

                        #endregion

                        lblStatus.Text = MainPageViewModelInstance.CurrentBook.Description;


                    }



                    // MoveToPage();
                }

                stopWatch.Stop();
                MainPageViewModelInstance.MainPageLoadTime = stopWatch.Elapsed.Seconds;
            }
            catch (Exception ee)
            {

                Crashes.TrackError(ee, new Dictionary<string, string>() {
                    { "IsSubscribed", "true" }
                });
            }
        }
        private void MyViewModelInstance_OnMultimedia(object sender, MultimediaEventArgs e)
        {
            PlayMultimedia(e);
        }
        #endregion

        #region PROPERTIES
        // public bool SinglePageMode { get => MyViewModelInstance.SinglePageMode; set => MyViewModelInstance.SinglePageMode = value; }
        //  public bool BookEncoding { get => bookEncoding; set => bookEncoding = value; }
        public SKBitmap BmpYouTube { get => bmpYouTube; set => bmpYouTube = value; }
        public SKBitmap BmpExternalLink { get => bmpExternalLink; set => bmpExternalLink = value; }
        public SKBitmap BmpTextRiht { get => bmpTextRight; set => bmpTextRight = value; }
        public SKBitmap BmpTextLeft { get => bmpTextLeft; set => bmpTextLeft = value; }
        public SKBitmap BmpAudioRight { get => bmpAudioRight; set => bmpAudioRight = value; }
        public SKBitmap BmpAudioLeft { get => bmpAudioLeft; set => bmpAudioLeft = value; }
        public SKBitmap BmpVideoRight { get => bmpVideoRight; set => bmpVideoRight = value; }
        public SKBitmap BmpVideoLeft { get => bmpVideoLeft; set => bmpVideoLeft = value; }
        public SKBitmap BmpYoutubeRight { get => bmpYoutubeRight; set => bmpYoutubeRight = value; }
        public SKBitmap BmpYoutubeLeft { get => bmpYoutubeLeft; set => bmpYoutubeLeft = value; }
        public SKBitmap BmpLinkRight { get => bmpLinkRight; set => bmpLinkRight = value; }
        public SKBitmap BmpLinkLeft { get => bmpLinkLeft; set => bmpLinkLeft = value; }
        public SKColor DrawOnPageColor { get => drawOnPageColor; set => drawOnPageColor = value; }
        public DrawLineWidth DrawOnPagePenSize { get => drawOnPagePenSize; set => drawOnPagePenSize = value; }
        public DrawType DrawOnPagePenType { get => drawOnPagePenType; set => drawOnPagePenType = value; }
        public SKBitmap BmpAudioAdd { get => bmpAudioAdd; set => bmpAudioAdd = value; }
        public SKBitmap BmpVideoAdd { get => bmpVideoAdd; set => bmpVideoAdd = value; }
        public SKBitmap BmpPhotoAdd { get => bmpPhotoAdd; set => bmpPhotoAdd = value; }
        public SKBitmap BmpFileAdd { get => bmpFileAdd; set => bmpFileAdd = value; }
        public SKBitmap BmpLinkAdd { get => bmpLinkAdd; set => bmpLinkAdd = value; }
        public SKBitmap BmpYoutubeAdd { get => bmpYoutubeAdd; set => bmpYoutubeAdd = value; }
        public MultimediaType ActualNewCustomMediaType { get => actualNewCustomMediaType; set => actualNewCustomMediaType = value; }
        public SKBitmap BmpFile { get => bmpFile; set => bmpFile = value; }
        public SKBitmap BmpFileRight { get => bmpFileRight; set => bmpFileRight = value; }
        public SKBitmap BmpFileLeft { get => bmpFileLeft; set => bmpFileLeft = value; }
        public SKBitmap BmpFileRightCustom { get => bmpFileRightCustom; set => bmpFileRightCustom = value; }
        public SKBitmap BmpFileLeftCustom { get => bmpFileLeftCustom; set => bmpFileLeftCustom = value; }
        public bool SelectTextActive { get => selectTextActive; set => selectTextActive = value; }
        //public DrawItemOnPageBase DrawItemOnPageLeft {
        //    get { return dipLeft; }
        //    set { dipLeft = value; }
        //}
        //public DrawItemOnPageBase DipRight { get => dipRight; set => dipRight = value; }


        #endregion

        #region LEFT MENU

        private void BookSync_Clicked(object sender, EventArgs e)
        {

            //    setImagesLeftMenu();
            setLabelText();
            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //{
            //    btnBookSync.Source = ImageSource.FromResource("XBook2.media.load.orange.synchronize_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //}

            //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //{
            //    btnBookSync.Source = ImageSource.FromResource("XBook2.media.load.green.synchronize_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //}
            
            MainPageViewModelInstance.BtnSettingsMenuPress = false;
            //RightPage.LoadPage();
        }
        private void btnCursor_Clicked(object sender, EventArgs e)
        {
            //setImagesLeftMenu();
            leftSubmenuGrid.IsVisible = false;
            
            MainPageViewModelInstance.BtnSettingsMenuPress = false;
            drawSubMenu.IsVisible = false;
            attachSubMenu.IsVisible = false;

            if (MainPageViewModelInstance.SinglePageMode)
                LeftPage.EnableSwipe();
            //BookSync.Source = is1; // ImageSource.FromResource("XBook2.media.load.synchronize_select.png");
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                //btnCursor.Source = ImageSource.FromResource("XBook2.media.load.orange.cursor_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }
            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                // btnCursor.Source = ImageSource.FromResource("XBook2.media.load.green.cursor_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }
        }

        private void btnLibrary_Clicked(object sender, EventArgs e)
        {
            //setImagesLeftMenu();
            
            MainPageViewModelInstance.BtnSettingsMenuPress = false;
            drawSubMenu.IsVisible = false;
            attachSubMenu.IsVisible = false;

            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //    btnLibrary.Source = ImageSource.FromResource("XBook2.media.load.orange.library_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //{
            //    btnLibrary.Source = ImageSource.FromResource("XBook2.media.load.green.library_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //}

            if (librarySubMenu.IsVisible == false)
            {
                //double w = 100;
                //double h = 106;
                //var newPos = new Rectangle(60, h, w, h);
                //leftSubMenu.LayoutTo(newPos, 80);
                librarySubMenu.IsVisible = true;

            }
            else
            {
                librarySubMenu.IsVisible = false;

            }
        }

        private void btnSelect_Clicked(object sender, EventArgs e)
        {
            //setImagesLeftMenu();
            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //{
            //    btnSelect.Source = ImageSource.FromResource("XBook2.media.load.orange.select_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //}

            //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //{
            //    btnSelect.Source = ImageSource.FromResource("XBook2.media.load.green.select_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //}
            SelectTextActive = true;
            LeftPage.SelectTextActive = SelectTextActive;
            if (MainPageViewModelInstance.SinglePageMode == false)
            {
                RightPage.SelectTextActive = SelectTextActive;
            }

        }

        private void btnUnselect_Clicked(object sender, EventArgs e)
        {
            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //{
            //    btnUnselect.Source = ImageSource.FromResource("XBook2.media.load.orange.unselect_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //}

            //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //{
            //    btnUnselect.Source = ImageSource.FromResource("XBook2.media.load.green.unselect_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //}

            SelectTextActive = false;
            LeftPage.SelectTextActive = SelectTextActive;
            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.SelectTextActive = SelectTextActive;

            LeftPage.PrepareSelectIntoClipoard = "";
            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.PrepareSelectIntoClipoard = "";

            lblStatus.Text = "";
            LeftPage.LoadPage();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.LoadPage();

        }

        #region LEFT MENU - Draw ON Page
        private void btnPen_Clicked(object sender, EventArgs e)
        {

            //setImagesLeftMenu();
            librarySubMenu.IsVisible = false;
            attachSubMenu.IsVisible = false;
            MainPageViewModelInstance.BtnSettingsMenuPress = false;

            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //btnPen.Source = ImageSource.FromResource("XBook2.media.load.orange.pen-button-select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            // if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //btnPen.Source = ImageSource.FromResource("XBook2.media.load.green.pen_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);


            if (drawSubMenu.IsVisible == false)
            {
                drawSubMenu.IsVisible = true;
            }
            else
            {
                drawSubMenu.IsVisible = false;

            }
        }

        private void btnPenDraw_Clicked(object sender, EventArgs e)
        {
            try
            {
                Xamarin.Forms.Application.Current.MainPage = new DrawingOnPage(loggedUser, MainPageViewModelInstance.BookID, MainPageViewModelInstance.CurrentBook, MainPageViewModelInstance.PageNumLeft, DrawType.Pen, drawOnPagePenSize, idrawOnPageColor);
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            //drawOnPagePenType = DrawType.Pen;

            //SetDrawing();

            //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //    btnPenDraw.Source = ImageSource.FromResource("XBook2.media.load.green.pen-button-up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //    btnPenDraw.Source = ImageSource.FromResource("XBook2.media.load.orange.pen-button-up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            //task: Nikola_1105
            drawSubMenu.IsVisible = false;

        }

        private void btnLineDraw_Clicked(object sender, EventArgs e)
        {
            try
            {
                Xamarin.Forms.Application.Current.MainPage = new DrawingOnPage(loggedUser, MainPageViewModelInstance.BookID, MainPageViewModelInstance.CurrentBook, MainPageViewModelInstance.PageNumLeft, DrawType.Line, drawOnPagePenSize, idrawOnPageColor);
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            //drawOnPagePenType = DrawType.Line;

            //SetDrawing();

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                btnLineDraw.Source = ImageSource.FromResource("XBook2.media.load.green.line_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                btnLineDraw.Source = ImageSource.FromResource("XBook2.media.load.orange.line_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            drawSubMenu.IsVisible = false;
        }

        private void btnDeleteDraw_Clicked(object sender, EventArgs e)
        {
            drawOnPagePenType = DrawType.Delete;

            LeftPage.StopDrawing();
            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.StopDrawing();

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                btnDeleteDraw.Source = ImageSource.FromResource("XBook2.media.load.green.erase_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                btnDeleteDraw.Source = ImageSource.FromResource("XBook2.media.load.orange.erase_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            drawSubMenu.IsVisible = false;

        }

        private void btnLineSizetThin_Clicked(object sender, EventArgs e)
        {
            drawOnPagePenSize = DrawLineWidth.Thin;

            SetDrawing();
            selectedDrawSize = true;
        }

        private void btnLineSizeMedium_Clicked(object sender, EventArgs e)
        {
            drawOnPagePenSize = DrawLineWidth.Medium;

            SetDrawing();


            selectedDrawSize = true;
        }

        private void btnLineSizeThick_Clicked(object sender, EventArgs e)
        {
            drawOnPagePenSize = DrawLineWidth.Thick;


            SetDrawing();
            selectedDrawSize = true;
        }

        bool selectedDrawColor = false;
        bool selectedDrawSize = false;
        private void btnLineColorRose_Clicked(object sender, EventArgs e)
        {
            //lblStatus.Text = "btnLineColorRose_Clicked";
            drawOnPageColor = SKColors.LightPink;
            idrawOnPageColor = DrawLineColor.LightPink;


            SetDrawing();


            selectedDrawColor = true;
        }

        private void btnLineColorBlue_Clicked(object sender, EventArgs e)
        {
            //lblStatus.Text = "btnLineColorBlue_Clicked";
            drawOnPageColor = SKColors.LightBlue;
            idrawOnPageColor = DrawLineColor.LightBlue;

            SetDrawing();


            selectedDrawColor = true;
        }

        private void btnLineColorGreen_Clicked(object sender, EventArgs e)
        {
            //lblStatus.Text = "btnLineColorGreen_Clicked";
            drawOnPageColor = SKColors.LightGreen;
            idrawOnPageColor = DrawLineColor.LightGreen;

            SetDrawing();


            selectedDrawColor = true;
        }
        private void SetDrawing()
        {
            LeftPage.StartDrawing(drawOnPagePenType, drawOnPagePenSize, idrawOnPageColor);

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.StartDrawing(drawOnPagePenType, drawOnPagePenSize, idrawOnPageColor);
        }
        #endregion

        #region LEFT MENU - NOTE
        bool addNewMultimedia = false;
        private void btnNote_Clicked(object sender, EventArgs e)
        {
            //setImagesLeftMenu();
            librarySubMenu.IsVisible = false;
            MainPageViewModelInstance.BtnSettingsMenuPress = false;
            drawSubMenu.IsVisible = false;
            attachSubMenu.IsVisible = false;

            if (addNewMultimedia == false)
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnNote.Source = ImageSource.FromResource("XBook2.media.load.orange.notes_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnNote.Source = ImageSource.FromResource("XBook2.media.load.green.notes_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //task:Darko_1205_AddNote
                //activate move for mouse
                //on click, set possition and start Note,
                //  1.on mouse move draw red icon(MultimediaParser
                //  2 page change

                //  3.wait on SinglePageView mouse click release to set possition for new Note
                //  4.call OnAddMultimedia from mouse released
                //  5.main page catch event OnAddMultimedia, LeftPage_OnAddMultimedia and show note cotrol to add new note
                //  6. note control store new Note into XML and send event for close to main page

                ActualNewCustomMediaType = MultimediaType.Note;
                LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);
                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                addNewMultimedia = true;
                //MultimediaEventArgs me = new MultimediaEventArgs(MultimediaType.Note, 600, 500, null, "");
                //showNote(me);
            }
            else
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnNote.Source = ImageSource.FromResource("XBook2.media.load.orange.notes_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnNote.Source = ImageSource.FromResource("XBook2.media.load.green.notes_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                addNewMultimedia = false;
                LeftPage.StopAddingMultimediaOnPage();
                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StopAddingMultimediaOnPage();
            }
        }
        #endregion

        #region LEFT MENU -ATTACH
        private void btnAttach_Clicked(object sender, EventArgs e)
        {
            //setImagesLeftMenu();
            librarySubMenu.IsVisible = false;
            drawSubMenu.IsVisible = false;
            MainPageViewModelInstance.BtnSettingsMenuPress = false;

            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //    btnAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //    btnAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            if (attachSubMenu.IsVisible == false)
            {
                attachSubMenu.IsVisible = true;
            }
            else
            {
                attachSubMenu.IsVisible = false;
            }



        }
        private void btnFileAttach_Clicked(object sender, EventArgs e)
        {
            if (addNewMultimedia == false)
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //task:Darko_1205_AddNote
                //activate move for mouse
                //on click, set possition and start Note,
                //1. wait on SinglePageView mouse click release to set possition for new Note
                //2. call OnAddMultimedia from mouse released
                //3. main page catch event OnAddMultimedia, LeftPage_OnAddMultimedia and show note cotrol to add new note
                //4. note control store new Note into XML and send event for close to main page
                //5. 
                ActualNewCustomMediaType = MultimediaType.File;
                LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                addNewMultimedia = true;
                //MultimediaEventArgs me = new MultimediaEventArgs(MultimediaType.Note, 600, 500, null, "");
                //showNote(me);
                attachSubMenu.IsVisible = false;
            }
            else
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                addNewMultimedia = false;
                LeftPage.StopAddingMultimediaOnPage();
                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StopAddingMultimediaOnPage();
            }
        }

        private void btnLinkAttach_Clicked(object sender, EventArgs e)
        {
            if (addNewMultimedia == false)
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //task:Darko_1205_AddNote
                //activate move for mouse
                //on click, set possition and start Note,
                //1. wait on SinglePageView mouse click release to set possition for new Note
                //2. call OnAddMultimedia from mouse released
                //3. main page catch event OnAddMultimedia, LeftPage_OnAddMultimedia and show note cotrol to add new note
                //4. note control store new Note into XML and send event for close to main page
                //5. 
                ActualNewCustomMediaType = MultimediaType.Link;
                LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                addNewMultimedia = true;
                //MultimediaEventArgs me = new MultimediaEventArgs(MultimediaType.Note, 600, 500, null, "");
                //showNote(me);
                attachSubMenu.IsVisible = false;
            }
            else
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                addNewMultimedia = false;
                LeftPage.StopAddingMultimediaOnPage();

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StopAddingMultimediaOnPage();
            }
        }

        private void btnYouTubeAttach_Clicked(object sender, EventArgs e)
        {
            if (addNewMultimedia == false)
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //task:Darko_1205_AddNote
                //activate move for mouse
                //on click, set possition and start Note,
                //1. wait on SinglePageView mouse click release to set possition for new Note
                //2. call OnAddMultimedia from mouse released
                //3. main page catch event OnAddMultimedia, LeftPage_OnAddMultimedia and show note cotrol to add new note
                //4. note control store new Note into XML and send event for close to main page
                //5. 
                ActualNewCustomMediaType = MultimediaType.YouTube;
                LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                addNewMultimedia = true;
                //MultimediaEventArgs me = new MultimediaEventArgs(MultimediaType.Note, 600, 500, null, "");
                //showNote(me);
                attachSubMenu.IsVisible = false;
            }
            else
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                addNewMultimedia = false;
                LeftPage.StopAddingMultimediaOnPage();

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StopAddingMultimediaOnPage();
            }
        }

        private void btnPhotoAttach_Clicked(object sender, EventArgs e)
        {
            if (addNewMultimedia == false)
            {

                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //task:Darko_1205_AddNote
                //activate move for mouse
                //on click, set possition and start Note,
                //1. wait on SinglePageView mouse click release to set possition for new Note
                //2. call OnAddMultimedia from mouse released
                //3. main page catch event OnAddMultimedia, LeftPage_OnAddMultimedia and show note cotrol to add new note
                //4. note control store new Note into XML and send event for close to main page
                //5. 
                ActualNewCustomMediaType = MultimediaType.PhotoGalery;
                LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                addNewMultimedia = true;
                //MultimediaEventArgs me = new MultimediaEventArgs(MultimediaType.Note, 600, 500, null, "");
                //showNote(me);
                attachSubMenu.IsVisible = false;

            }
            else
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                addNewMultimedia = false;
                LeftPage.StopAddingMultimediaOnPage();

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StopAddingMultimediaOnPage();

            }
        }

        private void btnVideoAttach_Clicked(object sender, EventArgs e)
        {
            if (addNewMultimedia == false)
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //task:Darko_1205_AddNote
                //activate move for mouse
                //on click, set possition and start Note,
                //1. wait on SinglePageView mouse click release to set possition for new Note
                //2. call OnAddMultimedia from mouse released
                //3. main page catch event OnAddMultimedia, LeftPage_OnAddMultimedia and show note cotrol to add new note
                //4. note control store new Note into XML and send event for close to main page
                //5. 
                ActualNewCustomMediaType = MultimediaType.Video;
                LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                addNewMultimedia = true;
                //MultimediaEventArgs me = new MultimediaEventArgs(MultimediaType.Note, 600, 500, null, "");
                //showNote(me);
                attachSubMenu.IsVisible = false;
            }
            else
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                addNewMultimedia = false;

                LeftPage.StopAddingMultimediaOnPage();

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StopAddingMultimediaOnPage();
            }
        }

        private void btnAudioAttach_Clicked(object sender, EventArgs e)
        {
            if (addNewMultimedia == false)
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //task:Darko_1205_AddNote
                //activate move for mouse
                //on click, set possition and start Note,
                //1. wait on SinglePageView mouse click release to set possition for new Note
                //2. call OnAddMultimedia from mouse released
                //3. main page catch event OnAddMultimedia, LeftPage_OnAddMultimedia and show note cotrol to add new note
                //4. note control store new Note into XML and send event for close to main page
                //5. 
                ActualNewCustomMediaType = MultimediaType.Audio;
                LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                addNewMultimedia = true;
                //MultimediaEventArgs me = new MultimediaEventArgs(MultimediaType.Note, 600, 500, null, "");
                //showNote(me);
                attachSubMenu.IsVisible = false;
            }
            else
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.orange.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnFileAttach.Source = ImageSource.FromResource("XBook2.media.load.green.attach_document_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                addNewMultimedia = false;
                LeftPage.StopAddingMultimediaOnPage();

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StopAddingMultimediaOnPage();
            }
        }
        #endregion

        #region LEFT MENU - TOOLS
        private void btnTools_Clicked(object sender, EventArgs e)
        {
            //  setImagesLeftMenu();
            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //    btnTools.Source = ImageSource.FromResource("XBook2.media.load.orange.tools_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //    btnTools.Source = ImageSource.FromResource("XBook2.media.load.green.tools_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            //draw on blackboard
            try
            {
                Xamarin.Forms.Application.Current.MainPage = new Tools(loggedUser, MainPageViewModelInstance.CurrentBook);
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        #endregion

        #endregion

        #region BOTTOM MENU CLICK
        private void btnPrevious_Clicked(object sender, EventArgs e)
        {

            PrevPage();
        }
        private void PrevPage()
        {
            if (MainPageViewModelInstance.SinglePageMode)
            {
                MainPageViewModelInstance.PageNumLeft -= 1;
            }
            else
            {
                if (leftPageFullScreen || rightPageFullScreen)
                    MainPageViewModelInstance.PageNumLeft -= 1;
                else
                    MainPageViewModelInstance.PageNumLeft -= 2;
            }
            MoveToPage();
        }
        private void btnNext_Clicked(object sender, EventArgs e)
        {

            NextPage();
        }
        private void NextPage()
        {
            if (MainPageViewModelInstance.SinglePageMode)
            {
                MainPageViewModelInstance.PageNumLeft += 1;
            }
            else
            {
                if (leftPageFullScreen || rightPageFullScreen)
                {
                    MainPageViewModelInstance.PageNumLeft += 1;
                }
                else
                    MainPageViewModelInstance.PageNumLeft += 2;
            }
            MoveToPage();
        }
        private void btnGotoPage_Clicked(object sender, EventArgs e)
        {
            int iPagenumLeft = 0;
            int.TryParse(txtGoToPage.Text, out iPagenumLeft);
            MainPageViewModelInstance.PageNumLeft = iPagenumLeft;
            if (MainPageViewModelInstance.SinglePageMode == false)
            {
                if (MainPageViewModelInstance.PageNumLeft % 2 == 0)
                {

                }
                else
                {
                    MainPageViewModelInstance.PageNumLeft = MainPageViewModelInstance.PageNumLeft - 1;
                }
            }
            MoveToPage();
        }
        private void ImageButtonClose_Clicked(object sender, EventArgs e)
        {
            try
            {
                //CoreApplication.Exit();
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }

        private async void btnAlbas_Clicked(object sender, EventArgs e)
        {
            await Common.OpenWebPage();
        }



        #endregion

        #region EVENTS
        private void absoluteMain_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.Height - 565 > 0)
                {
                    leftSubmenuGridRow9.Height = this.Height - 565;
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            if (IsSmallScreen == false)
            {
                MainPageViewModelInstance.IsTablet = true;
                LeftMenuBar.IsVisible = true;
            }
            else
            {
                MainPageViewModelInstance.IsTablet = false;
            }

            MainPageViewModelInstance.WaitOnBook = true;
            try
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                if (MainPageViewModelInstance != null)
                {

                    //scrollContent.IsVisible = false;
                    lblBookTitle.Text = MainPageViewModelInstance.CurrentBook.Title;
                    lblBookDesc.Text = MainPageViewModelInstance.CurrentBook.Description;
                    if (MainPageViewModelInstance.CurrentBook.htThumbPages != null)
                    {
                        if (MainPageViewModelInstance.CurrentBook.htThumbPages.Count > 0)
                        {
                            SKBitmap myBitmap = (SKBitmap)MainPageViewModelInstance.CurrentBook.htThumbPages["1"];
                            MainPageViewModelInstance.CurrentBook.SetImageSource(gridLibraryTopMenu1, myBitmap);
                        }
                    }
                    if (this.Height > 0)
                    {
                        MainGridRow2.Height = this.Height - MainPageViewModelInstance.MenuGridSize - MainPageViewModelInstance.MenuGridSize;

                    }


                }
                else
                {

                }
                stopWatch.Stop();
                MainPageViewModelInstance.MainPageLoadTime = stopWatch.Elapsed.Seconds;
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

            MainPageViewModelInstance.WaitOnBook = false;


        }
        //LeftPageTranslationX for landscape to put left page near right page
        //
        private void SetPageMode()
        {
            try
            {
                if (this.Height > 0)
                {
                    MainPageViewModelInstance.BtnSettingsMenuPress = false;
                    MainPageViewModelInstance.PageWidth = this.Width;
                    MainGridRow2.Height = this.Height - MainPageViewModelInstance.MenuGridSize - MainPageViewModelInstance.MenuGridSize;

                    if (this.Height < this.Width)
                    {
                        MainPageViewModelInstance.PageOrientation = Orientation.Landscape;
                    }
                    else
                    {
                        MainPageViewModelInstance.PageOrientation = Orientation.Portrait;
                    }

                    if (MainPageViewModelInstance.LeftPageFullScreen == false && MainPageViewModelInstance.RightPageFullScreen == false)
                    {
                        if (MainPageViewModelInstance.PageOrientation == Orientation.Landscape)
                        {
                            //LANDSCAPE
                            //TWO PAGE MODE
                            MainPageViewModelInstance.LeftPageTranslationX = 140;
                            LeftPage.TranslationX = MainPageViewModelInstance.LeftPageTranslationX;
                            leftPageFullScreen = false;
                            MainPageViewModelInstance.SinglePageMode = false;
                            LeftPage.SinglePageMode = false;
                            SetColumnSpanProperty(LeftPage, 5);

                            rightPageFullScreen = false;
                            RightPage.SinglePageMode = false;
                            MainPageViewModelInstance.RightPageVisible = true;
                            RightPage.IsVisible = true;

                        }
                        else
                        {
                            //PORTRAIT
                            //SinglePageMode
                            MainPageViewModelInstance.LeftPageTranslationX = 0;
                            LeftPage.TranslationX = MainPageViewModelInstance.LeftPageTranslationX;
                            leftPageFullScreen = false;
                            rightPageFullScreen = false;
                            MainPageViewModelInstance.SinglePageMode = true;
                            LeftPage.SinglePageMode = true;
                            SetColumnSpanProperty(LeftPage, 10);
                            MainPageViewModelInstance.RightPageVisible = false;
                            RightPage.IsVisible = false;
                        }
                    }
                    if (MainPageViewModelInstance.LeftPageFullScreen == true)
                    {
                        SetLeftPageFullScreen();
                    }
                    if (MainPageViewModelInstance.RightPageFullScreen == true)
                    {
                        SetRighttPageFullScreen();
                    }


                    double lineHeight = Device.OnPlatform(1.2, 1.2, 1.3);
                    double charWidth = 0.5;

                    MainPageViewModelInstance.SettingsTranslateY = bottomMenuBar.Y - (6 * 60);
                    MainPageViewModelInstance.SettingsTranslateX = btnConfigure.X;
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }

        private void ContentPage_SizeChanged(object sender, EventArgs e)
        {
            if (MainPageViewModelInstance != null)
            {
                SetPageMode();
                MoveToPage();
            }
            else
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), null, "MainPageViewModelInstance is null, check error in construcor!");
            }
        }
        private void txtGoToPage_Focused(object sender, FocusEventArgs e)
        {
            txtGoToPage.Text = "";
        }

        private void txtGoToPage_TextChanged(object sender, TextChangedEventArgs e)
        {
            int test = 0;
            if (e.NewTextValue != null && e.NewTextValue != " - " && e.NewTextValue != "")
            {
                var arr = e.NewTextValue.Split('-');
                foreach (var item in arr)
                {
                    if (int.TryParse(item.Trim(), out test) == false)
                    {
                        txtGoToPage.Text = e.OldTextValue;
                    }
                }
            }
        }

        private void btnBookshelf_Clicked(object sender, EventArgs e)
        {
            try
            {
                //Xamarin.Forms.Application.Current.MainPage = new BookGaleryControl(loggedUser);
                Xamarin.Forms.Application.Current.MainPage = new BookGaleryList(loggedUser, IsSmallScreen);
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void LeftPage_OnPageChanged(object sender, PageMouseMoveEventArgs e)
        {
            //lblStatus.Text = e.PageType.ToString() + " X: " + e.X.ToString("#") + " Y: " + e.Y.ToString("#") + " w: " + e.Width.ToString() + " h: " + e.Height.ToString();

            if (addNewMultimedia)
            {

                if (remeberPageType != e.PageType)
                {
                    if (MainPageViewModelInstance.SinglePageMode == false)
                        RightPage.StopAddingMultimediaOnPage();

                    LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                    LeftPage.LoadPage();
                    if (MainPageViewModelInstance.SinglePageMode == false)
                        RightPage.LoadPage();
                }
                if (e.Y > e.Height)
                {
                    LeftPage.StopAddingMultimediaOnPage();
                }
                else
                {
                    LeftPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);
                }

                if (e.X < 45)
                {
                    LeftPage.StopAddingMultimediaOnPage();
                }



                remeberPageType = e.PageType;
            }


        }

        private void RightPage_OnPageChanged(object sender, PageMouseMoveEventArgs e)
        {
            //lblStatus.Text = e.PageType.ToString() + " X: " + e.X.ToString("#") + " Y: " + e.Y.ToString("#") + " w: " + e.Width.ToString() + " h: " + e.Height.ToString();

            if (addNewMultimedia)
            {
                if (remeberPageType != e.PageType)
                {
                    LeftPage.StopAddingMultimediaOnPage();

                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);

                    LeftPage.LoadPage();

                    RightPage.LoadPage();
                }

                if (e.Y > e.Height)
                {
                    RightPage.StopAddingMultimediaOnPage();
                }
                else
                {
                    RightPage.StartAddingMultimediaOnPage(ActualNewCustomMediaType);
                }

                if (e.X < 15)
                {
                    RightPage.StopAddingMultimediaOnPage();
                }


                if (e.X > e.Width)
                {
                    RightPage.StopAddingMultimediaOnPage();
                }


                remeberPageType = e.PageType;
            }
        }

        private void btnLeftMenu_Clicked(object sender, EventArgs e)
        {
            if (LeftMenuBar.IsVisible)
            {
                LeftMenuBar.IsVisible = false;
                librarySubMenu.IsVisible = false;
                MainPageViewModelInstance.BtnSettingsMenuPress = false;
                languageMenu.IsVisible = false;
            }
            else
            {
                LeftMenuBar.IsVisible = true;


            }
            MainPageViewModelInstance.BtnSettingsMenuPress = false;
            //multimediaPreview.IsVisible = false;
            drawSubMenu.IsVisible = false;
            attachSubMenu.IsVisible = false;
        }

        internal void MoviePositionChanged(float oldMoviePosition, float newMoviePosition)
        {
            throw new NotImplementedException();
        }

        private void LeftPage_OnMenuReset(object sender, ProgressEventArgs e)
        {
            if (LeftMenuBar.IsVisible)
            {
                LeftMenuBar.IsVisible = false;
                librarySubMenu.IsVisible = false;
                MainPageViewModelInstance.BtnSettingsMenuPress = false;
                languageMenu.IsVisible = false;
                drawSubMenu.IsVisible = false;
                attachSubMenu.IsVisible = false;
            }
        }

        private void SwipeNext_Invoked(object sender, EventArgs e)
        {
            NextPage();
        }

        private void SwipePrev_Invoked(object sender, EventArgs e)
        {
            PrevPage();
        }

        private void LeftPage_OnSwipe(object sender, ProgressEventArgs e)
        {
            if (e.Text == "Next")
            {
                NextPage();
            }
            else
            {
                PrevPage();
            }
        }
        private void RightPage_OnSwipe(object sender, ProgressEventArgs e)
        {
            if (e.Text == "Next")
            {
                NextPage();
            }
            else
            {
                PrevPage();
            }
        }
        #endregion

        #region METHODS


        private void setLabelsVisible(bool state)
        {

            try
            {
                #region leftMenu
                lblBookSync.IsVisible = state;
                lblCursor.IsVisible = state;
                lblLibrary.IsVisible = state;
                lblSelect.IsVisible = state;
                lblUnSelect.IsVisible = state;
                lblPen.IsVisible = state;
                lblNote.IsVisible = state;
                lblAttach.IsVisible = state;
                lblTools.IsVisible = state;

                lblBookSync.IsVisible = state;
                #endregion

                #region BottomMenu
                lblGoToPage.IsVisible = state;
                lblBookmarks.IsVisible = state;
                // lblClose.IsVisible = state;
                lblAlbas.IsVisible = state;
                lblBookshelf.IsVisible = state;
                lblConfigure.IsVisible = state;
                //lblFullScreen.IsVisible = state;
                #endregion
                lblHelp.IsVisible = state;
                lblLanguage.IsVisible = state;
                lblBackground.IsVisible = state;
                lblIconSize.IsVisible = state;
                lblTabletMode.IsVisible = state;
                lblDefaultMode.IsVisible = state;
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        private void setLabelText()
        {

            try
            {
                #region leftMenu
                lblCursor.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_cursor"); // "Cursor";
                lblLibrary.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_library"); // "Library";
                lblSelect.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_select"); //"Select";
                lblUnSelect.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_unselect"); //"Unselect";
                lblPen.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_pen"); //"Pen";
                lblNote.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_notes"); //"Note";
                lblAttach.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_attach"); //"Attach";
                lblTools.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_tools"); //"Tolls";
                lblBookSync.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_synchronize"); //"Synhronize";

                #endregion

                #region BottomMenu
                lblGoToPage.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_go"); //"Go";
                lblBookmarks.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_bookmarks"); //"Bookmarks";
                //lblClose.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_exit"); //"Exit";
                lblAlbas.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_portal"); //"Portal";
                lblBookshelf.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_bookshelf"); //"Shelf";
                lblConfigure.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_config"); //"Settings";
                lblFullScreen.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_minimize"); //"Maximize";
                #endregion

                lblHelp.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_bookshelf_help");            //"Help";
                lblLanguage.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_config_language");       //"Language";
                lblBackground.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_config_background");   //"Background";
                lblIconSize.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_config_interface");      //"Icon Size";
                lblTabletMode.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_config_tablet");       //"Tablet Mode";
                lblDefaultMode.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_config_explanations");//"Default Mode";


                #region LibraryMenu
                lblContent.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_library_content"); //"Content";
                lblR1Materials.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_library_material"); //"Materials";
                lblR1TeacherBook.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_library_book_teacher"); //"Teacher's book";
                lblR1SearchInBook.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_library_search"); //"Search";

                lblR2Multimedia.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_material_multimedia"); //"Multimedia";
                lblR2Notes.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_material_note"); //"Notes";
                lblR2Bookmarks.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_material_bookmark"); //"Bookmarks";
                lblR2Back.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_material_back"); //"Back";
                #endregion
                lblFileAttach.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_attach_document");     //"File";
                lblLinkAttach.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_attach_link");         //"Link";
                lblYouTubeAttach.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_attach_youtube");   //"Youtube";
                lblPhotoAttach.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_attach_photo");       //"Photo";
                lblAudioAttach.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_attach_audio");       //"Audio";
                lblVideoAttach.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_attach_video");       //"Video";

                //Multimedia filter
                labelQuiz.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "multimedia_filter_quiz");       //"Video";
                labelAudio.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "multimedia_filter_audio");       //"Video";
                labelYoutube.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "multimedia_filter_youtube");       //"Video";
                labelVideo.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "multimedia_filter_video");       //"Video";
                labelInformation.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "multimedia_filter_information");       //"Video";
                labelGalery.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "multimedia_filter_gallery");       //"Video";
                labelLink.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "multimedia_filter_link");       //"Video";
                labelSelectAll.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "multimedia_filter_select_all");       //"Video";

                //Multimedia text color
                labelQuiz.TextColor = Color.White;
                labelAudio.TextColor = Color.White;
                labelYoutube.TextColor = Color.White;
                labelVideo.TextColor = Color.White;
                labelInformation.TextColor = Color.White;
                labelGalery.TextColor = Color.White;
                labelLink.TextColor = Color.White;
                labelSelectAll.TextColor = Color.White;

                cbQuiz.Color = Color.Gray;
                cbAudio.Color = Color.Gray;
                cbYoutube.Color = Color.Gray;
                cbVideo.Color = Color.Gray;
                cbInformation.Color = Color.Gray;
                cbGalery.Color = Color.Gray;
                cbLink.Color = Color.Gray;
                cbSelectAll.Color = Color.Gray;

                //cbQuiz.Color = Color.FromRgb(16, 110, 127);
                //cbAudio.Color = Color.FromRgb(16, 110, 127);
                //cbYoutube.Color = Color.FromRgb(16, 110, 127);
                //cbVideo.Color = Color.FromRgb(16, 110, 127);
                //cbInformation.Color = Color.FromRgb(16, 110, 127);
                //cbGalery.Color = Color.FromRgb(16, 110, 127);
                //cbLink.Color = Color.FromRgb(16, 110, 127);
                //cbSelectAll.Color = Color.FromRgb(16, 110, 127);


            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }

        public void MoveToPage(string strFindAndLocation = "")
        {
            if (this.Height > 0)
            {
                //SetPageMode();
            }
            //leftPageDoubleClickReset();

            //if (MainPageViewModelInstance.SinglePageMode == false)
            //    rightPageDoubleClickReset();

            if (MainPageViewModelInstance.PageNumLeft < -2)
            {
                MainPageViewModelInstance.PageNumLeft = -2;
            }
            if (MainPageViewModelInstance.CurrentBook.BookPageMode == 2)
            {
                if (MainPageViewModelInstance.PageNumLeft + 2 > MainPageViewModelInstance.CurrentBook.BookPages)
                {
                    MainPageViewModelInstance.PageNumLeft = MainPageViewModelInstance.CurrentBook.BookPages - 2;
                }
            }

            if (leftPageFullScreen == false && rightPageFullScreen == false)
            {
                if (MainPageViewModelInstance.SinglePageMode == false)
                {
                    if (MainPageViewModelInstance.PageNumLeft % 2 == 0)
                    {

                    }
                    else
                    {
                        MainPageViewModelInstance.PageNumLeft = MainPageViewModelInstance.PageNumLeft + 1;
                    }
                }
            }


            MainPageViewModelInstance.PageNumRight = MainPageViewModelInstance.PageNumLeft + 1;




            LeftPage.PageNumber = MainPageViewModelInstance.PageNumLeft;

            if (MainPageViewModelInstance.CurrentBook.HTPageAnnos.Count > 0)
            {
                currentPageAnnoLeft = (PageAnno)MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.PageNumLeft];
                if (MainPageViewModelInstance.SinglePageMode == false)
                    currentPageAnnoRight = (PageAnno)MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.PageNumRight];
            }
            if (MainPageViewModelInstance.PageNumLeft >= 0)
            {
                if (MainPageViewModelInstance.SinglePageMode == false)
                {
                    if (rightPageFullScreen == false & leftPageFullScreen == false)
                    {
                        txtGoToPage.Text = MainPageViewModelInstance.PageNumLeft.ToString() + " - " + MainPageViewModelInstance.PageNumRight.ToString();

                    }
                    else
                    {
                        if (rightPageFullScreen)
                            txtGoToPage.Text = MainPageViewModelInstance.PageNumRight.ToString();

                        if (leftPageFullScreen)
                            txtGoToPage.Text = MainPageViewModelInstance.PageNumLeft.ToString();

                    }

                }
            }
            else
            {
                txtGoToPage.Text = " - ";
            }


            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.PageNumber = MainPageViewModelInstance.PageNumRight;

            LeftPage.LoadPage(strFindAndLocation);

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.LoadPage(strFindAndLocation);


            bool bookmarked = MainPageViewModelInstance.CurrentBook.GetBookmarkStatusFromXML(MainPageViewModelInstance.PageNumLeft);
            MainPageViewModelInstance.SetBookmark(bookmarked);

            #region Remember page, will be used when program start
            MainPageViewModelInstance.SetUserInfoToXML();
            #endregion

            #region close menu
            LeftMenuBar.IsVisible = false;
            languageMenu.IsVisible = false;
            librarySubMenu.IsVisible = false;
            #endregion


        }

        #endregion

        #region Remmember Page


        #region Download demo book
        private void btnGetBook_Clicked(object sender, EventArgs e)
        {
            //lblBookName.Text = "downloading the book...";
            logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "Book " + MainPageViewModelInstance.BookID + " start downloading ");
            getBookFromUrl();


        }
        public void getBookFromUrl()
        {
            try
            {
                //Common.LocalPath = @"/book/OLD/";
                //Download dwn = new Download(Common.LocalPath);
                //dwn.OnFinishDownloadZipEvent += Dwn_finishDownloadEvent;
                //dwn.GetZipFile("http://85.215.209.4/158.zip");
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }

        }
        private void Dwn_finishDownloadEvent(object sender, EventArgs e)
        {
            logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "Book " + MainPageViewModelInstance.BookID + " is downloaded ");
            MainPageViewModelInstance.CurrentBook = new Book(user, MainPageViewModelInstance.BookBasePath, MainPageViewModelInstance.BookID, MainPageViewModelInstance.BookEncoding);
            //lblBookName.Text = currentBook.Title;
            // waitBook.IsVisible = false;

            if (MainPageViewModelInstance.CurrentBook.Status == true)
            {
                LeftPage.BmpAudio = bmpAudio;
                LeftPage.BmpVideo = bmpVideo;
                LeftPage.BmpOpenWindow = bmpOpenWindow;
                LeftPage.BmpPhotoSlide = bmpPhotoSlide;
                LeftPage.BookPath = MainPageViewModelInstance.BookBasePath;
                LeftPage.BookID = MainPageViewModelInstance.BookID;
                LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;
                LeftPage.Book = MainPageViewModelInstance.BookFiles;

                if (MainPageViewModelInstance.SinglePageMode == false)
                {
                    RightPage.BmpAudio = bmpAudio;
                    RightPage.BmpVideo = bmpVideo;
                    RightPage.BmpOpenWindow = bmpOpenWindow;
                    RightPage.BmpPhotoSlide = bmpPhotoSlide;
                    RightPage.BookPath = MainPageViewModelInstance.BookBasePath;
                    RightPage.BookID = MainPageViewModelInstance.BookID;
                    RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;
                    RightPage.Book = MainPageViewModelInstance.BookFiles;
                }

                MainPageViewModelInstance.GetUserInfoFromXML();
                MoveToPage();
                lblStatus.Text = MainPageViewModelInstance.CurrentBook.Description;
            }



        }
        #endregion

        #region bookmark page
        private void btnBookmark_Clicked(object sender, EventArgs e)
        {
            bool bookmarked = MainPageViewModelInstance.CurrentBook.SetBookmarkToXML(MainPageViewModelInstance.PageNumLeft);
            MainPageViewModelInstance.SetBookmark(bookmarked);
            if (bookmarked)
            {
                
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnBookmark.Source = ImageSource.FromResource("XBook2.media.load.orange.bookmarks_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnBookmark.Source = ImageSource.FromResource("XBook2.media.load.green.bookmarks_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }
            else
            {
                //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                //    btnBookmark.Source = ImageSource.FromResource("XBook2.media.load.orange.bookmarks_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                //    btnBookmark.Source = ImageSource.FromResource("XBook2.media.load.green.bookmarks_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }
            //check is this page there,
            //if not add
            //  setImagesLeftMenu();

        }
        #endregion

        #region Play Multimedia / Add / Delete
        private void CloseNotePreview()
        {
            //for (int i = multimediaPreview.Children.Count - 1; i >= 0; i--)
            //{
            //    if (multimediaPreview.Children[i] is NoteControl)
            //    {
            //        multimediaPreview.Children.RemoveAt(i);
            //    }
            //}
            //multimediaPreview.IsVisible = false;
            //if (addNewMultimedia)
            {

                LeftPage.StopAddingMultimediaOnPage();

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.StopAddingMultimediaOnPage();

                addNewMultimedia = false;

                if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                {
                    btnNote.Source = ImageSource.FromResource("XBook2.media.load.orange.note_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
                if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                {
                    btnNote.Source = ImageSource.FromResource("XBook2.media.load.green.note_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
            }
        }
        //add new note
        private void NotePreview_closeEvent(object sender, EventArgs e)
        {
            CloseNotePreview();
        }
        private void VideoPreview_closeEvent(object sender, EventArgs e)
        {
            //multimediaPreview.Children.Clear();

            //multimediaPreview.IsVisible = false;
            //BackOtherOnMultimediaFinished();
        }
        private void PhotoViewer_closeEvent(object sender, EventArgs e)
        {
            try
            {
                ////multimediaPreview.Children.Clear();
                //if( multimediaPreview.Children.Count==1)
                //{
                //    multimediaPreview.Children.RemoveAt(0);
                //}

                //multimediaPreview.IsVisible = false;

                //bottomMenuBar.IsVisible = true;

            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                throw;
            }
        }

        private void RightPage_OnMultimedia(object sender, MultimediaEventArgs e)
        {
            ClearMultimediaPreview();

            PlayMultimedia(e);
        }
        private void ClearMultimediaPreview()
        {
            //for (int i = multimediaPreview.Children.Count - 1; i >= 0; i--)
            //{

            //    multimediaPreview.Children.RemoveAt(i);

            //}
        }

        private void CloseOtherToStartMultimedia()
        {
            LeftMenuBar.IsVisible = false;
            languageMenu.IsVisible = false;
            librarySubMenu.IsVisible = false;
            bottomMenuBar.IsVisible = false;
        }
        private void BackOtherOnMultimediaFinished()
        {
            //LeftMenuBar.IsVisible = false;
            //languageMenu.IsVisible = false;
            //librarySubMenu.IsVisible = false;
            bottomMenuBar.IsVisible = true;
        }
        private async void PlayMultimedia(MultimediaEventArgs e)
        {
            CloseOtherToStartMultimedia();
            switch (e.MultimediaType)
            {
                case MultimediaType.PhotoGalery:
                    photoViewer = new ImageControl(loggedUser, e.ImageWidth, e.ImageHeight, e.FindAnno.Hint.Text, e.FindAnno, MainPageViewModelInstance.CurrentBook, e.FindAnno.Cutom,
                      leftPageFullScreen, rightPageFullScreen, MainPageViewModelInstance.PageNumLeft);
                    photoViewer.closeEvent += PhotoViewer_closeEvent;
                    photoViewer.OnRemoveMultimediaHandler += NotePreview_OnRemoveMultimedia;
                    foreach (Photo photoItem in e.FindAnno.Action.Photos)
                    {
                        string autidoUrl = photoItem.Url.Replace("./files/", "");
                        autidoUrl = autidoUrl.Replace("/", Common.GetPathSeparator());
                        string resourceIDP = MainPageViewModelInstance.BookFiles + autidoUrl;
                        Photo newPhotoItem = new Photo();
                        if (e.FindAnno.Cutom == true)
                        {
                            photoViewer.BookEncoding = false;
                        }
                        newPhotoItem.Title = photoItem.Title;
                        newPhotoItem.Description = photoItem.Description;
                        newPhotoItem.Url = resourceIDP;
                        photoViewer.AddImageToList(newPhotoItem);
                    }
                    photoViewer.ShowFirst();
                    Xamarin.Forms.Application.Current.MainPage = photoViewer;
                    // multimediaPreview.Children.Add(photoViewer);
                    //multimediaPreview.IsVisible = true;
                    //multimediaPreview.HeightRequest = 800;
                    break;
                case MultimediaType.Audio:
                    if (e.FindAnno != null)
                    {
                        if (e.FindAnno.Action != null)
                        {
                            if (e.FindAnno.Action.AudioURL != null)
                            {
                                string auduoPath = e.FindAnno.Action.AudioURL;

                                if (e.FindAnno.Action.AudioURL.StartsWith("./files/"))
                                {
                                    auduoPath = e.FindAnno.Action.AudioURL.Substring(8, e.FindAnno.Action.AudioURL.Length - 8);
                                }
                                string resourceID = MainPageViewModelInstance.BookFiles + auduoPath;
                                audioPlayer = new AudioControl(loggedUser, loggedUser.currentColorTempalte, resourceID, e.FindAnno.Hint.Text, e.FindAnno, e.FindAnno.Cutom, MainPageViewModelInstance.CurrentBook,
                                     leftPageFullScreen, rightPageFullScreen, MainPageViewModelInstance.PageNumLeft);
                                audioPlayer.closeEvent += VideoPreview_closeEvent;
                                audioPlayer.OnRemoveMultimediaHandler += NotePreview_OnRemoveMultimedia;
                                //audioPlayer.WidthRequest = e.ImageWidth;
                                //multimediaPreview.Children.Add(audioPlayer);
                                //multimediaPreview.IsVisible = true;
                                Xamarin.Forms.Application.Current.MainPage = audioPlayer;
                            }
                        }
                    }

                    break;
                case MultimediaType.Video:
                    if (e.FindAnno != null)
                    {
                        if (e.FindAnno.Action != null)
                        {
                            string videoUrl = e.FindAnno.Action.ResourceContent;
                            if (videoUrl != null)
                            {
                                string leadToRemove = "./files/";
                                if (videoUrl.StartsWith(leadToRemove))
                                    videoUrl = videoUrl.Substring(leadToRemove.Length, videoUrl.Length - leadToRemove.Length);
                                string resourceIDA =  MainPageViewModelInstance.BookFiles + videoUrl;
                                videoPlayer = new VideoControl(loggedUser, loggedUser.currentColorTempalte, resourceIDA, e.FindAnno.Hint.Text, e.FindAnno, e.FindAnno.Cutom, MainPageViewModelInstance.CurrentBook,
                                    leftPageFullScreen, rightPageFullScreen, MainPageViewModelInstance.PageNumLeft);
                                if (e.FindAnno.Cutom == true)
                                {
                                    //videoPreview.BookEncoding = false;
                                }
                                videoPlayer.closeEvent += VideoPreview_closeEvent;
                                videoPlayer.WidthRequest = e.ImageWidth;
                                videoPlayer.OnRemoveMultimediaHandler += NotePreview_OnRemoveMultimedia;
                                //multimediaPreview.Children.Add(videoPlayer);
                                //multimediaPreview.IsVisible = true;
                                Xamarin.Forms.Application.Current.MainPage = videoPlayer;
                            }
                        }
                    }
                    break;
                case MultimediaType.Note:
                    showNote(e);
                    break;
                case MultimediaType.File:
                    showFile(e);
                    break;
                case MultimediaType.Link:
                    await Common.OpenWebPage(e.FindAnno.Action.Url);
                    break;
                default:
                    logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "MultimediaType not a list: " + e.MultimediaType);
                    break;
            }
            //multimediaPreview.IsVisible = true;
        }



        private void showNote(MultimediaEventArgs e)
        {
            //multimediaPreview.Children.Clear();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.StopAddingMultimediaOnPage();

            LeftPage.StopAddingMultimediaOnPage();
            addNewMultimedia = false;

            notePreview = new NoteControl(MainPageViewModelInstance.CurrentBook, loggedUser, e.ImageWidth, e.ImageHeight, e.FindAnno, e.AddNew);

            //todo: 1. add to page collection of multimedia
            //todo: 2. this have to be done on load from xml also!
            if (MainPageViewModelInstance.CurrentBook.HTPageAnnos.Count > 0)
            {
                PageAnno currentPageAnno = (PageAnno)MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage];
                currentPageAnno.Annos.Add(e.FindAnno);
                MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage] = currentPageAnno;
                LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;
                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            }



            notePreview.closeEvent += NotePreview_closeEvent;
            notePreview.OnRemoveMultimediaHandler += NotePreview_OnRemoveMultimedia;
            notePreview.WidthRequest = e.ImageWidth;
            //multimediaPreview.Children.Add(notePreview);
            //multimediaPreview.IsVisible = true;
            Xamarin.Forms.Application.Current.MainPage = notePreview;
        }
        private void showFile(MultimediaEventArgs e)
        {
            //multimediaPreview.Children.Clear();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.StopAddingMultimediaOnPage();

            LeftPage.StopAddingMultimediaOnPage();
            addNewMultimedia = false;

            notePreview = new NoteControl(MainPageViewModelInstance.CurrentBook, loggedUser, e.ImageWidth, e.ImageHeight, e.FindAnno, e.AddNew);

            //todo: 1. add to page collection of multimedia
            //todo: 2. this have to be done on load from xml also!
            if (MainPageViewModelInstance.CurrentBook.HTPageAnnos.Count > 0)
            {
                PageAnno currentPageAnno = (PageAnno)MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage];
                currentPageAnno.Annos.Add(e.FindAnno);
                MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage] = currentPageAnno;
                LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            }



            notePreview.closeEvent += NotePreview_closeEvent;
            notePreview.OnRemoveMultimediaHandler += NotePreview_OnRemoveMultimedia;
            notePreview.WidthRequest = e.ImageWidth;
            //multimediaPreview.Children.Add(notePreview);
            //multimediaPreview.IsVisible = true;
            Application.Current.MainPage = notePreview;
        }
        private void addCustomFile(MultimediaEventArgs e)
        {
            logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "addCustomFile start ");
            //multimediaPreview.Children.Clear();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.StopAddingMultimediaOnPage();

            LeftPage.StopAddingMultimediaOnPage();
            addNewMultimedia = false;

            filePreview = new FileControl(MainPageViewModelInstance.CurrentBook, loggedUser, e.ImageWidth, e.ImageHeight, e.FindAnno, e.AddNew);


            //todo: 1. add to page collection of multimedia
            //todo: 2. this have to be done on load from xml also!
            if (MainPageViewModelInstance.CurrentBook.HTPageAnnos.Count > 0)
            {
                PageAnno currentPageAnno = (PageAnno)MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage];
                currentPageAnno.Annos.Add(e.FindAnno);
                MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage] = currentPageAnno;
                LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            }



            filePreview.closeEvent += FilePreview_closeEvent;
            filePreview.OnRemoveMultimediaHandler += FilePreview_OnRemoveMultimediaHandler;
            filePreview.WidthRequest = e.ImageWidth;
            //multimediaPreview.Children.Add(filePreview);
            //multimediaPreview.IsVisible = true;
            Application.Current.MainPage = filePreview;
        }
        private void showLink(MultimediaEventArgs e)
        {
            //multimediaPreview.Children.Clear();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.StopAddingMultimediaOnPage();

            LeftPage.StopAddingMultimediaOnPage();
            addNewMultimedia = false;

            linkPreview = new LinkControl(MainPageViewModelInstance.CurrentBook, loggedUser, e.ImageWidth, e.ImageHeight, e.FindAnno, e.AddNew);

            //todo: 1. add to page collection of multimedia
            //todo: 2. this have to be done on load from xml also!
            if (MainPageViewModelInstance.CurrentBook.HTPageAnnos.Count > 0)
            {
                PageAnno currentPageAnno = (PageAnno)MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage];
                currentPageAnno.Annos.Add(e.FindAnno);
                MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage] = currentPageAnno;
                LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            }



            linkPreview.closeEvent += FilePreview_closeEvent;
            linkPreview.OnRemoveMultimediaHandler += FilePreview_OnRemoveMultimediaHandler;
            linkPreview.WidthRequest = e.ImageWidth;
            //multimediaPreview.Children.Add(linkPreview);
            //multimediaPreview.IsVisible = true;
            Xamarin.Forms.Application.Current.MainPage = linkPreview;
        }
        private void showYouTube(MultimediaEventArgs e)
        {
            //multimediaPreview.Children.Clear();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.StopAddingMultimediaOnPage();

            LeftPage.StopAddingMultimediaOnPage();
            addNewMultimedia = false;

            youTubePreview = new YouTubeControl(MainPageViewModelInstance.CurrentBook, loggedUser, e.ImageWidth, e.ImageHeight, e.FindAnno, e.AddNew);

            //todo: 1. add to page collection of multimedia
            //todo: 2. this have to be done on load from xml also!
            if (MainPageViewModelInstance.CurrentBook.HTPageAnnos.Count > 0)
            {
                PageAnno currentPageAnno = (PageAnno)MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage];
                currentPageAnno.Annos.Add(e.FindAnno);
                MainPageViewModelInstance.CurrentBook.HTPageAnnos[MainPageViewModelInstance.CurrentBook.CurrentPage] = currentPageAnno;
                LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

                if (MainPageViewModelInstance.SinglePageMode == false)
                    RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            }



            youTubePreview.closeEvent += FilePreview_closeEvent;
            youTubePreview.OnRemoveMultimediaHandler += FilePreview_OnRemoveMultimediaHandler;
            youTubePreview.WidthRequest = e.ImageWidth;
            //multimediaPreview.Children.Add(youTubePreview);
            //multimediaPreview.IsVisible = true;
            Xamarin.Forms.Application.Current.MainPage = youTubePreview;
        }


        private void FilePreview_OnRemoveMultimediaHandler(object sender, MultimediaRemoveEventArgs e)
        {
            RemoveCustomMultimedia(e);
        }
        private async void RemoveCustomMultimedia(MultimediaRemoveEventArgs e)
        {
            logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "RemoveCustomMultimedia start ");
            if (e.FindAnno.Cutom)
            {
                string removeID = e.FindAnno.AnnoId;

                string path = MultimediaPath.GetCustomMultimediaPath(e.FindAnno.MultimediaTypeToAdd);
                await MainPageViewModelInstance.CurrentBook.RemoveNote(e.FindAnno, path);
            }

            LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            CloseNotePreview();
            LeftPage.LoadPage();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.LoadPage();

            //if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            //    btnNote.Source = ImageSource.FromResource("XBook2.media.load.orange.notes_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            //if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            //    btnNote.Source = ImageSource.FromResource("XBook2.media.load.green.notes_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
        }
        private void FilePreview_closeEvent(object sender, EventArgs e)
        {
            CloseNotePreview();
        }

        private async void NotePreview_OnRemoveMultimedia(object sender, MultimediaRemoveEventArgs e)
        {
            logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "Delete note start ");
            //delete note
            if (e.FindAnno.Cutom)
            {
                string removeID = e.FindAnno.AnnoId;
                string path = MultimediaPath.GetCustomMultimediaPath(e.FindAnno.MultimediaTypeToAdd);
                await MainPageViewModelInstance.CurrentBook.RemoveNote(e.FindAnno, path);
            }

            LeftPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.CurrentBook = MainPageViewModelInstance.CurrentBook;

            CloseNotePreview();
            LeftPage.LoadPage();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.LoadPage();

            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                btnNote.Source = ImageSource.FromResource("XBook2.media.load.orange.notes_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                btnNote.Source = ImageSource.FromResource("XBook2.media.load.green.notes_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
        }


        private void LeftPage_OnAddMultimedia(object sender, MultimediaEventArgs e)
        {

            AddMultimedia(e);
        }

        private void RightPage_OnAddMultimedia(object sender, MultimediaEventArgs e)
        {
            AddMultimedia(e);
        }
        private void AddMultimedia(MultimediaEventArgs e)
        {
            switch (actualNewCustomMediaType)
            {
                case MultimediaType.Note:
                    showNote(e);
                    break;
                case MultimediaType.File:
                    addCustomFile(e);
                    break;
                case MultimediaType.Link:
                    showLink(e);
                    break;
                case MultimediaType.YouTube:
                    showYouTube(e);
                    break;
                case MultimediaType.Video:
                    addCustomFile(e);
                    break;
                case MultimediaType.Audio:
                    addCustomFile(e);
                    break;
                case MultimediaType.PhotoGalery:
                    addCustomFile(e);
                    break;
                default:
                    logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "AddMultimedia actualNewCustomMediaType NOT in a list: " + actualNewCustomMediaType);
                    break;
            }
        }
        #endregion

        #region Events From Page Click
        private void RightPage_OnSearchTextFind(object sender, ProgressEventArgs e)
        {
            lblStatus.Text = e.Text;
        }

        private void RightPage_OnLogText(object sender, ProgressEventArgs e)
        {
            //double w = absoluteMain.Width;
            //double h = absoluteMain.Height;
            //lblStatus.Text = e.Text + " w: " + w.ToString("#") + " h: " + h.ToString("#") + "           ";
            lblStatus.Text = e.Text;

            //logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, e.Text);


            #region hide dynamic menu
            //librarySubMenu.IsVisible = false;
            //librarySubMenu.IsVisible = false;
            #endregion
        }

        private void LeftPage_OnSearchTextFind(object sender, ProgressEventArgs e)
        {

        }

        private void LeftPage_OnLogText(object sender, ProgressEventArgs e)
        {

        }
        #endregion

        #region Library Sub Menu

        #region FIRST 1 ROW
        private void resetLbraryContent1Row()
        {
            //gridMuldimedia.IsVisible = false;
            //scrollContent.IsVisible = false;
            scrollBookmark.IsVisible = false;
            //scrollMultimedia.IsVisible = false;
            scrollFinds.IsVisible = false;
            if (btnDefaultMode.Source == null)
            {
                if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                {
                    //btnContent.Source = ImageSource.FromResource("XBook2.media.load.orange.library_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    //btnR1Materials.Source = ImageSource.FromResource("XBook2.media.load.orange.material_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    //btnR1TeacherBook.Source = ImageSource.FromResource("XBook2.media.load.orange.teachermaterial_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    //btnR1SearchInBook.Source = ImageSource.FromResource("XBook2.media.load.orange.search_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }

                if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                {
                    //btnContent.Source = ImageSource.FromResource("XBook2.media.load.green.library_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    //btnR1Materials.Source = ImageSource.FromResource("XBook2.media.load.green.material_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    //btnR1TeacherBook.Source = ImageSource.FromResource("XBook2.media.load.green.teachermaterial_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    //btnR1SearchInBook.Source = ImageSource.FromResource("XBook2.media.load.green.search_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
            }

            lblR1Materials.TextColor = Color.White;
            lblR1TeacherBook.TextColor = Color.White;
            lblR1SearchInBook.TextColor = Color.White;
            showLbraryContent2Row(false);
        }

        #region Library 1.1 CONTENT
        bool btnContentSelected = false;


        private void SmallPageButton_OnBookClick(object sender, BookThumbEventArgs e)
        {
            if (e.PageId > 0)
            {
                MainPageViewModelInstance.PageNumLeft = e.PageId;
                if (MainPageViewModelInstance.SinglePageMode == false)
                {
                    if (MainPageViewModelInstance.PageNumLeft % 2 == 0)
                    {

                    }
                    else
                    {
                        MainPageViewModelInstance.PageNumLeft = MainPageViewModelInstance.PageNumLeft - 1;
                    }
                }

                MoveToPage();
            }
        }
        #endregion

        #region Library 1.2 MATERIALS
        private void btnMaterials_Clicked(object sender, EventArgs e)
        {
            resetLbraryContent1Row();
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                // btnR1Materials.Source = ImageSource.FromResource("XBook2.media.load.orange.material_over.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR1Materials.TextColor = Color.Orange;
            }

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                // btnR1Materials.Source = ImageSource.FromResource("XBook2.media.load.green.material_over.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR1Materials.TextColor = Color.Green;
            }
            showLbraryContent2Row(true);
        }

        #endregion

        #region Library 1.3 TEACHER BOOK
        private void btnTeacherBook_Clicked(object sender, EventArgs e)
        {
            resetLbraryContent1Row();
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                //btnR1TeacherBook.Source = ImageSource.FromResource("XBook2.media.load.orange.teachermaterial_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR1TeacherBook.TextColor = Color.Orange;
            }

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                //btnR1TeacherBook.Source = ImageSource.FromResource("XBook2.media.load.green.teachermaterial_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR1TeacherBook.TextColor = Color.Green;
            }
        }

        #endregion

        #region Library 1.4 SEARCH
        private void btnSearchInBook_Clicked(object sender, EventArgs e)
        {
            resetLbraryContent1Row();
            scrollFinds.IsVisible = true;
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                // btnR1SearchInBook.Source = ImageSource.FromResource("XBook2.media.load.orange.search_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR1SearchInBook.TextColor = Color.Orange;
            }

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                // btnR1SearchInBook.Source = ImageSource.FromResource("XBook2.media.load.green.search_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR1SearchInBook.TextColor = Color.Green;
            }

        }
        private void btnSearch_Clicked(object sender, EventArgs e)
        {
            FillSearchList();
        }
        private void btnClearSearch_Clicked(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            stackLibFinds.Children.Clear();
            MoveToPage();
        }
        private void FillSearchList()
        {
            try
            {

                List<SearchListItem> FindItemsInBoo;


                FindItemsInBoo = MainPageViewModelInstance.CurrentBook.getSearchFromBook(txtSearch.Text);

                stackLibFinds.Children.Clear();
                stackLibFinds.IsVisible = true;
                int counter = 1;
                foreach (SearchListItem item in FindItemsInBoo)
                {

                    if (item.PageID > 0)
                    {
                        int realPageId = item.PageID + 2;
                        SearchItem searchOnBookItem = new SearchItem();

                        // SKBitmap myBitmap = (SKBitmap)currentBook.htThumbPages[realPageId.ToString()];
                        //smallPageButton.SetImage(myBitmap);
                        // smallPageButton.SetTitle(Title);

                        searchOnBookItem.SetPageID(item.PageID.ToString());
                        searchOnBookItem.SetTitle(item.SearchItem);
                        searchOnBookItem.SetLocation(item.strLocation);

                        //smallPageButton.SetAnno(item.multimediaAnno);
                        searchOnBookItem.OnSearchItemNavClick += SearchOnBookItem_OnSearchItemNavClick;

                        //Label label = new Label();
                        //label.Text = counter.ToString();

                        stackLibFinds.Children.Add(searchOnBookItem);
                        //counter++;  
                    }
                }

            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void SearchOnBookItem_OnSearchItemNavClick(object sender, SearchEventArgs e)
        {
            MainPageViewModelInstance.PageNumLeft = e.PageNum;
            string strLocation = e.SearchTextLocation;
            if (MainPageViewModelInstance.SinglePageMode == false)
            {
                if (MainPageViewModelInstance.PageNumLeft % 2 == 0)
                {

                }
                else
                {
                    MainPageViewModelInstance.PageNumLeft = MainPageViewModelInstance.PageNumLeft - 1;
                }
            }
            MoveToPage(strLocation);
        }




        #endregion

        #endregion

        #region SECOND 2 ROW
        private void resetLbraryContent2Row()
        {
            //scrollContent.IsVisible = false;
            scrollBookmark.IsVisible = false;
            //scrollMultimedia.IsVisible = false;
            //stackLibContentMenu.IsVisible = false;
            // gridMuldimedia.IsVisible = false;

            scrollNotes.IsVisible = false;
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                //btnR2Multimedia.Source = ImageSource.FromResource("XBook2.media.load.orange.multimedia_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                // btnR2Notes.Source = ImageSource.FromResource("XBook2.media.load.orange.note_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                //btnR2Bookmarks.Source = ImageSource.FromResource("XBook2.media.load.orange.bookmark_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                // btnR2Back.Source = ImageSource.FromResource("XBook2.media.load.orange.back_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                //btnR2Multimedia.Source = ImageSource.FromResource("XBook2.media.load.green.multimedia_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                //btnR2Notes.Source = ImageSource.FromResource("XBook2.media.load.green.note_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                //btnR2Bookmarks.Source = ImageSource.FromResource("XBook2.media.load.green.bookmark_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                //btnR2Back.Source = ImageSource.FromResource("XBook2.media.load.green.back_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }
            lblR2Multimedia.TextColor = Color.White;
            lblR2Notes.TextColor = Color.White;
            lblR2Bookmarks.TextColor = Color.White;
            lblR2Back.TextColor = Color.White;
        }
        private void showLbraryContent2Row(bool state)
        {
            btnR2Multimedia.IsVisible = state;
            btnR2Notes.IsVisible = state;
            btnR2Bookmarks.IsVisible = state;
            btnR2Back.IsVisible = state;

            lblR2Back.IsVisible = state;
            lblR2Bookmarks.IsVisible = state;
            lblR2Multimedia.IsVisible = state;
            lblR2Notes.IsVisible = state;
        }
        #endregion

        #region Library 2.1 MULTIMEDIA
        private void btnMultimedia_Clicked(object sender, EventArgs e)
        {

            FillMultimediaList();
        }
        private void FillMultimediaList()
        {
            resetLbraryContent2Row();
            //imageFilter.IsVisible = true;
            //imageFilter.BackgroundColor = Color.White;
            //cbMultimadiaFilter.IsVisible = true;
            lblFilter.IsVisible = true;

            scrollNotes.IsVisible = false;
            //scrollMultimedia.IsVisible = true;


            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                // btnR2Multimedia.Source = ImageSource.FromResource("XBook2.media.load.orange.multimedia_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Multimedia.TextColor = Color.Orange;
            }

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                // btnR2Multimedia.Source = ImageSource.FromResource("XBook2.media.load.green.multimedia_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Multimedia.TextColor = Color.Green;
            }
            MainPageViewModelInstance.GetAllMultimediaFromXML(loggedUser, MainPageViewModelInstance.CurrentBook).Wait();

            /*
            try
            {
                
                List<MultimediaListItemOLD> BookmarkedPages;

                BookmarkedPages = currentBook.GetAllMultimediaFromXML(
                    cbLink.IsChecked,
                    cbGalery.IsChecked,
                    cbInformation.IsChecked,
                    cbVideo.IsChecked,
                    cbYoutube.IsChecked,
                    cbAudio.IsChecked,
                    cbQuiz.IsChecked);
                //scrollBookmark.IsVisible = true;
                //stackLibBookmarkMenu.Children.Clear();
                //stackLibBookmarkMenu.IsVisible = true;

                scrollMultimedia.IsVisible = true;
                stackLibMultimediaMenu.Children.Clear();
                stackLibMultimediaMenu.IsVisible = true;
                int counter = 1;
                foreach (MultimediaListItemOLD item in BookmarkedPages)
                {

                    if (item.PageID > 0)
                    {
                        int realPageId = item.PageID + 2;
                        MultimediaNavItem smallPageButton = new MultimediaNavItem(loggedUser.currentLanguage);

                        // SKBitmap myBitmap = (SKBitmap)currentBook.htThumbPages[realPageId.ToString()];
                        //smallPageButton.SetImage(myBitmap);
                        // smallPageButton.SetTitle(Title);

                        smallPageButton.SetPageID(item.PageID.ToString());
                        smallPageButton.SetTitle(item.Title.ToString());
                        smallPageButton.SetType(item.type);
                        smallPageButton.SetAnno(item.multimediaAnno);
                        smallPageButton.OnMultimediaNavClick += SmallPageButton_OnMultimediaNavClick;

                        //Label label = new Label();
                        //label.Text = counter.ToString();

                        stackLibMultimediaMenu.Children.Add(smallPageButton);
                        //counter++;  
                    }
                }
                scrollMultimedia.IsVisible = true;
                gridMuldimedia.IsVisible = true;
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            */
        }

        private void SmallPageButton_OnMultimediaNavClick(object sender, MultimediaEventArgs e)
        {
            try
            {
                ClearMultimediaPreview();

                PlayMultimedia(e);
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion

        #region Library 2.2 NOTES
        private void btnNotes_Clicked(object sender, EventArgs e)
        {
            resetLbraryContent2Row();
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                // btnR2Notes.Source = ImageSource.FromResource("XBook2.media.load.orange.note_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Notes.TextColor = Color.Orange;
            }
            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                //  btnR2Notes.Source = ImageSource.FromResource("XBook2.media.load.green.note_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Notes.TextColor = Color.Green;
            }

            //imageFilter.IsVisible = true;
            //imageFilter.BackgroundColor = Color.Gray;
            // cbMultimadiaFilter.IsVisible = false;
            lblFilter.IsVisible = false;
            scrollNotes.IsVisible = true;
            FillNotesList();
        }
        private void FillNotesList()
        {
            resetLbraryContent2Row();
            //imageFilter.IsVisible = true;
            //imageFilter.BackgroundColor = Color.White;
            //cbMultimadiaFilter.IsVisible = true;
            lblFilter.IsVisible = true;

            scrollNotes.IsVisible = true;
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                // btnR2Notes.Source = ImageSource.FromResource("XBook2.media.load.orange.note_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Notes.TextColor = Color.Orange;
            }

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                //  btnR2Notes.Source = ImageSource.FromResource("XBook2.media.load.green.note_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Notes.TextColor = Color.Green;
            }

            /*
            try
            {

                List<MultimediaListItemOLD> BookmarkedPages;

                BookmarkedPages = currentBook.GetAllNotesFromXML();
                //scrollBookmark.IsVisible = true;
                //stackLibBookmarkMenu.Children.Clear();
                //stackLibBookmarkMenu.IsVisible = true;

                scrollMultimedia.IsVisible = true;
                stackLibNotes.Children.Clear();
                stackLibNotes.IsVisible = true;
                int counter = 1;
                foreach (MultimediaListItemOLD item in BookmarkedPages)
                {

                    if (item.PageID > 0)
                    {
                        int realPageId = item.PageID + 2;
                        MultimediaNavItem smallPageButton = new MultimediaNavItem(loggedUser.currentLanguage);

                        // SKBitmap myBitmap = (SKBitmap)currentBook.htThumbPages[realPageId.ToString()];
                        //smallPageButton.SetImage(myBitmap);
                        // smallPageButton.SetTitle(Title);

                        smallPageButton.SetPageID(item.PageID.ToString());
                        smallPageButton.SetTitle(item.Title.ToString());
                        smallPageButton.SetType(item.type);
                        smallPageButton.SetAnno(item.multimediaAnno);
                        smallPageButton.OnMultimediaNavClick += SmallPageButton_OnMultimediaNavClick;

                        //Label label = new Label();
                        //label.Text = counter.ToString();

                        stackLibNotes.Children.Add(smallPageButton);
                        //counter++;  
                    }
                }
                scrollMultimedia.IsVisible = true;
                gridMuldimedia.IsVisible = true;
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            */
        }
        #endregion

        #region Library 2.3 BOOKMARK

        private void btnBookmarks_Clicked(object sender, EventArgs e)
        {
            resetLbraryContent2Row();
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                // btnR2Bookmarks.Source = ImageSource.FromResource("XBook2.media.load.orange.bookmark_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Bookmarks.TextColor = Color.Orange;
            }
            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                //  btnR2Bookmarks.Source = ImageSource.FromResource("XBook2.media.load.green.bookmark_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Bookmarks.TextColor = Color.Green;
            }
            try
            {
                List<int> BookmarkedPages = MainPageViewModelInstance.CurrentBook.GetAllBookmarkFromXML();
                scrollBookmark.IsVisible = true;
                //stackLibBookmarkMenu.Children.Clear();
                stackLibBookmarkMenu.IsVisible = true;

                foreach (int pageId in BookmarkedPages)
                {
                    if (pageId > 0)
                    {
                        int realPageId = pageId + 2;
                        ThumbBook smallPageButton = new ThumbBook(pageId);

                        SKBitmap myBitmap = (SKBitmap)MainPageViewModelInstance.CurrentBook.htThumbPages[realPageId.ToString()];
                        smallPageButton.SetImage(myBitmap);


                        smallPageButton.SetTitle(pageId.ToString());

                        smallPageButton.OnBookClick += SmallPageButton_OnBookClick;


                        //stackLibBookmarkMenu.Children.Add(smallPageButton);
                    }
                }
                scrollBookmark.IsVisible = true;
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }


        }
        #endregion

        #region Library 2.4. BACK
        private void btnR2Back_Clicked(object sender, EventArgs e)
        {
            //resetLbraryContent2Row();
            if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
            {
                //  btnR2Back.Source = ImageSource.FromResource("XBook2.media.load.orange.back_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Back.TextColor = Color.Orange;
            }

            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                //  btnR2Back.Source = ImageSource.FromResource("XBook2.media.load.green.back_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                lblR2Back.TextColor = Color.Green;
            }
            //showLbraryContent2Row(false);
        }
        #endregion

        #endregion

        #endregion

        #region Language
        private void btnLanguage_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (languageMenu.IsVisible == true)
                {
                    languageMenu.IsVisible = false;
                }
                else
                {
                    if (languageMenu.Children.Count == 0)
                    {
                        LanguageSettings ls = new LanguageSettings(loggedUser);
                        ls.OnLanguageChange += Ls_OnLanguageChange;
                        languageMenu.Children.Add(ls);
                    }
                    languageMenu.IsVisible = true;
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void Ls_OnLanguageChange(object sender, LanguageSettingsEventArgs e)
        {
            try
            {
                loggedUser.currentLanguage = e.Language;
                setLabelText();





                languageMenu.IsVisible = false;
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        #endregion

        #region Color Scheme
        //private void btnThemeOrange_Clicked(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        loggedUser.currentColorTempalte = ColorTemlates.Orange;
        //        btnDefaultMode.Source = null;

        //        btnThemeOrange.IsVisible = false;
        //        btnThemeGreen.IsVisible = false;
        //        // setImagesLeftMenu();
        //        //   setImagesBottomMenu();
        //        SetUserInfoToXML();

        //    }
        //    catch (Exception ee)
        //    {

        //        logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
        //    }
        //}

        //private void btnThemeGreen_Clicked(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        loggedUser.currentColorTempalte = ColorTemlates.Green;
        //        btnDefaultMode.Source = null;

        //        btnThemeOrange.IsVisible = false;
        //        btnThemeGreen.IsVisible = false;

        //        SetUserInfoToXML();
        //    }
        //    catch (Exception ee)
        //    {

        //        logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
        //    }
        //}
        #endregion

        #region ICON SIZE
        private void btnIconSize_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                {
                    // btnThemeOrange.Source = ImageSource.FromResource("XBook2.media.orange_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    //  btnThemeGreen.Source = ImageSource.FromResource("XBook2.media.green_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }

                if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                {
                    //  btnThemeOrange.Source = ImageSource.FromResource("XBook2.media.orange_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    //  btnThemeGreen.Source = ImageSource.FromResource("XBook2.media.green_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
                if (btnThemeOrange.IsVisible == false)
                {
                    btnThemeOrange.IsVisible = true;
                    btnThemeGreen.IsVisible = true;
                }
                else
                {
                    btnThemeOrange.IsVisible = false;
                    btnThemeGreen.IsVisible = false;
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        #endregion

        #region Tablet Mode
        private void btnTabletMode_Clicked(object sender, EventArgs e)
        {
            if (MainPageViewModelInstance.TabletMode == 0)
            {
                MainPageViewModelInstance.TabletMode = 1;
                if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                {
                    //  btnTabletMode.Source = ImageSource.FromResource("XBook2.media.load.orange.tablet_1_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
                else
                {
                    //   btnTabletMode.Source = ImageSource.FromResource("XBook2.media.load.green.tablet_1_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
            }
            else
            {
                MainPageViewModelInstance.TabletMode = 0;
                if (loggedUser.currentColorTempalte == ColorTemlates.Orange)
                {
                    //  btnTabletMode.Source = ImageSource.FromResource("XBook2.media.load.orange.tablet_0_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
                else
                {
                    //   btnTabletMode.Source = ImageSource.FromResource("XBook2.media.load.green.tablet_0_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
            }
            LeftPage.TabletMode = MainPageViewModelInstance.TabletMode;

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.TabletMode = MainPageViewModelInstance.TabletMode;

            LeftPage.LoadPage();

            if (MainPageViewModelInstance.SinglePageMode == false)
                RightPage.LoadPage();
        }
        #endregion

        #region DefaultMode
        private void btnDefaultMode_Clicked(object sender, EventArgs e)
        {

        }
        #endregion

        #region Multimedia Filter
        private void cbFilter_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (cbMultimadiaFilter.IsChecked)
            {

                filterList.IsVisible = true;
            }
            else
            {

                filterList.IsVisible = false;
                //refresh multimedia list
                FillMultimediaList();
            }
        }
        private void cbSelectAll_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (cbSelectAll.IsChecked)
            {
                cbLink.IsChecked = true;
                cbGalery.IsChecked = true;
                cbVideo.IsChecked = true;
                cbYoutube.IsChecked = true;
                cbAudio.IsChecked = true;
                cbQuiz.IsChecked = true;
                cbInformation.IsChecked = true;

            }
            else
            {
                cbLink.IsChecked = false;
                cbGalery.IsChecked = false;
                cbVideo.IsChecked = false;
                cbYoutube.IsChecked = false;
                cbAudio.IsChecked = false;
                cbQuiz.IsChecked = false;
                cbInformation.IsChecked = false;
            }
        }

        private void cbLink_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }

        private void cbGalery_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            //this.MyViewModelInstance.CbG
        }

        private void cbVideo_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }

        private void cbYoutube_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }

        private void cbAudio_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }

        private void cbQuiz_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }

        private void cbInformation_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }



        #endregion

        #region DoubleClick
        private void LeftPage_OnDoubleClick(object sender, EventArgs e)
        {
            leftPageDoubleClick();
        }
        private void SetLeftPageFullScreen()
        {
            MainPageViewModelInstance.BtnSettingsMenuPress = false;
            //ColumnDefinition c1 = new ColumnDefinition();
            //c1.Width = new GridLength(20, GridUnitType.Star);
            MainPageViewModelInstance.RightPageVisible = false;
            RightPage.IsVisible = false;
            rightPageFullScreen = false;
            //mainGridColPageRight.Width = 0;

            leftPageFullScreen = true;
            SetColumnSpanProperty(LeftPage, 10);
            LeftPage.SinglePageMode = true;
            LeftPage.ShowRuler = true;


            txtGoToPage.Text = MainPageViewModelInstance.PageNumLeft.ToString();
        }
        private void BackLeftPageFromFullScreen()
        {

            leftPageFullScreen = false;
            SetColumnSpanProperty(LeftPage, 5);
            LeftPage.ShowRuler = true;

            RightPage.SetValue(Grid.ColumnProperty, 5);
            SetColumnSpanProperty(RightPage, 5);

            //todo: only when is Portrait
            if (MainPageViewModelInstance.PageOrientation == Orientation.Portrait)
            {
                RightPage.IsVisible = false;
                MainPageViewModelInstance.SinglePageMode = true;
                LeftPage.SinglePageMode = true;
                RightPage.SinglePageMode = true;
            }
            else
            {
                RightPage.IsVisible = true;
                RightPage.ShowRuler = true;
                rightPageFullScreen = false;
                MainPageViewModelInstance.SinglePageMode = false;
                LeftPage.SinglePageMode = false;
                RightPage.SinglePageMode = false;
            }





            txtGoToPage.Text = MainPageViewModelInstance.PageNumLeft.ToString() + " - " + MainPageViewModelInstance.PageNumRight.ToString();
            MoveToPage();
        }
        private void leftPageDoubleClick()
        {
            MainPageViewModelInstance.BtnSettingsMenuPress = false;
            #region TWO PAGES
            if (MainPageViewModelInstance.SinglePageMode == false)
            {
                if (leftPageFullScreen == false)
                {
                    //set left page in full scree, hide right page
                    SetLeftPageFullScreen();
                }
                else
                {
                    //left page go back from full screen
                    BackLeftPageFromFullScreen();
                }
            }
            #endregion

            #region ONE PAGE
            if (MainPageViewModelInstance.SinglePageMode == true)
            {
                RightPage.IsVisible = false;
                if (leftPageFullScreen == false)
                {
                    //go to zoom, one page mode
                    
                    leftPageFullScreen = true;

                    SetColumnSpanProperty(LeftPage, 10);
                    LeftPage.ShowRuler = false;
                }
                else
                {
                    if (LeftPage.ZoomPage == false)
                    {
                        //go to two page mode, only on landscape!!
                        //MainPageViewModelInstance.SinglePageMode = false;
                        
                        leftPageFullScreen = false;
                        rightPageFullScreen = false;

                        SetColumnSpanProperty(LeftPage, 10);
                    }
                    else
                    {
                        BackLeftPageFromFullScreen();
                    }


                }
            }
            #endregion
        }

        private void leftPageDoubleClickReset()
        {
            if (MainPageViewModelInstance.SinglePageMode == false)
            {
                if (leftPageFullScreen == true)
                {
                    //RightPage.IsVisible = true;
                    leftPageFullScreen = false;
                    SetColumnSpanProperty(LeftPage, 2);
                    LeftPage.ShowRuler = true;
                }
            }
        }

        private void RightPage_OnDoubleClick(object sender, EventArgs e)
        {
            rightPageDoubleClick();
        }
        private void SetRighttPageFullScreen()
        {
            try
            {
                MainPageViewModelInstance.BtnSettingsMenuPress = false;

                MainPageViewModelInstance.LeftPageVisible = false;
                LeftPage.IsVisible = false;

                rightPageFullScreen = true;
                RightPage.SetValue(Grid.ColumnProperty, 1);
                mainGridColPageRight.Width = this.Width - (4 * MainPageViewModelInstance.MenuGridSize);
                SetColumnSpanProperty(RightPage, 8);
                MainPageViewModelInstance.SinglePageMode = true;
                RightPage.SinglePageMode = true;
                RightPage.ShowRuler = true;

                RightPage.PageNumber = MainPageViewModelInstance.PageNumRight;
                txtGoToPage.Text = MainPageViewModelInstance.PageNumRight.ToString();
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            
        }
        private void BackRightPageFromFullScreen()
        {
            try
            {
                MainPageViewModelInstance.LeftPageVisible = true;
                rightPageFullScreen = false;
                mainGridColPageRight.Width = new GridLength(1, GridUnitType.Star);
                RightPage.SetValue(Grid.ColumnProperty, 5);
                SetColumnSpanProperty(RightPage, 5);
                RightPage.ShowRuler = true;

                leftPageFullScreen = false;
                //todo: only when is Portrait
                if (MainPageViewModelInstance.PageOrientation == Orientation.Portrait)
                {
                    RightPage.IsVisible = true;
                    LeftPage.IsVisible = false;
                    MainPageViewModelInstance.SinglePageMode = true;
                }
                else
                {
                    RightPage.IsVisible = true;
                    LeftPage.IsVisible = true;

                    LeftPage.SinglePageMode = false;
                    MainPageViewModelInstance.SinglePageMode = false;
                }


                SetColumnSpanProperty(LeftPage, 5);


                MoveToPage();
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        private void rightPageDoubleClick()
        {
            try
            {
                //showing two pages
                if (MainPageViewModelInstance.SinglePageMode == false)
                {
                    if (rightPageFullScreen == false)
                    {
                        //set right page in full scree, hide right page
                        SetRighttPageFullScreen();
                    }
                    else
                    {
                        //right page go back from full screen
                        BackRightPageFromFullScreen();
                    }
                    return;
                }
                //show one page
                if (MainPageViewModelInstance.SinglePageMode == true)
                {
                    if (rightPageFullScreen == false)
                    {

                        MainPageViewModelInstance.LeftPageVisible = false;
                        rightPageFullScreen = true;
                        SetColumnSpanProperty(RightPage, 9);
                        RightPage.ShowRuler = false;
                    }
                    else
                    {

                        MainPageViewModelInstance.LeftPageVisible = false;
                        rightPageFullScreen = false;
                        SetColumnSpanProperty(RightPage, 5);
                        //LeftPage.ShowRuler = true;
                    }

                    if (RightPage.ZoomPage == false)
                    {
                        //go to two page mode
                        MainPageViewModelInstance.SinglePageMode = false;

                        LeftPage.IsVisible = true;
                        leftPageFullScreen = false;

                        RightPage.IsVisible = true;
                        rightPageFullScreen = false;
                        mainGridColPageRight.Width = new GridLength(1, GridUnitType.Star);
                        RightPage.SetValue(Grid.ColumnProperty, 5);
                        SetColumnSpanProperty(RightPage, 5);
                    }
                    else
                    {
                        BackRightPageFromFullScreen();
                    }
                }

                //RightPage.LoadPage();
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        private void rightPageDoubleClickReset()
        {
            try
            {
                if (rightPageFullScreen = true)
                {
                    MainPageViewModelInstance.LeftPageVisible = true;
                    rightPageFullScreen = false;
                    //mainGridColPageLeft.Width = leftPageColLen;

                    // mainGridCol2.Width = leftPageColLen;
                    RightPage.ShowRuler = true;
                    //LeftPage.SetValue(Grid.ColumnSpanProperty, 2);

                }
                //RightPage.LoadPage();
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion
        private void SetColumnSpanProperty(SinglePageView pageView, int columnSpan)
        {
            try
            {
                // pageView.SetValue(Grid.ColumnSpanProperty, columnSpan);
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }

        private void btnQuiz_Clicked(object sender, EventArgs e)
        {
            try
            {
                Xamarin.Forms.Application.Current.MainPage = new Quiz(loggedUser, MainPageViewModelInstance.CurrentBook);
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

      
    }

}