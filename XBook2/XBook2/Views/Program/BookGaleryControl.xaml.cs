﻿using KlasimeBase;
using KlasimeBase.Log;
using KlasimeViews;
using Newtonsoft.Json;
using SharedKlasime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2.Helpers;
using static System.Net.Mime.MediaTypeNames;
using static XBook2.Helpers.BookGalery;
using Application = Xamarin.Forms.Application;

namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookGaleryControl : ContentPage
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        #region VARIABLES
        string bookid = "";
        int downloadTestBook = 0;
        bool startTimerForCheckFiles = false;
        HttpClient client;
        CUser loggedUser;
        private int bookButtonWidth = 100;
        //private int bookTotal=12;
        int maxBooksPerRow = 0;
        int bookCounter = 1;
        //int lastPage = 1;
        int rows = 1;
        //int fromPage = 1;
        //int toPage = 1;
        string user;
        int currentPage = 1;
        Hashtable htPages = new Hashtable();
        Hashtable htMyBooks = new Hashtable();
        List<string> lstActivatedBooks = new List<string>();
        List<AlbasBook> userBooks = new List<AlbasBook>();
        List<AlbasBook> userActivatedBooks = new List<AlbasBook>();
        FileInfoList BookPageList;
        int ibookId = 0;
        private bool _isFirstVisible;
        double fileProgress;
        private int fileDownloadCounter = 1;
        List<BookFile> listBookFiles = new List<BookFile>();
        DownloadBook downloadBook;
        Hashtable htBookFiles = new Hashtable();
        Timer fileWatcherTimer;
        string VideoFilesDir;
        string filesDir;
        #endregion

        #region Contstructor
        public BookGaleryControl(CUser _user)
        {
            loggedUser = _user;
            user = loggedUser.firstname + "-" + loggedUser.lastname;
            InitializeComponent();
            client = new HttpClient();
            var stack = new StackLayout();

            headerGrid.BackgroundColor = Color.FromRgb(88, 89, 91);
            imgAlbas.BackgroundColor = Color.FromRgb(255, 152, 1);
            btnPortal.Source = ImageSource.FromResource("XBook2.media.portal_login_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            lblPortal.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_portal"); //"Portal";
            //lblExit.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_exit"); //"Exit";
            lblLogin.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_button"); //"Login";
            lblLicence.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_copyright_bookshelf"); //"Login";


            lblStatus.TextColor = Color.FromRgb(128, 0, 128);

            // lblWaitMessage.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_open_during_loading"); //"Loading....Please wait";
            //lblWaitMessage.FontSize = 22;
            //lblWaitMessage.TextColor = Color.White;
            //bookGaleryGrid.Children.Add(lblWaitMessage, 1, 1);


            //

        }
        #endregion

        #region EVENTS

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            //ExplanationsContentList res = await getExplanationsForBook(loggedUser.token, "84");
            if (loggedUser.token != null)
            {
                lblUser.Text = loggedUser.firstname + " " + loggedUser.lastname;
                loadingActivityIndicator.IsVisible = true;
                loadingActivityIndicator.IsRunning = true;
                userBooks = await RequestAvailableBooks(loggedUser.token);
                userActivatedBooks = await RequestActivatedBooks(loggedUser.token);
                //ibookId = 46;

                //            var t = await updateBookToVersion(loggedUser.token, ibookId.ToString());
                //var b = await activateBook(loggedUser.token, "157", "93A-HOH-L8F");

                //await sendExplanationsContentRating(loggedUser.token, "157",4);

                //task1.Wait();
                // userBooks = task1.Result;
               
                await GetMyBooks();
                await PreparePages();

            }
        }
        private async void ContentPage_SizeChanged(object sender, EventArgs e)
        {
            //await PreparePages();
            double w = this.Width;
            double h = this.Height;
            if (h >= w)
            {
                scrollBooks.Orientation = ScrollOrientation.Vertical;
                bookList.Orientation = StackOrientation.Vertical;
            }
            else
            {
                bookList.Orientation = StackOrientation.Horizontal;
                scrollBooks.Orientation = ScrollOrientation.Horizontal;
            }

        }

        private async void btnPreviousPage_Clicked(object sender, EventArgs e)
        {

            bookList.Children.Clear();
            //stackSecondRow.Children.Clear();
            currentPage--;
            if (currentPage < 1)
                currentPage = 1;

            //await ShowPage();
        }

        private async void btnNextPage_Clicked(object sender, EventArgs e)
        {

            bookList.Children.Clear();
            //stackSecondRow.Children.Clear();
            currentPage++;
            if (currentPage > htPages.Count)
                currentPage = htPages.Count;
            //await ShowPage();
        }

        private void btnLogin_Clicked(object sender, EventArgs e)
        {

            Xamarin.Forms.Application.Current.MainPage = new Login();
        }

        private async void btnPortal_Clicked(object sender, EventArgs e)
        {
            await Common.OpenWebPage();
        }

        private async void Bw_OnBookClick(object sender, BookEventArgs e)
        {
            await DownloadButtonAsync(e);

        }
        private async Task DownloadButtonAsync(BookEventArgs e)
        {

        }


        #endregion

        #region Custom Event
        public delegate void FinichHandler(object sender, EventArgs e);
        public delegate void ProgressHandler(object sender, ProgressUpdateEventArgs e);
        public event ProgressHandler OnProgressUpdate;
        public event ProgressHandler OnFinish;
        public event FinichHandler OnFinishBook;

        private async void OnProgress(object sender, ProgressUpdateEventArgs e)
        {
            if (OnProgressUpdate != null)
            {
                OnProgressUpdate(sender, e);
            }
        }

        //private void OnSendLog(object sender, ProgressUpdateEventArgs e)
        //{
        //    if (OnFinish != null)
        //    {
        //        OnFinish(sender, e);
        //    }
        //}
        private async void OnFinishBookDownload(object sender, EventArgs e)
        {
            if (OnFinishBook != null)
            {
                if (fileWatcherTimer != null)
                {
                    fileWatcherTimer.Enabled = false;
                    fileWatcherTimer.Stop();
                }
                OnFinishBook(sender, e);
            }
        }
        #endregion

        #region PROPERTIES
        public double FileProgress { get => fileProgress; set => fileProgress = value; }
        public int FileDownloadCounter { get => fileDownloadCounter; set => fileDownloadCounter = value; }
        public List<BookFile> ListBookFiles { get => listBookFiles; set => listBookFiles = value; }
        #endregion

        #region METHODS
        //private BookControl CreateOneBookButton(AlbasBook albasBook)
        //{
        //    bool exist = false;
        //    bool activated = false;
        //    int bookId = 0;
        //    int.TryParse(albasBook.id.ToString(), out bookId);
        //    if (htMyBooks.ContainsKey(bookId))
        //    {
        //        exist = true;
        //    }
        //    if (lstActivatedBooks.Contains(bookId.ToString()))
        //    {
        //        activated = true;
        //    }
        //    BookControl bookControlItem = new BookControl(albasBook, exist, activated);
        //    bookControlItem.OnBookClick += BookControlItem_OnBookClick;
        //    //Button btnBook = new Button();
        //    try
        //    {
        //        if (bookControlItem != null)
        //        {

        //            bookControlItem.WidthRequest = 2 * bookButtonWidth;
        //        }
        //        else
        //        {
        //            logInstance.Log(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), null, "book is null");
        //        }
        //    }
        //    catch (Exception ee)
        //    {

        //        logInstance.Log(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
        //    }


        //    return bookControlItem;
        //}


        #endregion

        #region TASKS
        public string serializedBookList = "";
        //private async Task CreateButtonPageList()
        //{
        //    bookCounter = 0;

        //    if (loadingActivityIndicator != null)
        //    {
        //        loadingActivityIndicator.IsRunning = true;
        //        lblWaitMessage.IsVisible = true;
        //    }

        //    await Task.Run(() =>
        //    {
        //        try
        //        {

        //            Device.BeginInvokeOnMainThread(() =>
        //            {
        //                bookList.Children.Clear();
        //            });
                    
        //            List<BookControl> items = new List<BookControl>();
        //            int firstLoad = 10;
        //            if (userBooks.Count > firstLoad)
        //            {
        //                for (int i = 0; i < firstLoad; i++)
        //                {
        //                    bool exist = false;
        //                    bool activated = false;
        //                    int bookId = 0;
        //                    AlbasBook ab = userBooks[i];
        //                    int.TryParse(ab.id.ToString(), out bookId);
        //                    if (htMyBooks.ContainsKey(bookId))
        //                    {
        //                        exist = true;
        //                    }
        //                    if (lstActivatedBooks.Contains(ab.id))
        //                    {
        //                        activated = true;
        //                    }

        //                    BookControl bookControlItem = new BookControl(ab, exist, activated);
        //                    bookControlItem.OnBookClick += BookControlItem_OnBookClick;

        //                    //items.Add(bookControlItem);
        //                    Device.BeginInvokeOnMainThread(() =>
        //                    {
        //                        bookList.Children.Add(bookControlItem);
        //                    });


        //                }

                       
        //            }
                    




        //        }
        //        catch (Exception ee)
        //        {

        //            logInstance.Log(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
        //        }
        //    });


        //    //await Task.Run(() =>
        //    //{
        //    //    int res = 0;
        //    //    for (int i = 5; i < userBooks.Count; i++)
        //    //    {
        //    //        bool exist = false;
        //    //        bool activated = false;
        //    //        int bookId = 0;
        //    //        AlbasBook ab = userBooks[i];
        //    //        int.TryParse(ab.id.ToString(), out bookId);
        //    //        if (htMyBooks.ContainsKey(bookId))
        //    //        {
        //    //            exist = true;
        //    //        }
        //    //        if (lstActivatedBooks.Contains(ab.id))
        //    //        {
        //    //            activated = true;
        //    //        }


        //    //        BookControl bookControlItem = new BookControl(ab, exist, activated);
        //    //        bookControlItem.OnBookClick += BookControlItem_OnBookClick;

        //    //        Device.BeginInvokeOnMainThread(() =>
        //    //        {
        //    //            bookList.Children.Add(bookControlItem);
        //    //        });

        //    //    }
        //    //}
        //    //);




        //    loadingActivityIndicator.IsVisible = false;
        //    loadingActivityIndicator.IsEnabled = false;
        //    loadingActivityIndicator.IsRunning = false;
        //    scrollBooks.IsVisible = true;
        //}

        //private async void BookControlItem_OnBookClick(object sender, BookEventArgs e)
        //{
        //    int ibookId = 0;
        //    int.TryParse(e.AlbasBook.id, out ibookId);

        //    if (e.Action == BookButtonAction.Download)
        //    {

        //        BookPreviewControl bcn = new BookPreviewControl(loggedUser, e.AlbasBook, false, false, e.Action);
        //        bookList.Children.Clear();
        //        //bookList.Children.Add(bcn);
        //        bcn.OnBookClick += Bcn_OnBookClick;
        //        Application.Current.MainPage = bcn;
        //    }

        //    if (e.Action == BookButtonAction.Update)
        //    {
        //       // DownloadBookControl newBook = new DownloadBookControl(loggedUser, e, downloadTestBook);
        //        //    pbPanel.Children.Add(newBook);
        //        //    newBook.OnFinishBook += NewBook_OnFinishBook;
        //        //    await newBook.updateBookToVersionAsync(loggedUser.token, e.AlbasBook.id);
        //    }

        //    if (e.Action == BookButtonAction.Activate)
        //    {
        //        BookPreviewControl bcn = new BookPreviewControl(loggedUser, e.AlbasBook, false, false, e.Action);
        //        bookList.Children.Clear();
        //        //bookList.Children.Add(bcn);
        //        bcn.OnBookClick += Bcn_OnBookClick;
        //        Application.Current.MainPage = bcn;
        //    }

        //    if (e.Action == BookButtonAction.Open)
        //    {
        //        Application.Current.MainPage = new MainPage(loggedUser, ibookId, null);
        //    }
        //    if (e.Action == BookButtonAction.Remove)
        //    {

        //        await CleanUp(e.AlbasBook.id);
        //        await GetMyBooks();
        //        await PreparePages();
        //    }
        //}

        //private async void Bcn_OnBookClick(object sender, BookEventArgs e)
        //{
        //    //todo: make loader visible...
        //    //close the book preview
        //    scrollBooks.IsVisible = false;
        //    htPages.Clear();
        //    loadingActivityIndicator.IsVisible = true;
        //    loadingActivityIndicator.IsEnabled = true;
        //    loadingActivityIndicator.IsRunning = true;
        //    bookList.Children.Clear();

        //    if (e.Action == BookButtonAction.Close)
        //    {
        //        await CreateButtonPageList();
        //    }
        //    if (e.Action == BookButtonAction.Download)
        //    {

        //        //DownloadBookControl newBook = new DownloadBookControl(loggedUser, e, downloadTestBook);
        //        //newBook.OnFinishBook += NewBook_OnFinishBook;
        //        //newBook.MyBookEventArgs = e;

        //        //try
        //        //{
        //        //    Application.Current.MainPage = newBook;
        //        //}
        //        //catch (Exception ee)
        //        //{

        //        //    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
        //        //}

        //        //bookList.Children.Add(newBook);


        //        scrollBooks.IsVisible = true;
        //        //bookList.IsVisible = true;
        //        loadingActivityIndicator.IsVisible = false;
        //        loadingActivityIndicator.IsEnabled = false;
        //        loadingActivityIndicator.IsRunning = false;
        //    }

        //    if (e.Action == BookButtonAction.Activate)
        //    {
        //        // UserControls.ActivateBookControl activateBook = new UserControls.ActivateBookControl(loggedUser, e);
        //        // pbPanel.Children.Add(activateBook);
        //        // activateBook.OnFinishBook += ActivateBook_OnFinishBook;
        //        //await newBook.updateBookToVersion(loggedUser.token, e.AlbasBook.id);
        //    }

        //    if (e.Action == BookButtonAction.Open)
        //    {

        //        Application.Current.MainPage = new MainPage(loggedUser, ibookId, null);
        //        //this.Close();
        //    }
        //    if (e.Action == BookButtonAction.Remove)
        //    {

        //        await CleanUp(e.AlbasBook.id);
        //        await GetMyBooks();
        //        await PreparePages();
        //    }
        //}
        private async Task CleanUp(string bookID)
        {
            string dir = Common.GetLocalPath() + Common.GetPathSeparator() + "book" + Common.GetPathSeparator() + bookID;
            do
            {
                try
                {
                    Directory.Delete(dir, true);
                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception ee)
                {
                    logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), ee, "");

                }
            } while (Directory.Exists(dir));
        }
        private async void NewBook_OnFinishBook(object sender, EventArgs e)
        {
            //start wait loader 
            //await CreateButtonPageList();
        }

        private async Task GetMyBooks()
        {
            try
            {
                logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "GetMyBooks start ");
                lblErrStatus.IsVisible = false;
                htMyBooks = new Hashtable();
                if (Directory.Exists(Common.GetLocalPath() + Common.GetPathSeparator() + "book"))
                {
                    string[] folders = Directory.GetDirectories(Common.GetLocalPath() + Common.GetPathSeparator() + "book");
                    foreach (string folder in folders)
                    {
                        var splitter = Common.GetPathSeparator().ToCharArray();
                        string[] tmpArray = folder.Split(splitter[0]);
                        if (tmpArray.Length > 1)
                        {
                            string bookId = tmpArray[tmpArray.Length - 1];
                            int myBookId = 0;
                            int.TryParse(bookId, out myBookId);
                            if (myBookId > 0)
                            {
                                if (htMyBooks.ContainsKey(myBookId) == false)
                                {
                                    htMyBooks.Add(myBookId, folder);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        private async Task PreparePages()
        {
            //lblErrStatus.IsVisible = false;
            //htPages = new Hashtable();
            //currentPage = 1;
            //bookCounter = 1;
            //// int.TryParse(txtBookTotal.Text, out bookTotal);
            //bookGaleryGrid.WidthRequest = this.Width;
            //headerGrid.WidthRequest = this.Width;

            //bookList.Children.Clear();
            ////stackSecondRow.Children.Clear();
            //int correction = 160;
            //if (this.Width < 1000)
            //    correction = 160;
            //if (this.Width > 1000)
            //    correction = 140;
            //if (this.Width > 1500)
            //    correction = 120;

            //maxBooksPerRow = (int)(this.Width / ((double)bookButtonWidth + (correction)));



            //if (this.Height > 0)
            //{
            //    if (this.Height > 800)
            //    {
            //        rows = 2;


            //    }
            //    else
            //    {
            //        rows = 1;

            //    }
            //    await CreateButtonPageList();

            //    if (loadingActivityIndicator != null)
            //    {
            //        loadingActivityIndicator.IsRunning = false;
            //        lblWaitMessage.IsVisible = false;
            //    }

              
            //}
        }

        private async Task<List<AlbasBook>> RequestAvailableBooks(string token)
        {
       

            try
            {
                logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "RequestAvailableBooks start ");
                lblErrStatus.IsVisible = false;
                if (File.Exists(Common.GetLocalPath() + Common.GetPathSeparator() + "serializedBookList.json"))
                {
                    serializedBookList = File.ReadAllText(Common.GetLocalPath() + Common.GetPathSeparator() + "serializedBookList.json");
                }


                if (serializedBookList == "")
                {
                    
                    serializedBookList = await client.GetStringAsync(Book.urlPart + "/api/book/index?token=" + token);
                    File.WriteAllText(Common.GetLocalPath() + Common.GetPathSeparator() + "serializedBookList.json", serializedBookList);
                }
                
                List<AlbasBook> obj = JsonConvert.DeserializeObject<List<AlbasBook>>(serializedBookList);
                

                return obj;
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee, serializedBookList);
                return new List<AlbasBook>();
            }

        }
        private async Task<List<AlbasBook>> RequestActivatedBooks(string token)
        {
            string jsonString = "";
            lstActivatedBooks = new List<string>();
            try
            {
                //logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "RequestActivatedBooks start ");
                lblErrStatus.IsVisible = false;
                jsonString = await client.GetStringAsync(Book.urlPart + "/api/book/activated?token=" + token);
                //task.Wait();
                //string jsonString = task.Result.ToString();
                List<AlbasBook> obj = JsonConvert.DeserializeObject<List<AlbasBook>>(jsonString);

                foreach (AlbasBook item in obj)
                {
                    if (lstActivatedBooks.Contains(item.id.ToString()) == false)
                    {
                        lstActivatedBooks.Add(item.id.ToString());
                    }
                }

                return obj;
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return new List<AlbasBook>();
            }

        }
        bool updateWork = false;



        //public bool IsFirstVisible { 
        //    get => _isFirstVisible; 
        //    set => _isFirstVisible = value;
        //    RaisePropertyChanged ("IsStopVisible");}


      
        private async Task<ExplanationsContentList> getExplanationsContent(string token, string id)
        {
            string jsonString = "";
            try
            {
                logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "getExplanationsContent start book id: " + id);
                lblErrStatus.IsVisible = false;
                jsonString = await client.GetStringAsync(Book.urlPart + "/api/explainations/" + id + "/content?token=" + token);
                //task.Wait();
                //string jsonString = task.Result.ToString();
                ExplanationsContentList obj = JsonConvert.DeserializeObject<ExplanationsContentList>(jsonString);
                return obj;
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return new ExplanationsContentList();
            }

        }
        private async Task<ExplanationsContentList> getExplanationsForBook(string token, string id)
        {
            string jsonString = "";
            ExplanationsContentList res = new ExplanationsContentList();
            try
            {
                logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "getExplanationsForBook start book id: " + id);
                res._ExplanationsContent = new List<ExplanationsContent>();
                lblErrStatus.IsVisible = false;
                jsonString = await client.GetStringAsync(Book.urlPart + "/api/explainations/" + id + "?token=" + token);
                //task.Wait();
                //string jsonString = task.Result.ToString();
                List<ExplanationReturn> obj = JsonConvert.DeserializeObject<List<ExplanationReturn>>(jsonString);
                foreach (ExplanationReturn item in obj)
                {
                    ExplanationsContentList ecl = await getExplanationsContent(token, item.id);
                    foreach (ExplanationsContent ec in ecl._ExplanationsContent)
                    {
                        res._ExplanationsContent.Add(ec);
                    }
                }

            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return res;
            }
            return res;
        }
        //

        private async Task<bool> sendExplanationsContentRating(string token, string id, int stars)
        {
            try
            {
                logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "sendExplanationsContentRating start book id: " + id);
                lblErrStatus.IsVisible = false;
                HttpContent c = new StringContent(stars.ToString());
                Uri u = new Uri(Book.urlPart + "/api/explainations/" + id + "/rate?token=" + token);
                var response = string.Empty;
                HttpRequestMessage request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = u,
                    Content = c
                };

                HttpResponseMessage result = await client.SendAsync(request);
                if (result.IsSuccessStatusCode)
                {
                    response = result.StatusCode.ToString();
                    return true;
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                return false;
            }
            return false;
        }




        #endregion


    }
}