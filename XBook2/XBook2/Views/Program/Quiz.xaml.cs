﻿using KlasimeBase;
using KlasimeBase.Log;
using SharedKlasime;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Svg;
using Xamarin.Forms.Xaml;
using XBook2.ViewModels;

namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Quiz : ContentPage
    {
        public ApiLog logInstance = ApiLog.GetInstance();
        public static string user = "program";
        public Book mybook = null;
        #region VARIABLES
        public ToolsViewModel MainPageViewModelInstance;
        // SKBitmap bmpAudio;
        CUser loggedUser;

        SKCanvas Canvas;
        private SkiaSharp.SKSurface surface;
        SKBitmap myBitmap;
        bool lineDraw = false;
        bool penDraw = false;
        bool startDrawingLine = false;
        bool startDrawingPen = false;
        SKPoint pointStartLine;
        SKPoint pointEndLine;
        //SKPoint pointDrag;
        SKPoint pointStartPen;
        Hashtable htLines = new Hashtable();
        Hashtable htDraws = new Hashtable();
        List<SKPoint> listPoints = new List<SKPoint>();
        int lineCounter = 0;
        int drawCounter = 0;

        //bool drag = false;
        Dictionary<long, SKPath> inProgressPaths = new Dictionary<long, SKPath>();
        List<SKPath> completedDrawingPaths = new List<SKPath>();

        SKPaint paint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.White,
            StrokeWidth = 10,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        };
        #endregion

        public Quiz(CUser muser, Book _currentBook)
        {
            MainPageViewModelInstance = new ToolsViewModel(muser, _currentBook);
            MainPageViewModelInstance.DemoSVGimage = "http://85.215.209.4:8033/svg/login-.svg";
            mybook = _currentBook;
            loggedUser = muser;
            BindingContext = MainPageViewModelInstance;
            
            InitializeComponent();
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            try
            {
                //QuizzDemo();
               //SvgDemo();


            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        public void SvgDemo()
        {
            string sep = Common.GetPathSeparator();
            string baseLocalDir = KlasimeBase.Common.GetLocalPath() + sep + mybook.Id + sep + "svg";
            string uriString = baseLocalDir + sep + "login-.svg";
            if (File.Exists(uriString) ==false) {
                DownloadFileForSvg(baseLocalDir, "", "login-.svg");
            }

            if (File.Exists(uriString) == true)
            {
                imgSVGdemo.Source= SvgImageSource.FromSvgUri(uriString, 200, 200, default(Color)); 
            }
        }
        public void QuizzDemo()
        {
            string sep = Common.GetPathSeparator();
            string baseLocalDir = KlasimeBase.Common.GetLocalPath() + sep + mybook.Id + sep + "quiz";

            DownloadFileForQuizz(baseLocalDir, "", "index.html");
            baseLocalDir += sep + "data";
            DownloadFileForQuizz(baseLocalDir, "data", "browsersupport.js");
            DownloadFileForQuizz(baseLocalDir, "data", "html5-unsupported.html");
            DownloadFileForQuizz(baseLocalDir, "data", "player.js");
            DownloadFileForQuizz(baseLocalDir, "data", "apple-touch-icon.png");
            DownloadFileForQuizz(baseLocalDir, "data", "favicon.ico");
            DownloadFileForQuizz(baseLocalDir, "data", "html5.png");
            baseLocalDir += sep + "fonts";
            DownloadFileForQuizz(baseLocalDir, "data/fonts", "fnt0.woff");
            DownloadFileForQuizz(baseLocalDir, "data/fonts", "fnt1.woff");
            DownloadFileForQuizz(baseLocalDir, "data/fonts", "fnt2.woff");


            var source = new HtmlWebViewSource();
            //source.BaseUrl = LocalPath;


            string resourceID = KlasimeBase.Common.GetLocalPath() + sep + mybook.Id + sep + "quiz" + sep + "index.html";
            if (File.Exists(resourceID))
            {
                Browser.Source = resourceID;
            }




            //source.BaseUrl = "file://android_asset/";
            //Assembly assembly = GetType().GetTypeInfo().Assembly;
            //string resourceID = "XBook2.quiz.demo.index.html";
            //using (Stream sr = assembly.GetManifestResourceStream(resourceID))
            //{
            //    using (StreamReader sr2 = new StreamReader(sr))
            //    {
            //        var content = sr2.ReadToEnd();

            //        Browser.Source = content;
            //    }

            //}
        }
        private bool DownloadFileForQuizz(string baseLocalDir, string innerFolder, string resourceItem)
        {
            string sep = Common.GetPathSeparator();
            string LocalPath = baseLocalDir + sep + resourceItem;
            Download downloadItem;

            string Url = "http://85.215.209.4:8033/quiz/"  + resourceItem;
            if (innerFolder!= "")
            {
                Url = "http://85.215.209.4:8033/quiz/" + innerFolder + "/" + resourceItem;
            }
            
           

            if (!Directory.Exists(baseLocalDir))
            {
                Directory.CreateDirectory(baseLocalDir);
            }

            downloadItem = new Download(Url, LocalPath, Url, true);
            downloadItem.OnFileFinishEvent += DownloadItem_OnFileFinishEvent;
            downloadItem.OnFileProgressEvent += Dc_OnFileProgressEvent;

            bool res = downloadItem.getFileFromUrlSingle().Result;

            return res;
        }
        private bool DownloadFileForSvg(string baseLocalDir, string innerFolder, string resourceItem)
        {
            string sep = Common.GetPathSeparator();
            string LocalPath = baseLocalDir + sep + resourceItem;
            Download downloadItem;

            string Url = "http://85.215.209.4:8033/svg/" + resourceItem;
            if (innerFolder != "")
            {
                Url = "http://85.215.209.4:8033/svg/" + innerFolder + "/" + resourceItem;
            }



            if (!Directory.Exists(baseLocalDir))
            {
                Directory.CreateDirectory(baseLocalDir);
            }

            downloadItem = new Download(Url, LocalPath, Url, true);
            downloadItem.OnFileFinishEvent += DownloadItem_OnFileFinishEvent;
            downloadItem.OnFileProgressEvent += Dc_OnFileProgressEvent;

            bool res = downloadItem.getFileFromUrlSingle().Result;

            return res;
        }
        private void Dc_OnFileProgressEvent(object sender, BookDownloadEventArgs e)
        {
            string progress =  e.Progress.ToString() + e.Total.ToString();
        }

        private void DownloadItem_OnFileFinishEvent(object sender, BookDownloadEventArgs e)
        {
            string done = "";
        }

        private void btnExit_Clicked(object sender, EventArgs e)
        {
            try
            {
                Application.Current.MainPage = new MainPage(loggedUser, mybook.Id, mybook);
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
    }
}