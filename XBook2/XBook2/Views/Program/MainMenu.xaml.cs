﻿using KlasimeBase.Log;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Shapes;
using Xamarin.Forms.Xaml;
using XBook2.Model;
using XBook2.ViewModels;

namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenu : AlbasContentPage
    {

        #region VARIABLES
        public ApiLog logInstance = ApiLog.GetInstance();
        
        public MainMenuViewModel MyViewModelInstance;
        string token;
        
        #endregion

        #region CONSTRUCTOR
        public MainMenu(CUser _loggedUser, bool _IsSmallScreen) : base(_loggedUser.firstname + "-" + _loggedUser.lastname)
        {
            
            
            token = _loggedUser.token;

          
            InitializeComponent();
            MyViewModelInstance = new MainMenuViewModel(_loggedUser, _IsSmallScreen);
            

            BindingContext = MyViewModelInstance;


        }
        public MainMenu() : base("")
        {
            InitializeComponent();
        }
        #endregion

        #region EVENTS
        private async void btnHelp_Clicked(object sender, EventArgs e)
        {
            //panelReklame.TranslateTo(0, -1000, 1000);
            await OpenWebPage();
        }
        public static async Task OpenWebPage()
        {
            try
            {
                Uri uri = new Uri("http://www.portalishkollor.al/");
                await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
            }
            catch (Exception ee)
            {
                //logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            SetBackgroundCustom();
            if (token != null)
            {
                //new design
                //loadingSpin.IsLoading = true;
                List<AlbasBook> userBooks = new List<AlbasBook>();

                Task<Banner> res = MyViewModelInstance.requestBanner(token);
                MyViewModelInstance.BannerURL = res.Result.image_url;
                MyViewModelInstance.BannerTitle = res.Result.title;

                ReklameAnimation();
            }
            ReklameAnimation();
        }
        private void ReklameAnimation()
        {
            try
            {
                double X = panelReklame.X;
                btnNext.TranslateTo(0, 0, 1000);
                panelReklame.TranslateTo(0, 0, 1000);
                leftButton.TranslateTo(0, 0, 1000);
                rightButton.TranslateTo(0, 0, 1000);
                
            }
            catch (Exception ee)
            {

                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void btnExit_Clicked(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            catch (Exception ee)
            {

                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

     
        private void MainMenuForm_SizeChanged(object sender, EventArgs e)
        {
            SetBackground(MyViewModelInstance.User);
            SetBackgroundCustom();
        }
        #endregion

        #region METHODS
        public void SetBackgroundCustom()
        {
            if (this.Height < this.Width)
            {
                //LANDSCAPE
                MyViewModelInstance.PageOrientation = Orientation.Landscape;
                
                //centralColumn.Width = new GridLength(1, GridUnitType.Star);
                if (MyViewModelInstance.IsSmallScreen == true)
                {
                    //PHONE LANDSCAPE

                    MyViewModelInstance.LeftButtonPoligonTranslationX = 88;
                    MyViewModelInstance.RightButtonPoligonTranslationX = 26;


                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 36));

                    poligonHelp.Points = myPointCollection;

                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(30, 50));
                    myPointCollection.Add(new Point(0, 50));

                    poligonExit.Points = myPointCollection;
                }
                else
                {
                    //TABLET LANDSCAPE
                    MyViewModelInstance.LeftButtonPoligonTranslationX = 60;
                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 88));

                    poligonHelp.Points = myPointCollection;

                    MyViewModelInstance.RightButtonPoligonTranslationX = -40;
                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(40, 100));
                    myPointCollection.Add(new Point(0, 120));

                    poligonExit.Points = myPointCollection;
                }
            }
            else
            {
                //PORTRAIT
                MyViewModelInstance.PageOrientation = Orientation.Portrait;
                
                if (MyViewModelInstance.IsSmallScreen == true)
                {
                    //PHONE PORTRAIT

                    MyViewModelInstance.LeftButtonPoligonTranslationX = 68;
                    MyViewModelInstance.RightButtonPoligonTranslationX = -26;


                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 36));

                    poligonHelp.Points = myPointCollection;
                }
                else
                {
                    //TABLET PORTRAIT

                    MyViewModelInstance.LeftButtonPoligonTranslationX = 45;
                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(0, 90));

                    poligonHelp.Points = myPointCollection;

                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(40, 100));
                    myPointCollection.Add(new Point(0, 100));

                    poligonExit.Points = myPointCollection;
                    MyViewModelInstance.RightButtonPoligonTranslationX = -40;
                }

            }
        }

        #endregion
    }
}