﻿using KlasimeBase;
using KlasimeBase.Log;
using Newtonsoft.Json;
using SharedKlasime;
using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Xml;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Shapes;
using Xamarin.Forms.Xaml;
using XBook2.ViewModels;
using XBook2.Views;
using XBook2.Views.Program;

namespace XBook2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : AlbasContentPage
    {

        #region VARIABLES
        public ApiLog logInstance = ApiLog.GetInstance();
        HttpClient client;
 
        bool videoLoad = true;
        public LoginViewModel MyViewModelInstance;
        public static double ScreenWidth { get; } = DeviceDisplay.MainDisplayInfo.Width / DeviceDisplay.MainDisplayInfo.Density;
        public static double ScreenHeight { get; } = DeviceDisplay.MainDisplayInfo.Height / DeviceDisplay.MainDisplayInfo.Density;
        public static bool LoginIsSmallScreen { get; } = ScreenWidth <= 360;
        #endregion

        #region CONSTRUCTOR
        public Login() : base("")
        {
            InitializeComponent();
            MyViewModelInstance = new LoginViewModel();
            MyViewModelInstance.ImgSVGlogin = "http://85.215.209.4:8033/svg/login-.svg";
            MyViewModelInstance.IsSmallScreen = LoginIsSmallScreen;
            this.BindingContext= MyViewModelInstance;
            //set labels
            //if (loggedUser != null)
            /*
              
                lblRememberMe.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_save_password"); // "remember me";
                btnLogin.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_button"); // "sign in";
                btnRegister.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_message_link"); // "";
            */
            client = new HttpClient();

            //clean temp images
            foreach (string f in Directory.EnumerateFiles(KlasimeBase.Common.GetLocalPath(), "myBitmap*.iig"))
            {
                File.Delete(f);
            }
            lblVersion.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            try
            {
            }
            catch (Exception ee)
            {

                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee, "GetExecutingAssembly problem");
            }


        }
        #endregion

        #region EVENTS
        private void btnExit_Clicked(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            catch (Exception ee)
            {

                string s = ee.Message;
            }
        }

        private async void btnHelp_Clicked(object sender, EventArgs e)
        {
            await OpenWebPage();
        }
        private async void btnLogin_Clicked(object sender, EventArgs e)
        {
            string test = LoginName.Text;
            string urlFullString= string.Empty;
            var resString = "";
            try
            {
                
                MyViewModelInstance.ShowLoginButton = false;
                MyViewModelInstance.WaitOnLogin = true;

                urlFullString = Book.urlPart + "/api/authenticate?username=" + LoginName.Text + "&password=" + LoginPassword.Text;
                resString = await client.GetStringAsync(urlFullString);

                //response can have additional data, so we have to get token only
                int startToken = resString.IndexOf("{");
                int endToken = resString.IndexOf("}");
                string jsonString = resString.Substring(startToken, endToken - startToken + 1);
                CUser cuser = JsonConvert.DeserializeObject<CUser>(jsonString);
                if (ckRememberLogin.IsChecked)
                {
                    SetUserInfoToXML();
                }
                else
                {
                    RemoveUserInfoXML();
                }
                string loginToken = cuser.token;
                Task<CUser> task = requestUserProfile(loginToken);
                task.Wait();
                cuser = task.Result;
                cuser.token = loginToken;
                MyViewModelInstance.User = cuser.firstname + "-" + cuser.lastname;
                LoginStatus.TextColor = Color.Black;
                LoginStatus.Text = "Welcome " + cuser.firstname + " " + cuser.lastname;

                logInstance.Log(MyViewModelInstance.User, Severity.Trace, MethodBase.GetCurrentMethod(), null, "start loginToken: " + loginToken);


                Task<UserType> taskUserType = requestUserType(loginToken);
                taskUserType.Wait();
                cuser.currentUserType= taskUserType.Result;

                #region Display and OS

                logInstance.Log(MyViewModelInstance.User, Severity.Trace, MethodBase.GetCurrentMethod(), null, "device OS: " + Device.RuntimePlatform);


                // Get Metrics
                var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;

                double displayProportion = mainDisplayInfo.Width / mainDisplayInfo.Height;

                logInstance.Log(MyViewModelInstance.User, Severity.Trace, MethodBase.GetCurrentMethod(), null, "START mainDisplayInfo : " + mainDisplayInfo.ToString());
                logInstance.Log(MyViewModelInstance.User, Severity.Trace, MethodBase.GetCurrentMethod(), null, "W " + mainDisplayInfo.Width + " H " + mainDisplayInfo.Height + " P " + displayProportion.ToString("#.##"));

                #endregion
                MyViewModelInstance.WaitOnLogin = false;
                MyViewModelInstance.ShowLoginButton = true;
                // Application.Current.MainPage = new BookGaleryControl(cuser);
                //Application.Current.MainPage = new BookGaleryList(cuser, IsSmallScreen);
                Application.Current.MainPage = new MainMenu(cuser, MyViewModelInstance.IsSmallScreen);

            }
            catch (Exception ee)
            {
                MyViewModelInstance.WaitOnLogin = false;
                MyViewModelInstance.ShowLoginButton = true;
                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee, resString + Environment.NewLine + urlFullString);
                LoginStatus.TextColor = Color.Red;
                LoginStatus.Text = ee.Message;
            }

        }
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            if (ckRememberLogin.IsChecked)
                GetUserInfoFromXML();
            if (MyViewModelInstance.IsSmallScreen == true)
            {
                //PHONE
                MyViewModelInstance.ButtonRowHeight = 40;
                MyViewModelInstance.LoginIconSize = 40;
                MyViewModelInstance.LoginMainLabelHeight = 20;
                MyViewModelInstance.LoginEmptyLabelHeight = 0;
                MyViewModelInstance.InputHeight = 38;
                MyViewModelInstance.LoginNameFontSize = 14;
                MyViewModelInstance.LoginSignButtonFontSize = 14;
                MyViewModelInstance.RegisterFontSize = 10;
            }
            else
            {
                //TABLET
                MyViewModelInstance.ButtonRowHeight = 70;
                MyViewModelInstance.LoginIconSize = 80;
                MyViewModelInstance.LoginMainLabelHeight = 50;
                MyViewModelInstance.LoginEmptyLabelHeight = 8;
                MyViewModelInstance.InputHeight = 44;
                MyViewModelInstance.LoginNameFontSize = 24;
                MyViewModelInstance.LoginSignButtonFontSize = 16;
                MyViewModelInstance.RegisterFontSize = 14;
            }
            //test
            //loadingVideo.IsVisible = false;
            //loginGrid.IsVisible = true;
            //bottomRow.IsVisible = true;
            //leftButton.IsVisible = true;
            //rightButton.IsVisible = true;
            //videoLoad = false;
            //SetBackground(this, MyViewModelInstance.User);
            SetBackgroundCustom();
        }

        private async void btnRegister_Clicked(object sender, EventArgs e)
        {

            await Common.OpenWebPage("http://ti.portalishkollor.al/register");

        }

        private void loadingVideo_MediaEnded(object sender, EventArgs e)
        {
            loadingVideo.IsVisible = false;
            loginGrid.IsVisible = true;
            leftButton.IsVisible = true;
            rightButton.IsVisible = true;
            videoLoad = false;
            SetBackground( MyViewModelInstance.User);
            SetBackgroundCustom();
        }

        private void ContentPage_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                if (videoLoad == false)
                {
                    SetBackground(MyViewModelInstance.User);
                    SetBackgroundCustom();
                }

            }
            catch (Exception ee)
            {

                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee, "GetExecutingAssembly problem");
            }
        }
        #endregion

        #region API
        private async Task<CUser> requestUserProfile(string token)
        {
            string jsonString = "";
            try
            {
                Task<string> task = client.GetStringAsync(Book.urlPart + "/api/profile?token=" + token);
                task.Wait();
                jsonString = task.Result.ToString();
                CUser obj = JsonConvert.DeserializeObject<CUser>(jsonString);
                return obj;
            }
            catch (Exception ee)
            {
                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return new CUser();
            }

        }

        // api/usertype
        private async Task<UserType> requestUserType(string token)
        {
            string jsonString = "";
            try
            {
                Task<string> task = client.GetStringAsync(Book.urlPart + "/api/usertype?token=" + token);
                task.Wait();
                jsonString = task.Result.ToString();
                UserType obj = JsonConvert.DeserializeObject<UserType>(jsonString);
                return obj;
            }
            catch (Exception ee)
            {
                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return new UserType();
            }

        }

        #endregion

        #region Remmember User
        private void GetUserInfoFromXML()
        {
            string userInfoPath = "";
            try
            {
                userInfoPath = KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "login.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("name");
                    attr.Value = LoginName.Text;
                    nod.Attributes.Append(attr);

                    attr = doc.CreateAttribute("pass");
                    attr.Value = LoginPassword.Text;
                    nod.Attributes.Append(attr);

                    doc.AppendChild(nod);
                    doc.Save(userInfoPath);
                }
                else
                {
                    doc.Load(userInfoPath);
                    if (doc.ChildNodes.Count > 0)
                    {
                        XmlNode nod = doc.ChildNodes[0];
                        if (nod.Attributes.Count == 2)
                        {
                            LoginName.Text = nod.Attributes[0].InnerText;
                            LoginPassword.Text = nod.Attributes[1].InnerText;
                        }

                    }
                }

            }
            catch (Exception ee)
            {
                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        /// <summary>
        /// save last visitet age
        /// </summary>
        private void SetUserInfoToXML()
        {
            string userInfoPath = "";
            try
            {
                userInfoPath = KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "login.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("name");
                    attr.Value = LoginName.Text;
                    nod.Attributes.Append(attr);

                    attr = doc.CreateAttribute("pass");
                    attr.Value = LoginPassword.Text;
                    nod.Attributes.Append(attr);

                    doc.AppendChild(nod);
                    doc.Save(userInfoPath);
                }
                else
                {
                    doc.Load(userInfoPath);
                    if (doc.ChildNodes.Count > 0)
                    {
                        XmlNode nod = doc.ChildNodes[0];
                        if (nod.Attributes.Count == 2)
                        {
                            nod.Attributes[0].Value = LoginName.Text;
                            nod.Attributes[1].Value = LoginPassword.Text;
                            doc.Save(userInfoPath);
                        }
                        else
                        {
                            logInstance.Log(MyViewModelInstance.User, Severity.High, MethodBase.GetCurrentMethod(), null, "XML for user settings have attribute missing");
                        }


                    }
                    else
                    {
                        logInstance.Log(MyViewModelInstance.User, Severity.High, MethodBase.GetCurrentMethod(), null, "XML for user settings have no child nodes");
                    }
                }

            }
            catch (Exception ee)
            {
                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        private void RemoveUserInfoXML()
        {
            string userInfoPath = "";
            try
            {
                userInfoPath = KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "login.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == true)
                {
                    File.Delete(userInfoPath);
                }



            }
            catch (Exception ee)
            {
                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion

        #region METHODS
        public void SetBackgroundCustom()
        {
            if (this.Height < this.Width)
            {
                //LANDSCAPE
                MyViewModelInstance.PageOrientation = Orientation.Landscape;
                
                //centralColumn.Width = new GridLength(1, GridUnitType.Star);
                if (MyViewModelInstance.IsSmallScreen == true)
                {
                    //PHONE
                    MyViewModelInstance.ButtonColumnWidth = 140;
                    MyViewModelInstance.LeftButtonPoligonTranslationX = 88;
                    MyViewModelInstance.RightButtonPoligonTranslationX = 26;
                    MyViewModelInstance.LoginEmptyLabelHeight = 0;

                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 36));

                    poligonHelp.Points = myPointCollection;

                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(30, 50));
                    myPointCollection.Add(new Point(0, 50));

                    poligonExit.Points = myPointCollection;
                }
                else
                {
                    //TABLET
                    MyViewModelInstance.LeftButtonPoligonTranslationX = 88;
                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 88));
                    
                    poligonHelp.Points = myPointCollection;

                    MyViewModelInstance.RightButtonPoligonTranslationX = -26;
                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(40, 100));
                    myPointCollection.Add(new Point(0, 120));

                    poligonExit.Points = myPointCollection;
                }
            }
            else
            {
                //PORTRAIT
                MyViewModelInstance.PageOrientation = Orientation.Portrait;
                
                if (MyViewModelInstance.IsSmallScreen == true)
                {
                    //PHONE
                    MyViewModelInstance.ButtonColumnWidth = 70;
                    MyViewModelInstance.LeftButtonPoligonTranslationX = 68;
                    MyViewModelInstance.RightButtonPoligonTranslationX = -26;
                    MyViewModelInstance.LoginEmptyLabelHeight = 2;

                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 36));

                    poligonHelp.Points = myPointCollection;
                }
                else
                {
                    //TABLET

                    MyViewModelInstance.LeftButtonPoligonTranslationX = 88;
                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(0, 90));

                    poligonHelp.Points = myPointCollection;

                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(40, 100));
                    myPointCollection.Add(new Point(0, 100));

                    poligonExit.Points = myPointCollection;
                    MyViewModelInstance.RightButtonPoligonTranslationX = -26;
                }

            }
        }

    
        public static async Task OpenWebPage()
        {
            try
            {
                Uri uri = new Uri("http://www.portalishkollor.al/");
                await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
            }
            catch (Exception ee)
            {
                //logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion
    }

}