﻿using eBookGraphics;
using KlasimeBase;
using KlasimeBase.Log;
using Newtonsoft.Json;
using SharedKlasime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Shapes;
using Xamarin.Forms.Xaml;
using XBook2.ViewModels;
using XBook2.Views.Program;

namespace XBook2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginErr : ContentPage
    {
        public ApiLog logInstance = ApiLog.GetInstance();
        #region VARIABLES
        public static string user = "program";
        HttpClient client;
        CUser loggedUser;
        bool videoLoad = true;
        public LoginViewModel LoginViewModelInstance;
        public static double ScreenWidth { get; } = DeviceDisplay.MainDisplayInfo.Width / DeviceDisplay.MainDisplayInfo.Density;
        public static double ScreenHeight { get; } = DeviceDisplay.MainDisplayInfo.Height / DeviceDisplay.MainDisplayInfo.Density;
        public static bool IsSmallScreen { get; } = ScreenWidth <= 360;
        #endregion

        #region CONSTRUCTOR
        public LoginErr()
        {
            InitializeComponent();
            LoginViewModelInstance = new LoginViewModel();
            LoginViewModelInstance.ImgSVGlogin = "http://85.215.209.4:8033/svg/login-.svg";
            this.BindingContext = LoginViewModelInstance;
            //set labels
            if (loggedUser != null)
            {
                //lblTitle.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_header"); // "";
                //lblRememberMe.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_save_password"); // "remember me";
                //btnLogin.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_button"); // "sign in";
                //lblMessage.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_message"); // "";
                //btnRegister.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_login_message_link"); // "";
            }
            client = new HttpClient();

            //clean temp images
            foreach (string f in Directory.EnumerateFiles(KlasimeBase.Common.GetLocalPath(), "myBitmap*.iig"))
            {
                File.Delete(f);
            }
            //lblVersion.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            try
            {
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee, "GetExecutingAssembly problem");
            }


        }
        #endregion

        #region EVENTS

        private void ContentPage_Appearing(object sender, EventArgs e)
        {

            if (IsSmallScreen == true)
            {
                LoginViewModelInstance.ButtonRowHeight = 40;
                LoginViewModelInstance.LoginIconSize = 40;
                LoginViewModelInstance.LoginMainLabelHeight = 20;
                LoginViewModelInstance.LoginEmptyLabelHeight = 0;
                LoginViewModelInstance.InputHeight = 38;
                LoginViewModelInstance.LoginNameFontSize = 14;
                LoginViewModelInstance.LoginSignButtonFontSize = 14;
                LoginViewModelInstance.RegisterFontSize = 10;
            }
            else
            {
                LoginViewModelInstance.ButtonRowHeight = 100;
                LoginViewModelInstance.LoginIconSize = 80;
                LoginViewModelInstance.LoginMainLabelHeight = 50;
                LoginViewModelInstance.LoginEmptyLabelHeight = 8;
                LoginViewModelInstance.InputHeight = 44;
                LoginViewModelInstance.LoginNameFontSize = 24;
                LoginViewModelInstance.LoginSignButtonFontSize = 16;
                LoginViewModelInstance.RegisterFontSize = 14;
            }

        }

        private async void btnRegister_Clicked(object sender, EventArgs e)
        {

            await Common.OpenWebPage("http://ti.portalishkollor.al/register");

        }
        #endregion






        private void ContentPage_SizeChanged(object sender, EventArgs e)
        {
            try
            {


                SetBackground();


            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee, "GetExecutingAssembly problem");
            }
        }
        private void SetBackground()
        {
            if (this.Height < this.Width)
            {
                //LANDSCAPE
                LoginViewModelInstance.PageOrientation = Orientation.Landscape;
                this.BackgroundImageSource = "landscape.jpg";
                //centralColumn.Width = new GridLength(1, GridUnitType.Star);
                if (IsSmallScreen == true)
                {
                    //PHONE
                    LoginViewModelInstance.ButtonColumnWidth = 140;
                    LoginViewModelInstance.LeftButtonPoligonTranslationX = 88;
                    LoginViewModelInstance.RightButtonPoligonTranslationX = 26;
                    LoginViewModelInstance.LoginEmptyLabelHeight = 0;

                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 36));

                    // poligonHelp.Points = myPointCollection;

                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(30, 50));
                    myPointCollection.Add(new Point(0, 50));

                    //  poligonExit.Points = myPointCollection;
                }
                else
                {
                    //TABLET
                    LoginViewModelInstance.LeftButtonPoligonTranslationX = 88;
                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 88));

                    //   poligonHelp.Points = myPointCollection;

                    LoginViewModelInstance.RightButtonPoligonTranslationX = -26;
                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(40, 100));
                    myPointCollection.Add(new Point(0, 120));

                    //  poligonExit.Points = myPointCollection;
                }
            }
            else
            {
                //PORTRAIT
                LoginViewModelInstance.PageOrientation = Orientation.Portrait;
                this.BackgroundImageSource = "portret.jpg";
                if (IsSmallScreen == true)
                {
                    //PHONE
                    LoginViewModelInstance.ButtonColumnWidth = 70;
                    LoginViewModelInstance.LeftButtonPoligonTranslationX = 68;
                    LoginViewModelInstance.RightButtonPoligonTranslationX = -26;
                    LoginViewModelInstance.LoginEmptyLabelHeight = 2;

                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(30, 0));
                    myPointCollection.Add(new Point(0, 36));

                    //  poligonHelp.Points = myPointCollection;
                }
                else
                {
                    //TABLET

                    LoginViewModelInstance.LeftButtonPoligonTranslationX = 88;
                    PointCollection myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(0, 0));
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(0, 90));

                    //  poligonHelp.Points = myPointCollection;

                    myPointCollection = new PointCollection();
                    myPointCollection.Add(new Point(40, 0));
                    myPointCollection.Add(new Point(40, 100));
                    myPointCollection.Add(new Point(0, 100));

                    //  poligonExit.Points = myPointCollection;
                    LoginViewModelInstance.RightButtonPoligonTranslationX = -26;
                }

            }
        }

        private async void btnBack_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new Login();
        }
    }

}