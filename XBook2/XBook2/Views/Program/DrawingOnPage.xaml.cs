﻿using eBookGraphics;
using KlasimeBase;
using KlasimeBase.Log;
using Newtonsoft.Json;
using SharedKlasime;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Shapes;
using Xamarin.Forms.Xaml;
using XBook2.Touch;

namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DrawingOnPage : ContentPage
    {
        public static string user = "program";
        public ApiLog logInstance =  ApiLog.GetInstance();
        #region VARIABLES
        // SKBitmap bmpAudio;
        CUser loggedUser;
        int bookid;
        SKCanvas Canvas;
        private SkiaSharp.SKSurface surface;
        SKBitmap myBitmap;
        bool lineDraw = false;
        bool penDraw = false;
        bool startDrawingLine = false;
        bool startDrawingPen = false;
        SKPoint pointStartLine;
        SKPoint pointEndLine;
        //SKPoint pointDrag;
        SKPoint pointStartPen;
        
        Hashtable htDraws = new Hashtable();
        List<SKPoint> listPoints = new List<SKPoint>();
        int lineCounter = 0;
        int drawCounter = 0;
        Book currentBook = null;
        public CDrawItemOnPage DrawItemOnPage { get ; set; }
        DrawLineWidth MyDrawLineWidth { get; set; }
        SKColor MyLineColor { get; set; }
        DrawLineColor ColorMyDrawLineColorColor { get; set; }
        int PageNumber { get; set; }
        //bool drag = false;
        
       

        SKPaint paint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.White,
            StrokeWidth = 10,
            StrokeCap = SKStrokeCap.Round,
            StrokeJoin = SKStrokeJoin.Round
        };
        #endregion

        #region CONSTRUCTOR
        public DrawingOnPage(CUser muser, int mbookid, Book _currentBook, int pageNumber, DrawType drawingType, DrawLineWidth drawLineWidth, DrawLineColor sKColor)
        {
            PageNumber=pageNumber;
            if(drawingType==DrawType.Pen)
            {
                penDraw = true;
                lineDraw = false;
            }
            else
            {
                penDraw = false;
                lineDraw = true;
            }
            MyDrawLineWidth= drawLineWidth;            
          
            ColorMyDrawLineColorColor = sKColor;
          
            InitializeComponent();
            loggedUser = muser;
            bookid = mbookid;
            currentBook = _currentBook;
            
            DrawItemOnPage =  currentBook.GetCustomDrawing("drawing", PageNumber);
            if(DrawItemOnPage==null)
            {
                DrawItemOnPage = new CDrawItemOnPage();
                DrawItemOnPage.PageNumber= PageNumber;
            }
        }
        #endregion

        #region EVENTS
        private void OnTouchEffectAction(object sender, Touch.TouchActionEventArgs args)
        {
            switch (args.Type)
            {

                case TouchActionType.Pressed:
                    if (penDraw == true)
                    {
                        if (!DrawItemOnPage.inProgressPaths.ContainsKey(args.Id))
                        {
                            SKPath path = new SKPath();
                            SKPoint myPoint = ConvertToPixel(args.Location);
                            path.MoveTo(myPoint);
                            DrawItemOnPage.myLocationList.Add(myPoint);
                            DrawItemOnPage.inProgressPaths.Add(args.Id, path);

                        }


                    }
                    if (lineDraw == true)
                    {
                        startDrawingLine = true;
                        pointStartLine = ConvertToPixel(args.Location);
                        pointEndLine = pointStartLine;
                    }
                    skiaView.InvalidateSurface();
                    break;

                case TouchActionType.Moved:
                    if (penDraw == true)
                    {
                        if (DrawItemOnPage.inProgressPaths.ContainsKey(args.Id))
                        {
                            SKPath path = DrawItemOnPage.inProgressPaths[args.Id];
                            SKPoint myPoint = ConvertToPixel(args.Location);
                            path.LineTo(myPoint);
                            DrawItemOnPage.myLocationList.Add(myPoint);
                        }

                    }

                    if (lineDraw == true)
                    {
                        if (startDrawingLine)
                        {
                            pointEndLine = ConvertToPixel(args.Location);
                            skiaView.InvalidateSurface();
                        }
                    }
                    skiaView.InvalidateSurface();
                    break;

                case TouchActionType.Released:
                    if (penDraw == true)
                    {
                        if (DrawItemOnPage.inProgressPaths.ContainsKey(args.Id))
                        {
                            MyPenList mpl = new MyPenList(DrawItemOnPage.inProgressPaths[args.Id], ColorMyDrawLineColorColor, MyDrawLineWidth);
                            DrawItemOnPage.completedDrawingPaths.Add(mpl);
                            DrawItemOnPage.htPenDraws.Add(iPenDrawCounter++, DrawItemOnPage.myLocationList);
                            DrawItemOnPage.inProgressPaths.Remove(args.Id);
                            DrawItemOnPage.myLocationList = new List<SKPoint>();
                            skiaView.InvalidateSurface();

                        }
                    }

                    if (lineDraw == true)
                    {
                        if (startDrawingLine)
                        {
                            MyLineList ml = new MyLineList(pointStartLine, pointEndLine, MyDrawLineWidth, ColorMyDrawLineColorColor);
                            DrawItemOnPage.htLines.Add(lineCounter++, ml);
                            startDrawingLine = false;
                        }
                    }
                    StoreCustomDrawing();
                    break;

                case TouchActionType.Cancelled:
                    if (DrawItemOnPage.inProgressPaths.ContainsKey(args.Id))
                    {
                        DrawItemOnPage.inProgressPaths.Remove(args.Id);
                        skiaView.InvalidateSurface();
                    }
                    break;
            }
        }
        int iPenDrawCounter;
        private async void btnExit_Clicked(object sender, EventArgs e)
        {
            try
            {
                Application.Current.MainPage = new MainPage(loggedUser, bookid, currentBook);
                //await Navigation.PushAsync(new MainPage(loggedUser, bookid));
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            try
            {

                lbPen.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_pen_free"); //"Pen";
                btnPen.Source = ImageSource.FromResource("XBook2.media.load.orange.pen-button-up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                lblLine.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_pen_direct"); //"Line";
                btnLine.Source = ImageSource.FromResource("XBook2.media.load.orange.line_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                lblUnselect.Text = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "button_unselect"); //"Unselect";
                btnUnselect.Source = ImageSource.FromResource("XBook2.media.load.orange.unselect_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);


                SetLineColor();
                SetLineSize();

                lblTitle.Text = currentBook.Description;

                #region GetCurrentPageImage-decrypt
                string pageImageCorporate = currentBook.getPagePath(PageNumber);
                string resourceID = pageImageCorporate;


                using (var streamReader = new StreamReader(resourceID))
                {
                    var bytes = default(byte[]);
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        bytes = memstream.ToArray();
                        
                            byte[] res = Common.encodeByteArray(bytes);
                            myBitmap = SKBitmap.Decode(res);
                            Stream stream1 = new MemoryStream(res);
                            imgRegularPage.Source = ImageSource.FromStream(() => stream1);
                       
                        // encode the image (defaults to PNG)


                    }
                }

                #endregion

            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void btnPen_Clicked(object sender, EventArgs e)
        {
            penDraw = true;
            lineDraw = false;

            btnPen.Source = ImageSource.FromResource("XBook2.media.load.orange.pen-button-down.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            btnLine.Source = ImageSource.FromResource("XBook2.media.load.orange.line_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);


            startDrawingLine = false;
            skiaView.InvalidateSurface();
        }

        private void btnLine_Clicked(object sender, EventArgs e)
        {
            penDraw = false;

            btnLine.Source = ImageSource.FromResource("XBook2.media.load.orange.line_down.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            btnPen.Source = ImageSource.FromResource("XBook2.media.load.orange.pen-button-up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

            lineDraw = true;
            skiaView.InvalidateSurface();
        }

        private async void btnUnselect_Clicked(object sender, EventArgs e)
        {
            penDraw = false;
            lineDraw = false;
            startDrawingLine = false;
            DrawItemOnPage.completedDrawingPaths.Clear();
            listPoints.Clear();
            DrawItemOnPage.htLines.Clear();
            htDraws.Clear();
            drawCounter = 0;
            lineCounter = 0;

            pointStartLine = new SKPoint(0, 0);
            pointEndLine = new SKPoint(0, 0);

            await DropCustomDrawing();

            DrawItemOnPage = new CDrawItemOnPage();
            DrawItemOnPage.PageNumber= PageNumber;


            skiaView.InvalidateSurface();
        }

        private void skiaView_PaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {

            SKCanvas canvas = args.Surface.Canvas;
            canvas.Clear();

            SKPaint lpaint = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                Color = SKColors.White,
                StrokeWidth = 10,
                StrokeCap = SKStrokeCap.Round,
                StrokeJoin = SKStrokeJoin.Round
            };

            if (DrawItemOnPage != null)
            {
                foreach (MyPenList mpl in DrawItemOnPage.completedDrawingPaths)
                {

                    switch (mpl.DrawLineColorEnum)
                    {
                        case DrawLineColor.LightBlue:
                            lpaint.Color = SKColors.LightBlue;

                            break;
                        case DrawLineColor.LightPink:
                            lpaint.Color = SKColors.LightPink;

                            break;
                        case DrawLineColor.LightGreen:
                            lpaint.Color = SKColors.LightGreen;

                            break;
                    }
                    lpaint.StrokeWidth = (float)mpl.MyDrawLineWidth;
                    canvas.DrawPath(mpl.sKPath, lpaint);
                }

                foreach (SKPath path in DrawItemOnPage.inProgressPaths.Values)
                {
                    canvas.DrawPath(path, paint);
                }

                #region startDrawingLine
                {
                    canvas.DrawLine(pointStartLine, pointEndLine, paint);


                    for (int i = 0; i < DrawItemOnPage.htLines.Count; i++)
                    {
                        MyLineList ml = DrawItemOnPage.htLines[i];
                        switch (ml.customDrawLineColor)
                        {
                            case DrawLineColor.LightBlue:
                                lpaint.Color = SKColors.LightBlue;

                                break;
                            case DrawLineColor.LightPink:
                                lpaint.Color = SKColors.LightPink;

                                break;
                            case DrawLineColor.LightGreen:
                                lpaint.Color = SKColors.LightGreen;

                                break;
                        }
                        lpaint.StrokeWidth = (float)ml.customDrawLineWidth;
                        canvas.DrawLine(ml.PointStart, ml.PointEnd, lpaint);
                    }
                }
            }
            #endregion

        }

        private void btnLineSizetThin_Clicked(object sender, EventArgs e)
        {
            MyDrawLineWidth = DrawLineWidth.Thin;

            SetLineSize();
        }

        private void btnLineSizeMedium_Clicked(object sender, EventArgs e)
        {
            MyDrawLineWidth = DrawLineWidth.Medium;
            SetLineSize();
        }

        private void btnLineSizeThick_Clicked(object sender, EventArgs e)
        {
            MyDrawLineWidth = DrawLineWidth.Thick;
            SetLineSize();
        }

        private void btnLineColorRose_Clicked(object sender, EventArgs e)
        {
            ColorMyDrawLineColorColor = DrawLineColor.LightPink;
            SetLineColor();
        }

        private void btnLineColorBlue_Clicked(object sender, EventArgs e)
        {
            ColorMyDrawLineColorColor = DrawLineColor.LightBlue;
            SetLineColor();
        }

        private void btnLineColorGreen_Clicked(object sender, EventArgs e)
        {
            ColorMyDrawLineColorColor = DrawLineColor.LightGreen;
            SetLineColor();
        }

        #endregion

        #region METHODS
        private async void StoreCustomDrawing()
        {
            await currentBook.AddCustomDrawing(DrawItemOnPage, "drawing");
        }
        private async Task<CDrawItemOnPage> GetManualDrawing()
        {
            return currentBook.GetCustomDrawing("drawing", PageNumber);
        }
        private async Task<CDrawItemOnPage> DropCustomDrawing()
        {

            DrawItemOnPage = currentBook.DropCustomDrawing("drawing", PageNumber);
            return DrawItemOnPage;
        }
        private void SetLineSize()
        {
            try
            {
                paint.StrokeWidth = (float)MyDrawLineWidth;

                switch (MyDrawLineWidth)
                {
                    case DrawLineWidth.Thin:
                        btnLineSizetThin.Source = ImageSource.FromResource("XBook2.media.load.brush1_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineSizeMedium.Source = ImageSource.FromResource("XBook2.media.load.brush2_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineSizeThick.Source = ImageSource.FromResource("XBook2.media.load.brush3_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        break;
                    case DrawLineWidth.Medium:
                        btnLineSizeMedium.Source = ImageSource.FromResource("XBook2.media.load.brush2_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineSizetThin.Source = ImageSource.FromResource("XBook2.media.load.brush1_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineSizeThick.Source = ImageSource.FromResource("XBook2.media.load.brush3_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        break;
                    case DrawLineWidth.Thick:
                        btnLineSizeThick.Source = ImageSource.FromResource("XBook2.media.load.brush3_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineSizetThin.Source = ImageSource.FromResource("XBook2.media.load.brush1_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineSizeMedium.Source = ImageSource.FromResource("XBook2.media.load.brush2_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        break;
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
           
        }
        private void SetLineColor()
        {
            try
            {
                switch (ColorMyDrawLineColorColor)
                {
                    case DrawLineColor.LightBlue:
                        paint.Color = SKColors.LightBlue;
                        btnLineColorBlue.Source = ImageSource.FromResource("XBook2.media.load.color2_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineColorRose.Source = ImageSource.FromResource("XBook2.media.load.color1_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineColorGreen.Source = ImageSource.FromResource("XBook2.media.load.color3_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        break;
                    case DrawLineColor.LightPink:
                        paint.Color = SKColors.LightPink;
                        btnLineColorRose.Source = ImageSource.FromResource("XBook2.media.load.color1_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineColorBlue.Source = ImageSource.FromResource("XBook2.media.load.color2_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineColorGreen.Source = ImageSource.FromResource("XBook2.media.load.color3_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);

                        break;
                    case DrawLineColor.LightGreen:
                        paint.Color = SKColors.LightGreen;
                        btnLineColorGreen.Source = ImageSource.FromResource("XBook2.media.load.color3_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineColorRose.Source = ImageSource.FromResource("XBook2.media.load.color1_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        btnLineColorBlue.Source = ImageSource.FromResource("XBook2.media.load.color2_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        break;
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            
          
            
        }
      
    
        SKPoint ConvertToPixel(Point pt)
        {
            return new SKPoint((float)(skiaView.CanvasSize.Width * pt.X / skiaView.Width),
                              (float)(skiaView.CanvasSize.Height * (pt.Y - lblTitle.Height) / skiaView.Height));
        }

        #endregion

    }
}