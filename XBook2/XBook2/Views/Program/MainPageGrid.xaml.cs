﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPageGrid : ContentPage
    {
        public MainPageGrid()
        {
            InitializeComponent();
        }

        private void btnMenu_Clicked(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            lblStatus.Text = btn.Text;
        }
       
    }
}