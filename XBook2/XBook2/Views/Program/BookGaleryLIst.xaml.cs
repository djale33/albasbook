﻿using KlasimeBase.Log;
using SharedKlasime;
using System;
using System.Reflection;
using System.Runtime.ConstrainedExecution;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2.Model;
using XBook2.ViewModels;


namespace XBook2.Views.Program
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookGaleryList : AlbasContentPage
    {

        #region VARIABLES
        public ApiLog logInstance = ApiLog.GetInstance();
        public BookGaleryListViewModel MyViewModelInstance;
        private uint delay = 80;
        Easing animationtype = Easing.Linear;

        #endregion

        #region PROPERTIES
        public AlbasBook AlbasBook { get; set; }
        BookItem CurrentBookItem { get; set; }

        double ButtonTranslationY{ get; set; }
        double BottomMenuWidth { get; set; }

        #endregion

        #region CONSTRUCTOR
        public BookGaleryList(CUser _loggedUser, bool _IsSmallScreen):base(_loggedUser.firstname + "-" + _loggedUser.lastname)
        {
            
            
           
            InitializeComponent();
            MyViewModelInstance= new BookGaleryListViewModel(_loggedUser, _IsSmallScreen);
            
            BindingContext = MyViewModelInstance;


        }
        protected async override void OnAppearing()
        {

            base.OnAppearing();
            MyViewModelInstance.WaitOnBook = true;
            await MyViewModelInstance.PrepareBookList();
            MyViewModelInstance.WaitOnBook = false;
           
        }
        #endregion

        #region EVENTS

        private void ContentPage_LayoutChanged(object sender, EventArgs e)
        {
            SetBackground(MyViewModelInstance.User);
            SetBackgroundCustom();
        }
        private async void mainMenu_Clicked(object sender, EventArgs e)
        {
            //if (menuSettings.TranslationX < 0)
            //{

            //    await menuSettings.TranslateTo(0, 0, delay, animationtype);
            //    await menuBooks.TranslateTo(0, 0, delay, animationtype);
            //    await menuTeams.TranslateTo(0, 0, delay, animationtype);
            //    await menuCalendar.TranslateTo(0, 0, delay, animationtype);
            //    await menuKookFor.TranslateTo(0, 0, delay, animationtype);
            //    if (MyViewModelInstance.IsSmallScreen == true)
            //    {
            //        bottomGrid.WidthRequest = 800;
            //    }
            //    else
            //    {
            //        if (MyViewModelInstance.DeviceOrientation == ItemsLayoutOrientation.Vertical)
            //        {

            //        }
            //        else
            //        {
            //            bottomGrid.WidthRequest = this.Width;
            //        }
            //    }
            //}
            //else
            //{
            //    double stepBack = 0;
            //    double step = -300;
            //    stepBack += step;
            //    await menuSettings.TranslateTo(stepBack, 0, delay, animationtype);
            //    stepBack += step;
            //    await menuBooks.TranslateTo(stepBack, 0, delay, animationtype);
            //    stepBack += step;
            //    await menuTeams.TranslateTo(stepBack, 0, delay, animationtype);
            //    stepBack += step;
            //    await menuCalendar.TranslateTo(stepBack, 0, delay, animationtype);
            //    stepBack += step;
            //    await menuKookFor.TranslateTo(stepBack, 0, delay, animationtype);
            //    bottomGrid.WidthRequest = this.Width;
            //}
        }


        private void btnBack_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new MainMenu(MyViewModelInstance.LoggedUser, MyViewModelInstance.IsSmallScreen);
        }

        #endregion

        #region METHODS
        public void SetBackgroundCustom()
        {
            try
            {

                var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;
                if (mainDisplayInfo != null)
                {
                    bottomGrid.WidthRequest = this.Width;
                    if (this.Height > this.Width)
                    {
                        //PORTRAIT
                      
                        //CollectionOne.ItemsLayout = new LinearItemsLayout(ItemsLayoutOrientation.Vertical);
                        // HeaderImageAndUserName.Orientation = StackOrientation.Vertical;
                        MyViewModelInstance.DeviceOrientation = ItemsLayoutOrientation.Vertical;
                        if (MyViewModelInstance.IsSmallScreen == true)
                        {
                            //PHONE PORTRAIT
                            MyViewModelInstance.PoligonHeightRequest = 80;
                            MyViewModelInstance.MenuTranslationY = -115;
                            MyViewModelInstance.MenuHeightRequest = 100;
                            MyViewModelInstance.BehaviourBookHeight = 200;
                            MyViewModelInstance.FrameMargin = new Thickness(150, 0, 150, 0);
                            MyViewModelInstance.FrameWidthRequest = 180;

                            MyViewModelInstance.BookRowHeightLow = 5;
                            MyViewModelInstance.PeekAreaInsets = 98;
                            MyViewModelInstance.CarouselBookHeight = 550;
                            MyViewModelInstance.BottomOffset = 1;
                            MyViewModelInstance.ButtonTranslationY = -80;

                            BottomMenuWidth = 500;
                        }
                        else
                        {
                            //TABLET PORTRAIT
                            MyViewModelInstance.PoligonHeightRequest = 100;
                            MyViewModelInstance.MenuTranslationY = -140;
                            MyViewModelInstance.MenuHeightRequest = 140;
                            MyViewModelInstance.BehaviourBookHeight = 500;
                            MyViewModelInstance.FrameMargin = new Thickness(150, 0, 150, 0);
                            MyViewModelInstance.FrameWidthRequest = 80;

                            MyViewModelInstance.BookRowHeightLow = 5;
                            MyViewModelInstance.PeekAreaInsets = 200;
                            MyViewModelInstance.CarouselBookHeight = 1100;
                            MyViewModelInstance.BottomOffset = 20;
                            //MyViewModelInstance.ButtonTranslationY = -80;
                            BottomMenuWidth = 500;
                        }

                    }
                    else
                    {
                        //LANDSCAPE                        
                   
                        MyViewModelInstance.DeviceOrientation = ItemsLayoutOrientation.Horizontal;
                        //CollectionOne.ItemsLayout = new LinearItemsLayout(ItemsLayoutOrientation.Horizontal);

                        if (MyViewModelInstance.IsSmallScreen == true)
                        {
                            //Phone LANDSCAPE
                            MyViewModelInstance.PoligonHeightRequest = 80;
                            MyViewModelInstance.MenuTranslationY = -115;
                            MyViewModelInstance.MenuHeightRequest = 100;

                            MyViewModelInstance.BehaviourBookHeight = 220;
                            MyViewModelInstance.FrameMargin = new Thickness(150, 0, 150, 0);
                            MyViewModelInstance.FrameWidthRequest = 100;

                            MyViewModelInstance.BookRowHeightLow = 5;
                            MyViewModelInstance.PeekAreaInsets = 260;
                            MyViewModelInstance.CarouselBookHeight = 150;
                            MyViewModelInstance.BottomOffset = 1;
                            MyViewModelInstance.ButtonTranslationY = 1;

                            BottomMenuWidth = 500;
                        }
                        else
                        {
                            //Tablet LANDSCAPE
                            MyViewModelInstance.PoligonHeightRequest = 100;
                            MyViewModelInstance.MenuTranslationY = -130;
                            MyViewModelInstance.MenuHeightRequest = 140;
                            MyViewModelInstance.BehaviourBookHeight = 400.0;
                            MyViewModelInstance.FrameMargin = new Thickness(10, 100, 10, 100);
                            MyViewModelInstance.FrameWidthRequest = 80;

                            MyViewModelInstance.BookRowHeightLow = 5;
                            MyViewModelInstance.PeekAreaInsets = 500;
                            MyViewModelInstance.CarouselBookHeight = 600;
                            MyViewModelInstance.BottomOffset = 20;
                            //MyViewModelInstance.ButtonTranslationY = -80;
                            BottomMenuWidth = 500;
                        }
                    }
                    carouselViewParallaxBehaviorHeighValue.BookHeight = MyViewModelInstance.BehaviourBookHeight;
                    foreach (BookItem item in MyViewModelInstance.Books)
                    {
                        item.FrameWidthRequest = MyViewModelInstance.FrameWidthRequest;

                    }
                }


            }
            catch (Exception ee)
            {

                logInstance.Log(MyViewModelInstance.User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion

        private async void btnHelp_Clicked(object sender, EventArgs e)
        {
            await OpenWebPage();
        }
        public static async Task OpenWebPage()
        {
            try
            {
                Uri uri = new Uri("http://www.portalishkollor.al/");
                await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
            }
            catch (Exception ee)
            {
                //logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), ee);
            }
        }
    }
}