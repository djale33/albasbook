﻿using KlasimeBase;
using KlasimeBase.Log;
using KlasimeBase.Multimedia;
using KlasimeBase.Preview;
using KlasimeViews;
using SharedKlasime;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using File = System.IO.File;
using XBook2.Views.Program;
using XBook2.Helpers;
using XBook2.ViewModels;

namespace XBook2.Views.Multimedia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FileControl : ContentPage
    {
        public static string user = "program";
        public ApiLog logInstance =  ApiLog.GetInstance();
        #region VARIABLES
        public MultimediaControlViewModel MyViewModelInstance;
        private int imageStep = 10;
        float irectHeight = 60;
        private string filePath;
        private int imagePossition = 0;
        private List<Photo> images = new List<Photo>();
        private Hashtable listPhotoRect = new Hashtable();
        SKCanvas canvas;
        bool BookEncoding = true;
        public event EventHandler closeEvent;

        bool AddNew = false;
        Anno myAnno = null;
        CUser loggedUser;
        Book currentBook;
        string TitleCode;

        private string newFileName = "";
        private byte[] newFileData;
        public Hashtable documentList = new Hashtable();
        #endregion

        #region PROPERTIES
        public CUser LoggedUser { get => loggedUser; set => loggedUser = value; }
        public Book CurrentBook { get => currentBook; set => currentBook = value; }
        public string NewFileName { get => newFileName; set => newFileName = value; }
        public byte[] NewFileData { get => newFileData; set => newFileData = value; }
        #endregion

        #region CONSTRUCTOR
        public FileControl(Book _currentBook, CUser _loggedUser)
        {
            user = _loggedUser.firstname + "-" + _loggedUser.lastname;
            CurrentBook = _currentBook;
            if (CurrentBook.CurrentPage == 0)
            {
                logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "CurrentBook.CurrentPage==0");
            }
            LoggedUser = _loggedUser;
          
            InitializeComponent();
            MyViewModelInstance = new MultimediaControlViewModel(LoggedUser);

            BindingContext = MyViewModelInstance;
        }
        public FileControl(Book currentBook, CUser loggedUser, int _Width, int height, Anno findAno, bool addNew)
        {
            //TitleCode = title;
            user = loggedUser.firstname + "-" + loggedUser.lastname;
            CurrentBook = currentBook;
            if (CurrentBook.CurrentPage == 0)
            {
                logInstance.Log(user,Severity.Trace, MethodBase.GetCurrentMethod(), null, "CurrentBook.CurrentPage==0");
            }
            LoggedUser = loggedUser;
            myAnno = findAno;
            InitializeComponent();
            MyViewModelInstance = new MultimediaControlViewModel(loggedUser);

            BindingContext = MyViewModelInstance;
            this.WidthRequest = _Width;
            this.HeightRequest = _Width;
            AddNew = addNew;
            if (addNew == false)
            {

                //txtNote.IsVisible = false;

                Common.SetTitle(findAno.Hint.Text, this.lblTitle);
                // Common.SetNote(findAno.Action.BodyText, this.lblNote);
            }
            else
            {
                //txtNote.IsVisible = true;
                this.lblTitle.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "");
                switch (myAnno.MultimediaTypeToAdd)
                {
                    case MultimediaType.PhotoGalery:
                        lblTitle.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_photo_title");
                        lblMainDesc.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_photo_info");
                        lblGaleryName.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_photo_gallery_name");
                        lblFileName.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_photo_name");

                        lblDesc.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_document_description");
                        break;
                    case MultimediaType.Audio:
                        lstDocumentList.IsVisible = false;
                        btnMinusFile.IsVisible = false;
                        btnPlusFile.IsVisible = false;
                        btnSave.IsVisible = true;

                        lblTitle.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_sound_title");
                        lblGaleryName.Text = "";
                        lblFileName.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_sound_name");

                        lblDesc.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_document_description");
                        break;
                    case MultimediaType.Video:
                        lstDocumentList.IsVisible = false;
                        btnMinusFile.IsVisible = false;
                        btnPlusFile.IsVisible = false;
                        btnSave.IsVisible = true;

                        lblTitle.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_video_title");
                        lblGaleryName.Text = "";
                        lblFileName.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_video_name");

                        lblDesc.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_document_description");
                        break;
                    case MultimediaType.File:
                        lstDocumentList.IsVisible = false;
                        btnMinusFile.IsVisible = false;
                        btnPlusFile.IsVisible = false;
                        btnSave.IsVisible = true;
                        imgeMainIcon.IsVisible = false;
                        txtGaleryName.IsVisible = false;

                        lblTitle.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_document_name");
                        lblGaleryName.Text = "";
                        lblFileName.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_video_name");

                        lblDesc.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "explanations_add_document_description");
                        break;

                }


            }
            //this.BackgroundColor = Color.FromRgb(95, 61, 142);
            this.BackgroundColor = Color.FromRgb(99, 101, 104);
            lblTitle.BackgroundColor = Color.FromRgb(95, 61, 142);
            lblMainDesc.BackgroundColor = Color.FromRgb(95, 61, 142);
            btnClose.BackgroundColor = Color.FromRgb(95, 61, 142);
            //if (findAno.Cutom == false)
            //{
            //    btnDelete.IsEnabled = false;
            //}
        }

        #endregion

        #region CustomEvents
        public delegate void MultimediaRemveHandler(object sender, MultimediaRemoveEventArgs e);
        public event MultimediaRemveHandler OnRemoveMultimediaHandler;
        private void OnDeleteMultimedia(object sender, MultimediaRemoveEventArgs e)
        {
            if (OnRemoveMultimediaHandler != null)
            {
                OnRemoveMultimediaHandler(sender, e);
            }
        }
        #endregion

        #region EVENTS
        private void lstDocumentList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                string selected = e.SelectedItem.ToString();
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        private async void btnSave_Clicked(object sender, EventArgs e)
        {
            await SaveNewMultimedia();
        }
        private async void btnAdd_Clicked(object sender, EventArgs e)
        {
            await AddNewFile();
        }
        private async void btnMinusFile_Clicked(object sender, EventArgs e)
        {
            await RemoveFileInPhotoLib();
        }
        private async void btnPlusFile_Clicked(object sender, EventArgs e)
        {
            await AddFileToPhotoLib();
        }
        /// <summary>
        /// close event is used for Adding new custom Note
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btnClose_Clicked(object sender, EventArgs e)
        {
            //var handler = closeEvent;
            //if (handler != null)
            //{
            //    handler(this, EventArgs.Empty);
            //}
            Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook);
        }
        private async void btnDelete_Clicked(object sender, EventArgs e)
        {
            try
            {
                bool answer = await DisplayAlert("Alert, File ", "Are you sure?", "Yes", "No");
                if (answer)
                {
                    MultimediaRemoveEventArgs pe4 = new MultimediaRemoveEventArgs(myAnno);
                    OnRemoveMultimediaHandler(this, pe4);
                    logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "The File was deleted " + this.lblFileName.Text);
                    
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook);
        }

        private void Grid_Focused(object sender, FocusEventArgs e)
        {
            this.BackgroundColor = Color.FromRgb(14, 75, 42);
        }
        #endregion

        #region METHODS
        /// <summary>
        /// add new file from disk
        /// </summary>
        /// <returns></returns>
        private async Task AddNewFile()
        {
            try
            {
                IPhotoLibrary photoLibrary = DependencyService.Get<IPhotoLibrary>();
                NewFile newFile = new NewFile();

                switch (myAnno.MultimediaTypeToAdd)
                {
                    case MultimediaType.PhotoGalery:

                        var filep = await FilePicker.PickAsync();
                        if (filep != null)
                        {
                            newFile = new NewFile();
                            newFile.FullPath = filep.FullPath;
                            newFile.Name = filep.FileName;
                            var stream = await filep.OpenReadAsync();
                            NewFileData = ReadFully(stream);
                            txtFile.Text = newFile.Name;
                        }
                        break;

                    case MultimediaType.Audio:
                        var audioFile = await FilePicker.PickAsync();
                        if (audioFile != null)
                        {
                            newFile = new NewFile();
                            newFile.FullPath = audioFile.FullPath;
                            newFile.Name = audioFile.FileName;
                            var stream = await audioFile.OpenReadAsync();
                            NewFileData = ReadFully(stream);
                            txtFile.Text = newFile.Name;
                            NewFileName = newFile.Name;
                        }                      
                        break;

                    case MultimediaType.Video:
                        var videoFile = await FilePicker.PickAsync();
                        if (videoFile != null)
                        {
                            newFile = new NewFile();
                            newFile.FullPath = videoFile.FullPath;
                            newFile.Name = videoFile.FileName;
                            var stream = await videoFile.OpenReadAsync();
                            NewFileData = ReadFully(stream);
                            txtFile.Text = newFile.Name;
                            NewFileName = newFile.Name;
                        }                       
                        break;

                    case MultimediaType.File:
                        try
                        {
                            var file = await FilePicker.PickAsync();
                            if(file != null)
                            {
                                newFile = new NewFile();
                                newFile.FullPath = file.FullPath;
                                newFile.Name = file.FileName;
                                var stream = await file.OpenReadAsync();
                                NewFileData = ReadFully(stream);
                                txtFile.Text = newFile.Name;
                            }
                        }
                        catch (Exception ee)
                        {
                            logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                        }
                        break;
                }


            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }
        }
        /// <summary>
        /// remove file from hashtable
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// add file to hashtable
        /// </summary>
        /// <returns></returns>
        private async Task AddFileToPhotoLib()
        {
            try
            {
                List<string> listSource = new List<string>();
                if (documentList.ContainsKey(txtFile.Text) == false)
                {
                    NewDocument nd = new NewDocument(txtFile.Text, NewFileName, txtDesc.Text,newFileData);
                    documentList.Add(txtFile.Text, nd);
                }
                foreach (DictionaryEntry de in documentList)
                {
                    listSource.Add(de.Key.ToString());
                }
                lstDocumentList.ItemsSource = listSource;
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        private async Task RemoveFileInPhotoLib()
        {
            try
            {
                if (documentList.ContainsKey(txtFile.Text) == true)
                {
                    documentList.Remove(txtFile.Text);
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        private async Task SaveNewMultimedia()
        {
            try
            {
                //save note
                if (AddNew)
                {
                    myAnno.AnnoId = Guid.NewGuid().ToString();
                    Hint ht = new Hint();
                    ht.Text = this.lblTitle.Text;
                    myAnno.Hint = ht;


                    string destFileName = "";
                    switch (myAnno.MultimediaTypeToAdd)
                    {
                        case MultimediaType.Video:

                            destFileName = KlasimeBase.Common.GetLocalPath() + currentBook.BookPath + Common.GetPathSeparator() + "files" + Common.GetPathSeparator() + "pageConfig" + Common.GetPathSeparator() + NewFileName;
                            File.WriteAllBytes(destFileName, NewFileData);

                            myAnno.Action.ResourceContent = "pageConfig" + Common.GetPathSeparator() + NewFileName;
                            myAnno.Hint.Text = txtDesc.Text;
                            break;

                        case MultimediaType.Audio:

                            destFileName = KlasimeBase.Common.GetLocalPath() + currentBook.BookPath + Common.GetPathSeparator() + "files" + Common.GetPathSeparator() + "pageConfig" + Common.GetPathSeparator() + NewFileName;
                            File.WriteAllBytes(destFileName, NewFileData);

                            myAnno.Action.AudioURL = "pageConfig" + Common.GetPathSeparator() + NewFileName;
                            myAnno.Hint.Text = txtDesc.Text;
                            break;


                        case MultimediaType.PhotoGalery:
                            List<Photo> myPhotoList = new List<Photo>();
                            //from list
                            foreach (DictionaryEntry de in documentList)
                            {
                                NewDocument dn = (NewDocument)de.Value;
                                Photo myPhoto = new Photo();
                                myPhoto.Url = "pageConfig" + Common.GetPathSeparator() + dn.FileName;
                                myPhoto.Title = dn.FileDesc;
                                myPhoto.Description = txtDesc.Text;
                                destFileName = KlasimeBase.Common.GetLocalPath() + currentBook.BookPath + Common.GetPathSeparator() + "files" + Common.GetPathSeparator() + "pageConfig" + Common.GetPathSeparator() + dn.FileName;
                                File.WriteAllBytes(destFileName, dn.FileData);

                                myPhotoList.Add(myPhoto);

                            }

                            myAnno.Action.Photos = myPhotoList;
                            
                            
                            
                            myAnno.Hint.Text = txtGaleryName.Text;
                            break;
                    }


                    //myAnno.Location.X= e.
                    myAnno.Location.Width = 0.033;
                    myAnno.Location.Height = 0.027;
                    bool res = await currentBook.AddCustomNote(myAnno, MultimediaPath.GetCustomMultimediaPath(myAnno.MultimediaTypeToAdd));
                    if (res)
                    {
                        logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), null, "new multimedia File aded: myAnnoID: " + myAnno.AnnoId);
                    }
                    //currentBook.getBookMedia();
                }
                //var handler = closeEvent;
                //if (handler != null)
                //{
                //    handler(this, EventArgs.Empty);
                //}
                Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook);
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
       /// <summary>
       /// get buteys from storage
       /// and encrypt it
       /// </summary>
       /// <param name="input"></param>
       /// <returns></returns>
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                byte[] result = Common.encodeByteArray(ms.ToArray());
                ms.Flush();
                return result;
            }
        }

        #endregion
      
    }
    public class NewDocument
    {
        string fileName;
        string filePath;
        string fileDesc;
        byte[] fileData;

        public string FileName { get => fileName; set => fileName = value; }
        public string FilePath { get => filePath; set => filePath = value; }
        public string FileDesc { get => fileDesc; set => fileDesc = value; }
        public byte[] FileData { get => fileData; set => fileData = value; }

        public NewDocument(string fileName, string filePath, string fileDesc, byte[] fileData)
        {
            FileName = fileName;
            FilePath = filePath;
            FileDesc = fileDesc;
            this.fileData = fileData;
            FileData = fileData;
        }
    }
}