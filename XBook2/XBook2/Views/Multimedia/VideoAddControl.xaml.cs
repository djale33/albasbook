﻿using KlasimeBase;
using KlasimeBase.Log;
using KlasimeBase.Preview;
using KlasimeViews;
using SharedKlasime;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XBook2.Views.Multimedia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VideoAddControl : Grid, IPreviewControlBase
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        public static string user = "VideoAddControl";
        #region VARIABLES
        private int imageStep = 10;
        float irectHeight = 60;
        private string filePath;
        private int imagePossition = 0;
        private List<Photo> images = new List<Photo>();
        private Hashtable listPhotoRect = new Hashtable();
        SKCanvas canvas;
        bool BookEncoding = true;
        public event EventHandler closeEvent;

        bool AddNew = false;
        Anno myAnno = null;
        CUser loggedUser;
        Book currentBook;
        string newVideo;
        string TitleCode;
        #endregion

        #region PROPERTIES
        public CUser LoggedUser { get => loggedUser; set => loggedUser = value; }
        public Book CurrentBook { get => currentBook; set => currentBook = value; }
        #endregion

        #region CONSTRUCTOR
        public VideoAddControl(Book currentBook, CUser loggedUser, int _Width, int height, Anno findAno, bool addNew, string title)
        {
            try
            {
                user = loggedUser.firstname + "-" + loggedUser.lastname;
                TitleCode = title;
                CurrentBook = currentBook;
                if (CurrentBook.CurrentPage == 0)
                {
                    logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "CurrentBook.CurrentPage==0");
                }
                LoggedUser = loggedUser;
                myAnno = findAno;
                InitializeComponent();
                this.WidthRequest = _Width;
                this.HeightRequest = _Width;
                AddNew = addNew;
                if (addNew == false)
                {

                    txtNote.IsVisible = false;

                    Common.SetTitle(findAno.Hint.Text, this.lblTitle);
                    Common.SetNote(findAno.Action.BodyText, this.lblNote);
                }
                else
                {
                    txtNote.IsVisible = true;
                    this.lblTitle.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, title);
                }
                //green template
                if (loggedUser.currentColorTempalte == ColorTemlates.Green)
                {
                    this.BackgroundColor = Color.FromRgb(14, 75, 42);
                    txtNote.BackgroundColor = Color.FromRgb(14, 75, 42);
                    lblNote.BackgroundColor = Color.FromRgb(14, 75, 42);
                }
                else
                {
                    this.BackgroundColor = Color.FromRgb(96, 96, 96);
                    txtNote.BackgroundColor = Color.FromRgb(64, 64, 64);
                    lblNote.BackgroundColor = Color.FromRgb(64, 64, 64);
                }

                if (findAno.Cutom == false)
                {
                    btnDelete.IsEnabled = false;
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
          
        }

        #endregion

        #region CustomEvents
        public delegate void MultimediaRemveHandler(object sender, MultimediaRemoveEventArgs e);
        public event MultimediaRemveHandler OnRemoveMultimediaHandler;
        private void OnDeleteMultimedia(object sender, MultimediaRemoveEventArgs e)
        {
            if (OnRemoveMultimediaHandler != null)
            {
                OnRemoveMultimediaHandler(sender, e);
            }
        }
        #endregion

        #region EVENTS
        /// <summary>
        /// close event is used for Adding new custom Note
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void btnClose_Clicked(object sender, EventArgs e)
        {
            try
            {
                //save note
                if (AddNew)
                {
                    myAnno.AnnoId = Guid.NewGuid().ToString();
                    Hint ht = new Hint();
                    ht.Text = this.lblTitle.Text;
                    myAnno.Hint = ht;

                    //TEST
                    string newFile = "";
                    if (TitleCode == "button_attach_video")
                    {
                        newFile = "fq-121-ngritja-e-flamurit.mp4";
                    }
                    if (TitleCode == "button_attach_audio")
                    {
                        newFile = "fq-121-ngritja-e-flamurit.mp4";
                    }
                    newVideo = "pageConfig" + Common.GetPathSeparator() + newFile;
                    myAnno.Action.ResourceContent = newVideo;

                    //myAnno.Location.X= e.
                    myAnno.Location.Width = 0.033;
                    myAnno.Location.Height = 0.027;
                    bool res = await currentBook.AddCustomNote(myAnno, MultimediaPath.GetCustomMultimediaPath(myAnno.MultimediaTypeToAdd));
                    if (res)
                    {
                        logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "new multimedia Note aded: myAnnoID: " + myAnno.AnnoId);
                    }
                    //currentBook.getBookMedia();
                }
                var handler = closeEvent;
                if (handler != null)
                {
                    handler(this, EventArgs.Empty);
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
          
        }
        private void btnDelete_Clicked(object sender, EventArgs e)
        {
            MultimediaRemoveEventArgs pe4 = new MultimediaRemoveEventArgs(myAnno);
            OnRemoveMultimediaHandler(this, pe4);
        }

        private void Grid_Focused(object sender, FocusEventArgs e)
        {
            this.BackgroundColor = Color.FromRgb(14, 75, 42);
        }
        #endregion
    }
}