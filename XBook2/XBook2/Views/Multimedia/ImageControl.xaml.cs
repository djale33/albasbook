﻿using eBookGraphics;
using KlasimeBase;
using KlasimeBase.Log;
using KlasimeBase.Preview;
using SharedKlasime;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2.Properties;
using XBook2.Views.Program;
using static Xamarin.Forms.Internals.GIFBitmap;


namespace XBook2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageControl : ContentPage
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        public ObservableCollection<PhotoItem> Monkeys { get; set; } = new ObservableCollection<PhotoItem>();
        #region Variables
        public static string user = "ImageControl";
        private int imageStep = 10;
        float irectHeight = 120;
        private string filePath;
        private int imagePossition = 0;
        private List<Photo> images = new List<Photo>();
        private Hashtable listPhotoRect = new Hashtable();
        SKCanvas canvas;
        bool bookEncoding = true;
        public event EventHandler closeEvent;
        Book currentBook;
        CUser loggedUser;
        bool CustomMultimedia = false;
        Anno myAnno = null;
        bool leftPageFullScreen;
        bool rightPageFullScreen;
        int PageNumLeft;
        #endregion

        #region Constructor
        public ImageControl(CUser _user, int _Width, int height, string title, Anno findAno, Book book, bool custom,
            bool _leftPageFullScreen, bool _rightPageFullScreen, int _PageNumLeft)
        {
            myAnno = findAno;
            CustomMultimedia = custom;
            leftPageFullScreen = _leftPageFullScreen;
            rightPageFullScreen = _rightPageFullScreen;
            PageNumLeft = _PageNumLeft;
            loggedUser = _user;
            user = _user.firstname + " " + _user.lastname;
            currentBook = book;
            InitializeComponent();
            this.WidthRequest = _Width;
            this.HeightRequest = _Width;
            Common.SetTitle(title, this.lblTitle);
            BindingContext = this;
            Monkeys.Clear();

            btnDelete.IsVisible = custom;

        }
        #endregion

        #region METHODS
        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                //imgPreview.Source = ImageSource.FromFile(filePath);

            }
        }
        public void AddImageToList(Photo imagePreview)
        {
            try
            {
                PhotoItem myMonk = new PhotoItem();
                string resourceID = imagePreview.Url;
                using (var streamReader = new StreamReader(resourceID))
                {
                    var bytes = default(byte[]);
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        bytes = memstream.ToArray();

                        byte[] res = Common.encodeByteArray(bytes);
                        //myBitmap = SKBitmap.Decode(res);
                        Stream stream1 = new MemoryStream(res);
                        myMonk.Name = imagePreview.Description;
                        myMonk.Image = ImageSource.FromStream(() => stream1);

                        // encode the image (defaults to PNG)


                    }
                }
                Monkeys.Add(myMonk);
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            //if (!images.Contains(imagePreview))
            //{
            //    images.Add(imagePreview);
            //}



        }

        //public List<Photo> Images { get => images; set => images = value; }
        public bool BookEncoding { get => bookEncoding; set => bookEncoding = value; }
        #endregion

        #region CustomEvents
        public delegate void MultimediaRemveHandler(object sender, MultimediaRemoveEventArgs e);
        public event MultimediaRemveHandler OnRemoveMultimediaHandler;
        private void OnDeleteMultimedia(object sender, MultimediaRemoveEventArgs e)
        {
            if (OnRemoveMultimediaHandler != null)
            {
                OnRemoveMultimediaHandler(sender, e);
            }
        }
        #endregion

        #region EVENTS
        private void btnImageNext_Clicked(object sender, EventArgs e)
        {
            imagePossition++;
            if (imagePossition > images.Count - 1)
            {
                imagePossition = images.Count - 1;
            }
            // SetImage(imagePossition);
        }

        private void btnImagePrevious_Clicked(object sender, EventArgs e)
        {
            imagePossition--;
            if (imagePossition < 0)
            {
                imagePossition = 0;
            }

            //SetImage(imagePossition);
        }
        public void ShowFirst()
        {
            try
            {

                imagePossition = 0;
                //SetImage(imagePossition);
            }
            catch (System.Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        int tempImageNum = 1;



        void btnClose_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook, leftPageFullScreen,
             rightPageFullScreen, PageNumLeft);
        }

        float ImageSliderX = 0;
        private void skiaView_Touch(object sender, SKTouchEventArgs e)
        {
            ImageSliderX = e.Location.X;
        }



        private void Grid_Focused(object sender, FocusEventArgs e)
        {
            this.BackgroundColor = Color.FromRgb(14, 75, 42);
        }

        private void Grid_SizeChanged(object sender, EventArgs e)
        {
            this.HeightRequest = 802;
        }
        #endregion

        private async void btnDelete_Clicked(object sender, EventArgs e)
        {
            try
            {
                bool answer = await DisplayAlert("Alert, delete Multimedia", "Are you sure?", "Yes", "No");
                if (answer)
                {
                    MultimediaRemoveEventArgs pe4 = new MultimediaRemoveEventArgs(myAnno);
                    //OnDeleteMultimedia(this, pe4);
                    OnRemoveMultimediaHandler(this, pe4);
                    logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "The Multimecia(Photo Galery) was deleted ");

                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook, leftPageFullScreen,
             rightPageFullScreen, PageNumLeft);
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

        }

    }
    public class PhotoItem
    {
        public string Name { get; set; }
        public ImageSource Image { get; set; }
    }
}