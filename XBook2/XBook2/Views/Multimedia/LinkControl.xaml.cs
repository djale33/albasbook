﻿using KlasimeBase;
using KlasimeBase.Log;
using KlasimeBase.Preview;
using KlasimeViews;
using SharedKlasime;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2.ViewModels;
using XBook2.Views.Program;

namespace XBook2.Views.Multimedia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LinkControl : ContentPage
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        public static string user = "LinkControl";
        #region VARIABLES
        public MultimediaControlViewModel MyViewModelInstance;
        private int imageStep = 10;
        float irectHeight = 60;
        private string filePath;
        private int imagePossition = 0;
        private List<Photo> images = new List<Photo>();
        private Hashtable listPhotoRect = new Hashtable();
        SKCanvas canvas;
        bool BookEncoding = true;
        public event EventHandler closeEvent;

        bool AddNew = false;
        Anno myAnno = null;
        CUser loggedUser;
        Book currentBook;
        #endregion

        #region PROPERTIES
        public CUser LoggedUser { get => loggedUser; set => loggedUser = value; }
        public Book CurrentBook { get => currentBook; set => currentBook = value; }
        #endregion

        #region CONSTRUCTOR
        public LinkControl(Book currentBook, CUser loggedUser, int _Width, int height, Anno findAno, bool addNew)
        {
            user = loggedUser.firstname + "-" + loggedUser.lastname;
            CurrentBook = currentBook;
            if (CurrentBook.CurrentPage == 0)
            {
                logInstance.Log(user,Severity.Trace, MethodBase.GetCurrentMethod(),null, "CurrentBook.CurrentPage==0");
            }
            LoggedUser = loggedUser;
            myAnno = findAno;
            InitializeComponent();

            MyViewModelInstance = new MultimediaControlViewModel(loggedUser);
            BindingContext = MyViewModelInstance;

            this.WidthRequest = _Width;
            this.HeightRequest = _Width;
            AddNew = addNew;
            if (addNew == false)
            {

                txtNote.IsVisible = false;

                Common.SetTitle(findAno.Hint.Text, this.lblTitle);
                Common.SetNote(findAno.Action.BodyText, this.lblNote);
            }
            else
            {
                txtNote.IsVisible = true;
                this.lblTitle.Text = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "button_attach_link");
            }
            //green template
            if (loggedUser.currentColorTempalte == ColorTemlates.Green)
            {
                this.BackgroundColor = Color.FromRgb(14, 75, 42);
                txtNote.BackgroundColor = Color.FromRgb(14, 75, 42);
                lblNote.BackgroundColor = Color.FromRgb(14, 75, 42);
            }
            else
            {
                this.BackgroundColor = Color.FromRgb(96, 96, 96);
                txtNote.BackgroundColor = Color.FromRgb(64, 64, 64);
                lblNote.BackgroundColor = Color.FromRgb(64, 64, 64);
            }

            if (findAno.Cutom == false)
            {
                btnDelete.IsEnabled = false;
            }
        }

        #endregion

        #region CustomEvents
        public delegate void MultimediaRemveHandler(object sender, MultimediaRemoveEventArgs e);
        public event MultimediaRemveHandler OnRemoveMultimediaHandler;
        private void OnDeleteMultimedia(object sender, MultimediaRemoveEventArgs e)
        {
            if (OnRemoveMultimediaHandler != null)
            {
                OnRemoveMultimediaHandler(sender, e);
            }
        }
        #endregion

        #region EVENTS
        /// <summary>
        /// close event is used for Adding new custom Note
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       async void btnClose_Clicked(object sender, EventArgs e)
        {
            //save note
            if (AddNew)
            {
                myAnno.AnnoId = Guid.NewGuid().ToString();
                Hint ht = new Hint();
                ht.Text = this.lblTitle.Text;
                myAnno.Hint = ht;

                myAnno.Action.BodyText = this.txtNote.Text;
                myAnno.Action.Url = this.txtNote.Text;
                //myAnno.Location.X= e.
                myAnno.Location.Width = 0.033;
                myAnno.Location.Height = 0.027;
                bool res = await currentBook.AddCustomNote(myAnno, MultimediaPath.GetCustomMultimediaPath(myAnno.MultimediaTypeToAdd));
                if (res)
                {
                    logInstance.Log(user,Severity.Trace, MethodBase.GetCurrentMethod(),null, "new multimedia Link aded: myAnnoID: " + myAnno.AnnoId);
                }
                //currentBook.getBookMedia();
                Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook);
            }
            Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook);
        }
        private async void btnDelete_Clicked(object sender, EventArgs e)
        {
            try
            {
                bool answer = await DisplayAlert("Alert, delete Link", "Are you sure?", "Yes", "No");
                if (answer)
                {
                    MultimediaRemoveEventArgs pe4 = new MultimediaRemoveEventArgs(myAnno);
                    OnRemoveMultimediaHandler(this, pe4);
                    logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "The Link was deleted " + this.txtNote.Text);
                    
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook);
        }

        private void Grid_Focused(object sender, FocusEventArgs e)
        {
            this.BackgroundColor = Color.FromRgb(14, 75, 42);
        }
        #endregion
    }
}