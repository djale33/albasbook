﻿using KlasimeBase;
using KlasimeBase.Log;
using KlasimeBase.Multimedia;
using KlasimeBase.Preview;
using LibVLCSharp.Shared;
using SharedKlasime;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Xamarin.CommunityToolkit.Core;
using Xamarin.Essentials;
//using Windows.ApplicationModel;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.Xaml;
using XBook2.Views.Program;

namespace XBook2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VideoControl : ContentPage
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        #region VARIABLES
        public static string user = "VideoControl";
        private string audioPath;
        bool bookEncoding = true;
        int tempImageNum = 1;
        LibVLC _libVLC;
        MediaPlayer _mediaPlayer;
        Book currentBook;
        CUser loggedUser;
        Anno myAnno = null;
        bool leftPageFullScreen;
        bool rightPageFullScreen;
        int PageNumLeft;

        #endregion

        #region PROPERTIES
        ColorTemlates ColorTemplate { get; set; }
        string VideoFile { get; set; }
        public string AudioPath { get => audioPath; set => audioPath = value; }
        public bool BookEncoding { get => bookEncoding; set => bookEncoding = value; }
        #endregion

        #region CustomEvents
        public delegate void MultimediaRemveHandler(object sender, MultimediaRemoveEventArgs e);
        public event MultimediaRemveHandler OnRemoveMultimediaHandler;
        private void OnDeleteMultimedia(object sender, MultimediaRemoveEventArgs e)
        {
            if (OnRemoveMultimediaHandler != null)
            {
                OnRemoveMultimediaHandler(sender, e);
            }
        }
        #endregion

        #region MoviePosition
        public static readonly BindableProperty MoviePositionProperty = BindableProperty.Create(nameof(MoviePosition), typeof(float), typeof(MainPage), propertyChanged: (obj, old, newV) =>
        {
            var me = obj as MainPage;
            if (newV != null && !(newV is float)) return;
            var oldMoviePosition = (float)old;
            var newMoviePosition = (float)newV;
            me?.MoviePositionChanged(oldMoviePosition, newMoviePosition);
        });



        /// <summary>
        /// A bindable property
        /// </summary>
        public float MoviePosition
        {
            get => (float)GetValue(MoviePositionProperty);
            set => SetValue(MoviePositionProperty, value);
        }
        #endregion

        public event EventHandler closeEvent;

        #region EVENTS
        private void btnClose_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (_mediaPlayer != null)
                    _mediaPlayer.Stop();

                Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook, leftPageFullScreen,
             rightPageFullScreen, PageNumLeft);
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void btnPlayStop_Clicked(object sender, EventArgs e)
        {
            if (_mediaPlayer != null)
                _mediaPlayer.Stop();
        }

        private void ChangeMediaVolume(object sender, ValueChangedEventArgs e)
        {
            if (_mediaPlayer != null)
                _mediaPlayer.Volume = (int)volumeSlider.Value;
        }

        private void btnPlayPause_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (_mediaPlayer != null)
                {
                    if (_mediaPlayer.State == VLCState.Ended)
                    {
                        _mediaPlayer = new MediaPlayer(_libVLC)
                        {
                            Media = new Media(_libVLC, new FileMediaSource { File = VideoFile })
                        };

                        VideoView.MediaPlayer = _mediaPlayer;
                        _mediaPlayer.PositionChanged += MediaPlayerPositionChanged;
                        _mediaPlayer.TimeChanged += MediaPlayer_TimeChanged;
                        _mediaPlayer.EncounteredError += MediaPlayer_EncounteredError;
                        _mediaPlayer.EndReached += MediaPlayer_EndReached;
                        _mediaPlayer.Playing += MediaPlayer_Playing;
                        _mediaPlayer.Play();

                        _mediaPlayer.Volume = (int)volumeSlider.Value;

                        if (ColorTemplate == ColorTemlates.Orange)
                        {
                            FontImageSource fis = new FontImageSource()
                            {
                                Glyph = FontAwsome.Helpers.IconFont.PauseCircle,
                                Color = Color.OrangeRed,
                                Size = 30,
                                FontFamily = "FAS"
                            };
                            btnPlayPause.Source = fis;
                            
                        }
                        else
                        {
                            FontImageSource fis = new FontImageSource()
                            {
                                Glyph = FontAwsome.Helpers.IconFont.PlayCircle,
                                Color = Color.Green ,
                                Size = 30,
                                FontFamily = "FAS"
                            };
                            btnPlayPause.Source = fis;
                            
                        }
                    }
                    else
                    {
                        if (_mediaPlayer.State == VLCState.Playing)
                        {
                            if (_mediaPlayer != null)
                            {
                                _mediaPlayer.Pause();
                                if (ColorTemplate == ColorTemlates.Orange)
                                {
                                    FontImageSource fis = new FontImageSource()
                                    {
                                        Glyph = FontAwsome.Helpers.IconFont.PauseCircle,
                                        Color = Color.OrangeRed,
                                        Size = 30,
                                        FontFamily = "FAS"
                                    };
                                    btnPlayPause.Source = fis;
                                    
                                }
                                else
                                {
                                    FontImageSource fis = new FontImageSource()
                                    {
                                        Glyph = FontAwsome.Helpers.IconFont.PauseCircle,
                                        Color = Color.Green,
                                        Size = 30,
                                        FontFamily = "FAS"
                                    };
                                    btnPlayPause.Source = fis;
                                    
                                }
                            }
                        }
                        else
                        {
                            if (_mediaPlayer != null)
                            {
                                _mediaPlayer.Play();
                                if (ColorTemplate == ColorTemlates.Orange)
                                {
                                    FontImageSource fis = new FontImageSource()
                                    {
                                        Glyph = FontAwsome.Helpers.IconFont.PlayCircle,
                                        Color = Color.OrangeRed,
                                        Size = 30,
                                        FontFamily = "FAS"
                                    };
                                    btnPlayPause.Source = fis;
                                    
                                }
                                else
                                {
                                    FontImageSource fis = new FontImageSource()
                                    {
                                        Glyph = FontAwsome.Helpers.IconFont.PlayCircle,
                                        Color = Color.Green,
                                        Size = 30,
                                        FontFamily = "FAS"
                                    };
                                    btnPlayPause.Source = fis;
                                    
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
         

        }
        private void MediaPlayer_Playing(object sender, EventArgs e)
        {
            if (_mediaPlayer != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    lblProgressTotal.Text = string.Format("{0:mm\\:ss}", TimeSpan.FromMilliseconds(_mediaPlayer.Length));
                });
            }
        }

        private void MediaPlayer_EndReached(object sender, EventArgs e)
        {
            if (_mediaPlayer != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (ColorTemplate == ColorTemlates.Orange)
                    {
                        FontImageSource fis = new FontImageSource()
                        {
                            Glyph = FontAwsome.Helpers.IconFont.PauseCircle,
                            Color = Color.OrangeRed,
                            Size = 30,
                            FontFamily = "FAS"
                        };
                        btnPlayPause.Source = fis;
                        
                    }
                    else
                    {
                        FontImageSource fis = new FontImageSource()
                        {
                            Glyph = FontAwsome.Helpers.IconFont.PauseCircle,
                            Color = Color.Green,
                            Size = 30,
                            FontFamily = "FAS"
                        };
                        btnPlayPause.Source = fis;
                        
                    }
                });
            }
        }

        private void MediaPlayer_EncounteredError(object sender, EventArgs ee)
        {
            logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), null, ee.ToString());
        }

        private void MediaPlayer_TimeChanged(object sender, MediaPlayerTimeChangedEventArgs e)
        {
            if (_mediaPlayer != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    lblProgressStatus.Text = string.Format("{0:mm\\:ss}", TimeSpan.FromMilliseconds(_mediaPlayer.Time));
                    mediaProgress.Progress = (double)_mediaPlayer.Time / (double)_mediaPlayer.Length;
                   
                });
            }
        }

        private void MediaPlayerPositionChanged(object sender, MediaPlayerPositionChangedEventArgs e)
        {
            MoviePosition = e.Position * 100;
        }
        #endregion

        #region CONSTRUCTOR
        public VideoControl(CUser _user, ColorTemlates colorTemplate, string path, string title, Anno findAno, bool custom, Book book,
            bool _leftPageFullScreen, bool _rightPageFullScreen, int _PageNumLeft)
        {
            try
            {
                leftPageFullScreen= _leftPageFullScreen;
                rightPageFullScreen= _rightPageFullScreen;
                PageNumLeft= _PageNumLeft;
                loggedUser = _user;
                myAnno = findAno;
                user = _user.firstname + " " + _user.lastname;
                currentBook = book;
                ColorTemplate = colorTemplate;
                InitializeComponent();
                btnDelete.IsVisible = custom;
                if (ColorTemplate == ColorTemlates.Orange)
                {
                    FontImageSource fis = new FontImageSource()
                    {
                        Glyph = FontAwsome.Helpers.IconFont.PauseCircle,
                        Color = Color.OrangeRed,
                        Size = 30,
                        FontFamily = "FAS"
                    };
                    btnPlayPause.Source = fis;
                    
                }
                else
                {
                    FontImageSource fis = new FontImageSource()
                    {
                        Glyph = FontAwsome.Helpers.IconFont.PauseCircle,
                        Color = Color.Green,
                        Size = 30,
                        FontFamily = "FAS"
                    };
                    btnPlayPause.Source = fis;

                }

                if (custom == true)
                {
                    BookEncoding = false;
                }
                switch (Device.RuntimePlatform)
                {
                    case "UWP":
                        try
                        {
                            if (File.Exists(path))
                            {
                                if (BookEncoding)
                                {
                                    string resourceID = path;
                                    using (var streamReader = new StreamReader(resourceID))
                                    {

                                        var bytes = default(byte[]);
                                        using (var memstream = new MemoryStream())
                                        {
                                            streamReader.BaseStream.CopyTo(memstream);
                                            bytes = memstream.ToArray();
                                            byte[] res = Common.encodeByteArray(bytes);
                                            tempImageNum++;
                                            try
                                            {

                                                File.WriteAllBytes(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "myAudio" + tempImageNum + ".iig", res);
                                                path = KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "myAudio" + tempImageNum + ".iig";

                                                // videoPlayer.Source = new Uri(path);
                                            }
                                            catch (Exception ee)
                                            {
                                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                                tempImageNum++;
                                                File.WriteAllBytes(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "myAudio" + tempImageNum + ".iig", res);
                                            }
                                        }
                                    }


                                }
                                else
                                {
                                    //videoPlayer.Source = new Uri(path);
                                }

                            }
                            else
                            {
                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), null, "Path to video file is not valid");
                            }
                        }
                        catch (Exception ee)
                        {
                            string msg = ee.Message;
                            throw;
                        }

                        break;
                    case "Android":
                        try
                        {
                            if (File.Exists(path))
                            {
                                if (BookEncoding)
                                {
                                    string resourceID = path;
                                    using (var streamReader = new StreamReader(resourceID))
                                    {

                                        var bytes = default(byte[]);
                                        using (var memstream = new MemoryStream())
                                        {
                                            streamReader.BaseStream.CopyTo(memstream);
                                            bytes = memstream.ToArray();
                                            byte[] res = Common.encodeByteArray(bytes);
                                            tempImageNum++;
                                            try
                                            {
                                                string folder = Path.GetTempPath();
                                                string localTempName = "myAudio" + tempImageNum + ".iig";
                                                VideoFile = Path.Combine(folder, localTempName);

                                                File.WriteAllBytes(VideoFile, res);

                                                if (File.Exists(VideoFile))
                                                {

                                                    //videoPlayer.Source = new Uri($"ms-appdata:///temp/{localTempName}");
                                                    //videoPlayer.Source = new Uri(npath);
                                                    _libVLC = new LibVLC();
                                                    _mediaPlayer = new MediaPlayer(_libVLC)
                                                    {
                                                        Media = new Media(_libVLC, new FileMediaSource { File = VideoFile })
                                                    };

                                                    VideoView.MediaPlayer = _mediaPlayer;
                                                    _mediaPlayer.PositionChanged += MediaPlayerPositionChanged;
                                                    _mediaPlayer.TimeChanged += MediaPlayer_TimeChanged;
                                                    _mediaPlayer.EncounteredError += MediaPlayer_EncounteredError;
                                                    _mediaPlayer.EndReached += MediaPlayer_EndReached;
                                                    _mediaPlayer.Playing += MediaPlayer_Playing;
                                                    _mediaPlayer.Play();

                                                    _mediaPlayer.Volume = (int)volumeSlider.Value;
                                                }
                                            }
                                            catch (Exception ee)
                                            {
                                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                                tempImageNum++;
                                                File.WriteAllBytes(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "myAudio" + tempImageNum + ".iig", res);
                                            }
                                        }
                                    }


                                }
                                else
                                {
                                    _libVLC = new LibVLC();
                                    _mediaPlayer = new MediaPlayer(_libVLC)
                                    {
                                        Media = new Media(_libVLC, new FileMediaSource { File = path })
                                    };

                                    VideoView.MediaPlayer = _mediaPlayer;
                                    _mediaPlayer.PositionChanged += MediaPlayerPositionChanged;
                                    _mediaPlayer.TimeChanged += MediaPlayer_TimeChanged;
                                    _mediaPlayer.EncounteredError += MediaPlayer_EncounteredError;
                                    _mediaPlayer.EndReached += MediaPlayer_EndReached;
                                    _mediaPlayer.Playing += MediaPlayer_Playing;
                                    _mediaPlayer.Play();

                                    _mediaPlayer.Volume = (int)volumeSlider.Value;
                                }

                            }
                            else
                            {
                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), null, "Path to video file is not valid");
                            }
                        }
                        catch (Exception ee)
                        {
                            string msg = ee.Message;
                            throw;
                        }
                        break;
                    case "iOS":
                        break;
                    default:

                        break;
                }

                Common.SetTitle(title, this.lblTitle);
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                throw;
            }

        }

        #endregion

        private async void btnDelete_Clicked(object sender, EventArgs e)
        {
            try
            {

                if (_mediaPlayer != null)
                    _mediaPlayer.Stop();

                bool answer = await DisplayAlert("Alert, delete Video", "Are you sure?", "Yes", "No");
                if (answer)
                {
                    MultimediaRemoveEventArgs pe4 = new MultimediaRemoveEventArgs(myAnno);
                    //OnDeleteMultimedia(this, pe4);
                    OnRemoveMultimediaHandler(this, pe4);
                    logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "The Video was deleted " );

                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook,  leftPageFullScreen,
             rightPageFullScreen, PageNumLeft);
        }
    }


}