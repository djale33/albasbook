﻿
using KlasimeBase;
using KlasimeBase.Log;
using KlasimeBase.Multimedia;
using KlasimeBase.Preview;
using SharedKlasime;
using System;
using System.IO;
using System.Reflection;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.Xaml;
using XBook2.Views.Program;

namespace XBook2.Views.Multimedia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AudioControl : ContentPage
    {
        #region VARIABLES
        public ApiLog logInstance =  ApiLog.GetInstance();
        public static string user = "AudioControl";
        ColorTemlates ColorTemplate { get; set; }
        private string audioPath;
        bool bookEncoding = true;
        int tempImageNum = 1;
        double mediaDuration = 0;
        public event EventHandler closeEvent;
        Book currentBook;
        CUser loggedUser;
        Anno myAnno = null;
        bool leftPageFullScreen;
        bool rightPageFullScreen;
        int PageNumLeft;
        #endregion

        #region PROPERTIES
        public string AudioPath { get => audioPath; set => audioPath = value; }
        public bool BookEncoding { get => bookEncoding; set => bookEncoding = value; }
        #endregion

        #region CustomEvents
        public delegate void MultimediaRemveHandler(object sender, MultimediaRemoveEventArgs e);
        public event MultimediaRemveHandler OnRemoveMultimediaHandler;
        private void OnDeleteMultimedia(object sender, MultimediaRemoveEventArgs e)
        {
            if (OnRemoveMultimediaHandler != null)
            {
                OnRemoveMultimediaHandler(sender, e);
            }
        }
        #endregion

        #region CONSTRUCTOR
        public AudioControl(CUser _user, ColorTemlates colorTemplate, string path, string title, Anno findAno, bool custom, Book book,
            bool _leftPageFullScreen, bool _rightPageFullScreen, int _PageNumLeft)
        {
            try
            {
                leftPageFullScreen = _leftPageFullScreen;
                rightPageFullScreen = _rightPageFullScreen;
                PageNumLeft = _PageNumLeft;
                myAnno = findAno;
                loggedUser = _user;
                user = _user.firstname + " " + _user.lastname;
                currentBook = book;
                ColorTemplate = colorTemplate;

                InitializeComponent();

                if (ColorTemplate == ColorTemlates.Orange)
                {
                    btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.orange.pause_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    btnClose.Source = ImageSource.FromResource("XBook2.media.load.orange.exit_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
                else
                {
                    btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.green.pause_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    btnClose.Source = ImageSource.FromResource("XBook2.media.load.green.exit_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }

                if (custom == true)
                {
                    BookEncoding = false;
                    btnDelete.IsVisible = true;
                }
                switch (Device.RuntimePlatform)
                {
                    case "UWP":
                        try
                        {
                            if (File.Exists(path))
                            {
                                if (BookEncoding)
                                {
                                    string resourceID = path;
                                    using (var streamReader = new StreamReader(resourceID))
                                    {

                                        var bytes = default(byte[]);
                                        using (var memstream = new MemoryStream())
                                        {
                                            streamReader.BaseStream.CopyTo(memstream);
                                            bytes = memstream.ToArray();
                                            byte[] res = Common.encodeByteArray(bytes);
                                            tempImageNum++;
                                            try
                                            {

                                                File.WriteAllBytes(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "myAudio" + tempImageNum + ".iig", res);
                                                path = KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "myAudio" + tempImageNum + ".iig";

                                                audioPlayer.Source = new Uri(path);

                                            }
                                            catch (Exception ee)
                                            {
                                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                                tempImageNum++;
                                                File.WriteAllBytes(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "myAudio" + tempImageNum + ".iig", res);
                                            }
                                        }
                                    }


                                }
                                else
                                {
                                    audioPlayer.Source = new Uri(path);
                                }

                            }
                            else
                            {
                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), null, "Path to video file is not valid");
                            }
                        }
                        catch (Exception ee)
                        {
                            string msg = ee.Message;
                            throw;
                        }

                        break;
                    case "Android":
                        try
                        {
                            if (File.Exists(path))
                            {
                                if (BookEncoding)
                                {
                                    string resourceID = path;
                                    using (var streamReader = new StreamReader(resourceID))
                                    {

                                        var bytes = default(byte[]);
                                        using (var memstream = new MemoryStream())
                                        {
                                            streamReader.BaseStream.CopyTo(memstream);
                                            bytes = memstream.ToArray();
                                            byte[] res = Common.encodeByteArray(bytes);
                                            tempImageNum++;
                                            try
                                            {
                                                string folder = Path.GetTempPath();
                                                string localTempName = "myAudio" + tempImageNum + ".iig";
                                                string audioFile = Path.Combine(folder, localTempName);

                                                File.WriteAllBytes(audioFile, res);

                                                if (File.Exists(audioFile))
                                                {

                                                    audioPlayer.Source = new Uri($"ms-appdata:///temp/{localTempName}");


                                                    Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                                                    {
                                                        // called every 1 second
                                                        // do stuff here
                                                        if (audioPlayer != null)
                                                        {
                                                            if (audioPlayer.Source != null)
                                                            {

                                                                lblProgressStatus.Text = TimeSpan.FromSeconds(audioPlayer.Position.TotalSeconds).ToString(@"hh\:mm\:ss");


                                                                //mediaProgress.Min = 0;
                                                                mediaDuration = audioPlayer.Duration.Value.TotalSeconds;
                                                                lblProgressTotal.Text = TimeSpan.FromSeconds(mediaDuration).ToString(@"hh\:mm\:ss");
                                                                //mediaProgress.Maximum = audioPlayer.NaturalDuration.TimeSpan.TotalSeconds;
                                                                double current = audioPlayer.Position.TotalSeconds;
                                                                double dblProgressPercent = current / audioPlayer.Duration.Value.TotalSeconds;
                                                                Device.BeginInvokeOnMainThread(() =>
                                                                {
                                                                    mediaProgress.Progress = dblProgressPercent;
                                                                });
                                                                
                                                            }

                                                            return true; // return true to repeat counting, false to stop timer
                                                        }
                                                        else
                                                            return false;
                                                    });
                                                }
                                            }
                                            catch (Exception ee)
                                            {
                                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                                tempImageNum++;
                                                File.WriteAllBytes(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "myAudio" + tempImageNum + ".iig", res);
                                            }
                                        }
                                    }


                                }
                                else
                                {
                                    audioPlayer.Source = new Uri(path);
                                    Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                                    {
                                        // called every 1 second
                                        // do stuff here
                                        if (audioPlayer != null)
                                        {
                                            if (audioPlayer.Source != null)
                                            {

                                                lblProgressStatus.Text = TimeSpan.FromSeconds(audioPlayer.Position.TotalSeconds).ToString(@"hh\:mm\:ss");


                                                //mediaProgress.Min = 0;
                                                if (audioPlayer.Duration != null)
                                                {
                                                    mediaDuration = audioPlayer.Duration.Value.TotalSeconds;
                                                    lblProgressTotal.Text = TimeSpan.FromSeconds(mediaDuration).ToString(@"hh\:mm\:ss");
                                                    //mediaProgress.Maximum = audioPlayer.NaturalDuration.TimeSpan.TotalSeconds;
                                                    double current = audioPlayer.Position.TotalSeconds;
                                                    double dblProgressPercent = current / audioPlayer.Duration.Value.TotalSeconds;
                                                    Device.BeginInvokeOnMainThread(() =>
                                                    {
                                                        mediaProgress.Progress = dblProgressPercent;
                                                    });
                                                }

                                            }

                                            return true; // return true to repeat counting, false to stop timer
                                        }
                                        else
                                            return false;
                                    });
                                }

                            }
                            else
                            {
                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), null, "Path to video file is not valid");
                            }
                        }
                        catch (Exception ee)
                        {
                            string msg = ee.Message;
                            throw;
                        }
                        break;
                    case "iOS":
                        break;
                    default:

                        break;
                }

                Common.SetTitle(title, this.lblTitle);
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                throw;
            }
        }
        #endregion

        #region EVENTS
        private void btnClose_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (audioPlayer != null)    
                    audioPlayer.Stop();
                Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook, leftPageFullScreen,
             rightPageFullScreen, PageNumLeft);
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void btnPlayPause_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (audioPlayer.CurrentState == Xamarin.CommunityToolkit.UI.Views.MediaElementState.Playing)
                {
                    audioPlayer.Pause();
                    if (ColorTemplate == ColorTemlates.Orange)
                    {
                        btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.orange.pause_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    }
                    else
                    {
                        btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.green.pause_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    }
                }
                else
                {
                    audioPlayer.Play();
                    if (ColorTemplate == ColorTemlates.Orange)
                    {
                        btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.orange.pause_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    }
                    else
                    {
                        btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.green.pause_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    }
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void ChangeMediaVolume(object sender, ValueChangedEventArgs e)
        {
            audioPlayer.Volume = (double)volumeSlider.Value;
        }

        private void btnPlayStop_Clicked(object sender, EventArgs e)
        {
            if (audioPlayer.CurrentState == Xamarin.CommunityToolkit.UI.Views.MediaElementState.Playing)
            {
                audioPlayer.Pause();
                if (ColorTemplate == ColorTemlates.Orange)
                {
                    btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.orange.pause_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
                else
                {
                    btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.green.pause_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
            }
            else
            {
                audioPlayer.Play();
                if (ColorTemplate == ColorTemlates.Orange)
                {
                    btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.orange.pause_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }
                else
                {
                    btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.green.pause_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                }

            }
        }

        private void audioPlayer_MediaEnded(object sender, EventArgs e)
        {
            if (ColorTemplate == ColorTemlates.Orange)
            {
                btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.orange.pause_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }
            else
            {
                btnPlayPause.Source = ImageSource.FromResource("XBook2.media.load.green.pause_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
            }
        }

        #endregion

        private void imgVolumeMin_Clicked(object sender, EventArgs e)
        {
            if (volumeSlider != null)
            {
               if( volumeSlider.Value != 0)
                {
                    volumeSlider.Value = volumeSlider.Value - 0.1;
                }
            }
        }

        private void imgVolumeMax_Clicked(object sender, EventArgs e)
        {
            if (volumeSlider != null)
            {
                if (volumeSlider.Value != 1)
                {
                    volumeSlider.Value = volumeSlider.Value + 0.1;
                }
            }
        }

        private async void btnDelete_Clicked(object sender, EventArgs e)
        {
            try
            {
                bool answer = await DisplayAlert("Alert, delete Audio", "Are you sure?", "Yes", "No");
                if (answer)
                {
                    MultimediaRemoveEventArgs pe4 = new MultimediaRemoveEventArgs(myAnno);
                    //OnDeleteMultimedia(this, pe4);
                    OnRemoveMultimediaHandler(this, pe4);
                    logInstance.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "The Note was deleted " );

                }
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            Application.Current.MainPage = new MainPage(loggedUser, currentBook.Id, currentBook, leftPageFullScreen,
             rightPageFullScreen, PageNumLeft);
        }
    }
}