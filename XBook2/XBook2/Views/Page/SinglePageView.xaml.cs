﻿using eBookGraphics;
using eBookGraphics.DrawingOnPage;
using KlasimeBase;
using KlasimeBase.Interfaces;
using KlasimeBase.Log;
using KlasimeViews;
using Newtonsoft.Json;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2;
using XBook2.Touch;
using static Xamarin.Forms.Internals.GIFBitmap;


namespace XBook2.Views
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SinglePageView : ContentView
    {
        public ApiLog logInstance =  ApiLog.GetInstance();
        public string user = "SinglePageView";
        #region VARIABLES
        #region Variables - General

        bool testSearch = false;
        private Book currentBook;
        bool bookEncoding;
        bool rulerAnno = false;
        string book;
        bool showRuler = true;        //show ruler on normal page, not show on zoomed page
        bool leftRulerOn = true;      // Impoprtant, if we use left ruler on page !
        bool singlePageMode = false;// Important false for UWP, true for Mobile device !
        SKColor pageBackgroundColor = new SKColor(65, 64, 66, 255);
        bool showAllSearchForCalibration = true; //Important for test true, otherwise false!
        int pageNumber = 12;
        private bool zoomPage = false;
        int shadowWidth = 200;
        double displayProportion = 0;
        double imageProportion = 0;
        double pageProportion = 0;
        float rullerSmallStep = 4;
        float rullerBigStep = 14;
        bool autoBaseProp = true;
        string bookPath;
        int bookID;
        bool addMultimediaOnPage = false;
        bool multimediaPreview = false;
        float startXforRemove = 0;
        float newXforRemove = 0;
        bool readyForMove = false;
        string prepareSelectIntoClipoard = "";
        Hashtable htStrLines = new Hashtable();
        Hashtable htPageWordsOriginalCache = new Hashtable();
        Hashtable htPageWordsCache = new Hashtable();
        Hashtable htNumberAndWordsCache = new Hashtable();
        List<string> allWordsOnPageCache;
        PageAnno currentPageAnno;
        SKBitmap myBitmap;
        SKPoint imagePossition;
        SearchTextParser searchTextParser;
        CPageRefresh cPageRefresh;

        MultimediaParser multimediaParser;
        SKImageInfo imageInfo;


        float FSCALEMIN = 0.5f;
        float fScale = 0.5f;

        PageType thisPageType;

        Anno hoverAnno = null;

        bool showMultimediaHoverText = false;
        Dictionary<long, TouchManipulationInfo> touchDictionary = new Dictionary<long, TouchManipulationInfo>();
        string sep = Common.GetPathSeparator();
        bool closingStatus = false;

        MultimediaType multimediaTypeToAdd;

        private bool searchTextActive;

        private double hProp;
        private double yProp;
        private double xProp;
        private double wProp;
        private int pageRullerWidth;
        private int tabletMode;
        private int defaultMode;
        private int lastPageNum = 0;
        private bool selectTextActive = false;

        private OrigWordPossition findItem = null;
        private List<OrigWordPossition> selectedText = new List<OrigWordPossition>();
        double lastBaseProp = 0;
        #endregion     

        #region VARIABLES - Bitmap

        SKBitmap bmpAudio;
        SKBitmap bmpPhotoSlide;
        SKBitmap bmpPhotoRight;
        SKBitmap bmpPhotoLeft;
        SKBitmap bmpOpenWindow;
        SKBitmap bmpVideo;
        SKBitmap bmpText;
        SKBitmap bmpTextRight;
        SKBitmap bmpTextLeft;
        SKBitmap bmpYouTube;
        SKBitmap bmpExternalLink;
        SKBitmap bmpFile;

        SKBitmap bmpAudioRight;
        SKBitmap bmpAudioLeft;
        SKBitmap bmpVideoRight;
        SKBitmap bmpVideoLeft;
        SKBitmap bmpYoutubeRight;
        SKBitmap bmpYoutubeLeft;
        SKBitmap bmpLinkRight;
        SKBitmap bmpLinkLeft;
        SKBitmap bmpFileRight;
        SKBitmap bmpFileLeft;

        SKBitmap bmpNoteAdd;
        SKBitmap bmpAudioAdd;
        SKBitmap bmpVideoAdd;
        SKBitmap bmpPhotoAdd;
        SKBitmap bmpFileAdd;
        SKBitmap bmpLinkAdd;
        SKBitmap bmpYoutubeAdd;
        //
        SKBitmap bmpPhotoRightCustom;
        SKBitmap bmpPhotoLeftCustom;

        SKBitmap bmpTextRightCustom;
        SKBitmap bmpTextLeftCustom;

        SKBitmap bmpAudioRightCustom;
        SKBitmap bmpAudioLeftCustom;

        SKBitmap bmpVideoRightCustom;
        SKBitmap bmpVideoLeftCustom;

        SKBitmap bmpYoutubeRightCustom;
        SKBitmap bmpYoutubeLeftCustom;

        SKBitmap bmpLinkRightCustom;
        SKBitmap bmpLinkLeftCustom;

        SKBitmap bmpFileRightCustom;
        SKBitmap bmpFileLeftCustom;
        #endregion

        #region VARIABLES-DRAWING
        CDrawItemOnPage dip;

        #endregion
        string strFindAndLocation;
        #endregion

        #region PROPERTIES

        public ColumnDefinitionCollection BookGridColls { get; set; }
        public RowDefinitionCollection BookGridRows { get; set; }
        public ColumnDefinitionCollection MainGridColls { get; set; }
        public ColumnDefinitionCollection BootomGridColls { get; set; }

        public object Constants { get; private set; }
        public PageType ThisPageType { get => thisPageType; set => thisPageType = value; }
        public int PageNumber { get => pageNumber; set => pageNumber = value; }
        public bool SearchTextActive { get => searchTextActive; set => searchTextActive = value; }
        public bool ZoomPage
        {
            get => zoomPage;
            set
            {
                zoomPage = value;
                if (value)
                {
                    skiaViewLeftFull.IsVisible = true;
                    swipViewPage.IsVisible = false;
                }
                else
                {
                    skiaViewLeftFull.IsVisible = false;
                    swipViewPage.IsVisible = true;
                }
            }

        }
        public double HProp { get => hProp; set => hProp = value; }
        public double YProp { get => yProp; set => yProp = value; }
        public double XProp { get => xProp; set => xProp = value; }
        public double WProp { get => wProp; set => wProp = value; }
        public int PageRullerWidth { get => pageRullerWidth; set => pageRullerWidth = value; }
        public float FScale { get => fScale; set => fScale = value; }
        public float RullerSmallStep { get => rullerSmallStep; set => rullerSmallStep = value; }
        public int ShadowWidth { get => shadowWidth; set => shadowWidth = value; }
        public float RullerBigStep { get => rullerBigStep; set => rullerBigStep = value; }
        public SKImageInfo ImageInfo { get => imageInfo; set => imageInfo = value; }
        public bool AutoBaseProp { get => autoBaseProp; set => autoBaseProp = value; }
        public SKBitmap BmpAudio { get => bmpAudio; set => bmpAudio = value; }
        public SKBitmap BmpPhotoSlide { get => bmpPhotoSlide; set => bmpPhotoSlide = value; }
        public SKBitmap BmpOpenWindow { get => bmpOpenWindow; set => bmpOpenWindow = value; }
        public SKBitmap BmpVideo { get => bmpVideo; set => bmpVideo = value; }
        public bool SinglePageMode { get => singlePageMode; set => singlePageMode = value; }
        public int BookID { get => bookID; set => bookID = value; }
        public string BookPath { get => bookPath; set => bookPath = value; }
        public Book CurrentBook { get => currentBook; set => currentBook = value; }
        public string Book { get => book; set => book = value; }
        public bool MultimediaPreview { get => multimediaPreview; set => multimediaPreview = value; }
        public bool ShowAllSearchForCalibration { get => showAllSearchForCalibration; set => showAllSearchForCalibration = value; }
        public bool ShowRuler { get => showRuler; set => showRuler = value; }
        public bool Encoding { get => bookEncoding; set => bookEncoding = value; }
        public SKBitmap BmpText { get => bmpText; set => bmpText = value; }
        public SKBitmap BmpYouTube { get => bmpYouTube; set => bmpYouTube = value; }
        public SKBitmap BmpExternalLink { get => bmpExternalLink; set => bmpExternalLink = value; }
        public SKBitmap BmpPhotoRight { get => bmpPhotoRight; set => bmpPhotoRight = value; }
        public SKBitmap BmpPhotoLeft { get => bmpPhotoLeft; set => bmpPhotoLeft = value; }
        public SKBitmap BmpTextRight { get => bmpTextRight; set => bmpTextRight = value; }
        public SKBitmap BmpTextLeft { get => bmpTextLeft; set => bmpTextLeft = value; }
        public SKBitmap BmpAudioRight { get => bmpAudioRight; set => bmpAudioRight = value; }
        public SKBitmap BmpAudioLeft { get => bmpAudioLeft; set => bmpAudioLeft = value; }
        public SKBitmap BmpVideoRight { get => bmpVideoRight; set => bmpVideoRight = value; }
        public SKBitmap BmpVideoLeft { get => bmpVideoLeft; set => bmpVideoLeft = value; }
        public SKBitmap BmpYoutubeRight { get => bmpYoutubeRight; set => bmpYoutubeRight = value; }
        public SKBitmap BmpYoutubeLeft { get => bmpYoutubeLeft; set => bmpYoutubeLeft = value; }
        public SKBitmap BmpLinkRight { get => bmpLinkRight; set => bmpLinkRight = value; }
        public SKBitmap BmpLinkLeft { get => bmpLinkLeft; set => bmpLinkLeft = value; }
        public int TabletMode { get => tabletMode; set => tabletMode = value; }
        public int DefaultMode { get => defaultMode; set => defaultMode = value; }
        public SKBitmap BmpNoteAdd { get => bmpNoteAdd; set => bmpNoteAdd = value; }
        public SKBitmap BmpAudioAdd { get => bmpAudioAdd; set => bmpAudioAdd = value; }
        public SKBitmap BmpVideoAdd { get => bmpVideoAdd; set => bmpVideoAdd = value; }
        public SKBitmap BmpPhotoAdd { get => bmpPhotoAdd; set => bmpPhotoAdd = value; }
        public bool AddMultimediaOnPage { get => addMultimediaOnPage; set => addMultimediaOnPage = value; }
        public SKBitmap BmpPhotoRightCustom { get => bmpPhotoRightCustom; set => bmpPhotoRightCustom = value; }
        public SKBitmap BmpPhotoLeftCustom { get => bmpPhotoLeftCustom; set => bmpPhotoLeftCustom = value; }
        public SKBitmap BmpTextRightCustom { get => bmpTextRightCustom; set => bmpTextRightCustom = value; }
        public SKBitmap BmpTextLeftCustom { get => bmpTextLeftCustom; set => bmpTextLeftCustom = value; }
        public SKBitmap BmpAudioRightCustom { get => bmpAudioRightCustom; set => bmpAudioRightCustom = value; }
        public SKBitmap BmpAudioLeftCustom { get => bmpAudioLeftCustom; set => bmpAudioLeftCustom = value; }
        public SKBitmap BmpVideoRightCustom { get => bmpVideoRightCustom; set => bmpVideoRightCustom = value; }
        public SKBitmap BmpVideoLeftCustom { get => bmpVideoLeftCustom; set => bmpVideoLeftCustom = value; }
        public SKBitmap BmpYoutubeRightCustom { get => bmpYoutubeRightCustom; set => bmpYoutubeRightCustom = value; }
        public SKBitmap BmpYoutubeLeftCustom { get => bmpYoutubeLeftCustom; set => bmpYoutubeLeftCustom = value; }
        public SKBitmap BmpLinkRightCustom { get => bmpLinkRightCustom; set => bmpLinkRightCustom = value; }
        public SKBitmap BmpLinkLeftCustom { get => bmpLinkLeftCustom; set => bmpLinkLeftCustom = value; }
        public bool ReadyForMove { get => readyForMove; set => readyForMove = value; }
        public SKBitmap BmpFileAdd { get => bmpFileAdd; set => bmpFileAdd = value; }
        public SKBitmap BmpLinkAdd { get => bmpLinkAdd; set => bmpLinkAdd = value; }
        public SKBitmap BmpYoutubeAdd { get => bmpYoutubeAdd; set => bmpYoutubeAdd = value; }
        public SKBitmap BmpFile { get => bmpFile; set => bmpFile = value; }
        public SKBitmap BmpFileRight { get => bmpFileRight; set => bmpFileRight = value; }
        public SKBitmap BmpFileLeft { get => bmpFileLeft; set => bmpFileLeft = value; }
        public SKBitmap BmpFileRightCustom { get => bmpFileRightCustom; set => bmpFileRightCustom = value; }
        public SKBitmap BmpFileLeftCustom { get => bmpFileLeftCustom; set => bmpFileLeftCustom = value; }
        public bool SelectTextActive { get => selectTextActive; set => selectTextActive = value; }
        public OrigWordPossition FindItem { get => findItem; set => findItem = value; }
        public List<OrigWordPossition> SelectedText { get => selectedText; set => selectedText = value; }
        public Hashtable HtStrLines { get => htStrLines; set => htStrLines = value; }
        public Hashtable HtPageWordsCache { get => htPageWordsCache; set => htPageWordsCache = value; }
        public Hashtable HtNumberAndWordsCache { get => htNumberAndWordsCache; set => htNumberAndWordsCache = value; }
        public List<string> AllWordsOnPageCache { get => allWordsOnPageCache; set => allWordsOnPageCache = value; }
        public Hashtable HtPageWordsOriginalCache { get => htPageWordsOriginalCache; set => htPageWordsOriginalCache = value; }
        public string PrepareSelectIntoClipoard { get => prepareSelectIntoClipoard; set => prepareSelectIntoClipoard = value; }
        public SKColor PageBackgroundColor { get => pageBackgroundColor; set => pageBackgroundColor = value; }
        public CDrawItemOnPage DrawItemOnPage { get => dip; set => dip = value; }
        public string StrFindAndLocation { get => strFindAndLocation; set => strFindAndLocation = value; }


        #endregion

        #region Custom Event
        public delegate void DoubleClickHandler(object sender, EventArgs e);
        public delegate void SearchTextHandler(object sender, ProgressEventArgs e);
        public delegate void MultimediaHandler(object sender, MultimediaEventArgs e);
        public delegate void PageMouseMoveHandler(object sender, PageMouseMoveEventArgs e);
        public delegate void MultimediaRullerHandler(object sender, MultimediaRulerEventArgs e);
        public delegate void MultimediaRemveHandler(object sender, MultimediaRemoveEventArgs e);
        public delegate void SwipePagetHandler(object sender, ProgressEventArgs e);

        public event SearchTextHandler OnSearchTextFind;
        public event PageMouseMoveHandler OnPageChanged;
        public event SearchTextHandler OnLogText;
        public event SearchTextHandler OnMenuReset;
        public event MultimediaHandler OnMultimedia;
        public event MultimediaHandler OnAddMultimedia;
        public event MultimediaRullerHandler OnMultimediaRuller;
        public event DoubleClickHandler OnDoubleClick;
        public event MultimediaRemveHandler OnRemoveMultimedia;
        public event SwipePagetHandler OnSwipe;

        private void OnSwipeEvent(object sender, ProgressEventArgs e)
        {
            if (OnSwipe != null)
            {
                OnSwipe(sender, e);
            }
        }
        private void OnRemoveMultimediaH(object sender, MultimediaRemoveEventArgs e)
        {
            if (OnRemoveMultimedia != null)
            {
                OnRemoveMultimedia(sender, e);
            }
        }
        private void OnDoubleClickEvent(object sender, EventArgs e)
        {
            if (OnDoubleClick != null)
            {
                OnDoubleClick(sender, e);
            }
        }
        private void OnSearchText(object sender, ProgressEventArgs e)
        {
            if (OnSearchTextFind != null)
            {
                OnSearchTextFind(sender, e);
            }
        }
        private void OnPageChange(object sender, PageMouseMoveEventArgs e)
        {
            if (OnPageChanged != null)
            {
                OnPageChanged(sender, e);
            }
        }
        private void OnSendLog(object sender, ProgressEventArgs e)
        {
            //use this event to hide menu if is open

            if (OnLogText != null)
            {
                OnLogText(sender, e);
            }
        }
        private void OnMenuResetEvent(object sender, ProgressEventArgs e)
        {
            //use this event to hide menu if is open

            if (OnMenuReset != null)
            {
                OnMenuReset(sender, e);
            }
        }
        private void OnPlayMultimedia(object sender, MultimediaEventArgs e)
        {
            if (OnMultimedia != null)
            {
                OnMultimedia(sender, e);
            }
        }
        private void c(object sender, MultimediaEventArgs e)
        {
            if (OnAddMultimedia != null)
            {
                OnAddMultimedia(sender, e);
            }
        }
        #endregion

        #region CONSTRUCTOR
        public SinglePageView()
        {

            InitializeComponent();

            #region Display
            // Get Metrics
            var mainDisplayInfo = DeviceDisplay.MainDisplayInfo;

            displayProportion = mainDisplayInfo.Width / mainDisplayInfo.Height;

            //logInstance.Log(user,Severity.Trace, MethodBase.GetCurrentMethod(), null, "mainDisplayInfo : " + mainDisplayInfo.ToString());
            //logInstance.Log(user,Severity.Trace, MethodBase.GetCurrentMethod(),null,"W " + mainDisplayInfo.Width + " H " + mainDisplayInfo.Height + " P " + displayProportion.ToString("#.##"));

            #endregion

            var template = new DataTemplate(typeof(TextCell));
            template.SetValue(TextCell.TextColorProperty, Color.Orange);
            template.SetBinding(TextCell.TextProperty, ".");

            BindingContext = this;

        }

        #endregion

        #region ADD MULTIMEDIA ON PAGE
        public void StartAddingMultimediaOnPage(MultimediaType multimediaType)
        {
            //swipeMain.IsEnabled = false;
            multimediaTypeToAdd = multimediaType;

            //activate move
            //on move show icon for that kind of multimedia type
            //finish on mouse click
            AddMultimediaOnPage = true;
        }
        public void StopAddingMultimediaOnPage()
        {
            // swipeMain.IsEnabled = true;

            //activate move
            //on move show icon for that kind of multimedia type
            //finish on mouse click
            multimediaTypeToAdd = MultimediaType.Unknown;
            AddMultimediaOnPage = false;
            this.skiaViewPage.InvalidateSurface();
        }
        private void MultimediaParser_OnSetMultimedia(object sender, MultimediaRulerEventArgs e)
        {
            //resend to main page to set dragable images for ruler
            e.imageInfo = imageInfo;
            //OnMultimediaRuller(this, e);
        }

        #endregion

        public async void LoadPage(string strFindAndLocation = "")
        {
            try
            {
                StrFindAndLocation = strFindAndLocation;
                prepareSelectIntoClipoard = "";
                hoverAnno = null;
                findItem = null;
                // Dip = null; get dip from cache or XML
                try
                {
                    DrawItemOnPage = await GetManualDrawing();
                }
                catch (Exception ee)
                {

                    await logInstance.LogAsync(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                }

                selectedText = new List<OrigWordPossition>();
                showMultimediaHoverText = true;

                if (CurrentBook != null)
                {
                    if (CurrentBook.HTPageAnnos.Count > 0)
                    {
                        currentPageAnno = (PageAnno)CurrentBook.HTPageAnnos[pageNumber];
                    }
                    CurrentBook.CurrentPage = pageNumber;
                    if (Application.Current.MainPage != null)
                    {
                        if (Application.Current.MainPage.Height > Application.Current.MainPage.Width)
                        {
                            regularPage.HeightRequest = Application.Current.MainPage.Height - 100;
                            skiaViewPage.HeightRequest = Application.Current.MainPage.Height - 100;
                        }
                        else
                        {
                            regularPage.HeightRequest = Application.Current.MainPage.Width - 100;
                            //skiaViewPage.HeightRequest = (Application.Current.MainPage.Width /2) - 110;
                            skiaViewPage.HeightRequest = (Application.Current.MainPage.Width / 2) - 60;
                        }

                        skiaViewPage.InvalidateSurface();
                        if (ZoomPage)
                            skiaViewLeftFull.InvalidateSurface();
                    }
                }
                else
                {
                    await logInstance.LogAsync(user, Severity.Critical, MethodBase.GetCurrentMethod(), null, "CurrentBook book is null");
                }

            }
            catch (Exception ee)
            {

                await logInstance.LogAsync(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        /// <summary>
        /// stop drawing from Delete Button
        /// </summary>

        #region DRAW ON PAGE

        private async void skiaViewLeft_PaintSurface(object sender, SkiaSharp.Views.Forms.SKPaintSurfaceEventArgs e)
        {
            searchTextParser = await ShowPage_PaintSurface_EventHandlerAsync(e, hoverAnno, searchTextParser, multimediaParser, startXforRemove, newXforRemove);
        }
        /// <summary>
        /// Handler for skiashar PaintSurface --- DRAW
        /// </summary>
        /// <param name="pageType"></param>
        /// <param name="PageNum"></param>
        /// <param name="e"></param>
        /// <param name="hoverAnno"></param>
        /// <param name="searchTextParser"></param>
        /// <param name="multimediaParser"></param>
        private async Task<SearchTextParser> ShowPage_PaintSurface_EventHandlerAsync(
            SKPaintSurfaceEventArgs e,
            Anno hoverAnno,
            SearchTextParser searchTextParser,
            MultimediaParser multimediaParser,
            float startXforRemove,
            float newXforRemove)
        {
            Common.user = user;
            Base.user = user;
            searchTextParser = new SearchTextParser(user, e, bookEncoding);
            multimediaParser = null;
            searchTextParser.CurrentPage = pageNumber;
            searchTextParser.RullerWidth = pageRullerWidth;
            searchTextParser.RullerSmallStep = RullerSmallStep;
            searchTextParser.ShadowWidth = ShadowWidth;
            searchTextParser.RullerBigStep = RullerBigStep;
            if (CurrentBook != null && CurrentBook.Status == true)
            {
                try
                {
                    //ProgressEventArgs pet2 = new ProgressEventArgs("BaseProp 1 : " + searchTextParser.BaseProp);
                    //OnSendLog(this, pet2);

                    searchTextParser.BaseProp = searchTextParser.BaseProp;
                    searchTextParser.HProp = HProp;
                    searchTextParser.YProp = YProp;
                    searchTextParser.XProp = XProp;
                    searchTextParser.WProp = WProp;

                    #region PreareForSearch -read xml and decode
                    if (DrawItemOnPage == null)
                    {
                        string searchFileCorporate = "";
                        searchFileCorporate = CurrentBook.getPageSearchXMLPath(PageNumber);
                        string[] strLines = null;
                        if (HtStrLines.ContainsKey(PageNumber))
                        {
                            strLines = (string[])HtStrLines[PageNumber];
                            foreach (var item in strLines)
                            {
                                searchTextParser.AllWordsOnPage.Add(item);
                            }
                        }
                        else
                        {
                            strLines = searchTextParser.parseSearchFileForFlipCorporate(searchFileCorporate);
                            HtStrLines.Add(PageNumber, strLines);
                        }
                        lastBaseProp = searchTextParser.BaseProp;
                    }

                    #endregion

                    #region GetCurrentPageImage-decrypt
                    string pageImageCorporate = CurrentBook.getPagePath(pageNumber);
                    string resourceID = pageImageCorporate;
                    if (lastPageNum != pageNumber)
                    {
                        using (var streamReader = new StreamReader(resourceID))
                        {
                            var bytes = default(byte[]);
                            using (var memstream = new MemoryStream())
                            {
                                streamReader.BaseStream.CopyTo(memstream);
                                bytes = memstream.ToArray();
                                if (Encoding)
                                {
                                    byte[] res = Common.encodeByteArray(bytes);
                                    myBitmap = SKBitmap.Decode(res);
                                }
                                else
                                {
                                    myBitmap = SKBitmap.Decode(bytes);
                                }


                            }
                        }
                    }
                    #endregion

                    if (DrawItemOnPage == null)
                        prepareSearch();

                    #region draw on canvas
                    if (myBitmap != null)
                    {
                        //if (SinglePageMode)
                        //    ShowRuler = false;

                        bool showShadow = true;
                        if (SinglePageMode)
                            showShadow = false;

                        float strLenKoef = 2;

                        //if(ZoomPage)
                        //{
                        //    //try to calculate scale
                        //    FScale = 1.5f;
                        //}
                        //float.TryParse(txtSearch.Text, out strLenKoef);
                        //1. draw page image
                        //2. draw hover if exist
                        imageInfo = await searchTextParser.ShowPageEventHandler(
                            ShowRuler,
                            showShadow,
                            thisPageType,
                            myBitmap,
                            FScale,
                            hoverAnno,
                            AutoBaseProp,
                            pageBackgroundColor,
                            DrawItemOnPage,
                            strLenKoef,
                            rulerAnno,
                            ReadyForMove,
                            SelectedText,
                            testSearch,
                            StrFindAndLocation);
                        searchTextParser.BaseProp = imageInfo.Height;
                        if (testSearch)
                        {
                            testSearch = false;
                        }

                        //ProgressEventArgs pet2 = new ProgressEventArgs("imageInfo W : " + imageInfo.Width + " H: " + imageInfo.Height );
                        //OnSendLog(this, pet2);
                    }
                    else
                    {
                        logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), null, "ShowPage_PaintSurface_EventHandlerAsync, myBitmap is null");
                    }
                    #endregion

                    string imageFileName = Book + pageImageCorporate;


                }
                catch (Exception ee)
                {

                    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                }


                #region MULTIMEDIA PARSER
                if (searchTextParser.Canvas != null)
                {
                    //we are not drawing
                    //if (Dip == null)
                    {
                        #region prepare
                        multimediaParser = new MultimediaParser(user, imagePossition, searchTextParser.Canvas, imageInfo);

                        multimediaParser.RullerWidth = pageRullerWidth;
                        multimediaParser.CurrentPage = PageNumber;
                        multimediaParser.AutoBaseProp = autoBaseProp;
                        multimediaParser.BmpAudio = BmpAudio;
                        multimediaParser.BmpExternalLink = BmpExternalLink;
                        multimediaParser.BmpPhotoSlide = BmpPhotoSlide;
                        multimediaParser.BmpFile = BmpFile;

                        multimediaParser.BmpPhotoRight = BmpPhotoRight;
                        multimediaParser.BmpPhotoLeft = BmpPhotoLeft;

                        multimediaParser.BmpTextLeft = BmpTextLeft;
                        multimediaParser.BmpTextRight = BmpTextRight;

                        multimediaParser.BmpAudioLeft = BmpAudioLeft;
                        multimediaParser.BmpAudioRight = BmpAudioRight;
                        multimediaParser.BmpVideoLeft = BmpVideoLeft;
                        multimediaParser.BmpVideoRight = BmpVideoRight;

                        multimediaParser.BmpYoutubeRight = bmpYoutubeRight;
                        multimediaParser.BmpYoutubeLeft = bmpYoutubeLeft;

                        multimediaParser.BmpLinkRight = BmpLinkRight;
                        multimediaParser.BmpLinkLeft = BmpLinkLeft;

                        multimediaParser.BmpFileRight = BmpFileRight;
                        multimediaParser.BmpFileLeft = BmpFileLeft;
                        //
                        multimediaParser.BmpPhotoRightCustom = bmpPhotoRightCustom;
                        multimediaParser.BmpPhotoLeftCustom = bmpPhotoLeftCustom;

                        multimediaParser.BmpTextRightCustom = bmpTextRightCustom;
                        multimediaParser.BmpTextLeftCustom = bmpTextLeftCustom;

                        multimediaParser.BmpAudioRightCustom = bmpAudioRightCustom;
                        multimediaParser.BmpAudioLeftCustom = bmpAudioLeftCustom;

                        multimediaParser.BmpVideoRightCustom = bmpVideoRightCustom;
                        multimediaParser.BmpVideoLeftCustom = bmpVideoLeftCustom;

                        multimediaParser.BmpYoutubeRightCustom = bmpYoutubeRightCustom;
                        multimediaParser.BmpYoutubeLeftCustom = bmpYoutubeLeftCustom;

                        multimediaParser.BmpLinkRightCustom = bmpLinkRightCustom;
                        multimediaParser.BmpLinkLeftCustom = bmpLinkLeftCustom;

                        multimediaParser.BmpFileRightCustom = BmpFileRightCustom;
                        multimediaParser.BmpFileLeftCustom = BmpFileLeftCustom;
                        //
                        multimediaParser.BmpOpenWindow = BmpOpenWindow;
                        multimediaParser.BmpVideo = BmpVideo;
                        multimediaParser.BmpText = BmpText;
                        multimediaParser.BmpYouTube = bmpYouTube;

                        multimediaParser.BmpNoteAdd = BmpNoteAdd;
                        multimediaParser.BmpAudioAdd = BmpAudioAdd;
                        multimediaParser.BmpVideoAdd = BmpVideoAdd;
                        multimediaParser.BmpPhotoAdd = bmpPhotoAdd;
                        multimediaParser.BmpFileAdd = BmpFileAdd;
                        multimediaParser.BmpYoutubeAdd = BmpYoutubeAdd;
                        multimediaParser.BmpLinkAdd = BmpLinkAdd;

                        multimediaParser.BaseProp = searchTextParser.BaseProp;
                        multimediaParser.HProp = HProp;
                        multimediaParser.YProp = YProp;
                        multimediaParser.XProp = XProp;
                        multimediaParser.WProp = WProp;
                        multimediaParser.OnSetMultimedia += MultimediaParser_OnSetMultimedia;
                        #endregion

                        //2. draw multimedia icon on page
                        //3. draw multimedia annonation on ruler


                        string res = await multimediaParser.MultimediaItemsDrawOnPage(
                            FScale, ShowRuler,
                            CurrentBook,
                            thisPageType,
                            readyForMove,
                            startXforRemove,
                            newXforRemove,
                            tabletMode,
                            multimediaTypeToAdd);

                        //ProgressEventArgs pet3 = new ProgressEventArgs(res);
                        //OnSendLog(this, pet3);
                    }
                }
                #endregion
            }
            else
            {
                logInstance.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "CurrentBook is null or status is false ");
            }
            lastPageNum = pageNumber;
            return searchTextParser;
        }
        #endregion

        #region SEARCH
        public void prepareSearch()
        {
            #region prepare search, load all words for that page
            //showAllSearchForCalibration=true will show rectangle over all search item on page
            //left page have ruler that move all search positions for widht !
            if (HtPageWordsCache.Contains(PageNumber))
            {
                searchTextParser.AllWordsOnPage = AllWordsOnPageCache;
                searchTextParser.HtPageWords = (Hashtable)HtPageWordsCache[pageNumber];
                searchTextParser.HtNumberAndWords = (Hashtable)HtNumberAndWordsCache[pageNumber];
                searchTextParser.HtPageWordsOriginal = (Hashtable)HtPageWordsOriginalCache[pageNumber];

            }
            else
            {
                ResetSearchParser();
            }

            #endregion
        }
        public void ResetSearchParser()
        {
            if (searchTextParser != null)
            {
                if (HtPageWordsCache.ContainsKey(pageNumber))
                {
                    HtPageWordsCache[pageNumber] = searchTextParser.SearchItems(thisPageType, FScale, leftRulerOn, ShowAllSearchForCalibration, searchTextActive, true);
                }
                else
                {
                    HtPageWordsCache.Add(pageNumber, searchTextParser.SearchItems(thisPageType, FScale, leftRulerOn, ShowAllSearchForCalibration, searchTextActive));
                }

                if (HtNumberAndWordsCache.ContainsKey(pageNumber))
                {
                    HtNumberAndWordsCache[pageNumber] = searchTextParser.HtNumberAndWords;

                }
                else
                {
                    HtNumberAndWordsCache.Add(pageNumber, searchTextParser.HtNumberAndWords);
                }

                if (HtPageWordsOriginalCache.ContainsKey(pageNumber))
                {
                    HtPageWordsOriginalCache[pageNumber] = searchTextParser.HtPageWordsOriginal;

                }
                else
                {
                    HtPageWordsOriginalCache.Add(pageNumber, searchTextParser.HtPageWordsOriginal);
                }


                AllWordsOnPageCache = searchTextParser.AllWordsOnPage;

                searchTextParser.HtPageWords = (Hashtable)HtPageWordsCache[pageNumber];
                searchTextParser.HtNumberAndWords = (Hashtable)HtNumberAndWordsCache[pageNumber];
                searchTextParser.HtPageWordsOriginal = (Hashtable)HtPageWordsOriginalCache[pageNumber];

                lastBaseProp = searchTextParser.BaseProp;
            }
        }
        #endregion

        #region MessageBox
        public void ShowLogInfo(string message)
        {
            var page = new ContentPage();
            page.DisplayAlert("Test Title", message, "OK");
        }
        #endregion

        #region Mouse Click


        SKPoint ConvertToPixel(Point pt)
        {
            return new SKPoint((float)(skiaViewPage.CanvasSize.Width * pt.X / skiaViewPage.Width),
                              (float)(skiaViewPage.CanvasSize.Height * (pt.Y) / skiaViewPage.Height));
        }
        DateTime lastKeyPress;
        private async Task<Anno> PageTouch_Touch_EventHandler(TouchActionEventArgs e, PageAnno currentPageAnno, SearchTextParser searchTextParser, SKCanvasView skiaView)
        {

            float selectedX = (float)e.Location.X;
            float selectedY = (float)e.Location.Y;
            imagePossition = ConvertToPixel(new Point(selectedX, selectedY));
            selectedX = imagePossition.X;
            selectedY = imagePossition.Y;
            rulerAnno = false;

            #region FIND HOVER

            Anno hoverAnno = null;

            if (AddMultimediaOnPage == false)
            {

                #region Automatic BaseProp
                try
                {
                    if (AutoBaseProp && searchTextParser != null)
                    {
                        
                        searchTextParser.BaseProp = imageInfo.Height;
                    }

                }
                catch (Exception ee)
                {

                    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                }
                #endregion

                try
                {
                    if (currentPageAnno != null)
                    {
                        hoverAnno = currentPageAnno.FindAnnoByLocation(selectedX, selectedY, fScale);
                        if (ShowRuler)
                        {
                            if (hoverAnno != null)
                            {
                                rulerAnno = false;
                            }
                            else
                            {
                                if (thisPageType == PageType.Right && selectedX > imageInfo.Width)
                                {
                                    Anno findAnnoR = currentPageAnno.FindAnnoByYLocation(selectedY, fScale);
                                    if (findAnnoR != null)
                                    {
                                        rulerAnno = true;
                                        hoverAnno = findAnnoR;
                                    }
                                }
                                if (thisPageType == PageType.Left && selectedX < PageRullerWidth)
                                {
                                    Anno findAnnoR = currentPageAnno.FindAnnoByYLocation(selectedY, fScale);
                                    if (findAnnoR != null)
                                    {
                                        rulerAnno = true;
                                        hoverAnno = findAnnoR;
                                    }
                                }
                            }
                        }
                    }


                }
                catch (Exception ee)
                {

                    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                }

                if (multimediaPreview == false)
                {
                    if (hoverAnno != null)
                    {
                        //set Hover this is for Windows only
                        if (showMultimediaHoverText == false)
                            skiaView.InvalidateSurface();

                        showMultimediaHoverText = true;
                    }
                    else
                    {
                        //why!!
                        //if (showMultimediaHoverText == true)
                        //  skiaView.InvalidateSurface();

                        showMultimediaHoverText = false;
                    }

                }

            }
            #endregion

            switch (e.Type)
            {
                case TouchActionType.Pressed:
                    #region SEARCH

                    //ProgressEventArgs pes = new ProgressEventArgs("MOUSE x: " + selectedX.ToString() + " y: " + selectedY.ToString() + " e.ActionType: " + e.Type);
                    //OnSendLog(this, pes);

                    if (SelectTextActive)
                    {
                        if (searchTextParser != null)
                        {
                            if (lastBaseProp != searchTextParser.BaseProp)
                            {
                                ResetSearchParser();
                            }
                            if (searchTextParser.HtPageWords.Count == 0)
                            {
                                ResetSearchParser();
                            }
                            findItem = searchTextParser.GetSearchItem(imagePossition);
                            if (findItem != null)
                            {
                                string findSearch = findItem.FindSearch;
                                if (findSearch != null)
                                {
                                    if (findSearch.EndsWith(":"))
                                    {
                                        findSearch = findSearch.Remove(findSearch.Length - 1);
                                        //txtSearch.Text = findSearch;
                                        try
                                        {

                                            ProgressEventArgs peF = new ProgressEventArgs(findSearch + " x: " + selectedX + " y: " + selectedY);
                                            OnSearchTextFind?.Invoke(this, peF);
                                            if (SelectedText.Contains(findItem) == false)
                                            {
                                                SelectedText.Add(findItem);
                                            }
                                            PrepareSelectIntoClipoard = "";
                                            foreach (OrigWordPossition oneFindItem in SelectedText)
                                            {

                                                if (oneFindItem != null)
                                                {
                                                    PrepareSelectIntoClipoard += oneFindItem.FindSearch + " ";
                                                }

                                            }
                                            await Clipboard.SetTextAsync(PrepareSelectIntoClipoard);
                                            skiaViewPage.InvalidateSurface();

                                        }
                                        catch (Exception ee)
                                        {

                                            logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                        }

                                    }
                                }
                            }
                            lastBaseProp = searchTextParser.BaseProp;
                        }
                    }
                    else
                    {
                        SelectedText = new List<OrigWordPossition>();
                    }
                    #endregion

                    #region CHECK IS CUSTOM MULTIMEDIA FOR MOVE
                    //if (hoverAnno != null && rulerAnno == true)
                    //{
                    //    ReadyForMove = true;
                    //    startXforRemove = (float)e.Location.X;
                    //    ProgressEventArgs pe = new ProgressEventArgs("PRESS ReadyForMove: " + ReadyForMove.ToString());
                    //    OnSendLog(this, pe);
                    //    LoadPage();
                    //}
                    #endregion

                    #region DRAWING ON PAGE
                    if (DrawItemOnPage != null)
                    {

                        var paintLine = new SKPaint
                        {
                            Style = SKPaintStyle.Stroke,
                            StrokeWidth = (float)DrawItemOnPage.DrawOnPagePenSize,
                            Color = DrawItemOnPage.DrawOnPageColor
                        };

                        if (DrawItemOnPage.DrawOnPagePenType == DrawType.Line)
                        {
                            DrawItemOnPage.PaintLine = paintLine;

                            double dnewX = selectedX / (searchTextParser.BaseProp * XProp);
                            double dnewY = selectedY / (searchTextParser.BaseProp * YProp);
                            DrawItemOnPage.PointStartLine = new SKPoint((float)dnewX, (float)dnewY);
                            MyLineList ml = new MyLineList(DrawItemOnPage.PointStartLine, DrawItemOnPage.PointStartLine, DrawItemOnPage.DrawOnPagePenSize, DrawItemOnPage.DrawLineColor);
                            DrawItemOnPage.HtLines.Add(DrawItemOnPage.LineCounter, ml);
                        }

                        if (DrawItemOnPage.DrawOnPagePenType == DrawType.Pen)
                        {
                            DrawItemOnPage.PaintLine = paintLine;
                            DrawItemOnPage.StartPenDraw = true;
                            DrawItemOnPage.PointStartPen = new SKPoint((float)(selectedX / (searchTextParser.BaseProp * XProp)), (float)(selectedY / (searchTextParser.BaseProp * YProp)));
                            //Dip.ListPoints = new List<SKPoint>();
                            MyLineList penList = new MyLineList(DrawItemOnPage.PointStartLine, DrawItemOnPage.PointEndLine, DrawItemOnPage.DrawOnPagePenSize, DrawItemOnPage.DrawLineColor);
                            if (DrawItemOnPage.HtDraws.ContainsKey(DrawItemOnPage.DrawCounter) == false)
                            {
                                DrawItemOnPage.HtDraws.Add(DrawItemOnPage.DrawCounter, penList);
                            }

                        }

                        skiaViewPage.InvalidateSurface();
                    }
                    #endregion

                    if (ReadyForMove == false)
                    {
                        #region MULTIMEDIA
                        if (hoverAnno != null)
                        {
                            int imageWidth = 600;
                            int imageHeight = 800;

                            //check for double click
                            if (lastKeyPress != null)
                            {
                                TimeSpan ts = DateTime.Now - lastKeyPress;

                                if (ts.TotalMilliseconds < 400)
                                {
                                    break;

                                }
                            }
                            switch (hoverAnno.Action.ActionType)
                            {
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenURL":

                                    await Common.OpenWebPage(hoverAnno.Action.Url);
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenWindow":
                                    //video??
                                    if (hoverAnno.Action.ResourceContent != null)
                                    {
                                        string videoUrl = hoverAnno.Action.ResourceContent.Replace("./files/", "");
                                        videoUrl = videoUrl.Replace("/", Common.GetPathSeparator());
                                        //video should be rendered as mp4, currently is flv
                                        //for test

                                        //send event
                                        //videoUrl = videoUrl.Replace(".flv", ".mp4");
                                        //PlayMultimedia(videoUrl, findAnno);

                                        try
                                        {
                                            MultimediaEventArgs pe2 = new MultimediaEventArgs(MultimediaType.Video, imageWidth, imageHeight, hoverAnno, videoUrl);
                                            OnPlayMultimedia(this, pe2);
                                        }
                                        catch (Exception ee)
                                        {

                                            logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                        }
                                    }
                                    else
                                    {
                                        logInstance.Log(user, Severity.Low, MethodBase.GetCurrentMethod(), null, "Video multimedia but no video file: Anno.Action.ResourceContent");
                                    }
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionShowInformation":
                                    try
                                    {
                                        imageWidth = 500;
                                        imageHeight = 400;
                                        hoverAnno.PageNumber = PageNumber;
                                        MultimediaEventArgs pe2 = new MultimediaEventArgs(MultimediaType.Note, imageWidth, imageHeight, hoverAnno, "");

                                        OnPlayMultimedia(this, pe2);
                                    }
                                    catch (Exception ee)
                                    {

                                        logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                    }
                                    break;
                                case "com.mobiano.flipbook.Action.TAnnoActionPlayAudio":
                                    {

                                        string autidoUrl = hoverAnno.Action.AudioURL.Replace("./files/", "");
                                        autidoUrl = autidoUrl.Replace("/", Common.GetPathSeparator());

                                        //send event
                                        //PlayMultimedia(autidoUrl, findAnno);
                                        try
                                        {
                                            MultimediaEventArgs pe2 = new MultimediaEventArgs(MultimediaType.Audio, imageWidth, imageHeight, hoverAnno, autidoUrl);
                                            OnPlayMultimedia(this, pe2);
                                        }
                                        catch (Exception ee)
                                        {

                                            logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                        }

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayAudio":
                                    {
                                        string autidoUrl = hoverAnno.Action.AudioURL.Replace("./files/", "");
                                        autidoUrl = autidoUrl.Replace("/", Common.GetPathSeparator());

                                        //send event
                                        //PlayMultimedia(autidoUrl, findAnno);
                                        try
                                        {
                                            MultimediaEventArgs pe2 = new MultimediaEventArgs(MultimediaType.Audio, imageWidth, imageHeight, hoverAnno, autidoUrl);
                                            OnPlayMultimedia(this, pe2);
                                        }
                                        catch (Exception ee)
                                        {

                                            logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                        }

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPhotoSlide":
                                    {

                                        try
                                        {
                                            MultimediaEventArgs pe2 = new MultimediaEventArgs(MultimediaType.PhotoGalery, imageWidth, imageHeight, hoverAnno, "");
                                            OnPlayMultimedia(this, pe2);
                                        }
                                        catch (Exception ee)
                                        {

                                            logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                        }

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionFile":
                                    {

                                        try
                                        {
                                            MultimediaEventArgs pe2 = new MultimediaEventArgs(MultimediaType.File, imageWidth, imageHeight, hoverAnno, "");
                                            OnPlayMultimedia(this, pe2);
                                        }
                                        catch (Exception ee)
                                        {

                                            logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                        }

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayVideo":
                                    {
                                        if (hoverAnno.Action.ResourceContent != null)
                                        {
                                            string videoUrl = hoverAnno.Action.ResourceContent.Replace("./files/", "");
                                            videoUrl = videoUrl.Replace("/", Common.GetPathSeparator());
                                            //video should be rendered as mp4, currently is flv
                                            //for test

                                            //send event
                                            videoUrl = videoUrl.Replace(".flv", ".mp4");

                                            try
                                            {
                                                MultimediaEventArgs pe2 = new MultimediaEventArgs(MultimediaType.Video, imageWidth, imageHeight, hoverAnno, videoUrl);
                                                OnPlayMultimedia(this, pe2);
                                            }
                                            catch (Exception ee)
                                            {

                                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                            }
                                        }
                                        if (hoverAnno.Action.WindowType == "TYPE_YOUTUBE")
                                        {
                                            try
                                            {
                                                //todo: youtube URL missing!
                                                string link = "";
                                                MultimediaEventArgs pe2 = new MultimediaEventArgs(MultimediaType.Link, imageWidth, imageHeight, hoverAnno, link);
                                                OnPlayMultimedia(this, pe2);
                                            }
                                            catch (Exception ee)
                                            {

                                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                            }
                                        }
                                        break;
                                    }

                                default:
                                    string missing = hoverAnno.Action.ActionType;
                                    break;
                            }
                        }
                        else
                        {
                            #region zoom page
                            try
                            {
                                //find double click
                                if (lastKeyPress != null)
                                {
                                    TimeSpan ts = DateTime.Now - lastKeyPress;

                                    if (ts.TotalMilliseconds < 400)
                                    {
                                        string doubleClisk = "double click";
                                        if (ZoomPage == false)
                                        {
                                            //goto zoom
                                            ZoomPage = true;
                                            //skiaViewLeftFull.IsVisible = true;
                                            //swipViewPage.IsVisible = false;
                                            //swipeMain.IsEnabled = false;
                                        }
                                        else
                                        {
                                            //back to the regular
                                            ZoomPage = false;
                                            //skiaViewLeftFull.IsVisible = false;
                                            //swipViewPage.IsVisible = true;
                                            //swipeMain.IsEnabled = true;
                                        }
                                        OnDoubleClickEvent(this, e);
                                    }
                                }




                            }
                            catch (Exception ee)
                            {
                                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                            }
                            #endregion
                        }
                        #endregion
                    }
                    //keep info for last screen press
                    lastKeyPress = DateTime.Now;
                    break;

                case TouchActionType.Moved:
                    //ProgressEventArgs pes2 = new ProgressEventArgs("Mouse Moved x: " + selectedX.ToString() + " y: " + selectedY.ToString() + " e.ActionType: " + e.Type);
                    //OnSendLog(this, pes2);


                    if (DrawItemOnPage == null)
                    {
                        // PageMouseMoveEventArgs pem = new PageMouseMoveEventArgs(thisPageType, e.Location.X, e.Location.Y, imageInfo.Width, imageInfo.Height);
                        // OnPageChanged?.Invoke(this, pem);
                    }

                    #region AddMultimediaOnPage
                    if (AddMultimediaOnPage)
                    {
                        skiaView.InvalidateSurface();
                    }
                    #endregion

                    #region ReadyForMove
                    if (ReadyForMove == true)
                    {
                        newXforRemove = (float)e.Location.X;
                        if (ThisPageType == PageType.Left)
                        {
                            LoadPage();
                            if ((startXforRemove - newXforRemove) > 5)
                            {
                                //send event to remove custom multimedia
                                //ProgressEventArgs pet2 = new ProgressEventArgs("PRESS x: " + x.ToString() + " y: " + y.ToString());
                                //OnSendLog(this, pet2);
                                try
                                {
                                    if (hoverAnno != null)
                                    {
                                        hoverAnno.PageNumber = PageNumber;
                                        MultimediaRemoveEventArgs pe2 = new MultimediaRemoveEventArgs(hoverAnno);
                                        OnRemoveMultimediaH(this, pe2);
                                    }
                                }
                                catch (Exception ee)
                                {

                                    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                }
                                ReadyForMove = false;
                            }
                        }

                        if (ThisPageType == PageType.Right)
                        {
                            LoadPage();
                            if ((newXforRemove + startXforRemove) > 15)
                            {
                                //send event to remove custom multimedia
                                //ProgressEventArgs pet2 = new ProgressEventArgs("PRESS x: " + x.ToString() + " y: " + y.ToString());
                                //OnSendLog(this, pet2);
                                try
                                {
                                    if (hoverAnno != null)
                                    {
                                        hoverAnno.PageNumber = PageNumber;
                                        MultimediaRemoveEventArgs pe2 = new MultimediaRemoveEventArgs(hoverAnno);
                                        OnRemoveMultimediaH(this, pe2);
                                    }
                                }
                                catch (Exception ee)
                                {

                                    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                                }
                                ReadyForMove = false;
                            }
                        }
                    }
                    #endregion

                    #region DRAWING ON PAGE
                    /*
                    if (DrawItemOnPage != null)
                    {
                        if (DrawItemOnPage.DrawOnPagePenType == DrawType.Line)
                        {
                            if (DrawItemOnPage.PointStartLine != new SKPoint(0, 0))
                            {
                                DrawItemOnPage.PointEndLine = new SKPoint((float)(selectedX / (BaseProp * XProp)), (float)(selectedY / (BaseProp * YProp)));
                                if (DrawItemOnPage.HtLines.ContainsKey(DrawItemOnPage.LineCounter))
                                {
                                    MyLineList ml = (MyLineList)DrawItemOnPage.HtLines[DrawItemOnPage.LineCounter];
                                    ml.PointEnd = DrawItemOnPage.PointEndLine;
                                    skiaViewPage.InvalidateSurface();
                                }
                            }
                        }

                        if (DrawItemOnPage.DrawOnPagePenType == DrawType.Pen && DrawItemOnPage.StartPenDraw == true)
                        {
                            selectedX = (float)(selectedX / (BaseProp * XProp));
                            selectedY = (float)(selectedY / (BaseProp * YProp));
                            DrawItemOnPage.PointStartPen = new SKPoint(selectedX, selectedY);                            
                            if (DrawItemOnPage.HtDraws.ContainsKey(DrawItemOnPage.DrawCounter) == true)
                            {
                                MyLineList penList = (MyLineList)DrawItemOnPage.HtDraws[DrawItemOnPage.DrawCounter];
                                penList.ListPoints.Add(DrawItemOnPage.PointStartPen);
                                DrawItemOnPage.HtDraws[DrawItemOnPage.DrawCounter] = penList;

                                ProgressEventArgs pes = new ProgressEventArgs("Drawing free items: " + penList.ListPoints.Count);
                                //OnSendLog(this, pes);

                                skiaView.InvalidateSurface();
                            }

                        }
                    }
                    */
                    #endregion

                    break;

                case TouchActionType.Entered:

                    //lblSelect.Text = "Enterred: " + x.ToString() + " " + y.ToString();
                    //ProgressEventArgs pet = new ProgressEventArgs("PRESS x: " + selectedX.ToString() + " y: " + selectedY.ToString());
                    //OnSendLog(this, pet);
                    break;
                case TouchActionType.Released:
                    //ProgressEventArgs pet2 = new ProgressEventArgs("Released x: " + selectedX.ToString() + " y: " + selectedY.ToString());
                    //OnSendLog(this, pet2);

                    ReadyForMove = false;
                    startXforRemove = 0;
                    newXforRemove = 0;


                    #region ADD NEW MULTIMEDIA ON PAGE
                    //Note, File, Audio, Video, Youtoube, Link, Photo
                    if (AddMultimediaOnPage)
                    {
                        //load new note menu
                        int imageWidth = 600;
                        int imageHeight = 500;
                        hoverAnno = new Anno();
                        hoverAnno.Cutom = true;
                        hoverAnno.PageNumber = PageNumber;

                        KlasimeBase.Action act = new KlasimeBase.Action();
                        KlasimeBase.Location loc = new KlasimeBase.Location();

                        act.ActionType = MultimediaPath.GetActionType(multimediaTypeToAdd);
                        if (multimediaTypeToAdd == MultimediaType.YouTube)
                        {
                            act.WindowType = "TYPE_YOUTUBE";
                        }



                        hoverAnno.Action = act;
                        loc.X = selectedX / (searchTextParser.BaseProp * XProp);
                        loc.Y = selectedY / (searchTextParser.BaseProp * YProp);
                        hoverAnno.Location = loc;
                        hoverAnno.MultimediaTypeToAdd = multimediaTypeToAdd;
                        MultimediaEventArgs pe2 = new MultimediaEventArgs(multimediaTypeToAdd, imageWidth, imageHeight, hoverAnno, "");
                        pe2.AddNew = true;
                        OnAddMultimedia(this, pe2);

                        AddMultimediaOnPage = false;
                        multimediaTypeToAdd = MultimediaType.Unknown;
                    }
                    #endregion

                    #region DRAWING ON PAGE
                    /*
                    if (DrawItemOnPage != null)
                    {
                        bool storetocache = false;
                        if (DrawItemOnPage.DrawOnPagePenType == DrawType.Line)
                        {
                            DrawItemOnPage.LineCounter++;

                            DrawItemOnPage.PointStartLine = new SKPoint(0, 0);
                            DrawItemOnPage.PointEndLine = new SKPoint(0, 0);
                            storetocache = true;
                        }

                        if (DrawItemOnPage.DrawOnPagePenType == DrawType.Pen)
                        {
                            DrawItemOnPage.DrawCounter++;
                            DrawItemOnPage.StartPenDraw = false;
                            DrawItemOnPage.PointStartPen = new SKPoint(0, 0);
                            storetocache = true;
                        }
                        if(storetocache)
                        {
                            //store to cache/ XML
                            StoreCustomDrawing();
                        }
                    }
                    */
                    #endregion

                    break;
                case TouchActionType.Cancelled:
                    ProgressEventArgs pes3 = new ProgressEventArgs("Touch Canceled x: " + selectedX.ToString() + " y: " + selectedY.ToString() + " e.ActionType: " + e.Type);
                    //OnSendLog(this, pes3);
                    break;
            }
            return hoverAnno;
        }
        #endregion

        #region MANUAL DRAWING
        public async void StopDrawing()
        {
            DrawItemOnPage = null;
            //swipeMain.IsEnabled = true;
            await DropCustomDrawing();
            LoadPage();
            //clear from cache or XML
        }
        public void StartDrawing(DrawType drawOnPagePenType, DrawLineWidth drawOnPagePenSize, DrawLineColor drawOnPageColor)
        {
            //start drawing on page
            //get whole dip with lines from previous page, need
            //htLines and htDwars to be clear on new page
            //DrawItemOnPage = (DrawItemOnPage)dip;
            if (DrawItemOnPage == null)
            {
                DrawItemOnPage = new CDrawItemOnPage();

            }
            DrawItemOnPage.DrawLineColor = drawOnPageColor;
            DrawItemOnPage.DrawOnPagePenType = drawOnPagePenType;
            DrawItemOnPage.DrawOnPagePenSize = drawOnPagePenSize;


            DrawItemOnPage.PageNumber = this.pageNumber;
            //swipeMain.IsEnabled = false;
        }
        /*  private async void StoreCustomDrawing()
          {
              //

              await currentBook.AddCustomDrawing(DrawItemOnPage, "drawing");
          }*/
        /// <summary>
        /// get manual drawing for that page
        /// </summary>
        /// <returns></returns>
        private async Task<CDrawItemOnPage> GetManualDrawing()
        {
            return await Task.Run(() => CurrentBook.GetCustomDrawing("drawing", PageNumber));

        }

        private async Task<CDrawItemOnPage> DropCustomDrawing()
        {

            DrawItemOnPage = await Task.Run(() => CurrentBook.DropCustomDrawing("drawing", PageNumber));
            return DrawItemOnPage;
        }
        #endregion

        #region SWIPE
        public void EnableSwipe()
        {
            //swipeMain.IsEnabled = true;
        }
        public void DisableSwipe()
        {
            //swipeMain.IsEnabled = false;
        }

        private void rightSwiped_Swiped(object sender, SwipedEventArgs e)
        {
            if (DrawItemOnPage == null)
            {
                pageNumber--;
                if (pageNumber < 0)
                    pageNumber = 0;

                LoadPage();
            }
        }

        private void rightSwiped_Left(object sender, SwipedEventArgs e)
        {
            if (DrawItemOnPage == null)
            {
                pageNumber++;

                LoadPage();
            }
        }

        private async void OnTouchEffectAction(object sender, Touch.TouchActionEventArgs args)
        {
            try
            {
                //ProgressEventArgs pes = new ProgressEventArgs("MOUSE x: " + args.Location.X.ToString() + " y: " + args.Location.Y.ToString() + " e.ActionType: " + args.Type);
                //OnSendLog(this, pes);

                if (CurrentBook != null)
                {
                    if (CurrentBook.HTPageAnnos.Count > 0)
                    {
                        currentPageAnno = (PageAnno)CurrentBook.HTPageAnnos[pageNumber];
                    }
                }


                hoverAnno = await PageTouch_Touch_EventHandler(args, currentPageAnno, searchTextParser, skiaViewPage);

                //ProgressEventArgs pes = new ProgressEventArgs(" e.DeviceType: " + e.DeviceType);
                //OnMenuReset(this, pes);
            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }

        private void SwipeNext_Invoked(object sender, EventArgs e)
        {
            ProgressEventArgs pet = new ProgressEventArgs("Next");
            OnSwipeEvent(this, pet);
        }

        private void SwipePrev_Invoked(object sender, EventArgs e)
        {
            ProgressEventArgs pet = new ProgressEventArgs("Prev");
            OnSwipeEvent(this, pet);
        }




        private void PageScroll_Scrolled(object sender, ScrolledEventArgs e)
        {
            string c = "";
            //ProgressEventArgs pes = new ProgressEventArgs("SCROLL x: " + e.ScrollX + " x: " + e.ScrollY);
            //OnSendLog(this, pes);
        }
        #endregion

        private void skiaViewLeftFull_Focused(object sender, FocusEventArgs e)
        {

        }

        private void skiaViewLeftFull_Unfocused(object sender, FocusEventArgs e)
        {

        }

        private void ContentView_SizeChanged(object sender, EventArgs e)
        {
            ProgressEventArgs pes = new ProgressEventArgs("ContentView_SizeChanged H: " + this.Height);
            // OnSendLog(this, pes);
        }
    }
}