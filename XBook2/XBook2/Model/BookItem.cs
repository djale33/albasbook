﻿using KlasimeBase.Log;
using KlasimeBase;
using KlasimeViews;
using SharedKlasime;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XBook2.Views.Program;
using Xnook2.ViewModel;
using XBook2.ViewModels;

namespace XBook2.Model
{
    public class BookItem : ViewModelBase
    {
        #region VARIABLES
   
        string nameDownload, nameOpen, nameRemove, desc, errMessage, id;
        int bookId;
        AlbasBook albasBook;
        bool bookOpacity = true;
        #endregion

        #region COMMANDS
        public ICommand DownloadCommand { private set; get; }
        public ICommand ReadCommand { private set; get; }
        public ICommand RemoveCommand { private set; get; }
        public ICommand UnlockCommand { private set; get; }
        public BookGaleryListViewModel parentViewModel {get; set;}

        #endregion

        #region CONSTRUCTORS
        public BookItem(CUser _loggedUser, bool _IsSmallScreen): base (_loggedUser)
        {
            IsSmallScreen = _IsSmallScreen;
            LoggedUser = _loggedUser;
            InitCommands();
        }
        public BookItem(CUser _loggedUser, string id, string name, string desc, bool open, bool remove, bool exist, bool locked, bool download, bool notReady, bool _IsSmallScreen,double _ButtonTranslationY) : base(_loggedUser)
        {
            IsSmallScreen = _IsSmallScreen;
            LoggedUser = _loggedUser;
            Id = id;
            Name = name;
            Desc = desc;
            Open = open;
            Remove = remove;
            Exist = exist;
            Locked = locked;
            ButtonTranslationY = _ButtonTranslationY;
            Download = download;
            NotReady = notReady;

            if (remove)
            {
                NameRemove = "Remove" + name;
            }
            if (open)
            {
                NameOpen = "Read " + name;
            }
            if (download)
            {
                NameDownload = "Download " + name;
            }

            InitCommands();
        }
        #endregion

        #region PROPERTIES
        
        public int BookId { get; set; }
        public CUser loggedUser { get; set; }
        public string Name { get; set; }
        public string Authors { get; set; }
        public string ImageURL { get; set; }
        public bool exist;
        public bool notExist;
        public bool Open { get; set; }
        public bool Locked { get; set; }
        public bool Remove { get; set; }
        public bool Download { get; set; }
        public bool NotReady { get; set; }

        public bool Exist
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref exist, value);
                    NotExist = !exist;
                }
            }
            get
            {
                return exist;
            }
        }
        public bool NotExist
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref notExist, value);
                }
            }
            get
            {
                return notExist;
            }
        }
        public bool BookOpacity
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookOpacity, value);
                }
            }
            get
            {
                return BookOpacity;
            }
        }
        public string NameOpen
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref nameOpen, value);
                }
            }
            get
            {
                return nameOpen;
            }
        }
        public string NameRemove
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref nameRemove, value);
                }
            }
            get
            {
                return nameRemove;
            }
        }
        public string NameDownload
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref nameDownload, value);
                }
            }
            get
            {
                return nameDownload;
            }
        }
        public string Desc
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref desc, value);
                }
            }
            get
            {
                return desc;
            }
        }
        public string ErrMessage
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref errMessage, value);
                }
            }
            get
            {
                return errMessage;
            }
        }
        public string Id
        {
            set
            {
                if (value != null)
                {
                    int.TryParse(value, out bookId);
                    SetProperty(ref id, value);
                }
            }
            get
            {
                return id;
            }
        }
        public AlbasBook AlbasBook
        {
            set
            {
                if (value != null)
                {
                    
                    SetProperty(ref albasBook, value);
                }
            }
            get
            {
                return albasBook;
            }
        }
        //ButtonTranslationY        
        public double ButtonTranslationY { get; set; }
        public double FrameWidthRequest { get; set; }
        

        #endregion

        #region METHODS
        private void InitCommands()
        {
            DownloadCommand = new Command(() => DownloadBook());
            ReadCommand = new Command(() => ReadBook());
            RemoveCommand = new Command(() => RemoveBook());
            UnlockCommand = new Command(() => ActivateBook());
        }
        private async void ReadBook()
        {
            if(Exist)
            {

                //await Task.Run(() => { Application.Current.MainPage = new MainPage(loggedUser, bookId, null); });
                
                await Task.Run(() => {
                    parentViewModel.MainView = false;
                    parentViewModel.WaitOnBook = true; 
                    
                });
                                           
                
                System.Threading.Thread.Sleep(250);
                await Task.Run(() => {
                    
                    parentViewModel.WaitOnBook = true;

                });

                System.Threading.Thread.Sleep(250);
                Application.Current.MainPage = new MainPage(loggedUser, bookId, null);
            }    
                
        }
        private void DownloadBook()
        {
            BookPreviewControl bcn = new BookPreviewControl(loggedUser, albasBook, false, false,KlasimeBase.BookButtonAction.Download , IsSmallScreen);
            bcn.OnBookClick += Bcn_OnBookClick; ;
            Application.Current.MainPage = bcn;
        }
        private void ActivateBook()
        {
            BookPreviewControl bcn = new BookPreviewControl(loggedUser, albasBook, false, false, BookButtonAction.Activate, IsSmallScreen);
            bcn.OnBookClick += Bcn_OnBookClick;
            Application.Current.MainPage = bcn;
        }
        private async void RemoveBook()
        {
            try
            {
                bool answer = await App.Current.MainPage.DisplayAlert("Alert, delete Book", "Are you sure?", "Yes", "No");
                if (answer)
                {
                    await CleanUp(Id);
                    logInstance.Log(User, Severity.Trace, MethodBase.GetCurrentMethod(), null, "The Note was deleted ");
                    await App.Current.MainPage.DisplayAlert("Book was deleted", "Book Info", "Ok");
                    Application.Current.MainPage = new BookGaleryList(loggedUser, IsSmallScreen);
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            
            //await GetMyBooks();
            //await PreparePages();
        }
        private async Task CleanUp(string bookID)
        {
            string dir = BookBasePath + Common.GetPathSeparator() + bookID;
            do
            {
                try
                {
                    Directory.Delete(dir, true);
                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception ee)
                {
                    logInstance.Log(User, Severity.High, MethodBase.GetCurrentMethod(), ee, "");

                }
            } while (Directory.Exists(dir));
        }
        private void Bcn_OnBookClick(object sender, KlasimeBase.BookEventArgs e)
        {
            throw new NotImplementedException();
        }

        private double heightRequest;
        public double HeightRequest
        {
            get => heightRequest;
            set
            {
                if (value != null)
                {
                    SetProperty(ref heightRequest, value);
                }
            }
        }
        private double widthRequest;
        public double WidthRequest
        {
            get => widthRequest;
            set
            {
                if (value != null)
                {
                    SetProperty(ref widthRequest, value);
                }
            }
        }
        private double bookHeightRequest;

        public double BookHeightRequest
        {
            get => bookHeightRequest;
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookHeightRequest, value);
                }
            }
        }

        #endregion
    }
}