﻿namespace XBook2.Model
{
    public class Banner
    {

        public string title { get; set; }
        public string image_url { get; set; }
        public string url { get; set; }
        public string width { get; set; }
        public string height { get; set; }

    }
}