﻿using XBook2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using System.Collections;

namespace XBook2.Bahaviour
{
    internal class CarouselViewParallaxBehavior : Behavior<CarouselView>
    {
        public static readonly BindableProperty BookHeightProperty =
        BindableProperty.Create(nameof(BookHeight), typeof(double), typeof(CarouselViewParallaxBehavior), 200.0d,
            BindingMode.TwoWay, null,propertyChanged: OnEventNameChanged);
        //private double _HeightRequest = 300;
        private double _WidthRequest = 600;
        private Hashtable carouselItemsInit = new Hashtable();
        public double BookHeight
        {
            get { 
                return (double)GetValue(BookHeightProperty); 
            }
            set { 
                SetValue(BookHeightProperty, value); 
            }
        }
        static void OnEventNameChanged(BindableObject bindable, object oldValue, object newValue)
        {
            // Property changed implementation goes here
            string s= oldValue as string;
            string ns= newValue as string;
        }
        protected override void OnAttachedTo(CarouselView bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.Scrolled += OnScrolled;
        }

        protected override void OnDetachingFrom(CarouselView bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.Scrolled -= OnScrolled;
        }

        private void OnScrolled(object sender, ItemsViewScrolledEventArgs e)
        {
            var carousel = (CarouselView)sender;
            var carouselItems = carousel.ItemsSource.Cast<object>().ToList();
            var firstIndex = e.FirstVisibleItemIndex;
            var currentIndex = e.CenterItemIndex;
            var lastIndex = e.LastVisibleItemIndex;
            var layout = carousel.ItemsLayout;

            if (currentIndex > -1)
            {
                if (layout is LinearItemsLayout linearItemsLayout)
                {
                    if (linearItemsLayout.Orientation == ItemsLayoutOrientation.Horizontal)
                    {

                        var currentItem = carouselItems[currentIndex] as BookItem;
                        if (currentItem != null)
                        {
                            _WidthRequest = BookHeight * 0.7;
                            currentItem.HeightRequest = BookHeight;
                            currentItem.WidthRequest = _WidthRequest;
                            currentItem.BookHeightRequest = BookHeight;// - (BookHeight * 2) / 100;
                        }

                        int upLevel = currentIndex;
                        if (currentIndex < carouselItems.Count - 1)
                        {
                            upLevel = currentIndex + 1;
                        }
                        else
                        {
                            upLevel = 0;
                        }

                        int downLevel = currentIndex;
                        if (currentIndex > 0)
                        {
                            downLevel = currentIndex - 1;
                        }
                        else
                        {
                            downLevel = carouselItems.Count - 1;
                        }

                        var upItem = carouselItems[upLevel] as BookItem;
                        if (upItem != null)
                        {
                            upItem.HeightRequest = BookHeight - (BookHeight * 30) / 100;
                            _WidthRequest = upItem.HeightRequest * 0.7;
                            upItem.WidthRequest = _WidthRequest;
                            upItem.BookHeightRequest = BookHeight - (BookHeight * 20) / 100;
                        }


                        var downItem = carouselItems[downLevel] as BookItem;
                        if (downItem != null)
                        {
                            //downItem.Scale = 0.7;
                            downItem.HeightRequest = BookHeight - (BookHeight * 30) / 100;
                            _WidthRequest = upItem.HeightRequest * 0.7;
                            downItem.WidthRequest = _WidthRequest;
                            downItem.BookHeightRequest = BookHeight - (BookHeight * 20) / 100;
                        }

                        if (upLevel < carouselItems.Count - 1)
                        {
                            //upLevel = currentIndex + 2;
                            upLevel++;
                        }
                        else
                        {
                            upLevel = 0;
                        }

                        if (downLevel > 0)
                        {
                            //downLevel = currentIndex - 2;
                            downLevel--;
                        }
                        else
                        {
                            downLevel = carouselItems.Count - 1;

                        }


                        var upItem2 = carouselItems[upLevel] as BookItem;
                        if (upItem2 != null)
                        {
                            upItem2.HeightRequest = BookHeight - (BookHeight * 50) / 100;
                            _WidthRequest = upItem.HeightRequest * 0.7;
                            upItem2.WidthRequest = _WidthRequest;

                            upItem2.BookHeightRequest = BookHeight - (BookHeight * 50) / 100;
                        }


                        var downItem2 = carouselItems[downLevel] as BookItem;
                        if (downItem2 != null)
                        {
                            downItem2.HeightRequest = BookHeight - (BookHeight * 50) / 100;
                            _WidthRequest = upItem.HeightRequest * 0.7;
                            downItem2.WidthRequest = _WidthRequest;
                            downItem2.BookHeightRequest = BookHeight - (BookHeight * 50) / 100;

                        }

                    }


                }
            }
        }
    }
}
