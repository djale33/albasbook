using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly:ExportFont("FontAwesome5Solid.otf", Alias = "FAS")]
[assembly: ExportFont("FontAwesome5Regular.otf", Alias = "FAR")]
[assembly: ExportFont("FontAwesome5Brands.otf", Alias = "FAB")]
[assembly: ExportFont("FontAwesome6Free-Solid-900.otf", Alias = "6FAS")]
[assembly: ExportFont("FontAwesome6Free-Regular-400.otf", Alias = "6FAF")]
[assembly: ExportFont("FontAwesome6Brands-Regular-400.otf", Alias = "6FAB")]