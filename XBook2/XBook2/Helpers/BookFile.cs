﻿namespace XBook2.Helpers
{
    public class BookFile
    {
        public string JsonName { get; set; }
        public string Url { get; set; }
        public string LocalPath { get; set; }
        public bool download { get; set; }
        public BookFile()
        {
            download = false;
        }

        public BookFile(string jsonName, string url, string localPath)
        {
            JsonName = jsonName;
            Url = url;
            LocalPath = localPath;
            download = false;
        }
    }
}