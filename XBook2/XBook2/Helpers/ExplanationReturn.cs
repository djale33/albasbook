﻿namespace XBook2.Helpers
{
    public partial class BookGalery
    {
       

        #region HELPER CLASS
        public class ExplanationReturn
        {
            public string id { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string professor { get; set; }
            public string book { get; set; }
            public string grade { get; set; }
            public string version { get; set; }
            public string created_at { get; set; }
            public string updated_at { get; set; }
            public string sharing { get; set; }
            public string stars { get; set; }
        }

        #endregion


    }
}