﻿using KlasimeBase;
using KlasimeBase.Log;
using KlasimeViews;
using SharedKlasime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XBook2.Helpers
{
    public class DownloadBook: BaseLog
    {
        #region VARIABLES
        string jsonString = "";
        CUser loggedUser;
        Hashtable htPages = new Hashtable();
        int fileDownloadCounter = 0;
        int fileDownloadTotal = 0;
        int ibookId = 0;
        //FileInfoList BookPageList;
        bool timerWork = true;
        Download downloadItem;
        #endregion

        #region PROPERTIES
        public int FileDownloadCounter { get => fileDownloadCounter; set => fileDownloadCounter = value; }
        public int FileDownloadTotal { get => fileDownloadTotal; set => fileDownloadTotal = value; }
        public bool TimerWork { get => timerWork; set => timerWork = value; }
        #endregion

        #region CONSTRUCTOR
        public DownloadBook(CUser _loggedUser, int ibookId)
        {
            this.ibookId = ibookId;
            loggedUser = _loggedUser;
        }
        #endregion

        #region Custom Event

        public delegate void ProgressHandler(object sender, ProgressUpdateEventArgs e);
        public event ProgressHandler OnProgressUpdate;
        public event ProgressHandler OnFinish;


        private async void OnProgress(object sender, ProgressUpdateEventArgs e)
        {
            if (OnProgressUpdate != null)
            {
                OnProgressUpdate(sender, e);
            }
        }

        private void OnSendLog(object sender, ProgressUpdateEventArgs e)
        {
            if (OnFinish != null)
            {
                OnFinish(sender, e);
            }
        }

        #endregion

        #region METHODS
        public async Task TaskDownloadBook(List<BookFile> BookFileList)
        {
            try
            {
                FileDownloadTotal = 0;
                await logInstance.LogAsync(loggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "TaskDownloadBook start for " + ibookId);

                if (ibookId > 0)
                {

                    if (BookFileList != null)
                    {
                        FileDownloadCounter = 0;
                        FileDownloadTotal = BookFileList.Count;

                        await logInstance.LogAsync(loggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "Book download fileDownloadTotal: " + FileDownloadTotal);

                        #region Book Pages Download
                        foreach (BookFile item in BookFileList)
                        {
                            string filepath = item.LocalPath;
                            string[] arr = filepath.Split(Common.GetPathSeparator().ToCharArray());
                            string dir = "";
                            for (int i = 1; i < arr.Length - 1; i++)
                            {
                                string str = arr[i];
                                {
                                    dir += str + Common.GetPathSeparator();
                                    //string phDir = KlasimeBase.Common.GetLocalPath() + dir;
                                    if (Directory.Exists(dir) == false)
                                    {
                                        Directory.CreateDirectory(dir);
                                    }
                                }
                            }


                            downloadItem = new Download(item.Url, item.LocalPath, item.Url, true);
                            downloadItem.OnFileFinishEvent += DownloadItem_OnFileFinishEvent;
                            downloadItem.OnFileProgressEvent += Dc_OnFileProgressEvent;
                            //downloadItem.OnFileDownloaded += DownloadItem_OnFileDownloaded;


                            #region async
                            //bool res = await downloadItem.getFileFromUrlAsync();
                            //System.Threading.Thread.Sleep(300);
                            #endregion

                            #region single
                            bool res = await downloadItem.getFileFromUrlSingle();
                            if (res == true)
                            {
                                ProgressUpdateEventArgs pe2 = new ProgressUpdateEventArgs(FileDownloadCounter, FileDownloadTotal, item.LocalPath);
                                OnProgress(this, pe2);
                                //MessagingCenter.Send<DownloadBook>(this, "Hi");
                            }
                            else
                            {
                                await logInstance.LogAsync(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), null, "TaskDownloadBook error in downloading " + item.LocalPath);
                            }
                            #endregion
                        }
                        #endregion


                    }
                    else
                    {
                        await logInstance.LogAsync(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), null, "cannot download this book: listages = null");
                    }
                    //}
                    //else
                    //{
                    //    Application.Current.MainPage = new MainPage(loggedUser, ibookId);
                    //open the book
                    //}
                }
                else
                {
                    await logInstance.LogAsync(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), null, "DownloadBook problem ibookId < 1 ");
                }
            }
            catch (Exception ee)
            {

                await logInstance.LogAsync(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }

        private void DownloadItem_OnFileDownloaded(object sender, DownloadEventArgs e)
        {
            //update BookFileList

            //      logInstance.Log(BookGalery.user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "File download: " + e.FileSaved);

            //ProgressUpdateEventArgs pe2 = new ProgressUpdateEventArgs(FileDownloadCounter, FileDownloadTotal, e.ToString());
            // OnProgress(this, pe2);
        }

        private void DownloadItem_OnFileFinishEvent(object sender, BookDownloadEventArgs e)
        {
            //update BookFileList

            logInstance.Log(loggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "File download: " + e.FileName);

            ProgressUpdateEventArgs pe2 = new ProgressUpdateEventArgs(FileDownloadCounter, FileDownloadTotal, e.FileName);
            OnProgress(this, pe2);
        }

        private void Dc_OnFileProgressEvent(object sender, BookDownloadEventArgs e)
        {
            var s = e.Progress;
            var p = e.Total;
        }


        #endregion
    }
}