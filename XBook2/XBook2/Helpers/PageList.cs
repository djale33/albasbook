﻿//using MySqlX.XDevAPI.Relational;
using KlasimeViews;
using System.Collections.Generic;
using XBook2.Views.Program;

namespace XBook2.Helpers
{
    public class PageList
    {
        public int numberOfList;
        public List<BookControl> firstRowButtons = new List<BookControl>();
        public List<BookControl> SecondRowButtons = new List<BookControl>();
        public List<BookControl> ThirdRowButtons = new List<BookControl>();
        public PageList(int rows)
        {
            numberOfList = rows;
        }
    }
}