﻿using KlasimeBase.Multimedia;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace XBook2.Helpers
{
    public interface IPhotoLibrary
    {
        Task<NewFile> PickPhotoAsync();
        Task<NewFile> PickAudioAsync();
        Task<NewFile> PickVideoAsync();
        Task<NewFile> PickFileAsync();

        Task<bool> SavePhotoAsync(byte[] data, string folder, string filename, bool bookFolder);
    }
}
