﻿using KlasimeBase;
using KlasimeBase.Log;
using Newtonsoft.Json;
using SharedKlasime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XBook2.Helpers;
using XBook2.Model;
using XBook2.Views.Program;
using Xnook2.ViewModel;

namespace XBook2.ViewModels
{
    public class MainMenuViewModel : ViewModelBase
    {
        #region VARIABLES
       
        public string serializedBookList = "";
        
        string name = "Darko";
        string welcome = "Welcome";

        #endregion

        #region PROPERTIES
      

     
      

        private string loginInfo = "Hey";
        public string LoginInfo
        {
            set
            {
                SetProperty(ref loginInfo, value);
            }
            get
            {
                return loginInfo;
            }
        }

        private string loginName;
        public string LoginName
        {
            set
            {
                SetProperty(ref loginName, value);
            }
            get
            {
                return loginName;
            }
        }

        private string loginAvatar = "";
        public string LoginAvatar
        {
            set
            {
                SetProperty(ref loginAvatar, value);
            }
            get
            {
                return loginAvatar;
            }
        }
        private string loginAvatarBak = "https://ti.portalishkollor.al/uploads/users/72.png";
        public string LoginAvatarBak
        {
            set
            {
                SetProperty(ref loginAvatarBak, value);
            }
            get
            {
                return loginAvatarBak;
            }
        }

        private string loginMessage = "Let's learn interactive :)";
        public string LoginMessage
        {
            set
            {
                SetProperty(ref loginMessage, value);
            }
            get
            {
                return loginMessage;
            }
        }
        private string bannerTitle;
        public string BannerTitle
        {
            set
            {
                SetProperty(ref bannerTitle, value);
            }
            get
            {
                return bannerTitle;
            }
        }
        private string bannerURL;
        public string BannerURL
        {
            set
            {
                SetProperty(ref bannerURL, value);
            }
            get
            {
                return bannerURL;
            }
        }

        private int leftButtonPoligonTranslationX = 88;
        public int LeftButtonPoligonTranslationX
        {
            set
            {
                SetProperty(ref leftButtonPoligonTranslationX, value);
            }
            get
            {
                return leftButtonPoligonTranslationX;
            }
        }

        private int rightButtonPoligonTranslationX = 88;
        public int RightButtonPoligonTranslationX
        {
            set
            {
                SetProperty(ref rightButtonPoligonTranslationX, value);
            }
            get
            {
                return rightButtonPoligonTranslationX;
            }
        }

        private int loginSignButtonFontSize = 20;
        public int LoginSignButtonFontSize
        {
            set
            {
                SetProperty(ref loginSignButtonFontSize, value);
            }
            get
            {
                return loginSignButtonFontSize;
            }
        }

        //RegisterFontSize
        private int registerFontSize = 10;
        public int RegisterFontSize
        {
            set
            {
                SetProperty(ref registerFontSize, value);
            }
            get
            {
                return registerFontSize;
            }
        }
        #endregion

        #region CONSTRUCTOR
        public MainMenuViewModel()
        {
            client = new HttpClient();

        }
        public MainMenuViewModel(CUser _loggedUser, bool _IsSmallScreen) : base(_loggedUser)
        {
            client = new HttpClient();
           
            LoggedUser = _loggedUser;
            IsSmallScreen = _IsSmallScreen;

            User = LoggedUser.firstname + " " + LoggedUser.lastname;
            //Builder pattern!
            //setFonts();
            InitCommandsCustom();
            try
            {
                LoginName = LoggedUser.firstname + " " + LoggedUser.lastname;
                string avatarPath = LoggedUser.photo;
                LoginAvatar = LoginAvatarBak;
                if (!string.IsNullOrEmpty(avatarPath))
                {
                    LoginAvatar = avatarPath.Replace("/assets/user/index/", "/uploads/users/");
                }

            }
            catch (Exception ee)
            {
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }
        }
        #endregion

        #region API

        public async Task<Banner> requestBanner(string token)
        {
            string jsonString = "";
            try
            {
                
                Task<string> task = client.GetStringAsync(Book.urlPart + "/api/banner?token=" + token);
                task.Wait();
                jsonString = task.Result.ToString();
                Banner banner = JsonConvert.DeserializeObject<Banner>(jsonString);
                return banner;
            }
            catch (Exception ee)
            {
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString );
                return null;
            }

        }
        #endregion

        #region COMMANDS
        public ICommand btnGoToBookGalleryCommand { private set; get; }

        private void InitCommandsCustom()
        {
            btnGoToBookGalleryCommand = new Command(GoToBookGallery);
            

        }
        private void GoToBookGallery(object sender)
        {
            try
            {
                var mainPage = sender as MainMenu;
                Application.Current.MainPage = new BookGaleryList(base.LoggedUser, IsSmallScreen);
            }
            catch (Exception ee)
            {
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion
    }
}
