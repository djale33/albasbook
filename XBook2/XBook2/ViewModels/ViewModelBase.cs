﻿using KlasimeBase;
using KlasimeBase.Log;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XBook2;
using XBook2.ViewModels;
using XBook2.Views.Program;

namespace Xnook2.ViewModel
{
    public class ViewModelBase : BaseLog, INotifyPropertyChanged
    {
        #region VARIABLES
        public bool IsSmallScreen;
        public HttpClient client;
        public string User=string.Empty;
        public string sep = Common.GetPathSeparator();

        private Color buttonTemplateColor =  Color.FromHex("f74d18");
        public Color ButtonTemplateColor
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref buttonTemplateColor, value);
                }
            }
            get
            {
                return buttonTemplateColor;
            }
        }

        int menuGridSize = 40,iconPadding=4;
        public int IconPadding
        {
            set
            {
                // if (value != null)
                {

                    SetProperty(ref iconPadding, value);
                }
            }
            get
            {

                return iconPadding;

            }
        }
        public int MenuGridSize
        {
            set
            {
                // if (value != null)
                {

                    SetProperty(ref menuGridSize, value);
                }
            }
            get
            {

                return menuGridSize;

            }
        }
        string bookBasePath;
        public string BookBasePath
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref bookBasePath, value);
                }
            }
            get
            {
                return bookBasePath;
            }
        }
        private CUser loggedUser;
        public CUser LoggedUser
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref loggedUser, value);
                    BookBasePath= Common.GetLocalPath() + sep + loggedUser.firstname + "_" + loggedUser.lastname + Common.GetPathSeparator() + "books";
                }
            }
            get
            {
                return loggedUser;
            }
        }
        private int bookID;
        public int BookID
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookID, value);
                    if (BookBasePath == null || BookBasePath=="" )
                    {
                        BookBasePath = Common.GetLocalPath() + sep + loggedUser.firstname + "_" + loggedUser.lastname + Common.GetPathSeparator() + "books";
                            //+ Common.GetPathSeparator() + bookID.ToString();
                    }
                   

                    
                }
            }
            get
            {
                return bookID;
            }
        }

        Orientation pageOrientation;
        public Orientation PageOrientation
        {
            set
            {
                SetProperty(ref pageOrientation, value);

            }
            get
            {
                return pageOrientation;
            }
        }

        private int bookTotal = 20;
        public int BookTotal
        {
            set
            {
                SetProperty(ref bookTotal, value);
            }
            get
            {
                return bookTotal;
            }
        }
        #endregion

        #region PROPERTY CHANGED

        public event PropertyChangedEventHandler PropertyChanged;
        protected bool SetProperty<T>(ref T storage, T value,
                                    [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region CONSTRUCTOR
        public ViewModelBase()
        {
            InitCommands();
            setFonts();
        }
        public ViewModelBase(CUser _loggedUser)
        {
            loggedUser = _loggedUser;
            User = loggedUser.firstname + "-" + loggedUser.lastname;
            InitCommands();
            setFonts();
        }
        #endregion

        #region SIZE
        private int subMenuIconSize = 20;
        public int SubMenuIconSize
        {
            set
            {
                //if (value != null)
                {
                    SetProperty(ref subMenuIconSize, value);
                }
            }
            get
            {
                return subMenuIconSize;
            }
        }
        #endregion

        #region Font Awsome
        private FontImageSource btnNext;
        public FontImageSource BtnNext
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnNext, value);
                }
            }
            get
            {
                return btnNext;
            }
        }
        public void setFonts()
        {
            BtnNext = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.WindowClose,
                Size = subMenuIconSize,
                Color = ButtonTemplateColor
            };


        }
        #endregion

        #region COMMANDS
        public ICommand CloseCommand { private set; get; }
        private void InitCommands()
        {
            CloseCommand = new Command(() => Close());
            
        }
        private async void Close()
        {
            #region Close
            try
            {
                if(loggedUser != null)
                {
                    Application.Current.MainPage = new BookGaleryList(loggedUser, IsSmallScreen);
                }
                

            }
            catch (Exception ee)
            {
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }



            #endregion
        }
        #endregion
    }
}
