﻿using KlasimeBase;
using SharedKlasime;
using System.Windows.Input;
using Xamarin.Forms;
using Xnook2.ViewModel;

namespace XBook2.ViewModels
{
    public class PageItem : ViewModelBase
    {
        #region VARIABLES
        string title;
        #endregion

        #region COMMANDS
        public ICommand btnMenuOpenPageCommand { private set; get; }

        #endregion

        #region PROPERTIES
        public Book CurrentBook { get; set; }
        public int bookId { get; set; }
        public int selcetPageId { get; set; }
        public string Title { get; set; }
        public CUser loggedUser { get; set; }
        public string Name { get; set; }
        public string Authors { get; set; }
        public ImageSource Image { get; set; }
        public MainPageViewModel MainPageViewModelInstance { get; set; }
        #endregion

        #region CONSTRUCTORS
        public PageItem(MainPageViewModel _MainPageViewModelInstance)
        {
            MainPageViewModelInstance = _MainPageViewModelInstance;
            InitCommands();
        }
        public PageItem(string _title)
        {

            Title = _title;
            
            

            InitCommands();
        }
        #endregion

      
        private void InitCommands()
        {
            if(MainPageViewModelInstance != null)
            {
                btnMenuOpenPageCommand = new Command(() => MainPageViewModelInstance.MoveToPage(selcetPageId-2));
            }
            

        }
       
    }
}
