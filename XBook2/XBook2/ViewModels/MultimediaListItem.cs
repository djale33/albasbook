﻿using KlasimeBase;
using KlasimeViews;
using System.Windows.Input;
using Xamarin.Forms;
using Xnook2.ViewModel;

namespace XBook2.ViewModels
{
    public class MultimediaListItem : ViewModelBase
    {
       
        #region CUSTOM EVENTS
        public delegate void MultimediaHandler(object sender, MultimediaEventArgs e);
        public event MultimediaHandler OnMultimedia;
        private void OnPlayMultimedia(object sender, MultimediaEventArgs e)
        {
            if (OnMultimedia != null)
            {
                OnMultimedia(sender, e);
            }
        }
        #endregion

        #region CONSTRUCTOR        
        public MultimediaListItem(MultimediaType type, string _title, int _pageID, Anno multimediaAnno)
        {
            this.type = type;
            title = _title;
            pageID = _pageID;
            this.multimediaAnno = multimediaAnno;
            InitCommands();
        }
        #endregion

        #region PROPERTIES
        public MultimediaType type { get; set; }
        private string title;
        private int pageID;
        public string Title
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref title, value);
                }
            }
            get
            {
                return title;
            }
        }
        public int PageID
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref pageID, value);
                }
            }
            get
            {
                return pageID;
            }
        }
        public Anno multimediaAnno { get; set; }
        #endregion

        #region COMMANDS
        public ICommand btnMultimediaPreviewCommand { private set; get; }
        
        private void MultimediaPreview()
        {
            int imageWidth = 600;
            int imageHeight = 800;
            Anno hoverAnno = this.multimediaAnno;
            string videoUrl = null;
            if (hoverAnno.Action.ResourceContent != null)
            {
                videoUrl = hoverAnno.Action.ResourceContent.Replace("./files/", "");
                videoUrl = videoUrl.Replace("/", Common.GetPathSeparator());
            }
            MultimediaEventArgs pe2 = new MultimediaEventArgs(this.type, imageWidth, imageHeight, hoverAnno, videoUrl);
            OnPlayMultimedia(this, pe2);

            //BookPreviewControl bcn = new BookPreviewControl(loggedUser, albasBook, false, false, KlasimeBase.BookButtonAction.Download);

        }
        private void InitCommands()
        {

            btnMultimediaPreviewCommand = new Command(() => MultimediaPreview());

        }
        #endregion
    }
}
