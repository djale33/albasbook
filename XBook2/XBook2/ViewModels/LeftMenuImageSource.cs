﻿using Xamarin.Forms;
using Xnook2.ViewModel;

namespace XBook2.ViewModels
{
    public class LeftMenuImageSource : ViewModelBase
    {
        public LeftMenuImageSource(Color CustomerTemplateColor)
        {
            BtnBookSync = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Cloud,
                Size = 22,
                Color = CustomerTemplateColor
            };
            BtnLeftMenu = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Bars,
                Size = 22,
                Color = CustomerTemplateColor
            };


            BtnCursor = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.MousePointer,
                Size = 22,
                Color = CustomerTemplateColor
            };

            BtnLibrary = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.AddressBook,
                Size = 22,
                Color = CustomerTemplateColor
            };

            BtnSelect = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Highlighter,
                Size = 22,
                Color = CustomerTemplateColor
            };

            BtnUnselect = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.CheckCircle,
                Size = 22,
                Color = CustomerTemplateColor
            };

            BtnPen = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.PenAlt,
                Size = 22,
                Color = CustomerTemplateColor
            };
            BtnNote = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.StickyNote,
                Size = 22,
                Color = CustomerTemplateColor
            };
        }

        private FontImageSource btnLeftMenu;
        public FontImageSource BtnLeftMenu
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLeftMenu, value);
                }
            }
            get
            {
                return btnLeftMenu;
            }
        }

        //btnCursor
        private FontImageSource btnCursor;
        public FontImageSource BtnCursor
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnCursor, value);
                }
            }
            get
            {
                return btnCursor;
            }
        }
        //btnLibrary
        private FontImageSource btnLibrary;
        public FontImageSource BtnLibrary
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnLibrary, value);
                }
            }
            get
            {
                return btnLibrary;
            }
        }
        //
        private FontImageSource btnSelect;
        public FontImageSource BtnSelect
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnSelect, value);
                }
            }
            get
            {
                return btnSelect;
            }
        }
        //btnUnselect
        private FontImageSource btnUnselect;
        public FontImageSource BtnUnselect
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnUnselect, value);
                }
            }
            get
            {
                return btnUnselect;
            }
        }
        //btnPen
        private FontImageSource btnPen;
        public FontImageSource BtnPen
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnPen, value);
                }
            }
            get
            {
                return btnPen;
            }
        }
        //btnNote
        private FontImageSource btnNote;
        public FontImageSource BtnNote
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnNote, value);
                }
            }
            get
            {
                return btnNote;
            }
        }
    
        //btnBookSync
        private FontImageSource btnBookSync;
        public FontImageSource BtnBookSync
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnBookSync, value);
                }
            }
            get
            {
                return btnBookSync;
            }
        }
    }
}
