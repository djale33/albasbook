﻿using KlasimeBase;
using KlasimeBase.Log;
using KlasimeViews;
using LibVLCSharp.Shared;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Xamarin.Forms;
using XBook2.Views.Program;
using Xnook2.ViewModel;

namespace XBook2.ViewModels
{
    public class BookPreviewControlViewModel : ViewModelBase
    {
        #region VARIABLES
        
        string nameDownload, nameOpen, nameRemove, desc, errMessage, id, authors,bookTitle, totalSize,bookPages,bookPrice,downloadButton,activateButton, downloadText;
        int bookId;
        bool showActivate;
        BookButtonAction buttonAction;
        string user="init";
        AlbasBook albasBook;

        #endregion

        #region PROPERTIES
        public BookButtonAction ButtonAction
        {
            get { return buttonAction; }
            set {

                //if (value != null)
                //{
                    if (value == BookButtonAction.Download)
                    {
                        DownloadText = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_download_button");
                    }

                    if (value == BookButtonAction.Activate)
                    {
                        DownloadText = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_activate_button");
                    }
                    SetProperty(ref buttonAction, value);
                //}
                
            }
        }
        public int BookId { get; set; }
        public CUser loggedUser { get; set; }
        public string Name { get; set; }
      
        public ImageSource Image { get; set; }
        public bool Exist { get; set; }
        public bool Open { get; set; }
        public bool Locked { get; set; }
        public bool Remove { get; set; }
        public bool Download { get; set; }
        public bool NotReady { get; set; }

        public string NameOpen
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref nameOpen, value);
                }
            }
            get
            {
                return nameOpen;
            }
        }
        public string NameRemove
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref nameRemove, value);
                }
            }
            get
            {
                return nameRemove;
            }
        }
        public string NameDownload
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref nameDownload, value);
                }
            }
            get
            {
                return nameDownload;
            }
        }
        public string Desc
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref desc, value);
                }
            }
            get
            {
                return desc;
            }
        }
        public string Authors
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref authors, value);
                }
            }
            get
            {
                return authors;
            }
        }
        public string ErrMessage
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref errMessage, value);
                }
            }
            get
            {
                return errMessage;
            }
        }
        public string Id
        {
            set
            {
                if (value != null)
                {
                    int.TryParse(value, out bookId);
                    SetProperty(ref id, value);
                }
            }
            get
            {
                return id;
            }
        }
        public string BookTitle
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookTitle, value);
                }
            }
            get
            {
                return bookTitle;
            }
        }
        //albasBook.total_size
        public string TotalSize
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref totalSize, value);
                }
            }
            get
            {
                return totalSize;
            }
        }
        public string BookPages
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookPages, value);
                }
            }
            get
            {
                return bookPages;
            }
        }
        public string BookPrice
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookPrice, value);
                }
            }
            get
            {
                return bookPrice;
            }
        }
        public string DownloadButton
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref downloadButton, value);
                }
            }
            get
            {
                return downloadButton;
            }
        }
        //
        public string ActivateButton
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref activateButton, value);
                }
            }
            get
            {
                return activateButton;
            }
        }
        public string DownloadText
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref downloadText, value);
                }
            }
            get
            {
                return downloadText;
            }
        }
        //
        private double bookDownloadHeight = 300;
        public double BookDownloadHeight
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookDownloadHeight, value);
                }
            }
            get
            {
                return bookDownloadHeight;
            }
        }
        //BookDownloadWidth
        private double bookDownloadWidth = 300;
        public double BookDownloadWidth
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookDownloadWidth, value);
                }
            }
            get
            {
                return bookDownloadWidth;
            }
        }
        #endregion

        #region COMMANDS
        public ICommand DownloadBookCommand { private set; get; }
      
        #endregion

        #region CONSTRUCTOR
        public BookPreviewControlViewModel()
        {
            //Builder pattern!
            //setFonts();
            InitCommandsCustom();
        }
        public BookPreviewControlViewModel(CUser _loggedUser, AlbasBook MAlbasBook,bool _IsSmallScreen) : base(_loggedUser)
        {
            IsSmallScreen = _IsSmallScreen;
            loggedUser = _loggedUser;
            user = loggedUser.firstname + "-" + loggedUser.lastname;
            albasBook = MAlbasBook;
            Id = albasBook.id.ToString();
            Authors = albasBook.editors;
            BookTitle = albasBook.title;
            TotalSize = albasBook.total_size; ;
            BookPages = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_page_label") + ": " + albasBook.pages;
            BookPrice = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_price_label") + ": " + albasBook.price;
            DownloadButton = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_download_button");
            activateButton = Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_activate_button");
            //Builder pattern!
            //setFonts();
            InitCommandsCustom();
        }
        
        #endregion
        private void InitCommandsCustom()
        {
            //CloseCommand = new Command(() => Close());
            DownloadBookCommand = new Command(() => DownloadBook());
        }
        //private async void Close()
        //{
        //    #region Close
        //    try
        //    {
                
        //        Application.Current.MainPage = new BookGaleryList(loggedUser, IsSmallScreen);
                
        //    }
        //    catch (Exception ee)
        //    {
        //        logInstance.Log(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
        //    }


           
        //    #endregion
        //}
        private void DownloadBook()
        {
            #region DownloadBook
            try
            {
                //BookEventArgs be = new BookEventArgs(albasBook, ButtonAction);
                //DownloadBookControl dbc = new DownloadBookControl(loggedUser, albasBook);
                //dbc.ButtonAction = buttonAction;
                bool activate = false;
                if(buttonAction == BookButtonAction.Activate) {
                    activate = true;
                }
                DownloadBookControl2 dbc = new DownloadBookControl2(loggedUser, albasBook, buttonAction, IsSmallScreen);
                Application.Current.MainPage = dbc;
                

            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            #endregion
        }
    }
}
