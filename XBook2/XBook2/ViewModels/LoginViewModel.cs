﻿using KlasimeBase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xnook2.ViewModel;

namespace XBook2.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        #region Commands
        //RegisterClickCommand
        public ICommand RegisterClickCommand { private set; get; }
        public ICommand VideoClickCommand { private set; get; }
        #endregion

        #region CONSTRUCTOR
        public LoginViewModel()
        {
            RegisterClickCommand = new Command(() => SetBtnContentPress());
            VideoClickCommand = new Command(() => CloseVideo());
        }
        #endregion

        #region METHODS
        private async void SetBtnContentPress()
        {
            await Common.OpenWebPage("http://ti.portalishkollor.al/register");
        }
        private async void CloseVideo()
        {
            MediaEnded = true;
        }
        #endregion

        #region Properties
        //MediaEnded
        private bool mediaEnded = false;
        public bool MediaEnded
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref mediaEnded, value);
                }
            }
            get
            {
                return mediaEnded;
            }
        }


        private bool waitOnLogin = false;
        public bool WaitOnLogin
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref waitOnLogin, value);
                }
            }
            get
            {
                return waitOnLogin;
            }
        }

        private bool showLoginButton = true;
        public bool ShowLoginButton
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref showLoginButton, value);
                }
            }
            get
            {
                return showLoginButton;
            }
        }


        private string imgSVGlogin;
        public string ImgSVGlogin
        {
            set
            {
                SetProperty(ref imgSVGlogin, value);
            }
            get
            {
                return imgSVGlogin;
            }
        }

        private int cornerRadius = 8;
        public int CornerRadius
        {
            set
            {
                SetProperty(ref cornerRadius, value);
            }
            get
            {
                return cornerRadius;
            }
        }
        private int buttonColumnWidth = 100;
        public int ButtonColumnWidth
        {
            set
            {
                SetProperty(ref buttonColumnWidth, value);
            }
            get
            {
                return buttonColumnWidth;
            }
        }
        //LoginOpacity
        private double loginOpacity = 0.5;
        public double LoginOpacity
        {
            set
            {
                SetProperty(ref loginOpacity, value);
            }
            get
            {
                return loginOpacity;
            }
        }
        //InputHeight
        private int inputHeight = 44;
        public int InputHeight
        {
            set
            {
                SetProperty(ref inputHeight, value);
            }
            get
            {
                return inputHeight;
            }
        }
        //ButtonRowHeight
        private int buttonRowHeight = 70;
        public int ButtonRowHeight
        {
            set
            {
                SetProperty(ref buttonRowHeight, value);
            }
            get
            {
                return buttonRowHeight;
            }
        }
        //LoginIconSize
        private int loginIconSize = 70;
        public int LoginIconSize
        {
            set
            {
                SetProperty(ref loginIconSize, value);
            }
            get
            {
                return loginIconSize;
            }
        }
        //LoginMainLabelHeight
        private int loginMainLabelHeight = 30;
        public int LoginMainLabelHeight
        {
            set
            {
                SetProperty(ref loginMainLabelHeight, value);
            }
            get
            {
                return loginMainLabelHeight;
            }
        }
        private int loginEmptyLabelHeight = 2;
        public int LoginEmptyLabelHeight
        {
            set
            {
                SetProperty(ref loginEmptyLabelHeight, value);
            }
            get
            {
                return loginEmptyLabelHeight;
            }
        }

        //LoginNameFontSize
        private int loginNameFontSize = 20;
        public int LoginNameFontSize
        {
            set
            {
                SetProperty(ref loginNameFontSize, value);
            }
            get
            {
                return loginNameFontSize;
            }
        }
        //LeftButtonPoligonTranslationX
        private int leftButtonPoligonTranslationX = 88;
        public int LeftButtonPoligonTranslationX
        {
            set
            {
                SetProperty(ref leftButtonPoligonTranslationX, value);
            }
            get
            {
                return leftButtonPoligonTranslationX;
            }
        }

        private int rightButtonPoligonTranslationX = 88;
        public int RightButtonPoligonTranslationX
        {
            set
            {
                SetProperty(ref rightButtonPoligonTranslationX, value);
            }
            get
            {
                return rightButtonPoligonTranslationX;
            }
        }

        private int loginSignButtonFontSize = 20;
        public int LoginSignButtonFontSize
        {
            set
            {
                SetProperty(ref loginSignButtonFontSize, value);
            }
            get
            {
                return loginSignButtonFontSize;
            }
        }

        //RegisterFontSize
        private int registerFontSize = 10;
        public int RegisterFontSize
        {
            set
            {
                SetProperty(ref registerFontSize, value);
            }
            get
            {
                return registerFontSize;
            }
        }
        #endregion

    }
}

