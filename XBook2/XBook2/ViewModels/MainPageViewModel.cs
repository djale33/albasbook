﻿using FFImageLoading.Svg.Forms;
using KlasimeBase;
using KlasimeBase.Log;
using KlasimeViews;
using SharedKlasime;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml;
using Xamarin.Forms;
using XBook2.Views.Program;
using Xnook2.ViewModel;

namespace XBook2.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        #region VARIABLES
        //public LeftMenuImageSource bookLeftMenuImageSource;
       
        Book currentBook;
        int currentPageNumLeft;
        int menuVerticalOffset = 4;
        int iconPadding = 10, menuFontSize = 9, librarySubMenuTranslationY, drawSubMenuTranslationY, attachSubMenuTranslationY;
        bool libContentMenuVisible = false, libMultimediaMenuVisible = false;
        int menuTranslationX = 39, leftPageTranslationX=0;

        Color textColor;
        Hashtable htMyBooks = new Hashtable();
        
        bool cbSelectAll, cbLink, cbPhotoGalery, cbInfo, cbVideo, cbYoutube, cbAudio, cbQuiz;
        MainPage mainPageInstance;
        #endregion

        #region OBSERVAL COLLECTIONS
        public ObservableCollection<PageItem> ContentMenuPages { get; set; } = new ObservableCollection<PageItem>();
        public ObservableCollection<MultimediaListItem> ContentMultimediaPages { get; set; } = new ObservableCollection<MultimediaListItem>();
        public ObservableCollection<PageItem> BookmarkPages { get; set; } = new ObservableCollection<PageItem>();
        public ObservableCollection<MultimediaListItem> NotesPages { get; set; } = new ObservableCollection<MultimediaListItem>();
        #endregion

        #region CONSTRUCTOR
        public MainPageViewModel(MainPage _mainPageInstance,CUser loggedUser, int ibookId, bool bookEncoding)
        {
            mainPageInstance = _mainPageInstance;
            LoggedUser = loggedUser;
            
            //PageNumLeft = pageNumLeft;
            BookID = ibookId;
            BookEncoding = bookEncoding;
            InitCommands();
            string path = KlasimeBase.Common.GetLocalPath();
            //ookBasePath = sep+ loggedUser.firstname +"_"+ loggedUser.lastname + sep + "book" + sep + BookID.ToString() + sep;
            bookFiles = BookBasePath + sep + BookID.ToString() + sep + "files" + sep;
            
            string user = LoggedUser.firstname;
            CurrentBook = new Book(user, BookBasePath , BookID, BookEncoding);

            TextColor = Color.Green;
            librarySubMenuTranslationY = 3 * (MenuGridSize + menuVerticalOffset);
            menuVerticalOffset = 15;
            DrawSubMenuTranslationY = 5 * MenuGridSize + menuVerticalOffset;
            menuVerticalOffset = 36;
            AttachSubMenuTranslationY = 8 * MenuGridSize + menuVerticalOffset;
            CustomerTemplateColor = Color.Orange;

            GetUserInfoFromXML();

            menuIconSize = 22;
            subMenuIconSize = 12;

            SetLeftMenu();
           

        }

        public MainPageViewModel(CUser loggedUser, Book currentBook)
        {
            LoggedUser = loggedUser;
          
            //PageNumLeft = pageNumLeft;
            string user = LoggedUser.firstname;
            CurrentBook = currentBook;
            BookID = currentBook.Id;
            string path = KlasimeBase.Common.GetLocalPath();
           // bookBasePath = sep + "book" + sep + BookID.ToString() + sep;
            bookFiles = BookBasePath + sep + BookID.ToString() + sep + "files" + sep;

            TextColor = Color.Green;
            librarySubMenuTranslationY = 3 * (MenuGridSize + menuVerticalOffset);
            menuVerticalOffset = 15;
            DrawSubMenuTranslationY = 5 * MenuGridSize + menuVerticalOffset;
            menuVerticalOffset = 36;
            AttachSubMenuTranslationY = 8 * MenuGridSize + menuVerticalOffset;
            
            CustomerTemplateColor = Color.Orange;
            GetUserInfoFromXML();
            menuIconSize = 22;
            subMenuIconSize = 12;
            InitCommands();
            SetLeftMenu();
        }
        #endregion

        #region COMMANDS
        public ICommand btnMenuOpenPageCommand { private set; get; }
        public ICommand btnContentCommand { private set; get; }

        public ICommand btnMaterialsCommand { private set; get; }
        public ICommand btnMultimediaCommand { private set; get; }
        public ICommand btnNotesCommand { private set; get; }
        

        public ICommand btnThemeOrangeCommand { private set; get; }
        public ICommand btnThemeGreenCommand { private set; get; }
        public ICommand btnHelpButtonCommand { private set; get; }
        public ICommand btnConfigureButton { private set; get; }
        public ICommand btnBookmarkCommand { private set; get; }
        
        private void InitCommands()
        {
            btnContentCommand = new Command(() => SetBtnContentPress());
            btnMaterialsCommand = new Command(() => SetBtnMaterialsPress());
            btnBookmarkCommand = new Command(() => SetBtnBookmarkPress());

            btnMultimediaCommand = new Command(() => SetBtnMultimediaPress());
            btnNotesCommand = new Command(() => SetBtnNotesPress());
            btnThemeOrangeCommand = new Command(() => btnThemeCommandPress(Color.Orange));
            btnThemeGreenCommand = new Command(() => btnThemeCommandPress(Color.LightGreen));
            //settings menu
            btnHelpButtonCommand = new Command(() => BtnHelpCommand());
            btnConfigureButton = new Command<Grid>(bottomMenu => showHideSettingsMenu(bottomMenu));
            
        }
        /// <summary>
        /// we use bottomMenu to get location, so we can calculate settings menu to make
        /// TranslationY upper for 6 butons from bottom menu
        /// </summary>
        /// <param name="bottomMenu"></param>
        private void showHideSettingsMenu(Grid bottomMenu)
        {
            try
            {
                if (bottomMenu != null)
                {
                  
                    SettingsTranslateY = bottomMenu.Y - (6 * MenuGridSize) ;
                    double newX = 0;
                    ImageButton m = (ImageButton)bottomMenu.FindByName("btnConfigure");
                    if (m != null)
                    {
                        newX = m.X;
                        MenuSettingsGridSize = m.Width;
                    }

                   
                    SettingsTranslateX = newX;
                    SetConfigurationMenu();
                }
            }
            catch (Exception ee)
            {

                string s = ee.Message;
            }
        }
        void ExecuteMove(int direction)
        {
            settingsTranslateX = direction;
           

        }
        public void MoveToPage(int iPagenumLeft)
        {
            PageNumLeft=iPagenumLeft;
            if(mainPageInstance!= null)
            {
                mainPageInstance.MoveToPage(iPagenumLeft.ToString());
            }
            

          



        }

        private void SetBtnContentPress()
        {
            //if (BtnContentPress == false)
            {
                BtnContentPress = true;
                LibContentMenuVisible = true;
                
                BtnMaterialPress = false;
                LibMaterialsMenuVisible = false;
                LibMultimediaMenuVisible = false;
                LibBookmarkMenuVisible = false;
                LibNotesMenuVisible = false;
            }
            //else
            //{
            //    BtnContentPress = false;
            //    LibContentMenuVisible = true;
            //}
        }
        private void SetBtnMaterialsPress()
        {
           //if (BtnMaterialPress == false)
            {
                BtnMaterialPress = true;
                LibMaterialsMenuVisible = true;

                BtnContentPress = false;
                LibContentMenuVisible = false;
                LibBookmarkMenuVisible = false;
                LibNotesMenuVisible = false;
            }
            //else
            //{
            //    BtnMaterialPress = false;
            //    LibMaterialsMenuVisible = true;
            //}
        }
        private void SetBtnMultimediaPress()
        {
            //if (BtnMultimediaPress == false)
            {
                BtnMultimediaPress = true;
                LibMultimediaMenuVisible = true;

                LibBookmarkMenuVisible = false;
                LibContentMenuVisible = false;

                LibNotesMenuVisible = false;
            }
            //else
            //{
            //    BtnMultimediaPress = false;
            //    LibMultimediaMenuVisible = true;
            //}
        }
        private void SetBtnBookmarkPress()
        {
            BtnMultimediaPress = false;
            LibMultimediaMenuVisible = false;

            LibBookmarkMenuVisible = true;
            LibContentMenuVisible = false;
            LibNotesMenuVisible = false;
        }
        private void SetBtnNotesPress()
        {
            LibContentMenuVisible = false;
            LibMultimediaMenuVisible = false;
            LibNotesMenuVisible = true;
            LibBookmarkMenuVisible = false;


        }
        //setings menu
        private async void BtnHelpCommand()
        {
            await Common.OpenWebPage("http://portalishkollor.al/faqet/termat-e-perdorimit");
        }
        private void SetConfigurationMenu()
        {
            //settingsTranslateX= val;
            if (BtnSettingsMenuPress == true)
            {
                BtnSettingsMenuPress = false;
            }
            else
            {
                BtnSettingsMenuPress = true;
            }
        }
        #endregion

        #region PROPERTIES
        private bool waitOnBook;
        
        public bool WaitOnBook
        {
            set
            {
                SetProperty(ref waitOnBook, value);
            }
            get
            {
                return waitOnBook;
            }
        }
        private bool isProfesor;

        public bool IsProfesor
        {
            set
            {
                SetProperty(ref isProfesor, value);
            }
            get
            {
                return isProfesor;
            }
        }
        private bool isTablet;

        public bool IsTablet
        {
            set
            {
                SetProperty(ref isTablet, value);

                if (LoggedUser.currentUserType.type == "student")
                {
                    IsExtendmenuEnabled = false;
                }
                else
                {
                    //Profesor have extend menu but only on tablet
                    IsExtendmenuEnabled = isTablet;
                }
            }
            get
            {
                return isTablet;
            }
        }
        private bool isExtendmenuEnabled;

        public bool IsExtendmenuEnabled
        {
            set
            {                
                SetProperty(ref isExtendmenuEnabled, value);
            }
            get
            {
                isExtendmenuEnabled = true;
                if (LoggedUser.currentUserType.type == "student")
                {
                    isExtendmenuEnabled = false;
                }
                else
                {
                    //Profesor have extend menu but only on tablet
                    isExtendmenuEnabled = isTablet;
                }

                return isExtendmenuEnabled;
            }
        }
        private double mainPageLoadTime;
        public double MainPageLoadTime
        {
            set
            {
                SetProperty(ref mainPageLoadTime, value);
            }
            get
            {
                return mainPageLoadTime;
            }
        }
        string sep = Common.GetPathSeparator();
       
        public Book CurrentBook { get; set; }
       // public int BookID { get; set; }
        public string TitleValue { get; set; }
        public string EntryValue { get; set; }
        public Command ChangeTitleCommand { get; set; }

        bool bookEncoding;
        public bool BookEncoding
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref bookEncoding, value);
                }
            }
            get
            {
                return bookEncoding;
            }
        }

        private double settingsTranslateX;
        public double SettingsTranslateX
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref settingsTranslateX, value);
                }
            }
            get
            {
                return settingsTranslateX;
            }
        }

        private double settingsTranslateY;
        public double SettingsTranslateY
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref settingsTranslateY, value);
                }
            }
            get
            {
                return settingsTranslateY;
            }
        }

        private string bookFiles;
        public string BookFiles
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref bookFiles, value);
                }
            }
            get
            {
                return bookFiles;
            }
        }

        private double menuSettingsGridSize;
        public double MenuSettingsGridSize
        {
            set
            {
                SetProperty(ref menuSettingsGridSize, value);
            }
            get
            {
                return menuSettingsGridSize;
            }
        }

        string path;
        public string Path
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref path, value);
                }
            }
            get
            {
                return path;
            }
        }

        bool singlePageMode;
        public bool SinglePageMode
        {
            set
            {
                SetProperty(ref singlePageMode, value);               
                if(value==false)
                {
                    //two page mode, set the width
                    if (PageWidth > 0)
                    {
                        RightPageVisible = true;
                        //LeftPageWidth = (PageWidth  / 2) - (4 * MenuGridSize);
                        //RightPageWidth = (PageWidth / 2) - ( MenuGridSize);
                        LeftPageWidth = (PageWidth -(5* MenuGridSize)) / 2;
                        RightPageWidth = LeftPageWidth;
                    }
                }
                else
                {
                    //single page
                    if (PageWidth > 0)
                    {
                        RightPageVisible = false;
                        RightPageWidth = 40;
                        // LeftPageWidth = (PageWidth / 2) - (MenuGridSize/2);
                        LeftPageWidth = (PageWidth) - (5 * MenuGridSize);

                    }
                }
            }
            get
            {
                return singlePageMode;
            }
        }

      
        //LeftPageWidth
        private double leftPageWidth;
        public double LeftPageWidth
        {
            set
            {
                SetProperty(ref leftPageWidth, value);
            }
            get
            {
                return leftPageWidth;
            }
        }
        //RightPageWidth
        private double rightPageWidth;
        public double RightPageWidth
        {
            set
            {
                SetProperty(ref rightPageWidth, value);
            }
            get
            {
                return rightPageWidth;
            }
        }
        //LeftPageVisible
        private bool leftPageVisible = false;
        public bool LeftPageVisible
        {
            set
            {
                SetProperty(ref leftPageVisible, value);
            }
            get
            {
                return leftPageVisible;
            }
        }

        //RightPageVisible
        private bool rightPageVisible;
        public bool RightPageVisible
        {
            set
            {
                SetProperty(ref rightPageVisible, value);
            }
            get
            {
                return rightPageVisible;
            }
        }
        //leftPageFullScreen
        private bool leftPageFullScreen;
        public bool LeftPageFullScreen
        {
            set
            {
                SetProperty(ref leftPageFullScreen, value);
            }
            get
            {
                return leftPageFullScreen;
            }
        }
        //rightPageFullScreen
        private bool rightPageFullScreen;
        public bool RightPageFullScreen
        {
            set
            {
                SetProperty(ref rightPageFullScreen, value);
            }
            get
            {
                return rightPageFullScreen;
            }
        }
        //PageWidth
        private double pageWidth;
        public double PageWidth
        {
            set
            {
                SetProperty(ref pageWidth, value);
            }
            get
            {
                return pageWidth;
            }
        }
        private int tabletMode;
        public int TabletMode
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref tabletMode, value);
                }
            }
            get
            {
                return tabletMode;
            }
        }
        private int defaultMode;
        public int DefaultMode
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref defaultMode, value);
                }
            }
            get
            {
                return defaultMode;
            }
        }

        private int pageNumLeft = -2;
        public int PageNumLeft
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref pageNumLeft, value);
                }
            }
            get
            {
                return pageNumLeft;
            }
        }
        int pageNumRight;
        public int PageNumRight
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref pageNumRight, value);
                }
            }
            get
            {
                return pageNumRight;
            }
        }

        string userInfoPath;
        public string UserInfoPath
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref userInfoPath, value);
                }
            }
            get
            {
                return userInfoPath;
            }
        }       
        
        string title;
        public string Title
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref title, value);
                }
            }
            get
            {
                return title;
            }
        }

        private int menuIconSize;
        public int MenuIconSize
        {
            set
            {
                //if (value != null)
                {
                    SetProperty(ref menuIconSize, value);
                }
            }
            get
            {
                return menuIconSize;
            }
        }
        private int subMenuIconSize;
        public int SubMenuIconSize
        {
            set
            {
                //if (value != null)
                {
                    SetProperty(ref subMenuIconSize, value);
                }
            }
            get
            {
                return subMenuIconSize;
            }
        }
        //
        bool btnContentPress = true;
        public bool BtnContentPress
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnContentPress, value);
                }
            }
            get
            {
                return btnContentPress;
            }
        }
        //
        bool btnMaterialPress = true;
        public bool BtnMaterialPress
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnMaterialPress, value);
                }
            }
            get
            {
                return btnMaterialPress;
            }
        }

        bool btnMultimediaPress = true;
        public bool BtnMultimediaPress
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnMultimediaPress, value);
                }
            }
            get
            {
                return btnMultimediaPress;
            }
        }
        bool btnSettingsMenuPress = false;
        public bool BtnSettingsMenuPress
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnSettingsMenuPress, value);
                }
            }
            get
            {
                return btnSettingsMenuPress;
            }
        }
        private bool libMaterialsMenuVisible;
        public bool LibMaterialsMenuVisible
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref libMaterialsMenuVisible, value);
                }
            }
            get
            {
                return libMaterialsMenuVisible;
            }
        }
        public bool LibContentMenuVisible
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref libContentMenuVisible, value);
                }
            }
            get
            {
                return libContentMenuVisible;
            }
        }
        //
        public bool LibMultimediaMenuVisible
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref libMultimediaMenuVisible, value);
                }
            }
            get
            {
                return libMultimediaMenuVisible;
            }
        }
        private bool libNotesMenuVisible;
        //LibNotesMenuVisible
        public bool LibNotesMenuVisible
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref libNotesMenuVisible, value);
                }
            }
            get
            {
                GetNotesList();
                return libNotesMenuVisible;
            }
        }
        private bool libBookmarkMenuVisible;
        public bool LibBookmarkMenuVisible
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref libBookmarkMenuVisible, value);
                }
            }
            get
            {
                GetBookmarkList();
                return libBookmarkMenuVisible;
            }
        }
        //LibBookmarkMenuVisible
        public int CurrentPageNumLeft
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref currentPageNumLeft, value);
                }
            }
            get
            {
                return currentPageNumLeft;
            }
        }
        //LeftPageTranslationX
        public int LeftPageTranslationX
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref leftPageTranslationX, value);
                }
            }
            get
            {
                return leftPageTranslationX;
            }
        }
        public int MenuTranslationX
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref menuTranslationX, value);
                }
            }
            get
            {
                return menuTranslationX;
            }
        }
        public int AttachSubMenuTranslationY
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref attachSubMenuTranslationY, value);
                }
            }
            get
            {
                return attachSubMenuTranslationY;
            }
        }
        public int DrawSubMenuTranslationY
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref drawSubMenuTranslationY, value);
                }
            }
            get
            {
                return drawSubMenuTranslationY;
            }
        }
        public int LibrarySubMenutranslationY
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref librarySubMenuTranslationY, value);
                }
            }
            get
            {
                return librarySubMenuTranslationY;
            }
        }

        public int IconPadding
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref iconPadding, value);
                }
            }
            get
            {
                return iconPadding;
            }
        }
        public int MenuFontSize
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref menuFontSize, value);
                }
            }
            get
            {
                return menuFontSize;
            }
        }
        public Color TextColor
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref textColor, value);
                }
            }
            get
            {
                return textColor;
            }
        }
        public bool CbSelectAll
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref cbSelectAll, value);
                }
            }
            get
            {
                return cbSelectAll;
            }
        }
        public bool CbLink
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref cbLink, value);
                }
            }
            get
            {
                return cbLink;
            }
        }
        public bool CbPhotoGalery
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref cbPhotoGalery, value);
                }
            }
            get
            {
                return cbPhotoGalery;
            }
        }
        public bool CbInfo
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref cbInfo, value);
                }
            }
            get
            {
                return cbInfo;
            }
        }
        public bool CbVideo
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref cbVideo, value);
                }
            }
            get
            {
                return cbVideo;
            }
        }
        public bool CbYoutube
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref cbYoutube, value);
                }
            }
            get
            {
                return cbYoutube;
            }
        }
        public bool CbAudio
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref cbAudio, value);
                }
            }
            get
            {
                return cbAudio;
            }
        }
        public bool CbQuiz
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref cbQuiz, value);
                }
            }
            get
            {
                return cbQuiz;
            }
        }
        #endregion

        #region CUSTOM EVENTS
        public delegate void MultimediaHandler(object sender, MultimediaEventArgs e);
        public event MultimediaHandler OnMultimedia;
        private void OnPlayMultimedia(object sender, MultimediaEventArgs e)
        {
            if (OnMultimedia != null)
            {
                OnMultimedia(sender, e);
            }
        }
        #endregion

        #region METHODS
        private async Task GetBookmarkList()
        {
            try
            {
                List<int> BookmarkedPages = CurrentBook.GetAllBookmarkFromXML();
                //scrollBookmark.IsVisible = true;
                //stackLibBookmarkMenu.Children.Clear();
                // stackLibBookmarkMenu.IsVisible = true;
                BookmarkPages.Clear();
                foreach (int pageId in BookmarkedPages)
                {
                    if (pageId > 0)
                    {
                        int realPageId = pageId +2 ;
                        PageItem bi = new PageItem(this);
                        bi.Title = pageId.ToString();
                        bi.selcetPageId = realPageId;
                        bi.bookId = BookID;
                        bi.CurrentBook = CurrentBook;
                       // bi.Image = CurrentBook.GetThumbFileName(realPageId);

                        BookmarkPages.Add(bi);
                        //stackLibBookmarkMenu.Children.Add(smallPageButton);
                    }
                }
                //scrollBookmark.IsVisible = true;
            }
            catch (Exception ee)
            {
                await logInstance.LogAsync(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private async Task GetNotesList()
        {
            try
            {
                List<MultimediaListItem> BookmarkedPages;
                NotesPages.Clear();
                BookmarkedPages = GetAllNotesFromXML(LoggedUser, CurrentBook);
                foreach (MultimediaListItem item in BookmarkedPages)
                {
                    NotesPages.Add(item);
                }
            }
            catch (Exception ee)
            {

                await logInstance.LogAsync(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
          
            
        }

        public async Task PrepareBookListAsync()
        {

            // user = LoggedUser.firstname + "-" + LoggedUser.lastname;
            try
            {

                htMyBooks.Clear();

                for (int i = 1; i < CurrentBook.BookPages; i++)
                {
                    if (i > 2)
                    {
                        int realPageId = i - 2;
                        await AddBookToListAsync(i, realPageId, CurrentBook);
                    }
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        public async Task<bool> AddBookToListAsync(int i, int realPageId, Book currentBook)
        {
            try
            {
                PageItem bi = new PageItem(this);
                bi.Title = realPageId.ToString();
                bi.selcetPageId = realPageId;
                bi.bookId = BookID;
                bi.CurrentBook = CurrentBook;
                bi.Image = currentBook.GetThumbFileName(realPageId);

                ContentMenuPages.Add(bi);
            }
            catch (Exception ee)
            {

                await logInstance.LogAsync(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return true;
        }        
        private void btnThemeCommandPress(Color newColor)
        {
            this.CustomerTemplateColor = newColor;
            SetLeftMenu();
            SetUserInfoToXML();

        }
        #endregion

        #region Multimedia
        public async Task GetAllMultimediaFromXML(CUser _user, Book currentBook)
        {

            //List<MultimediaListItem> list = new List<MultimediaListItem>();
            ContentMultimediaPages.Clear();
            try
            {
                // Common.GetTitle(hoverAnno.Hint.Text)
                int pageID = 0;
                for (int i = 1; i < currentBook.htPageAnnos.Count; i++)
                {
                    PageAnno pageAnno = (PageAnno)currentBook.htPageAnnos[i];
                    pageID = i;

                    if (pageAnno != null)
                    {
                        MultimediaType type = MultimediaType.Unknown;
                        foreach (Anno anno in pageAnno.Annos)
                        {

                            string hintTitle = Common.GetTitle(anno.Hint.Text);
                            switch (anno.Action.ActionType)
                            {
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenURL":
                                    if (CbLink == true) type = MultimediaType.Link;
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenWindow":
                                    if (CbVideo == true) type = MultimediaType.Video;
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionShowInformation":
                                    if (CbInfo == true) type = MultimediaType.Note;
                                    break;
                                case "com.mobiano.flipbook.Action.TAnnoActionPlayAudio":
                                    {
                                        if (CbAudio == true) type = MultimediaType.Audio;

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPhotoSlide":
                                    {
                                        if (CbPhotoGalery == true) type = MultimediaType.PhotoGalery;

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayVideo":
                                    {
                                        if (anno.Action.ResourceContent == null)
                                        {
                                            if (anno.Action.WindowType == "TYPE_YOUTUBE")
                                            {
                                                if (CbYoutube == true) type = MultimediaType.YouTube;

                                            }
                                        }
                                        else
                                        {
                                            if (CbVideo == true) type = MultimediaType.Video;

                                        }

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayAudio":
                                    {
                                        if (anno.Action.ResourceContent == null)
                                        {
                                            if (CbYoutube == true) type = MultimediaType.Audio;
                                        }
                                        break;
                                    }
                                    //quiz!!!

                            }
                            if (type != MultimediaType.Unknown)
                            {
                                MultimediaListItem item = new MultimediaListItem(type, hintTitle, pageID, anno);
                                item.OnMultimedia += Item_OnMultimedia;
                                ContentMultimediaPages.Add(item);
                            }
                        }
                    }
                }

            }
            catch (Exception ee)
            {
                await logInstance.LogAsync(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }

        private void Item_OnMultimedia(object sender, MultimediaEventArgs e)
        {
            try
            {
                int imageWidth = 600;
                int imageHeight = 800;
                Anno hoverAnno = e.FindAnno;
                string videoUrl = e.VideoUrl;
                MultimediaEventArgs pe2 = new MultimediaEventArgs(e.MultimediaType, imageWidth, imageHeight, hoverAnno, videoUrl);
                OnPlayMultimedia(this, pe2);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MultimediaListItem> GetAllNotesFromXML(CUser _user, Book currentBook)
        {
            List<MultimediaListItem> list = new List<MultimediaListItem>();
            try
            {
                // Common.GetTitle(hoverAnno.Hint.Text)
                int pageID = 0;
                for (int i = 1; i < currentBook.htPageAnnos.Count; i++)
                {
                    PageAnno pageAnno = (PageAnno)currentBook.htPageAnnos[i];
                    pageID = i;

                    if (pageAnno != null)
                    {
                        MultimediaType type = MultimediaType.Unknown;
                        foreach (Anno anno in pageAnno.Annos)
                        {

                            string hintTitle = Common.GetTitle(anno.Hint.Text);
                            switch (anno.Action.ActionType)
                            {
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenURL":
                                    //type = MultimediaType.Link;
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenWindow":
                                    //type = MultimediaType.Video;
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionShowInformation":
                                    if (anno.Cutom)
                                        type = MultimediaType.Note;
                                    break;
                                case "com.mobiano.flipbook.Action.TAnnoActionPlayAudio":
                                    {
                                        //type = MultimediaType.Audio;

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPhotoSlide":
                                    {
                                        //type = MultimediaType.PhotoGalery;

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayVideo":
                                    {
                                        if (anno.Action.ResourceContent == null)
                                        {
                                            if (anno.Action.WindowType == "TYPE_YOUTUBE")
                                            {
                                                //     type = MultimediaType.YouTube;

                                            }
                                        }
                                        else
                                        {
                                            //type = MultimediaType.Video;

                                        }

                                        break;
                                    }
                                    //quiz!!!

                            }
                            if (type != MultimediaType.Unknown)
                            {
                                MultimediaListItem item = new MultimediaListItem(type, hintTitle, pageID, anno);
                                list.Add(item);
                            }
                        }
                    }
                }

            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return list;
        }
        #endregion

        #region FONT PROPERTIES
        private void SetLeftMenu()
        {
            //bookLeftMenuImageSource = new LeftMenuImageSource(CustomerTemplateColor);
            #region Left Menu
            BtnBookSync = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Cloud,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };
            BtnLeftMenu = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Bars,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };


            BtnCursor = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.MousePointer,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };

            BtnLibrary = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.AddressBook,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };

            BtnSelect = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Highlighter,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };

            BtnUnselect = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.CheckCircle,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };

            BtnPen = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.PenAlt,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };
            BtnNote = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.StickyNote,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };
            BtnAttach = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.FileAlt,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };
            BtnTools = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Toolbox,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };
            BtnQuiz = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.QuestionCircle,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };
            BtnContent = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Book,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };
            BtnR1Materials = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.FileVideo,
                Size = menuIconSize,
                Color = CustomerTemplateColor
            };
            BtnR1TeacherBook = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.UserTie,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnR1SearchInBook = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Search,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnR2Multimedia = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Music,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnR2Notes = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.NoteSticky,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnR2Bookmarks = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Bookmark,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnR2Back = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont6.HandPointLeft,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnBookmarkBottom = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Bookmark,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            #endregion

            #region BottomMenu
            BtnPrevious = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.CaretLeft,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnGoToPage = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.ArrowRight,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnBookmark = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Bookmark,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnAlbas = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Globe,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnBookshelf = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.AddressBook,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnConfigure = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.SlidersH,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            
            BtnFullScreen = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Display,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };

            BtnNext = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.CaretRight,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            #endregion

            #region attach submenu
            BtnFileAttach = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.File,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnLinkAttach = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Link,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnYouTubeAttach = new FontImageSource()
            {
                FontFamily = "6FAB",
                Glyph = FontAwsome.Helpers.IconFont6Brand.Youtube,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnPhotoAttach = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.PhotoFilm,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnVideoAttach = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.FileVideo,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnAudioAttach = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Music,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            #endregion

            #region settings menu
            BtnHelp = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.PersonShelter,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnLanguage = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Globe,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnBackground = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Campground,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnIconSize = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont.SortNumericUp,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnTabletMode = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Tablet,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnDefaultMode = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Mobile,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            #endregion

            #region drawing menu
            BtnPenDraw = new FontImageSource()
            {
                FontFamily = "FAS",
                Glyph = FontAwsome.Helpers.IconFont.Pen,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnLineDraw = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.DrawPolygon,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnDeleteDraw = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Eraser,
                Size = subMenuIconSize,
                Color = CustomerTemplateColor
            };
            BtnLineSizetThin = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Circle,
                Size = 5,
                Color = Color.White
            };
            BtnLineSizeMedium = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Circle,
                Size = subMenuIconSize,
                Color = Color.White
            };
            BtnLineSizeThick = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Circle,
                Size = subMenuIconSize,
                Color = Color.White
            };
            BtnLineColorRose = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Circle,
                Size = subMenuIconSize,
                Color = Color.Coral
            };
            btnLineColorBlue = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Circle,
                Size = subMenuIconSize,
                Color = Color.DeepSkyBlue
            };
            BtnLineColorGreen = new FontImageSource()
            {
                FontFamily = "6FAS",
                Glyph = FontAwsome.Helpers.IconFont6.Circle,
                Size = subMenuIconSize,
                Color = Color.LightGreen
            };
            #endregion

            #region svg
            //try
            //{
            //    BtnBottomMenuLabel = SvgImageSource.FromResource("XBook2.media.load.svg.logo-albas-libri.svg", typeof(ImageResourceExtension).GetTypeInfo().Assembly,400,40);
            //    //BtnBottomMenuLabel = new SvgImageSource(myimageSource, 200, 40, true);
            //}
            //catch (Exception ee)
            //{

            //    logInstance.Log(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            //}
            
            #endregion

        }


        private Color customerTemplateColor;
        public Color CustomerTemplateColor
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref customerTemplateColor, value);
                }
            }
            get
            {
                return customerTemplateColor;
            }
        }

        private FontImageSource btnLeftMenu;
        public FontImageSource BtnLeftMenu
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLeftMenu, value);
                }
            }
            get
            {
                return btnLeftMenu;
            }
        }
        //btnCursor
        private FontImageSource btnCursor;
        public FontImageSource BtnCursor
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnCursor, value);
                }
            }
            get
            {
                return btnCursor;
            }
        }
        //btnLibrary
        private FontImageSource btnLibrary;
        public FontImageSource BtnLibrary
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLibrary, value);
                }
            }
            get
            {
                return btnLibrary;
            }
        }
        //
        private FontImageSource btnSelect;
        public FontImageSource BtnSelect
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnSelect, value);
                }
            }
            get
            {
                return btnSelect;
            }
        }
        //btnUnselect
        private FontImageSource btnUnselect;
        public FontImageSource BtnUnselect
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnUnselect, value);
                }
            }
            get
            {
                return btnUnselect;
            }
        }
        //btnPen
        private FontImageSource btnPen;
        public FontImageSource BtnPen
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnPen, value);
                }
            }
            get
            {
                return btnPen;
            }
        }
        //btnNote
        private FontImageSource btnNote;
        public FontImageSource BtnNote
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnNote, value);
                }
            }
            get
            {
                return btnNote;
            }
        }

        //btnBookSync
        private FontImageSource btnBookSync;
        public FontImageSource BtnBookSync
        {
            set
            {
                // if (value != null)
                {
                    SetProperty(ref btnBookSync, value);
                }
            }
            get
            {
                return btnBookSync;
            }
        }
        //btnAttach
        private FontImageSource btnAttach;
        public FontImageSource BtnAttach
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnAttach, value);
                }
            }
            get
            {
                return btnAttach;
            }
        }
        //btnTools
        private FontImageSource btnTools;
        public FontImageSource BtnTools
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnTools, value);
                }
            }
            get
            {
                return btnTools;
            }
        }
        private FontImageSource btnQuiz;
        public FontImageSource BtnQuiz
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnQuiz, value);
                }
            }
            get
            {
                return btnQuiz;
            }
        }
        //
        //btnContent
        private FontImageSource btnContent;
        public FontImageSource BtnContent
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnContent, value);
                }
            }
            get
            {
                return btnContent;
            }
        }
        //btnR1Materials
        private FontImageSource btnR1Materials;
        public FontImageSource BtnR1Materials
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnR1Materials, value);
                }
            }
            get
            {
                return btnR1Materials;
            }
        }
        //btnR1TeacherBook
        private FontImageSource btnR1TeacherBook;
        public FontImageSource BtnR1TeacherBook
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnR1TeacherBook, value);
                }
            }
            get
            {
                return btnR1TeacherBook;
            }
        }
        //btnR1SearchInBook
        private FontImageSource btnR1SearchInBook;
        public FontImageSource BtnR1SearchInBook
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnR1SearchInBook, value);
                }
            }
            get
            {
                return btnR1SearchInBook;
            }
        }
        //btnR2Multimedia
        private FontImageSource btnR2Multimedia;
        public FontImageSource BtnR2Multimedia
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnR2Multimedia, value);
                }
            }
            get
            {
                return btnR2Multimedia;
            }
        }
        //btnR2Notes
        private FontImageSource btnR2Notes;
        public FontImageSource BtnR2Notes
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnR2Notes, value);
                }
            }
            get
            {
                return btnR2Notes;
            }
        }
        //btnR2Bookmarks
        private FontImageSource btnR2Bookmarks;

        private FontImageSource btnBookmarkBottom;
        public FontImageSource BtnR2Bookmarks
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnR2Bookmarks, value);
                }
            }
            get
            {
                return btnR2Bookmarks;
            }
        }
        //BtnBookmarkBottom
        public FontImageSource BtnBookmarkBottom
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnBookmarkBottom, value);
                }
            }
            get
            {
                return btnBookmarkBottom;
            }
        }

        //btnR2Back
        private FontImageSource btnR2Back;
        public FontImageSource BtnR2Back
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnR2Back, value);
                }
            }
            get
            {
                return btnR2Back;
            }
        }

        //bottom menu
        //btnPrevious
        private FontImageSource btnPrevious;
        public FontImageSource BtnPrevious
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnPrevious, value);
                }
            }
            get
            {
                return btnPrevious;
            }
        }
        //btnGoToPage
        private FontImageSource btnGoToPage;
        public FontImageSource BtnGoToPage
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnGoToPage, value);
                }
            }
            get
            {
                return btnGoToPage;
            }
        }
        //btnBookmark
        private FontImageSource btnBookmark;
        public FontImageSource BtnBookmark
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnBookmark, value);
                }
            }
            get
            {
                return btnBookmark;
            }
        }
        //btnAlbas
        private FontImageSource btnAlbas;
        public FontImageSource BtnAlbas
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnAlbas, value);
                }
            }
            get
            {
                return btnAlbas;
            }
        }
        //btnBookshelf
        private FontImageSource btnBookshelf;
        public FontImageSource BtnBookshelf
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnBookshelf, value);
                }
            }
            get
            {
                return btnBookshelf;
            }
        }
        //btnFullScreen
        private FontImageSource btnFullScreen;
        public FontImageSource BtnFullScreen
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnFullScreen, value);
                }
            }
            get
            {
                return btnFullScreen;
            }
        }
        //btnConfigure
        private FontImageSource btnConfigure;
        public FontImageSource BtnConfigure
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnConfigure, value);
                }
            }
            get
            {
                return btnConfigure;
            }
        }
        //btnNext
        private FontImageSource btnNext;
        public FontImageSource BtnNext
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnNext, value);
                }
            }
            get
            {
                return btnNext;
            }
        }
        //attach menu
        //btnFileAttach
        private FontImageSource btnFileAttach;
        public FontImageSource BtnFileAttach
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnFileAttach, value);
                }
            }
            get
            {
                return btnFileAttach;
            }
        }
        //btnLinkAttach
        private FontImageSource btnLinkAttach;
        public FontImageSource BtnLinkAttach
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLinkAttach, value);
                }
            }
            get
            {
                return btnLinkAttach;
            }
        }
        //btnYouTubeAttach
        private FontImageSource btnYouTubeAttach;
        public FontImageSource BtnYouTubeAttach
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnYouTubeAttach, value);
                }
            }
            get
            {
                return btnYouTubeAttach;
            }
        }
        //btnPhotoAttach
        private FontImageSource btnPhotoAttach;
        public FontImageSource BtnPhotoAttach
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnPhotoAttach, value);
                }
            }
            get
            {
                return btnPhotoAttach;
            }
        }
        //BtnVideoAttach
        private FontImageSource btnVideoAttach;
        public FontImageSource BtnVideoAttach
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnVideoAttach, value);
                }
            }
            get
            {
                return btnVideoAttach;
            }
        }
        //BtnAudioAttach
        private FontImageSource btnAudioAttach;
        public FontImageSource BtnAudioAttach
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnAudioAttach, value);
                }
            }
            get
            {
                return btnAudioAttach;
            }
        }

        //settings menu
        //btnHelp
        private FontImageSource btnHelp;
        public FontImageSource BtnHelp
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnHelp, value);
                }
            }
            get
            {
                return btnHelp;
            }
        }
        //BtnLanguage
        private FontImageSource btnLanguage;
        public FontImageSource BtnLanguage
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLanguage, value);
                }
            }
            get
            {
                return btnLanguage;
            }
        }
        //btnBackground
        private FontImageSource btnBackground;
        public FontImageSource BtnBackground
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnBackground, value);
                }
            }
            get
            {
                return btnBackground;
            }
        }
        //btnIconSize
        private FontImageSource btnIconSize;
        public FontImageSource BtnIconSize
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnIconSize, value);
                }
            }
            get
            {
                return btnIconSize;
            }
        }
        //btnTabletMode
        private FontImageSource btnTabletMode;
        public FontImageSource BtnTabletMode
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnTabletMode, value);
                }
            }
            get
            {
                return btnTabletMode;
            }
        }
        //        btnDefaultMode
        private FontImageSource btnDefaultMode;
        public FontImageSource BtnDefaultMode
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnDefaultMode, value);
                }
            }
            get
            {
                return btnDefaultMode;
            }
        }
        //drawing menu
        //btnPenDraw
        private FontImageSource btnPenDraw;
        public FontImageSource BtnPenDraw
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnPenDraw, value);
                }
            }
            get
            {
                return btnPenDraw;
            }
        }
        //btnLineDraw
        private FontImageSource btnLineDraw;
        public FontImageSource BtnLineDraw
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLineDraw, value);
                }
            }
            get
            {
                return btnLineDraw;
            }
        }
        //btnDeleteDraw
        private FontImageSource btnDeleteDraw;
        public FontImageSource BtnDeleteDraw
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnDeleteDraw, value);
                }
            }
            get
            {
                return btnDeleteDraw;
            }
        }
        //btnLineSizetThin
        private FontImageSource btnLineSizetThin;
        public FontImageSource BtnLineSizetThin
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLineSizetThin, value);
                }
            }
            get
            {
                return btnLineSizetThin;
            }
        }
        //btnLineSizeMedium
        private FontImageSource btnLineSizeMedium;
        public FontImageSource BtnLineSizeMedium
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLineSizeMedium, value);
                }
            }
            get
            {
                return btnLineSizeMedium;
            }
        }
        //btnLineSizeThick
        private FontImageSource btnLineSizeThick;
        public FontImageSource BtnLineSizeThick
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLineSizeThick, value);
                }
            }
            get
            {
                return btnLineSizeThick;
            }
        }
        //btnLineColorRose
        private FontImageSource btnLineColorRose;
        public FontImageSource BtnLineColorRose
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLineColorRose, value);
                }
            }
            get
            {
                return btnLineColorRose;
            }
        }
        //btnLineColorBlue
        private FontImageSource btnLineColorBlue;
        public FontImageSource BtnLineColorBlue
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLineColorBlue, value);
                }
            }
            get
            {
                return btnLineColorBlue;
            }
        }
        //btnLineColorGreen
        private FontImageSource btnLineColorGreen;
        public FontImageSource BtnLineColorGreen
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnLineColorGreen, value);
                }
            }
            get
            {
                return btnLineColorGreen;
            }
        }

        //
        private ImageSource btnBottomMenuLabel;
        public ImageSource BtnBottomMenuLabel
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref btnBottomMenuLabel, value);
                }
            }
            get
            {
                return btnBottomMenuLabel;
            }
        }
        #endregion

        #region User Settings
        public void GetUserInfoFromXML()
        {
            try
            {
                UserInfoPath =  BookBasePath + "user.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(UserInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("id");
                    attr.Value = BookID.ToString();
                    nod.Attributes.Append(attr);

                    attr = doc.CreateAttribute("PageNumLeft");
                    attr.Value = PageNumLeft.ToString();
                    nod.Attributes.Append(attr);

                    attr = doc.CreateAttribute("currentColorTempalte");
                    attr.Value = PageNumLeft.ToString();
                    nod.Attributes.Append(attr);

                    doc.AppendChild(nod);
                    doc.Save(UserInfoPath);
                }
                else
                {
                    string xmldoc= File.ReadAllText(UserInfoPath);
                    if (xmldoc != null)
                    {
                        if (xmldoc != "")
                        {



                            doc.LoadXml(xmldoc);
                            if (doc.ChildNodes.Count > 0)
                            {
                                XmlNode nod = doc.ChildNodes[0];
                                if (nod.Attributes.Count > 0)
                                {
                                    int iPageNumLeft = 0;
                                    int.TryParse(nod.Attributes[1].Value, out iPageNumLeft);
                                    PageNumLeft = iPageNumLeft;
                                }
                                if (nod.Attributes.Count == 3)
                                {
                                    LoggedUser.currentColorTempalte = ColorTemlates.Orange;
                                    CustomerTemplateColor = Color.Orange;

                                    if (nod.Attributes[2].InnerText == "Green")
                                    {
                                        LoggedUser.currentColorTempalte = ColorTemlates.Green;
                                        CustomerTemplateColor = Color.LightGreen;
                                    }
                                    SetLeftMenu();
                                }
                            }
                        }
                        else
                        {
                            File.Delete(UserInfoPath);
                        }
                    }
                    
                }

            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        /// <summary>
        /// save last visitet age
        /// </summary>
        public void SetUserInfoToXML()
        {
            try
            {
                UserInfoPath = BookBasePath + "user.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("id");
                    attr.Value = BookID.ToString();
                    nod.Attributes.Append(attr);

                    attr = doc.CreateAttribute("PageNumLeft");
                    attr.Value = PageNumLeft.ToString();
                    nod.Attributes.Append(attr);

                    attr = doc.CreateAttribute("currentColorTempalte");
                    attr.Value = PageNumLeft.ToString();
                    nod.Attributes.Append(attr);

                    doc.AppendChild(nod);
                    doc.Save(userInfoPath);
                }
                else
                {
                    doc.Load(userInfoPath);
                    if (doc.ChildNodes.Count > 0)
                    {
                        XmlNode nod = doc.ChildNodes[0];
                        if (nod.Attributes.Count > 0)
                        {
                            nod.Attributes[1].Value = PageNumLeft.ToString();
                            doc.Save(userInfoPath);
                        }
                        else
                        {
                            logInstance.Log(LoggedUser.firstname, Severity.High, MethodBase.GetCurrentMethod(), null, "XML for user settings have attribute missing");
                        }

                        if (nod.Attributes.Count == 3)
                        {
                            nod.Attributes[2].Value = LoggedUser.currentColorTempalte.ToString();
                            doc.Save(userInfoPath);
                        }
                        else
                        {
                            XmlAttribute attr = doc.CreateAttribute("currentColorTempalte");
                            attr.Value = LoggedUser.currentColorTempalte.ToString();
                            nod.Attributes.Append(attr);
                            doc.AppendChild(nod);
                            doc.Save(userInfoPath);


                            logInstance.Log(LoggedUser.firstname, Severity.High, MethodBase.GetCurrentMethod(), null, "XML for user settings have attribute missing");

                        }
                    }
                    else
                    {
                        logInstance.Log(LoggedUser.firstname, Severity.High, MethodBase.GetCurrentMethod(), null, "XML for user settings have no child nodes");
                    }
                }

            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.firstname, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        public void SetBookmark(bool bookmarkState)
        {
            if (bookmarkState)
            {
                BtnBookmarkBottom = new FontImageSource()
                {
                    FontFamily = "6FAS",
                    Glyph = FontAwsome.Helpers.IconFont6.BookBookmark,
                    Size = subMenuIconSize,
                    Color = CustomerTemplateColor
                };
            }
            else
            {
                BtnBookmarkBottom = new FontImageSource()
                {
                    FontFamily = "6FAS",
                    Glyph = FontAwsome.Helpers.IconFont6.Bookmark,
                    Size = subMenuIconSize,                   
                    Color = CustomerTemplateColor
                };
            }
        }
        #endregion

    }
    public enum Orientation
    {
        Portrait,
        Landscape
    }
}
