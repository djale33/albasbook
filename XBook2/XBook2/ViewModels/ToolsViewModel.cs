﻿using KlasimeBase;
using KlasimeBase.Log;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XBook2.Views.Program;
using Xnook2.ViewModel;

namespace XBook2.ViewModels
{
    public class ToolsViewModel : ViewModelBase
    {

        #region PROPERTIES
        public Book mybook = null;
        private CUser loggedUser;
        string name = string.Empty;
        string user="";
        
        public string Name
        {
            get => name;
            set
            {
                if (name == value)
                    return;
                name = value;
                OnPropertyChanged(nameof(Name));         
            }
        }


        private string demoSVGimage;

        public string DemoSVGimage
        {
            set
            {
                SetProperty(ref demoSVGimage, value);
            }
            get
            {
                return demoSVGimage;
            }
        }
        #endregion

        #region CONSTRUCTOR
        public ToolsViewModel()
        {
           
            InitCommands();
        }
        public ToolsViewModel(CUser muser,  Book _currentBook) 
        {
            this.loggedUser = muser; 
            this.mybook = _currentBook;
            user = loggedUser.firstname + "-" + loggedUser.lastname;
            InitCommands();
        }
        #endregion

        #region COMMANDS
        public ICommand ExitCommand { private set; get; }
        private void InitCommands()
        {
            ExitCommand = new Command(() => ExitButtonPress());
           
        }
        private void ExitButtonPress()
        {
            try
            {
                Application.Current.MainPage = new MainPage(loggedUser, mybook.Id, mybook);                
            }
            catch (Exception ee)
            {

                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion

    }
}
