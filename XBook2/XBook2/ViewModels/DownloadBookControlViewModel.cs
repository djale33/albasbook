﻿using KlasimeBase;
using KlasimeBase.Log;
using KlasimeViews;
using Newtonsoft.Json;
using SharedKlasime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using XBook2.Views.Program;
using Xnook2.ViewModel;
using XBook2.Helpers;
using static KlasimeViews.BookGalery;
using FFImageLoading;

namespace XBook2.ViewModels
{
    public class DownloadBookControlViewModel : ViewModelBase
    {
        #region VARIABLES
    
        int downloadTestBook = 0;
        string local_path="";
        private bool showActivate = false, showDownload=false, imageDownloadVisible = true;
        private string title, downloadProgress, percentText="0", activateDesc, activationCode;
        private string infoChannelText, enterTheCode;
        private double progressValue=0;
        //CUser loggedUser;
        AlbasBook book;
        HttpClient client = new HttpClient();
        BookButtonAction bookAction;
        
        //string bookBasePath;
        string VideoFilesDir;
        string filesDir;
        Timer fileWatcherTimer;
        Timer initTimer;

        bool checkfileWatcherTimer = true;
        string totalSize;
        Helpers.DownloadBook downloadBook;
        FileInfoList BookPageList;
        Hashtable htBookFiles = new Hashtable();
        List<BookFile> ListBookFiles = new List<BookFile>();
        bool startTimerForCheckFiles = false;
        bool startTimerForUpdate = false;

        private int fileDownloadCounter = 1;
        private bool bookReady = false;
        bool updateStart = true;
        #endregion

        #region OBSERVAL COLLECTIONS
        public ObservableCollection<string> Greetings { get; set; }
        #endregion

        #region COMMANDS
        //public ICommand DownloadBookCommand { private set; get; }
        // public ICommand CloseCommand { private set; get; }
        public ICommand ActivateSend { private set; get; }
        private void InitCommands()
        {

            //CloseCommand = new Command(() => Close());
            ActivateSend = new Command(() => ActivateBook());
            //DownloadBookCommand = new Command(() => DownloadBook());
        }
        private async void DeleteBook()
        {
            #region Close
            try
            {
                bool answer = await App.Current.MainPage.DisplayAlert("Alert, delete Book", "Are you sure?", "Yes", "No");
                if (answer)
                {
                    await CleanUp(book.id.ToString());
                    logInstance.Log(LoggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "The Note was deleted ");
                    await App.Current.MainPage.DisplayAlert("Book was deleted", "Book Info", "Ok");
                    Application.Current.MainPage = new BookGaleryList(LoggedUser, IsSmallScreen);
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

          
            #endregion
        }
        private async Task CleanUp(string bookID)
        {
            string dir = BookBasePath + Common.GetPathSeparator() + "book" + Common.GetPathSeparator() + bookID;
            do
            {
                try
                {
                    Directory.Delete(dir, true);
                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception ee)
                {
                    logInstance.Log(LoggedUser.username, Severity.High, MethodBase.GetCurrentMethod(), ee, "");

                }
            } while (Directory.Exists(dir));
        }
        private async void ActivateBook()
        {
            #region Close
            try
            {

                await logInstance.LogAsync(LoggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "try to activate book " + book.id + " with activationCode: " + ActivationCode);

                bool res = await activateBook(LoggedUser.token, book.id.ToString(), ActivationCode);
                if (res)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Application.Current.MainPage = new XBook2.Views.Program.BookGaleryList(LoggedUser, IsSmallScreen);
                    });
                }
            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            #endregion
        }
        #endregion

        #region PROPERTIES
        public int FileDownloadCounter { get => fileDownloadCounter; set => fileDownloadCounter = value; }
        public bool BookReady { get => bookReady; set => bookReady = value; }
        public bool CheckfileWatcherTimer { get => checkfileWatcherTimer; set => checkfileWatcherTimer = value; }

        public string Title
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref title, value);
                }
            }
            get
            {
                return title;
            }
        }
        public string EnterTheCode
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref enterTheCode, value);
                }
            }
            get
            {
                return enterTheCode;
            }
        }
        //
        public string DownloadProgress
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref downloadProgress, value);
                }
            }
            get
            {
                return downloadProgress;
            }
        }
        //
        public string PercentText
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref percentText, value);
                }
            }
            get
            {
                return percentText;
            }
        }
        //infoChannelText
        public string InfoChannelText
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref infoChannelText, value);
                }
            }
            get
            {
                return infoChannelText;
            }
        }
        //
        public double ProgressValue
        {
            set
            {
                SetProperty(ref progressValue, value);
            }
            get
            {
                return progressValue;
            }
        }
        //
        public string ActivateDesc
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref activateDesc, value);
                }
            }
            get
            {
                return activateDesc;
            }
        }
        //
        public string ActivationCode
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref activationCode, value);
                }
            }
            get
            {
                return activationCode;
            }
        }
        //
        public bool ShowActivate
        {
            set
            {
                SetProperty(ref showActivate, value);
            }
            get
            {
                return showActivate;
            }
        }
        //showDownload
        public bool ShowDownload
        {
            set
            {
                SetProperty(ref showDownload, value);
            }
            get
            {
                return showDownload;
            }
        }
        public bool ImageDownloadVisible
        {
            set
            {
                SetProperty(ref imageDownloadVisible, value);
            }
            get
            {
                return imageDownloadVisible;
            }
        }

        #endregion
        
        #region CONSTRUCTOR
        public DownloadBookControlViewModel(CUser _loggedUser, AlbasBook abook, BookButtonAction _bookAction,bool _IsSmallScreen) : base(_loggedUser)
        {
            try
            {
                IsSmallScreen = _IsSmallScreen;
                LoggedUser = _loggedUser; //must be set first
                book = abook;
                
                BookID = book.id;
                

                bookAction = _bookAction;
                Title = abook.title;
                DownloadProgress = "Download in progress";
                InfoChannelText = "Info channel text";
                EnterTheCode = KlasimeBase.Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "code_enter_title");
                ActivateDesc = KlasimeBase.Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "code_enter_message");

                client = new HttpClient();
                //Builder pattern!
                //setFonts();
                InitCommands();

                if (bookAction == BookButtonAction.Activate)
                {
                    showActivate = true;
                }
                if (bookAction == BookButtonAction.Download)
                {
                    showDownload = true;
                }

                //MessagingCenter.Subscribe<MainPage>(this, "Hi", (sender) =>
                //{
                //    Greetings.Add("Hi");
                //});
                //MessagingCenter.Subscribe<MainPage, string>(this, "Hi", (sender, arg) =>
                //{
                //    Greetings.Add("Hi " + arg);
                //});
            }
            catch (Exception ee)
            {

                 logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }   
           
        }

        #endregion

        #region Custom Event

        public delegate void FinichHandler(object sender, EventArgs e);
        public event FinichHandler OnFinishBook;

        private async void OnFinishBookDownload(object sender, EventArgs e)
        {
            if (OnFinishBook != null)
            {
                if (fileWatcherTimer != null)
                {
                    fileWatcherTimer.Enabled = false;
                    fileWatcherTimer.Stop();
                }
                OnFinishBook(sender, e);
            }
        }

        #endregion

        #region METHODS
        bool init = true;
        public async Task Demo()
        {
            initTimer = new System.Timers.Timer();
            initTimer.Interval = 1000;
            initTimer.Elapsed += InitTimer_Elapsed;
            initTimer.Enabled = true;
            initTimer.Start();
        }

        private async void InitTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (init)
            {
                //PercentText = DateTime.Now.ToString();
                
                init =false;
                initTimer.Stop();
                await DownloadBook();
            }
            
            
        }

        public async Task DownloadBook()
        {

            try
            {


                if (bookAction == BookButtonAction.Download)
                {
                    client = new HttpClient();


                    //bookBasePath = sep + "book" + sep + book.id.ToString() + sep;
                   
                    filesDir = BookBasePath + "files";

                    DownloadProgress = KlasimeBase.Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "connection_download_progress"); // "";


                    totalSize = book.total_size;

                    fileWatcherTimer = new System.Timers.Timer();
                    fileWatcherTimer.Interval = 8000;
                    fileWatcherTimer.Elapsed += FileWatcherTimer_Elapsed;
                    fileWatcherTimer.Enabled = true;
                    fileWatcherTimer.Start();
                    //fill start from timer

                    //DownloadBookAsync(book).Wait();
                    //_ = Task.Run(async () =>
                    //{
                    //    await DownloadBookAsync(book);
                    //});
                    await DownloadBookAsync(book);

                }
                else
                {
                     EnterTheCode = KlasimeBase.Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "code_enter_title");
                     ActivateDesc = KlasimeBase.Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "code_enter_message");


                    ////activate book
                    ImageDownloadVisible = false;
                    //imgDownload.IsVisible = false;
                    //lblDownloadProgress.IsVisible = false;
                    //gridText.IsVisible = false;
                    //pbDownloadBook.IsVisible = false;
                    //gridActivateWork.IsVisible = true;

                }
            }
            catch (Exception ee)
            {

                await logInstance.LogAsync(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        int percent = 0;
        public async Task DownloadBookAsync(AlbasBook book)
        {
            try
            {
                int ibookId = 0;
                int.TryParse(book.id.ToString(), out ibookId);

                if (bookAction == BookButtonAction.Download)
                {
                    if (downloadTestBook > 0)
                    {
                        ibookId = downloadTestBook;
                        book.id = ibookId;
                    }

                    BookPageList = await getBookFileList(LoggedUser.token, ibookId);

                    if (BookPageList.fileInfo == null)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Application.Current.MainPage = new BookPreviewControl(LoggedUser, book, false, false, bookAction, IsSmallScreen);

                        });
                    }

                    downloadBook = new DownloadBook(LoggedUser, ibookId);
                    downloadBook.OnProgressUpdate += NewBook_OnProgressUpdateAsync;

                    if (BookPageList.fileInfo != null)
                    {

                        foreach (XBook2.FileInfo fi in BookPageList.fileInfo)
                        {
                            if (htBookFiles.Contains(fi.local_path) == false)
                            {
                                try
                                {
                                    string lc = Common.RemoveVersionNumber(fi.local_path);
                                    local_path = BookBasePath + lc.Replace("/", Common.GetPathSeparator());
                                    
                                    BookFile bf = new BookFile(fi.local_path, fi.url, local_path);

                                    #region check path
                                    string[] arr = local_path.Split(Common.GetPathSeparator().ToCharArray());
                                    string dir = "";

                                    if(Device.RuntimePlatform == Device.iOS) {
                                        dir = "/";
                                    }
                                    for (int i = 1; i < arr.Length - 1; i++)
                                    {
                                        string str = arr[i];
                                        {
                                            dir += str + Common.GetPathSeparator();
                                            
                                            if (Directory.Exists(dir) == false)
                                            {
                                                try
                                                {
                                                    Directory.CreateDirectory(dir);
                                                }
                                                catch (Exception ee)
                                                {

                                                    //no need for test!
                                                }
                                                
                                            }
                                        }
                                    }
                                    #endregion

                                    #region download
                                    bool res = await getFileFromUrlSingle(fi.url, local_path);
                                    if (res == true)
                                    {

                                         percent = ((FileDownloadCounter - 1) * 100) / BookPageList.fileInfo.Count;
                                        double dblProgressPercent = (((double)FileDownloadCounter - 1)) / (double)BookPageList.fileInfo.Count;
                                        if (percent > 100) percent = 100;

                                        PercentText = percent.ToString() + "%";
                                        ProgressValue = dblProgressPercent;

                                        bf.download = true;
                                        htBookFiles.Add(bf.LocalPath, true);

                                        ListBookFiles.Add(bf);

                                    }
                                    else
                                    {
                                        await logInstance.LogAsync(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), null, "TaskDownloadBook error in downloading " + local_path);
                                        bf.download = false;
                                        htBookFiles.Add(bf.LocalPath, false);
                                        ListBookFiles.Add(bf);
                                    }
                                    #endregion

                                }
                                catch (Exception ee)
                                {
                                    await logInstance.LogAsync(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee, "");
                                }

                            }
                        }
                        if(CheckFileList() == true)
                        {
                            logInstance.Log(LoggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "CheckFilesAsync done");
                            await updateBookToVersionAsync(LoggedUser.token, book.id.ToString());

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Application.Current.MainPage = new XBook2.Views.Program.BookGaleryList(LoggedUser, IsSmallScreen);
                            });
                        }
                        else
                        {
                            //or just inform that all files are not downloaded
                            //to start again ....
                            //startTimerForCheckFiles = true;
                            await App.Current.MainPage.DisplayAlert("Not all pages downloaded, check Internet connection and try agian", "Book Problem", "OK" );
                        }
                        
                      
                    }
                    else
                    {
                        await logInstance.LogAsync(LoggedUser.username, Severity.High, MethodBase.GetCurrentMethod(), null, "BookPageList.fileInfo == null");
                        OnFinishBookDownload(this, new EventArgs());
                    }
                }


            }
            catch (Exception ee)
            {
                await logInstance.LogAsync(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }


        }
        public async Task<bool> getFileFromUrlSingle(string UrlPath, string LocalPath)
        {
            try
            {
                if (File.Exists(LocalPath) == true)
                {
                    FileDownloadCounter++;
                    return true;

                }
                else
                {
                    Uri uri = new Uri(UrlPath);
                    WebRequest request = WebRequest.Create(uri);
                    WebClient client = new WebClient();

                    client.DownloadFile(uri, LocalPath);

                    if (File.Exists(LocalPath) == true)
                    {
                        FileDownloadCounter++;
                        return true;

                    }
                    else
                    {
                        System.Threading.Thread.Sleep(2000);

                        if (File.Exists(LocalPath) == true)
                        {
                            FileDownloadCounter++;
                            return true;
                        }
                    }

                }
            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }
            return false;
        }
        async Task<Stream> GetStreamAsync(string uri)
        {
            TaskFactory factory = new TaskFactory();
            WebRequest request = WebRequest.Create(uri);
            WebResponse response = await factory.FromAsync<WebResponse>(request.BeginGetResponse, request.EndGetResponse, null);
            return response.GetResponseStream();
        }


        private async void NewBook_OnProgressUpdateAsync(object sender, ProgressUpdateEventArgs e)
        {
            try
            {

                foreach (BookFile bf in ListBookFiles)
                {
                    if (bf.LocalPath == e.FileName)
                    {
                        bf.download = true;
                        FileDownloadCounter++;
                    }
                }
                System.Threading.Thread.Sleep(500);
                //double check for files in folders
                int fileCount = 0;
                if (Directory.Exists(filesDir))
                {
                    fileCount = Directory.GetFiles(filesDir, "*.*", SearchOption.AllDirectories).Length;
                }
                int percent = ((FileDownloadCounter - 1) * 100) / e.TotalFilesToDownload;
                double dblProgressPercent = (((double)FileDownloadCounter - 1)) / (double)e.TotalFilesToDownload;
                if (percent > 100) percent = 100;



                Device.BeginInvokeOnMainThread(() =>
                {
                    PercentText = percent.ToString() + "%";

                });

                Device.BeginInvokeOnMainThread(() =>
                {
                    ProgressValue = dblProgressPercent;

                });


                //pbText.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate                { pbText.Text = percent.ToString() + "%"; }));


            }
            catch (Exception ee)
            {
                await logInstance.LogAsync(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }
        }
        bool DownloadStart { get; set; }
        private async void FileWatcherTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //INFO CHANNEL
            InfoChannelText = "Info channel text " + DateTime.Now.ToString();
            //if (DownloadStart == false)
            //{
            //    await DownloadBookAsync(book);
            //    DownloadStart = true;
            //}
            //try
            //{
            //    PercentText = percent.ToString() + "%";
            //    double dblProgressPercent = (((double)FileDownloadCounter - 1)) / (double)BookPageList.fileInfo.Count;
            //    ProgressValue = dblProgressPercent;
            //}
            //catch (Exception ee)
            //{

            //    logInstance.Log(loggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            //}
           

            if (startTimerForCheckFiles)
            {
                // logInstance.Log(loggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "timer tick");
                if (CheckfileWatcherTimer)
                {
                    logInstance.Log(LoggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "Checkfile Timer tick");
                    if (CheckFileList())
                    {
                        CheckfileWatcherTimer = false;

                        logInstance.Log(LoggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "CheckFilesAsync done");
                        await updateBookToVersionAsync(LoggedUser.token, book.id.ToString());
                        startTimerForUpdate = true;
                        //OnFinish(this, new EventArgs());
                    }
                }
                if (startTimerForUpdate)
                {
                    if (CheckFileList())
                    {
                        startTimerForUpdate = false;
                        BookReady = true;
                        //set this to make video conversion, 
                        FileDownloadCounter = 0;

                        //if video mp4 exist on server then
                        BookReady = true;
                    }


                }



                //go out from download to bookshelf page
                if (BookReady)
                {
                    //OnFinishBookDownload(this, new EventArgs());
                    //startTimerForCheckFiles = false;
                    try
                    {
                        fileWatcherTimer.Enabled = false;
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Application.Current.MainPage = new XBook2.Views.Program.BookGaleryList(LoggedUser, IsSmallScreen);
                        });


                    }
                    catch (Exception ee)
                    {

                        logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                    }
                }
            }
        }
        private bool CheckFileList()
        {
            foreach (BookFile bf in ListBookFiles)
            {
                if (bf.download == false)
                {
                    return false;
                }
            }
            return true;
        }
        public async Task<bool> updateBookToVersionAsync(string token, string id)
        {
            if (updateStart == true)
            {
                updateStart = false;
                //int ibookId = book.id;
               // int.TryParse(book.id, out ibookId);

                if (downloadTestBook > 0)
                {
                    book.id = downloadTestBook;
                }
               // startTimerForCheckFiles = true;
                //waitOnVideoConvert = false;
                htBookFiles = new Hashtable();
                ListBookFiles = new List<BookFile>();
                try
                {

                    logInstance.Log(LoggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "updateBookToVersion start for id: " + id);


                    BookPageList = await getUpdateBookFileList(LoggedUser.token, book.id);
                    if (BookPageList.fileInfo != null)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DownloadProgress = Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "connection_download_update_progress"); ;
                        });

                        //lblDownloadProgress.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
                        //{ lblDownloadProgress.Content = KlasimeBase.Localization.GetLocalization(GetType(), loggedUser.currentLanguage, "connection_download_update_progress"); }));

                        downloadBook = new DownloadBook(LoggedUser, book.id);
                        downloadBook.OnProgressUpdate += DownloadBook_OnProgressUpdate;

                        foreach (XBook2.FileInfo fi in BookPageList.fileInfo)
                        {
                            if (htBookFiles.Contains(fi.local_path) == false)
                            {
                                string lc = "";
                                //string locPath = "";
                                
                                try
                                {
                                    lc = Common.RemoveVersionNumber(fi.local_path);
                                    //local_path = "\\book" + Common.GetPathSeparator() + LoggedUser.firstname + "_" + LoggedUser.lastname +  lc.Replace("/", "\\");
                                    local_path = BookBasePath + lc.Replace("/", Common.GetPathSeparator());
                                    
                                    BookFile bf = new BookFile(fi.local_path, fi.url, local_path);

                                    #region check path
                                    string[] arr = local_path.Split(Common.GetPathSeparator().ToCharArray());
                                    string dir = "";
                                    for (int i = 1; i < arr.Length - 1; i++)
                                    {
                                        string str = arr[i];
                                        {
                                            dir += str + Common.GetPathSeparator();
                                            
                                            if (Directory.Exists(dir) == false)
                                            {
                                                Directory.CreateDirectory(dir);
                                            }
                                        }
                                    }
                                    #endregion

                                    #region download
                                    bool res = await getFileFromUrlSingle(fi.url, local_path);
                                    if (res == true)
                                    {

                                        int percent = ((FileDownloadCounter - 1) * 100) / BookPageList.fileInfo.Count;
                                        double dblProgressPercent = (((double)FileDownloadCounter - 1)) / (double)BookPageList.fileInfo.Count;
                                        if (percent > 100) percent = 100;

                                        PercentText = percent.ToString() + "%";
                                        ProgressValue = dblProgressPercent;

                                        bf.download = true;
                                        htBookFiles.Add(bf.LocalPath, true);

                                        ListBookFiles.Add(bf);

                                    }
                                    else
                                    {
                                        await logInstance.LogAsync(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), null, "TaskDownloadBook error in downloading " + local_path);
                                        bf.download = false;
                                        htBookFiles.Add(bf.LocalPath, false);
                                        ListBookFiles.Add(bf);
                                    }
                                    #endregion
                                    //htBookFiles.Add(bf.LocalPath, false);
                                    //ListBookFiles.Add(bf);

                                }
                                catch (Exception ee)
                                {
                                    logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee, local_path);
                                }

                            }
                        }
                        FileDownloadCounter = 1;
                        //await downloadBook.TaskDownloadBook(ListBookFiles);
                        if (CheckFileList() == true)
                        {
                            logInstance.Log(LoggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "CheckFilesAsync done");
                            await updateBookToVersionAsync(LoggedUser.token, book.id.ToString());

                        }
                        else
                        {
                            //or just inform that all files are not downloaded
                            //to start again ....
                            //startTimerForCheckFiles = true;
                            await App.Current.MainPage.DisplayAlert("Not all pages downloaded, check Internet connection and try agian", "Book Problem", "OK");
                        }

                    }
                    else
                    {
                        //OnFinish(this, new EventArgs());
                    }

                    return true;

                }
                catch (Exception ee)
                {
                    logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee, "");
                    //init for timer to check files to finish download, video convert
                    if (startTimerForCheckFiles == false)
                    {
                        startTimerForCheckFiles = true;
                    }
                }
            }
            return false;
        }

        private void DownloadBook_OnProgressUpdate(object sender, ProgressUpdateEventArgs e)
        {
            try
            {

                foreach (BookFile bf in ListBookFiles)
                {
                    if (bf.LocalPath == e.FileName)
                    {
                        bf.download = true;
                        FileDownloadCounter++;
                    }
                }
                System.Threading.Thread.Sleep(500);
                //double check for files in folders
                int fileCount = 0;
                if (Directory.Exists(filesDir))
                {
                    fileCount = Directory.GetFiles(filesDir, "*.*", SearchOption.AllDirectories).Length;
                }
                int percent = ((FileDownloadCounter - 1) * 100) / e.TotalFilesToDownload;
                double dblProgressPercent = (((double)FileDownloadCounter - 1)) / (double)e.TotalFilesToDownload;
                if (percent > 100) percent = 100;


                ProgressValue = dblProgressPercent;

                PercentText = percent.ToString() + "%";

            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }
        }
        #endregion

        #region API
        /// <summary>
        /// 6HG-BSB-1DK
        /// </summary>
        /// <param name="token"></param>
        /// <param name="id"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        private async Task<bool> activateBook(string token, string id, string code)
        {
            string jsonString = "";
            try
            {
                await logInstance.LogAsync(LoggedUser.username, Severity.Trace, MethodBase.GetCurrentMethod(), null, "activateBook start book id: " + id);
                // lblErrStatus.IsVisible = false;
                jsonString = await client.GetStringAsync(Book.urlPart + "/api/book/activate?book-id=" + id + "&code=" + code + "&token=" + token);
                //task.Wait();
                //string jsonString = task.Result.ToString();
                if (jsonString != null)
                {
                    if (jsonString.Contains("error"))
                    {
                        ActivateResponseFalse obj = JsonConvert.DeserializeObject<ActivateResponseFalse>(jsonString);
                        logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), null, jsonString);

                        ActivateDesc = KlasimeBase.Localization.GetLocalization(GetType(), LoggedUser.currentLanguage, "code_enter_not_match");

                        return false;
                    }
                    if (jsonString.Contains("success"))
                    {
                        ActivateResponseOK obj = JsonConvert.DeserializeObject<ActivateResponseOK>(jsonString);
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return false;
            }

        }

        private async Task<FileInfoList> getBookFileList(string token, int id)
        {
            string jsonString = "";
            try
            {
                //for test
                
                //android works, read from app location
                string fileName = "book" + id.ToString() + ".json";
                try
                {
                    //get demo book from web url
                    //http://85.215.209.4:8033/json/book41.json

                    jsonString = await client.GetStringAsync("http://85.215.209.4:8033/json/book" + id.ToString() + ".json");
                    /*
                     
                    if (File.Exists(fileName) == true)
                    {
                        //jsonString= File.ReadAllText(fileName); 
                        using (var stream = FileSystem.OpenAppPackageFileAsync(fileName).Result)
                        {
                            using (var reader = new StreamReader(stream))
                            {
                                jsonString = reader.ReadToEnd();
                            }
                        }
                    }
                    */
                }
                catch (Exception ee)
                {
                    logInstance.Log(LoggedUser.username, Severity.Low, MethodBase.GetCurrentMethod(), null, "Error in dowloading book id: " + id + Environment.NewLine + jsonString);
                }




                if (jsonString == "")
                {
                    jsonString = await client.GetStringAsync(Book.urlPart + "/api/book/content/" + id + "?token=" + token);
                    File.WriteAllText(Common.GetLocalPath() + Common.GetPathSeparator() + "book" + id + ".json", jsonString);
                }




                if (jsonString.Contains("error"))
                {
                    ActivateResponseFalse errobj = JsonConvert.DeserializeObject<ActivateResponseFalse>(jsonString);
                    // lblErrStatus.Content = errobj.error.ToString();
                    //              lblErrStatus.IsVisible = true;
                    logInstance.Log(LoggedUser.username, Severity.Low, MethodBase.GetCurrentMethod(), null, "Error in dowloading book id: " + id + Environment.NewLine + jsonString);

                    return new FileInfoList();
                }
                jsonString = KlasimeBase.Base.PrepareJsonArray(jsonString);

                FileInfoList obj = JsonConvert.DeserializeObject<FileInfoList>(jsonString);

                return obj;
            }
            catch (Exception ee)
            {
                await logInstance.LogAsync(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);

                return new FileInfoList();
            }

        }
        private async Task<FileInfoList> getUpdateBookFileList(string token, int id)
        {
            string jsonString = "";
            try
            {

                if (downloadTestBook == 0)
                {
                    jsonString = await client.GetStringAsync(Book.urlPart + "/api/book/update/" + id + "?token=" + token);
                    File.WriteAllText(Common.GetLocalPath() + Common.GetPathSeparator() + "Update_book" + id + ".json", jsonString);
                }
                else
                {
                    jsonString = File.ReadAllText(Common.GetLocalPath() + Common.GetPathSeparator() + "Update_book" + downloadTestBook.ToString() + ".json");
                }



                if (jsonString.Contains("error"))
                {
                    ActivateResponseFalse errobj = JsonConvert.DeserializeObject<ActivateResponseFalse>(jsonString);
                    // lblErrStatus.Content = errobj.error.ToString();
                    //              lblErrStatus.IsVisible = true;
                    logInstance.Log(LoggedUser.username, Severity.Low, MethodBase.GetCurrentMethod(), null, "Error in dowloading book id: " + id + Environment.NewLine + jsonString);
                    return new FileInfoList();
                }
                jsonString = KlasimeBase.Base.PrepareJsonArray(jsonString);

                FileInfoList obj = JsonConvert.DeserializeObject<FileInfoList>(jsonString);

                return obj;
            }
            catch (Exception ee)
            {
                logInstance.Log(LoggedUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return new FileInfoList();
            }

        }


        #endregion
    }
}
