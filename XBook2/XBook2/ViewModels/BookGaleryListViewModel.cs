﻿using KlasimeBase.Log;
using KlasimeBase;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xnook2.ViewModel;
using SharedKlasime;
using System.Collections.Generic;
using System.Collections;
using System.Net.Http;
using KlasimeViews;
using XBook2.Views.Program;
using Newtonsoft.Json;
using XBook2.Model;
using Xamarin.CommunityToolkit.UI.Views;
using XBook2.Helpers;

namespace XBook2.ViewModels
{
    public class BookGaleryListViewModel : ViewModelBase
    {
        #region VARIABLES
  
        public string serializedBookList = "";
        string bookid = "";
        int downloadTestBook = 0;
        bool startTimerForCheckFiles = false;
        HttpClient client;
       
        private int bookButtonWidth = 100;
        //private int bookTotal=12;
        int maxBooksPerRow = 0;
        int bookCounter = 1;
        //int lastPage = 1;
        int rows = 1;
        Thickness frameMargin;
        //int fromPage = 1;
        //int toPage = 1;

        Hashtable htPages = new Hashtable();
        Hashtable htMyBooks = new Hashtable();
        List<string> lstActivatedBooks = new List<string>();
        List<AlbasBook> userBooks = new List<AlbasBook>();
        List<AlbasBook> userActivatedBooks = new List<AlbasBook>();

        int ibookId = 0;

        ItemsLayoutOrientation deviceOrientation = ItemsLayoutOrientation.Vertical;

        List<BookFile> listBookFiles = new List<BookFile>();

        Hashtable htBookFiles = new Hashtable();
     
        string name = "Darko";
        string welcome="Welcome";

        #endregion

        #region OBSERVAL COLLECTIONS
        public ObservableCollection<BookItem> Books { get; set; } = new ObservableCollection<BookItem>();
        #endregion
        
        #region PROPERTIES

        public AlbasBook AlbasBook { get; set; }
        BookItem CurrentBookItem { get; set; }
        public ICommand ResetGpaCommand { private set; get; }

        
        public string Name
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref name, value);
                }
            }
            get
            {
                return name;
            }
        }
        //BookRowHeight
        private int bookRowHeight = 400;
        public int BookRow       {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookRowHeight, value);
                }
            }
            get
            {
                return bookRowHeight;
            }
        }
        private GridLength bookRowHeightLow = 5;
        public GridLength BookRowHeightLow
        {
            set
            {
                //if (value != null)
                {
                    SetProperty(ref bookRowHeightLow, value);
                }
            }
            get
            {
                GridLength g= new GridLength(5);
                

                return g; 
                //return bookRowHeightLow;
            }
        }

        private int bookColumnWidth = 400;
        public int BookColumnWidth
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bookColumnWidth, value);
                }
            }
            get
            {
                return bookColumnWidth;
            }
        }
        //FrameMargin
        public Thickness FrameMargin
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref frameMargin, value);
                }
            }
            get
            {
                return new Thickness(150, 0, 150, 0); //frameMargin;
            }
        }
        //
        public ItemsLayoutOrientation DeviceOrientation
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref deviceOrientation, value);
                }
            }
            get
            {
                return deviceOrientation;
            }
        }
        public string NameOfUser
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref User, value);
                }
            }
            get
            {
                return User;
            }
        }
        public string Welcome
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref welcome, value);
                }
            }
            get
            {
                return welcome;
            }
        }

        private bool waitOnBook;
        public bool WaitOnBook
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref waitOnBook, value);
                }
            }
            get
            {
                return waitOnBook;
            }
        }
        private bool mainView=true;
        public bool MainView
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref mainView, value);
                }
            }
            get
            {
                return mainView;
            }
        }

        //PeekAreaInsets
        private double peekAreaInsets = 400;
        public double PeekAreaInsets
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref peekAreaInsets, value);
                }
            }
            get
            {
                return peekAreaInsets;
            }
        }

        private double behaviourBookHeight = 0.0;
        public double BehaviourBookHeight
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref behaviourBookHeight, value);
                }
            }
            get
            {
                return behaviourBookHeight;
            }
        }

        //MenuHeightRequest
        private double menuHeightRequest = 140;
        public double MenuHeightRequest
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref menuHeightRequest, value);
                }
            }
            get
            {
                return menuHeightRequest;
            }
        }
        //MenuTranslationY
        private double menuTranslationY = -115;
        public double MenuTranslationY
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref menuTranslationY, value);
                }
            }
            get
            {
                return menuTranslationY;
            }
        }
        //PligonHeightRequest
        private double poligonHeightRequest = 80;
        public double PoligonHeightRequest
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref poligonHeightRequest, value);
                }
            }
            get
            {
                return poligonHeightRequest;
            }
        }
        //CarouselBookHeight
        private double carouselBookHeight = 500;
        public double CarouselBookHeight
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref carouselBookHeight, value);
                }
            }
            get
            {
                return carouselBookHeight;
            }
        }
        //BottomOffset
        private double bottomOffset = 20;
        public double BottomOffset
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref bottomOffset, value);
                }
            }
            get
            {
                return bottomOffset;
            }
        }    
        
        public double ButtonTranslationY { get; set; }

        //FrameWidthRequest
        private double frameWidthRequest = 80;
        public double FrameWidthRequest
        {
            set
            {
                if (value != null)
                {
                    SetProperty(ref frameWidthRequest, value);
                }
            }
            get
            {
                return frameWidthRequest;
            }
        }
        #endregion

        #region COMMANDS
        public ICommand LogoutCommand { private set; get; }
        public ICommand AlbasWebCommand { private set; get; }
        #endregion

        #region CONSTRUCTOR
        public BookGaleryListViewModel()
        {

            client = new HttpClient();
            ResetGpaCommand = new Command(() => Name = "new Name");
            InitCommandsCustom();
            
        }
        public BookGaleryListViewModel(CUser _loggedUser,bool _IsSmallScreen) : base(_loggedUser)
        {
            LoggedUser = _loggedUser;
            WaitOnBook = false; 
            MainView = true;
           
            User = LoggedUser.firstname + " " + LoggedUser.lastname;
            client = new HttpClient();
            ResetGpaCommand = new Command(() => Name = "new Name");
            InitCommandsCustom();

        }
        #endregion

        #region METHODS
        private void InitCommandsCustom()
        {
            LogoutCommand = new Command(() => Application.Current.MainPage = new Login());
            AlbasWebCommand = new Command(() => Common.OpenWebPage("http://portalishkollor.al/faqet/termat-e-perdorimit"));
        }
     
        public void AddBookToList(AlbasBook book, bool exist, bool locked, bool remove, bool download, bool notready)
        {
            try
            {
                BookItem bi = new BookItem(LoggedUser, IsSmallScreen);
                bi.parentViewModel = this;
                bi.AlbasBook= book;
                bi.loggedUser = LoggedUser;
                bi.Id = book.id.ToString();
                bi.Name = book.title;

                bi.Exist = exist;
                bi.Locked = locked;
                bi.Remove = remove;
                bi.Download = download;
                if(notready)
                {
                    //bi.BookOpacity = 0.4;
                }
                bi.NotReady = notready;
                bi.Desc = book.description;

                // bi.ImageURL = book.medium_photo_url;
                bi.ImageURL = book.medium_photo_url;
                //bi.ImageURL = "https://ti.portalishkollor.al/uploads/books/41/91003_400.jpg";
                //bi.ImageURL = "https://aka.ms/campus.jpg"; 
                bi.ButtonTranslationY = ButtonTranslationY;
                bi.FrameWidthRequest = FrameWidthRequest;
                Books.Add(bi);
            }
            catch (Exception ee)
            {

                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
           
        }
        private async Task GetMyBooks()
        {
            try
            {
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), null, "GetMyBooks start");
                //lblErrStatus.IsVisible = false;
                htMyBooks = new Hashtable();
              
                if (Directory.Exists(BookBasePath))
                {
                    string[] folders = Directory.GetDirectories(BookBasePath);
                    foreach (string folder in folders)
                    {
                        var splitter = Common.GetPathSeparator().ToCharArray();
                        string[] tmpArray = folder.Split(splitter[0]);
                        if (tmpArray.Length > 1)
                        {
                            string bookId = tmpArray[tmpArray.Length - 1];
                            // int myBookId = 0;
                            //int.TryParse(bookId, out myBookId);
                            //if (myBookId > 0)
                            {
                                if (htMyBooks.ContainsKey(bookId) == false)
                                {
                                    htMyBooks.Add(bookId, folder);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ee)
            {

                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        private void ButtonClick()
        {

            bool BookActivated = true;
            AlbasBook = new AlbasBook();

            if (CurrentBookItem != null)
            {
                foreach (AlbasBook item in userBooks)
                {
                    if (item.id.ToString() == CurrentBookItem.Id)
                        AlbasBook = item;
                }

                if (CurrentBookItem.Exist)
                {

                    int.TryParse(CurrentBookItem.Id, out ibookId);
                    #region GetUpdate
                    //try
                    //{
                    //    BookEventArgs b = new BookEventArgs(AlbasBook);
                    //    b.Update = true;

                    //    OnBookClickEvent(this, b);
                    //}
                    //catch (Exception ee)
                    //{
                    //    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                    //}
                    #endregion

                    #region OpenBook
                    try
                    {
                        Application.Current.MainPage = new MainPage(LoggedUser, ibookId, null);
                    }
                    catch (Exception ee)
                    {
                        logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                    }
                    #endregion
                }
                else
                {
                    if (CurrentBookItem.Download == true)
                    {
                        #region DownloadBook
                        try
                        {
                            BookPreviewControl bcn = new BookPreviewControl(LoggedUser, AlbasBook, false, false, BookButtonAction.Download, IsSmallScreen);

                            //bookList.Children.Add(bcn);
                            bcn.OnBookClick += Bcn_OnBookClick;
                            Application.Current.MainPage = bcn;
                        }
                        catch (Exception ee)
                        {
                            logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                        }
                        #endregion
                    }
                    if (CurrentBookItem.Locked == true)
                    {
                        #region ActivateBook
                        try
                        {
                            //BookEventArgs b = new BookEventArgs(AlbasBook, BookButtonAction.Activate);
                            //OnBookClickEvent(this, b);
                        }
                        catch (Exception ee)
                        {
                            logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                        }
                        #endregion
                    }
                }
            }
        }

        /// <summary>
        /// Button click from list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void Bcn_OnBookClick(object sender, BookEventArgs e)
        {
            throw new NotImplementedException();
        }

        public async Task PrepareBookList()
        {
            int testCounter = 1;
            htMyBooks.Clear();
            //Books.Clear();
            await GetMyBooks();
            userBooks = await RequestAvailableBooks(LoggedUser.token);
            userActivatedBooks = await RequestActivatedBooks(LoggedUser.token);

            foreach (var item in userBooks)
            {
                bool exist = false;
                bool locked = false;
                bool remove = false;
                bool download = true;
                bool notready = true;
                if (testCounter < BookTotal)
                {
                    //book exist
                    if (htMyBooks.ContainsKey(item.id.ToString()))
                    {
                        exist = true;
                        locked = false;
                        remove = true;
                        download = false;
                        notready = false;
                    }
                    else
                    {

                        //book no exist
                        //already activated
                        if (lstActivatedBooks.Contains(item.id.ToString()))
                        {
                            locked = false;
                            download = true;
                        }
                        else
                        {
                            //not activated
                            locked = true;
                            download = false;
                        }
                    }


                    AddBookToList(item, exist, locked, remove, download, notready);
                }
                testCounter++;
            }
        }

        #endregion

        #region API
        private async Task<List<AlbasBook>> RequestAvailableBooks(string token)
        {


            try
            {
                
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), null, "RequestAvailableBooks start ");
                //lblErrStatus.IsVisible = false;
                if (File.Exists(Common.GetLocalPath() + Common.GetPathSeparator() + "serializedBookList.json"))
                {
                    serializedBookList = File.ReadAllText(Common.GetLocalPath() + Common.GetPathSeparator() + "serializedBookList.json");
                }


                if (serializedBookList == "")
                {

                    serializedBookList = await client.GetStringAsync(Book.urlPart + "/api/book/index?token=" + token);
                    File.WriteAllText(Common.GetLocalPath() + Common.GetPathSeparator() + "serializedBookList.json", serializedBookList);
                }

                List<AlbasBook> obj = JsonConvert.DeserializeObject<List<AlbasBook>>(serializedBookList);


                return obj;
            }
            catch (Exception ee)
            {                
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee, serializedBookList);
                return new List<AlbasBook>();
            }

        }
        private async Task<List<AlbasBook>> RequestActivatedBooks(string token)
        {
            string jsonString = "";
            lstActivatedBooks = new List<string>();
            try
            {
                
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), null, "RequestActivatedBooks start ");
                //lblErrStatus.IsVisible = false;
                jsonString = await client.GetStringAsync(Book.urlPart + "/api/book/activated?token=" + token);
                //task.Wait();
                //string jsonString = task.Result.ToString();
                List<AlbasBook> obj = JsonConvert.DeserializeObject<List<AlbasBook>>(jsonString);

                foreach (AlbasBook item in obj)
                {
                    if (lstActivatedBooks.Contains(item.id.ToString()) == false)
                    {
                        lstActivatedBooks.Add(item.id.ToString());
                    }
                }

                return obj;
            }
            catch (Exception ee)
            {
                logInstance.Log(User, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return new List<AlbasBook>();
            }

        }
        #endregion

    }
}
