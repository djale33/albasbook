﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace XBook2.UWP
{
    internal class CloseApplication : ICloseApplication
    {
        public void closeApplication()
        {
            Application.Current.Exit();            
        }
    }
}
