﻿using KlasimeBase;
using KlasimeBase.Log;
using Newtonsoft.Json;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace WpfDemo
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public static string user = "program";
        HttpClient client;
        CUser loggedUser;

        public Login()
        {
            InitializeComponent();
            client = new HttpClient();
        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            client = new HttpClient();
            string test = LoginName.Text;
            var resString = "";
            try
            {
                resString = await client.GetStringAsync(Book.urlPart + "/api/authenticate?username=" + LoginName.Text + "&password=" + LoginPassword.Text);

                //response can have additional data, so we have to get token only
                int startToken = resString.IndexOf("{");
                int endToken = resString.IndexOf("}");
                string jsonString = resString.Substring(startToken, endToken - startToken + 1);
                CUser cuser = JsonConvert.DeserializeObject<CUser>(jsonString);
                if ((bool)ckRememberLogin.IsChecked)
                {
                    SetUserInfoToXML();
                }
                else
                {
                    RemoveUserInfoXML();
                }
                string loginToken = cuser.token;
                Task<CUser> task = requestUserProfile(loginToken);
                task.Wait();
                cuser = task.Result;
                cuser.token = loginToken;
                user = cuser.firstname + "-" + cuser.lastname;
                //LoginStatus.TextColor = Color.Black;
                LoginStatus.Content = "Welcome " + cuser.firstname + " " + cuser.lastname;

                ApiLog.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "start loginToken: " + loginToken);

                #region Display and OS

                //ApiLog.Log(user, Severity.Trace, MethodBase.GetCurrentMethod(), null, "device OS: " + Device.RuntimePlatform);


             

                #endregion
                //Application.Current.MainPage = new BookGalery(cuser);

            }
            catch (Exception ee)
            {

                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee, resString);
                //LoginStatus.TextColor = Color.Red;
                LoginStatus.Content = ee.Message;
            }
        }

     
        private async Task<CUser> requestUserProfile(string token)
        {
            string jsonString = "";
            try
            {
                Task<string> task = client.GetStringAsync(Book.urlPart + "/api/profile?token=" + token);
                task.Wait();
                jsonString = task.Result.ToString();
                CUser obj = JsonConvert.DeserializeObject<CUser>(jsonString);
                return obj;
            }
            catch (Exception ee)
            {
                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee, jsonString);
                return new CUser();
            }

        }

        #region Remmember User
        private void GetUserInfoFromXML()
        {
            string userInfoPath = "";
            try
            {
                userInfoPath = KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "login.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("name");
                    attr.Value = LoginName.Text;
                    nod.Attributes.Append(attr);

                    attr = doc.CreateAttribute("pass");
                    attr.Value = LoginPassword.Text;
                    nod.Attributes.Append(attr);

                    doc.AppendChild(nod);
                    doc.Save(userInfoPath);
                }
                else
                {
                    doc.Load(userInfoPath);
                    if (doc.ChildNodes.Count > 0)
                    {
                        XmlNode nod = doc.ChildNodes[0];
                        if (nod.Attributes.Count == 2)
                        {
                            LoginName.Text = nod.Attributes[0].InnerText;
                            LoginPassword.Text = nod.Attributes[1].InnerText;
                        }

                    }
                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        /// <summary>
        /// save last visitet age
        /// </summary>
        private void SetUserInfoToXML()
        {
            string userInfoPath = "";
            try
            {
                userInfoPath = KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "login.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("name");
                    attr.Value = LoginName.Text;
                    nod.Attributes.Append(attr);

                    attr = doc.CreateAttribute("pass");
                    attr.Value = LoginPassword.Text;
                    nod.Attributes.Append(attr);

                    doc.AppendChild(nod);
                    doc.Save(userInfoPath);
                }
                else
                {
                    doc.Load(userInfoPath);
                    if (doc.ChildNodes.Count > 0)
                    {
                        XmlNode nod = doc.ChildNodes[0];
                        if (nod.Attributes.Count == 2)
                        {
                            nod.Attributes[0].Value = LoginName.Text;
                            nod.Attributes[1].Value = LoginPassword.Text;
                            doc.Save(userInfoPath);
                        }
                        else
                        {
                            ApiLog.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "XML for user settings have attribute missing");
                        }


                    }
                    else
                    {
                        ApiLog.Log(user, Severity.High, MethodBase.GetCurrentMethod(), null, "XML for user settings have no child nodes");
                    }
                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        private void RemoveUserInfoXML()
        {
            string userInfoPath = "";
            try
            {
                userInfoPath = Common.GetLocalPath() + Common.GetPathSeparator() + "login.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == true)
                {
                    File.Delete(userInfoPath);
                }



            }
            catch (Exception ee)
            {
                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
        #endregion

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            if ((bool)ckRememberLogin.IsChecked)
                GetUserInfoFromXML();
        }

        private async void btnRegister_Clicked(object sender, EventArgs e)
        {

            await Common.OpenWebPage("http://ti.portalishkollor.al/register");

        }
    }

   

}
