﻿//using Google.Protobuf.WellKnownTypes;
using KlasimeBase.Interfaces;
using KlasimeBase.Log;
using KlasimeViews;
//using KlasimeViews;
using Newtonsoft.Json;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
//using Xamarin.Forms;
//using static Xamarin.Forms.Internals.GIFBitmap;

namespace KlasimeBase
{
    public class Book
    {
        public static string urlPart = @"https://ti.portalishkollor.al";
        public static string user = "program";
        #region VARIABLES

        bool testThumbs = false;
        XmlDocument xmlOutlineLinkLanguage;
        bool bOutlineLinkLanguage = false;
        private bool status;
        private string errMessage;
        private int id;
        private string version;
        private string title;
        private string author;
        private string name;
        private string url;
        private string description;
        private string thumbs;
        private Hashtable htPageAnnos = new Hashtable();
        private Hashtable htPageNotes = new Hashtable();
        private Hashtable htPageSearchItems = new Hashtable();
        private bool bookInfo = false;
        private bool bookMedia = false;
        private bool bookThumbs = false;
        private int bookPages;
        private int bookPageMode = 2;
        string sep = "";
        private int pageHeight;
        private int pageWidht;
        private Hashtable htPageList = new Hashtable(); //store in correct order pageNum with Page Image and Page Search XML file
        private int currentPage;

        #endregion

        #region CONSTRUCTOR
        public Book( string _user,string bookPath, bool encoding)
        {
            user = _user;
            BookPath = bookPath;
            htThumbPages = new Hashtable();
            EncodeBook = encoding;
            Sep = Common.GetPathSeparator();
            status = PreparePagelist(bookPath, out errMessage);
            if (status)
            {
                bookInfo = getBookInfo(bookPath);

                bookMedia = getBookMedia().Result;
                bookThumbs = getBookThumbs(bookPath);
                PrepareBookSearch();

            }
            else
            {
                //log error
            }
        }
        #endregion

        #region PROPERTIES
        public int Id { get => id; set => id = value; }
        public string Title { get => title; set => title = value; }
        public string Author { get => author; set => author = value; }
        public string Name { get => name; set => name = value; }
        public string Url { get => url; set => url = value; }
        public string Description { get => description; set => description = value; }
        public string Version { get => version; set => version = value; }
        public string Thumbs { get => thumbs; set => thumbs = value; }
        public Hashtable HTPageAnnos { get => htPageAnnos; set => htPageAnnos = value; }
        public bool BookInfo { get => bookInfo; }
        public bool BookMedia { get => bookMedia; }
        public int BookPages { get => bookPages; set => bookPages = value; }
        public Hashtable HtPageList { get => htPageList; }
        public bool Status { get => status; set => status = value; }
        public string ErrMessage { get => errMessage; set => errMessage = value; }
        public int BookPageMode { get => bookPageMode; set => bookPageMode = value; }
        public int PageHeight { get => pageHeight; set => pageHeight = value; }
        public int PageWidht { get => pageWidht; set => pageWidht = value; }
        public int CurrentPage { get => currentPage; set => currentPage = value; }
        public string BookPath { get; set; }
        bool EncodeBook { get; set; }
        public Hashtable htThumbPages { get; set; }
        public Hashtable HtPageNotes { get => htPageNotes; set => htPageNotes = value; }
        public string Sep { get => sep; set => sep = value; }
        public Hashtable HtPageSearchItems { get => htPageSearchItems; set => htPageSearchItems = value; }
        #endregion

        #region METHODS
        public bool getBookInfo(string bookID)
        {
            Base.user = user;
            string xmlstr = "";
            try
            {
                XmlDocument myXMLDoc = new XmlDocument();
                if (EncodeBook == false)
                {
                    myXMLDoc.Load(KlasimeBase.Common.GetLocalPath() + bookID + Sep + @"book.xml");
                }
                else
                {
                    //file read to 
                    byte[] source = File.ReadAllBytes(KlasimeBase.Common.GetLocalPath() + bookID + Sep + @"book.xml");
                    byte[] res = Common.encodeByteArray(source);
                    xmlstr = Encoding.UTF8.GetString(res);
                    myXMLDoc.LoadXml(xmlstr);
                }
                foreach (XmlNode nod in myXMLDoc.DocumentElement.ChildNodes)
                {
                    switch (nod.Name)
                    {
                        case "id": this.Id = Base.SetInt32(nod); break;
                        case "version": this.Version = nod.InnerText; break;
                        case "title": this.Title = nod.InnerText; break;
                        case "description": this.Description = nod.InnerText; break;
                        case "thumbs": this.Thumbs = nod.InnerText; break;

                    }
                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee, xmlstr);
                return false;
            }
            return true;
        }
        public bool getBookThumbs(string bookID)
        {

            try
            {
                #region image
                SKBitmap myBitmap = null;
                string resourceID = KlasimeBase.Common.GetLocalPath() + bookID + Sep + @"\files\thumb\thumb0.jpg";
                if (EncodeBook == false)
                {

                }
                else
                {
                    using (var streamReader = new StreamReader(resourceID))
                    {
                        var bytes = default(byte[]);
                        using (var memstream = new MemoryStream())
                        {
                            using (SKPaint paint = new SKPaint())
                            {
                                streamReader.BaseStream.CopyTo(memstream);
                                bytes = memstream.ToArray();
                                byte[] res1 = Common.encodeByteArray(bytes);
                                myBitmap = SKBitmap.Decode(res1);
                            }
                        }
                    }
                }
                #endregion
                #region XML data
                XmlDocument myXMLDoc = new XmlDocument();
                if (EncodeBook == false)
                {
                    myXMLDoc.Load(Common.GetLocalPath() + bookID + Sep + @"files" + Sep + "thumb" + Sep + "thumb0.xml");
                }
                else
                {
                    //file read to 
                    byte[] source = File.ReadAllBytes(Common.GetLocalPath() + bookID + Sep + @"files" + Sep + "thumb" + Sep + "thumb0.xml");
                    byte[] res = Common.encodeByteArray(source);
                    string xmlstr = Encoding.UTF8.GetString(res);
                    myXMLDoc.LoadXml(xmlstr);

                }
                int x = 0, y = 0, w = 0, h = 0;
                foreach (XmlNode nod in myXMLDoc.DocumentElement.ChildNodes)
                {
                    switch (nod.Name)
                    {
                        case "SubTexture":
                            if (nod.Attributes.Count == 5)
                            {
                                int.TryParse(nod.Attributes[1].Value.ToString(), out x);
                                int.TryParse(nod.Attributes[2].Value.ToString(), out y);
                                int.TryParse(nod.Attributes[3].Value.ToString(), out w);
                                int.TryParse(nod.Attributes[4].Value.ToString(), out h);

                                SubTexture sb = new SubTexture(nod.Attributes[0].InnerText, x, y, w, h);
                                SKBitmap myThumb = createPageThumb(sb, myBitmap);
                                string[] arr = nod.Attributes[0].InnerText.Split('_');
                                string PageId = "";
                                if (arr.Length == 2)
                                {
                                    PageId = arr[1];
                                }
                                htThumbPages.Add(PageId, myThumb);
                                #region test for Thumb
                                if (testThumbs)
                                {
                                    if (myThumb != null)
                                    {
                                        SKData source = myThumb.Encode(SKEncodedImageFormat.Png, 10);
                                        string destFileName = Common.GetLocalPath() + bookID + Sep + "a" + Sep + nod.Attributes[0].Value.ToString() + ".png";
                                        if (File.Exists(destFileName))
                                        {
                                            File.Delete(destFileName);
                                        }
                                        using (FileStream fs = new FileStream(destFileName, FileMode.CreateNew))
                                        {
                                            source.SaveTo(fs);
                                        }
                                    }
                                }
                                #endregion
                            }
                            break;
                    }
                }
                #endregion
            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                return false;
            }
            return true;
        }

        public SKBitmap createPageThumb(SubTexture sb, SKBitmap myBitmap)
        {
            SKBitmap res = null;
            try
            {
                SKRect cropRect = new SKRect(sb.X, sb.Y, sb.X + sb.Width, sb.Y + sb.Height);

                SKBitmap croppedBitmap = new SKBitmap((int)cropRect.Width,
                                                      (int)cropRect.Height);
                SKRect dest = new SKRect(0, 0, cropRect.Width, cropRect.Height);
                SKRect source = new SKRect(cropRect.Left, cropRect.Top,
                                           cropRect.Right, cropRect.Bottom);

                using (SKCanvas canvas = new SKCanvas(croppedBitmap))
                {
                    canvas.DrawBitmap(myBitmap, source, dest);
                }

                return croppedBitmap;
            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return res;
        }

        public async Task<bool> getBookMedia()
        {
            try
            {
                var timer = new Stopwatch();
                timer.Start();

                this.HTPageAnnos = new Hashtable();
                XmlDocument myXMLDoc = new XmlDocument();
                if (EncodeBook)
                {
                    if (bOutlineLinkLanguage == false)
                    {
                        byte[] source = File.ReadAllBytes(KlasimeBase.Common.GetLocalPath() + BookPath + Sep + @"outlineLinkLanguage.xml");
                        byte[] res = Common.encodeByteArray(source);
                        string xmlstr = Encoding.UTF8.GetString(res);
                        try
                        {
                            myXMLDoc.LoadXml(xmlstr);
                        }
                        catch (Exception ee)
                        {
                            //FileLog.Log(MethodBase.GetCurrentMethod(), ee);

                            int stop = xmlstr.IndexOf("?>");
                            xmlstr = xmlstr.Remove(0, stop + 2);
                            myXMLDoc.LoadXml(xmlstr);

                        }
                        //﻿<?xml version="1.0" encoding="utf-8"?>

                        xmlOutlineLinkLanguage = myXMLDoc;
                    }
                    else
                    {
                        myXMLDoc = xmlOutlineLinkLanguage;
                    }

                }
                else
                {
                    myXMLDoc.Load(KlasimeBase.Common.GetLocalPath() + BookPath + Sep + @"outlineLinkLanguage.xml");
                }

                foreach (XmlNode nod in myXMLDoc.DocumentElement.ChildNodes)
                {
                    if (nod.Name == "pagesAnnos")
                    {
                        foreach (XmlNode item in nod.ChildNodes)
                        {
                            if (item.Name == "pageAnnos")
                            {
                                PageAnno myPageAnno = new PageAnno();
                                myPageAnno.PageIndex = Base.SetInt32(item.Attributes["pageIndex"]);
                                foreach (XmlNode pageitem in item.ChildNodes)
                                {
                                    switch (pageitem.Name)
                                    {
                                        case "pageInformation":
                                            #region PageInformation
                                            PageInformation pageInformation = new PageInformation();
                                            foreach (XmlNode pagecol in pageitem.ChildNodes)
                                            {
                                                pageInformation.PageColor = Base.SetInt32(pagecol);
                                                myPageAnno.PageInformation = pageInformation;
                                            }

                                            break;
                                        #endregion
                                        case "anno":
                                            #region Anno
                                            Anno anno = new Anno();
                                            anno.Annotype = pageitem.Attributes["annotype"].Value;
                                            if (pageitem.Attributes["annoId"] != null)
                                            {
                                                anno.AnnoId = pageitem.Attributes["annoId"].Value;
                                            }
                                            else
                                            {
                                                anno.AnnoId = Guid.NewGuid().ToString();
                                            }
                                            anno.Alpha = Base.SetInt32(pageitem.Attributes["alpha"]);

                                            foreach (XmlNode pageann in pageitem.ChildNodes)
                                            {
                                                switch (pageann.Name)
                                                {
                                                    case "location":
                                                        #region Location
                                                        Location location = new Location();

                                                        location.TannoName = pageann.Attributes["tannoName"].Value;
                                                        location.X = Base.SetDouble(pageann.Attributes["x"]);
                                                        location.Y = Base.SetDouble(pageann.Attributes["y"]);
                                                        location.Width = Base.SetDouble(pageann.Attributes["width"]);
                                                        location.Height = Base.SetDouble(pageann.Attributes["height"]);
                                                        location.Rotation = Base.SetDouble(pageann.Attributes["rotation"]);
                                                        location.Reflection = Base.SetBool(pageann.Attributes["reflection"]);
                                                        location.ReflectionType = Base.SetInt32(pageann.Attributes["reflectionType"]);
                                                        location.ReflectionAlpha = Base.SetInt32(pageann.Attributes["reflectionAlpha"]);
                                                        location.PageWidth = Base.SetDouble(pageann.Attributes["pageWidth"]);
                                                        location.PageHeight = Base.SetDouble(pageann.Attributes["pageHeight"]);
                                                        anno.Location = location;
                                                        break;
                                                    #endregion
                                                    case "hint":
                                                        #region Hint
                                                        Hint hint = new Hint();

                                                        hint.HintShapeColor = Base.SetInt32(pageann.Attributes["hintShapeColor"]);
                                                        hint.HintShapeColor2 = Base.SetInt32(pageann.Attributes["hintShapeColor2"]);
                                                        hint.HintShapeAlpha = Base.SetInt32(pageann.Attributes["hintShapeAlpha"]);
                                                        hint.HintW = Base.SetInt32(pageann.Attributes["hintW"]);
                                                        hint.HintH = Base.SetInt32(pageann.Attributes["hintH"]);
                                                        hint.HintAuto = Base.SetBool(pageann.Attributes["hintAuto"]);
                                                        hint.HintShapeType = Base.SetInt32(pageann.Attributes["hintShapeType"]);
                                                        foreach (XmlNode txt in pageann.ChildNodes)
                                                        {
                                                            if (txt.Name == "text")
                                                            {
                                                                hint.Text = txt.InnerText;
                                                            }
                                                        }
                                                        anno.Hint = hint;
                                                        break;
                                                    #endregion
                                                    case "shadow":
                                                        #region Shadow
                                                        Shadow shadow = new Shadow();

                                                        shadow.HasDropShadow = Base.SetBool(pageann.Attributes["hasDropShadow"]);
                                                        shadow.ShadowDistance = Base.SetInt32(pageann.Attributes["shadowDistance"]);
                                                        shadow.ShadowAngle = Base.SetInt32(pageann.Attributes["shadowAngle"]);
                                                        shadow.ShadowColor = Base.SetInt32(pageann.Attributes["shadowColor"]);
                                                        shadow.ShadowAlpha = Base.SetDouble(pageann.Attributes["shadowAlpha"]);
                                                        shadow.ShadowBlurX = Base.SetInt32(pageann.Attributes["shadowBlurX"]);
                                                        shadow.ShadowBlurY = Base.SetInt32(pageann.Attributes["shadowBlurY"]);

                                                        anno.Shadow = shadow;
                                                        break;
                                                    #endregion
                                                    case "hotSpotsURL":
                                                        #region hotSpotsURL                                                       
                                                        anno.HotSpotsURL = pageann.InnerText;
                                                        break;
                                                    #endregion
                                                    case "action":
                                                        #region Action
                                                        Action action = new Action();

                                                        action.TriggerEventType = pageann.Attributes["triggerEventType"].Value;
                                                        action.ActionType = pageann.Attributes["actionType"].Value;
                                                        action.WindowType = pageann.Attributes["windowType"] == null ? "" : pageann.Attributes["windowType"].Value;
                                                        action.AutoPlay = Base.SetBool(pageann.Attributes["autoPlay"]);
                                                        action.AutoPlayAgain = Base.SetBool(pageann.Attributes["autoPlayAgain"]);
                                                        action.SoundStopTriggerEvt = pageann.Attributes["soundStopTriggerEvt"] == null ? "" : pageann.Attributes["soundStopTriggerEvt"].Value;

                                                        foreach (XmlNode innerNode in pageann.ChildNodes)
                                                        {
                                                            switch (innerNode.Name)
                                                            {
                                                                case "titleText": action.TitleText = innerNode.InnerText; break;
                                                                case "titleSize": action.TitleSize = innerNode.InnerText; break;
                                                                case "titleFont": action.TitleFont = innerNode.InnerText; break;
                                                                case "bodyText": action.BodyText = innerNode.InnerText; break;
                                                                case "bodyAlign": action.BodyAlign = innerNode.InnerText; break;
                                                                case "bodySize": action.BodySize = innerNode.InnerText; break;
                                                                case "bodyFont": action.BodyFont = innerNode.InnerText; break;
                                                                case "cpName": action.CpName = innerNode.InnerText; break;
                                                                case "className": action.ClassName = innerNode.InnerText; break;
                                                                case "audioURL": action.AudioURL = innerNode.InnerText; break;
                                                                case "url": action.Url = innerNode.InnerText; break;
                                                                case "linkTarget": action.LinkTarget = innerNode.InnerText; break;
                                                                case "photos":
                                                                    foreach (XmlNode photo in innerNode.ChildNodes)
                                                                    {
                                                                        Photo myPhoto = new Photo();
                                                                        foreach (XmlNode photoPart in photo.ChildNodes)
                                                                        {
                                                                            switch (photoPart.Name)
                                                                            {
                                                                                case "title": myPhoto.Title = photoPart.InnerText; break;
                                                                                case "desc": myPhoto.Description = photoPart.InnerText; break;
                                                                                case "url": myPhoto.Url = photoPart.InnerText; break;
                                                                            }
                                                                        }
                                                                        action.Photos.Add(myPhoto);
                                                                    }
                                                                    break;
                                                                case "sliderType": action.SliderType = Base.SetInt32(innerNode); break;
                                                                case "caption": action.Caption = innerNode.InnerText; break;
                                                                case "resourceContent": action.ResourceContent = innerNode.InnerText; break;
                                                                case "reloadEveryTime": action.ReloadEveryTime = Base.SetBool(innerNode); break;
                                                                case "drag": action.Drag = Base.SetBool(innerNode); break;
                                                                case "hideTitle": action.HideTitle = Base.SetBool(innerNode); break;
                                                                //

                                                                default:
                                                                    string missing = innerNode.Name;
                                                                    break;
                                                            }
                                                        }
                                                        anno.Action = action;
                                                        break;
                                                        #endregion
                                                }


                                            }
                                            myPageAnno.Annos.Add(anno);
                                            break;
                                            #endregion
                                    }
                                }
                                #region customMultimedia

                                await AddALLCustomMedia(myPageAnno);

                                #endregion

                                if (myPageAnno.PageIndex == 4)
                                {
                                    this.HTPageAnnos.Add(0, myPageAnno);
                                }
                                myPageAnno.PageIndex = reorderPage(myPageAnno.PageIndex);
                                this.HTPageAnnos.Add(myPageAnno.PageIndex, myPageAnno);
                            }
                        }
                    }
                }
                timer.Stop();
                TimeSpan timeTaken = timer.Elapsed;
                string foo = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");
            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                return false;
            }
            return true;
        }
        private async Task<bool> AddALLCustomMedia(PageAnno myPageAnno)
        {
            await AddCustomMedia(myPageAnno, MultimediaType.Note);
            await AddCustomMedia(myPageAnno, MultimediaType.File);
            await AddCustomMedia(myPageAnno, MultimediaType.Audio);
            await AddCustomMedia(myPageAnno, MultimediaType.Video);
            await AddCustomMedia(myPageAnno, MultimediaType.PhotoGalery);
            await AddCustomMedia(myPageAnno, MultimediaType.Link);
            await AddCustomMedia(myPageAnno, MultimediaType.YouTube);
            return true;
        }
        private async Task<bool> AddCustomMedia(PageAnno myPageAnno, MultimediaType multimediaType)
        {
            string customPath = MultimediaPath.GetCustomMultimediaPath(multimediaType);
            CustomAnnos customMedia = await GetCustomNotesFromXML(myPageAnno.PageIndex - 2, customPath);
            if (customMedia != null)
            {
                foreach (Anno customItem in customMedia.Annos)
                {
                    customItem.Cutom = true;
                    myPageAnno.Annos.Add(customItem);
                }
            }
            return true;
        }
        private async Task PrepareBookSearch()
        {
            foreach (DictionaryEntry de in htPageList)
            {
                int PageNumber = (int)de.Key;
                string searchFileCorporate = "";
                if (PageNumber > 0)
                {
                    searchFileCorporate = getPageSearchXMLPath(PageNumber);
                    var test = parseSearchFileForFlipCorporate(PageNumber,searchFileCorporate);
                }
            }
        }

        public string[] parseSearchFileForFlipCorporate(int PageNumber,string fileName)
        {
            List<string> allWordsOnPage = new List<string>();
            string[] strLines = new string[0];
            try
            {


                if (EncodeBook == true)
                {

                    byte[] source = System.IO.File.ReadAllBytes(fileName);
                    byte[] strres = Common.encodeByteArray(source);
                    string xmlstr = Encoding.UTF8.GetString(strres);
                    //strLines = xmlstr.Split(@"\n".ToCharArray());

                    //todo: remove this after test!
                    File.WriteAllText(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "WriteLines.txt", xmlstr);
                    strLines = System.IO.File.ReadAllLines(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "WriteLines.txt");
                    File.Delete(KlasimeBase.Common.GetLocalPath() + Common.GetPathSeparator() + "WriteLines.txt");

                }
                else
                {
                    strLines = System.IO.File.ReadAllLines(fileName);
                }

                foreach (var item in strLines)
                {
                    allWordsOnPage.Add(item);
                }
                if (htPageSearchItems.ContainsKey(PageNumber) == false)
                {
                    htPageSearchItems.Add(PageNumber, allWordsOnPage);
                }
            }
            catch (Exception ee)
            {
                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return strLines;
        }
        public List<SearchListItem> getSearchFromBook(string item)
        {
            List<SearchListItem> res = new List<SearchListItem>();
            for(int i=1;i<= htPageSearchItems.Count;i++)
            {
                List<string> allWordsOnPage = (List<string>)htPageSearchItems[i];
                //if(allWordsOnPage.Contains(item))
                {
                    foreach (string strItem in allWordsOnPage)
                    {
                        if (strItem.Contains(item))
                        {
                            SearchListItem newItem = new SearchListItem(item, i,strItem);
                            res.Add(newItem);
                        }
                    }
                }
            }

            return res;
        }
        public bool PreparePagelist(string bookPath, out string errmsg)
        {
            errmsg = "";
            try
            {
                string pagePath = KlasimeBase.Common.GetLocalPath() + bookPath + "files" + Sep + "page";
                string searchPath = KlasimeBase.Common.GetLocalPath() + bookPath + "files" + Sep + "search" + Sep + "search";
                string[] files = Directory.GetFiles(pagePath,"*.jpg");
                BookPages = files.Length;
                foreach (string file in files)
                {
                    if (HtPageList.ContainsKey(file) == false)
                    {
                        int pageOrder = 0;
                        FileInfo fileInfo = new FileInfo(file);
                        string strPageOrder = fileInfo.Name.Remove(fileInfo.Name.Length - 4, 4);
                        int.TryParse(strPageOrder, out pageOrder);

                        //search xml file
                        string xmlSearchFile = searchPath + pageOrder + ".xml";
                        if (File.Exists(xmlSearchFile) == false)
                        {
                            ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(),null, "Search XML file not exist: " + xmlSearchFile);
                        }
                        BookPageFiles bookPage = new BookPageFiles(file, xmlSearchFile);
                        if (pageOrder == 4)
                        {
                            HtPageList.Add(0, bookPage);
                        }
                        pageOrder = reorderPage(pageOrder);
                        if (htPageList.ContainsKey(pageOrder) == false)
                        {
                            HtPageList.Add(pageOrder, bookPage);
                        }
                        else
                        {
                            ApiLog.Log(user,Severity.Low, MethodBase.GetCurrentMethod(),null, "page exists " + pageOrder);
                        }


                    }
                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                errmsg = ee.Message;

                return false;
            }
            return true;
        }
        public int reorderPage(int pageOrder)
        {
            //change possition for first pages
            //page 2 help   1 possition
            //page 1 korica 2 possition
            //page 4 blank  3 possition
            //page 3        4 possition
            //page 4 blank  5 possition
            //page 5        6 possition
            try
            {
                switch (pageOrder)
                {
                    case 1: pageOrder = -1; break;
                    case 2: pageOrder = -2; break;
                    case 3: pageOrder = 1; break;
                    case 4: pageOrder = 2; break;
                }

                if (pageOrder > 4)
                {
                    pageOrder -= 2;

                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }
            return pageOrder;
        }
        public string getPagePath(int PageNum)
        {
            string res = "";
            BookPageFiles tmp;
            if (HtPageList.ContainsKey(PageNum))
            {
                tmp = (BookPageFiles)HtPageList[PageNum];

            }
            else
            {
                tmp = (BookPageFiles)HtPageList[4];

            }
            res = tmp.PageImage;
            return res;
        }
        public string getPageSearchXMLPath(int PageNum)
        {
            string res = "";
            BookPageFiles tmp;
            if (HtPageList.ContainsKey(PageNum))
            {
                tmp = (BookPageFiles)HtPageList[PageNum];

            }
            else
            {
                tmp = (BookPageFiles)HtPageList[4];

            }
            res = tmp.PageSearchXML;
            return res;
        }
        #endregion
        //public bool SetImageSource(Image img, SKBitmap resource)
        //{
        //    SKBitmap myBitmap = resource;
        //    if (myBitmap != null)
        //    {
        //        SKData sk = myBitmap.Encode(SKEncodedImageFormat.Png, 10);
        //        Stream stream = sk.AsStream();
        //        img.Source = ImageSource.FromStream(() => stream);
        //    }
        //    else
        //    {
        //        ApiLog.Log(user,Severity.Low, MethodBase.GetCurrentMethod(),null, "Thumb image missing for page=1");
        //        return false;
        //    }
        //    return true;
        //}
        //public bool SetImageSource(ImageButton img, SKBitmap resource)
        //{
        //    SKBitmap myBitmap = resource;
        //    if (myBitmap != null)
        //    {
        //        SKData sk = myBitmap.Encode(SKEncodedImageFormat.Png, 10);
        //        Stream stream = sk.AsStream();
        //        img.Source = ImageSource.FromStream(() => stream);
        //    }
        //    else
        //    {
        //        ApiLog.Log(user,Severity.Low, MethodBase.GetCurrentMethod(),null, "Thumb image missing for page=1");
        //        return false;
        //    }
        //    return true;
        //}

        #region bookmark page
        public bool SetBookmarkToXML(int pageNum)
        {
            bool bookmarked = false;
            try
            {
                string userInfoPath = KlasimeBase.Common.GetLocalPath() + BookPath + Sep + "userBookmark.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("id");
                    attr.Value = id.ToString();
                    nod.Attributes.Append(attr);
                    doc.AppendChild(nod);


                    XmlNode nodBookmark = doc.CreateElement("Page");
                    nodBookmark.InnerText = pageNum.ToString();
                    nod.AppendChild(nodBookmark);


                    doc.Save(userInfoPath);
                    //btnBookmark.Source = ImageSource.FromResource("XBook2.media.load.orange.bookmarks_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                    bookmarked = true;
                }
                else
                {
                    doc.Load(userInfoPath);



                    XmlNodeList nodes = doc.SelectNodes("//Page[text()='" + pageNum.ToString() + "']");
                    if (nodes.Count > 0)
                    {
                        //have node and we delete it
                        for (int i = nodes.Count - 1; i >= 0; i--)
                        {
                            nodes[i].ParentNode.RemoveChild(nodes[i]);
                        }
                        doc.Save(userInfoPath);


                        //btnBookmark.Source = ImageSource.FromResource("XBook2.media.load.orange.bookmarks_up.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        bookmarked = false;
                    }
                    else
                    {
                        //no node and we add
                        XmlNode nod = doc.FirstChild;
                        XmlNode nodBookmark = doc.CreateElement("Page");
                        nodBookmark.InnerText = pageNum.ToString();
                        nod.AppendChild(nodBookmark);

                        doc.Save(userInfoPath);
                        //btnBookmark.Source = ImageSource.FromResource("XBook2.media.load.orange.bookmarks_select.png", typeof(ImageResourceExtension).GetTypeInfo().Assembly);
                        bookmarked = true;
                    }

                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return bookmarked;
        }
        public bool GetBookmarkStatusFromXML(int pageNum)
        {
            bool exist = false;
            try
            {
                string userInfoPath = KlasimeBase.Common.GetLocalPath() + BookPath + Sep + "userBookmark.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("id");
                    attr.Value = id.ToString();
                    nod.Attributes.Append(attr);
                    doc.AppendChild(nod);


                    XmlNode nodBookmark = doc.CreateElement("Page");
                    nodBookmark.InnerText = pageNum.ToString();
                    nod.AppendChild(nodBookmark);


                    doc.Save(userInfoPath);

                }



                doc.Load(userInfoPath);

                XmlNodeList nodes = doc.SelectNodes("//Page[text()='" + pageNum.ToString() + "']");
                if (nodes.Count > 0)
                {
                    exist = true;
                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return exist;
        }
        public List<int> GetAllBookmarkFromXML()
        {
            List<int> list = new List<int>();
            try
            {
                string userInfoPath = Common.GetLocalPath() + BookPath + Sep + "userBookmark.xml";
                XmlDocument doc = new XmlDocument();
                if (File.Exists(userInfoPath) == false)
                {

                    XmlNode nod = doc.CreateElement("user");

                    XmlAttribute attr = doc.CreateAttribute("id");
                    attr.Value = id.ToString();
                    nod.Attributes.Append(attr);
                    doc.AppendChild(nod);


                    XmlNode nodBookmark = doc.CreateElement("Page");
                    //nodBookmark.InnerText = .ToString();
                    nod.AppendChild(nodBookmark);


                    doc.Save(userInfoPath);

                }
                doc.Load(userInfoPath);
                var nodes = doc.SelectNodes("//Page");
                foreach (XmlNode item in nodes)
                {
                    int page = 0;
                    int.TryParse(item.InnerText, out page);
                    list.Add(page);
                }



            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return list;
        }
        #endregion

        #region CustomDrawing
        public async Task<bool> AddCustomDrawing(DrawItemOnPage dip, string path)
        {
            
            try
            {
                string jsonString = JsonConvert.SerializeObject(dip);
                // DrawItemOnPage customNotes = new DrawItemOnPage();

                //customNotes = await GetCustomNotesFromXML(newAnno.PageNumber, path);
                //List<Anno> pageAnnos = new List<Anno>();

                //customNotes.Annos.Add(newAnno);


                string userInfoPath = KlasimeBase.Common.GetLocalPath() + BookPath + Sep + path + dip.PageNumber.ToString() + ".json";

                File.WriteAllText(userInfoPath, jsonString);


            }
            catch (Exception ee)
            {
                
                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return true;
        }
        public DrawItemOnPage GetCustomDrawing(string path,int PageNumber)
        {
            DrawItemOnPage dip = null;
            try
            {
                string userInfoPath = KlasimeBase.Common.GetLocalPath() + BookPath + Sep + path + PageNumber.ToString() + ".json";
                string jsonString="";
                if (File.Exists(userInfoPath))
                {
                    jsonString = File.ReadAllText(userInfoPath);

                    var settings = new JsonSerializerSettings
                    {
                        Error = OnError,
                        MissingMemberHandling = MissingMemberHandling.Error
                    };

                    dip = (DrawItemOnPage)JsonConvert.DeserializeObject(jsonString, typeof(DrawItemOnPage));
                    // DrawItemOnPage customNotes = new DrawItemOnPage();

                    //customNotes = await GetCustomNotesFromXML(newAnno.PageNumber, path);
                    //List<Anno> pageAnnos = new List<Anno>();

                    //customNotes.Annos.Add(newAnno);
                }

            }
            catch (Exception ee)
            {

                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return dip;
        }
        static  void OnError(object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args)
        {
            string s = "";
            //Console.WriteLine("Unable to find member '{0}' on object of type {1}", args.ErrorContext.Member, args.ErrorContext.OriginalObject.GetType().Name);

            // set the current error as handled
            //args.ErrorContext.Handled = true;
        }
        public DrawItemOnPage DropCustomDrawing(string path, int PageNumber)
        {
            DrawItemOnPage dip = null;
            try
            {
                string userInfoPath = KlasimeBase.Common.GetLocalPath() + BookPath + Sep + path + PageNumber.ToString() + ".json";
                
                if (File.Exists(userInfoPath))
                {
                    File.Delete(userInfoPath);
                }

            }
            catch (Exception ee)
            {

                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return dip;
        }
        #endregion
        #region CustomNote
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newAnno"></param>
        /// <param name="path">userNotes_</param>
        /// <returns></returns>
        public async Task<bool> AddCustomNote(Anno newAnno, string path)
        {
            bool bookmarked = false;
            try
            {
                CustomAnnos customNotes = new CustomAnnos();

                customNotes = await GetCustomNotesFromXML(newAnno.PageNumber, path);
                List<Anno> pageAnnos = new List<Anno>();
                //if (htPageNotes.ContainsKey(pageNum))
                //{
                //    pageAnnos = (List<Anno>)htPageNotes[pageNum];
                //}

                //pageAnnos.Add(newNote);

                customNotes.Annos.Add(newAnno);


                string userInfoPath = KlasimeBase.Common.GetLocalPath() + BookPath + Sep + path + newAnno.PageNumber.ToString() + ".xml";

                XmlSerializer ser = new XmlSerializer(typeof(CustomAnnos));

                using (FileStream fs = new FileStream(userInfoPath, FileMode.Create))
                {
                    ser.Serialize(fs, customNotes);
                }

                await getBookMedia();
                bookmarked = true;
            }
            catch (Exception ee)
            {
                bookmarked = false;
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return bookmarked;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNum"></param>
        /// <param name="path">userNotes_</param>
        /// <returns></returns>
        public async Task<CustomAnnos> GetCustomNotesFromXML(int pageNum, string path)
        {
            CustomAnnos customNotes = new CustomAnnos();

            List<Anno> pageAnno = new List<Anno>();
            try
            {
                string userInfoPath = KlasimeBase.Common.GetLocalPath() + BookPath + Sep + path + pageNum.ToString() + ".xml";

                if (File.Exists(userInfoPath))
                {
                    XmlSerializer ser = new XmlSerializer(typeof(CustomAnnos));

                    using (FileStream fs = new FileStream(userInfoPath, FileMode.Open))
                    {
                        customNotes = (CustomAnnos)ser.Deserialize(fs);

                        foreach (Anno anno in customNotes.Annos)
                        {
                            //myPageAnno.Annos.Add(anno);
                        }
                    }
                }
            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return customNotes;
        }

        public async Task<bool> RemoveNote(Anno newAnno, string path)
        {
            bool bookmarked = false;
            try
            {
                //find page number

                foreach (DictionaryEntry de in htPageAnnos)
                {
                    PageAnno pageAnnoitem = (PageAnno)de.Value;
                    if (pageAnnoitem.Annos.Contains(newAnno))
                    {

                        if (pageAnnoitem.htDrawAnno.ContainsKey(newAnno.AnnoId))
                        {
                            pageAnnoitem.htDrawAnno.Remove(newAnno.AnnoId);
                        }
                        //pageAnno.Annos
                        List<Anno> annoForDelte = pageAnnoitem.Annos.FindAll(x => x.AnnoId == newAnno.AnnoId);
                        foreach (Anno item in annoForDelte)
                        {
                            pageAnnoitem.Annos.Remove(item);
                        }




                    }
                }


                //remove from XML first
                CustomAnnos customNotes = await GetCustomNotesFromXML(newAnno.PageNumber, path);
                var res = customNotes.Annos.Find(x => x.AnnoId == newAnno.AnnoId);
                customNotes.Annos.Remove(res);

                //store new xml

                string userInfoPath = KlasimeBase.Common.GetLocalPath() + BookPath + Sep + path + newAnno.PageNumber.ToString() + ".xml";

                XmlSerializer ser = new XmlSerializer(typeof(CustomAnnos));

                using (FileStream fs = new FileStream(userInfoPath, FileMode.Create))
                {
                    ser.Serialize(fs, customNotes);
                }

                //reload
                await getBookMedia();
                bookmarked = true;
            }
            catch (Exception ee)
            {
                bookmarked = false;
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return bookmarked;
        }

        #endregion

        #region Multimedia
        public List<MultimediaListItem> GetAllMultimediaFromXML(bool link, bool galery, bool info, bool video, bool youtube, bool audio, bool quiz)
        {
            List<MultimediaListItem> list = new List<MultimediaListItem>();
            try
            {
                // Common.GetTitle(hoverAnno.Hint.Text)
                int pageID = 0;
                for (int i = 1; i < htPageAnnos.Count; i++)
                {
                    PageAnno pageAnno = (PageAnno)htPageAnnos[i];
                    pageID = i;

                    if (pageAnno != null)
                    {
                        MultimediaType type = MultimediaType.Unknown;
                        foreach (Anno anno in pageAnno.Annos)
                        {

                            string hintTitle = Common.GetTitle(anno.Hint.Text);
                            switch (anno.Action.ActionType)
                            {
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenURL":
                                    if(link==true) type = MultimediaType.Link;
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenWindow":
                                    if (video == true) type = MultimediaType.Video;
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionShowInformation":
                                    if (info == true) type = MultimediaType.Note;
                                    break;
                                case "com.mobiano.flipbook.Action.TAnnoActionPlayAudio":
                                    {
                                        if (audio == true) type = MultimediaType.Audio;

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPhotoSlide":
                                    {
                                        if (galery  == true) type = MultimediaType.PhotoGalery;

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayVideo":
                                    {
                                        if (anno.Action.ResourceContent == null)
                                        {
                                            if (anno.Action.WindowType == "TYPE_YOUTUBE")
                                            {
                                                if (youtube == true) type = MultimediaType.YouTube;

                                            }
                                        }
                                        else
                                        {
                                            if (video == true) type = MultimediaType.Video;

                                        }

                                        break;
                                    }
                                    //quiz!!!

                            }
                            if (type != MultimediaType.Unknown)
                            {
                                MultimediaListItem item = new MultimediaListItem(type, hintTitle, pageID, anno);
                                list.Add(item);
                            }
                        }
                    }
                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return list;
        }
        public List<MultimediaListItem> GetAllNotesFromXML()
        {
            List<MultimediaListItem> list = new List<MultimediaListItem>();
            try
            {
                // Common.GetTitle(hoverAnno.Hint.Text)
                int pageID = 0;
                for (int i = 1; i < htPageAnnos.Count; i++)
                {
                    PageAnno pageAnno = (PageAnno)htPageAnnos[i];
                    pageID = i;

                    if (pageAnno != null)
                    {
                        MultimediaType type = MultimediaType.Unknown;
                        foreach (Anno anno in pageAnno.Annos)
                        {

                            string hintTitle = Common.GetTitle(anno.Hint.Text);
                            switch (anno.Action.ActionType)
                            {
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenURL":
                                    //type = MultimediaType.Link;
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionOpenWindow":
                                    //type = MultimediaType.Video;
                                    break;
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionShowInformation":
                                    if(anno.Cutom)
                                        type = MultimediaType.Note;
                                    break;
                                case "com.mobiano.flipbook.Action.TAnnoActionPlayAudio":
                                    {
                                       //type = MultimediaType.Audio;

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPhotoSlide":
                                    {
                                         //type = MultimediaType.PhotoGalery;

                                        break;
                                    }
                                case "com.mobiano.flipbook.pageeditor.TAnnoActionPlayVideo":
                                    {
                                        if (anno.Action.ResourceContent == null)
                                        {
                                            if (anno.Action.WindowType == "TYPE_YOUTUBE")
                                            {
                                           //     type = MultimediaType.YouTube;

                                            }
                                        }
                                        else
                                        {
                                            //type = MultimediaType.Video;

                                        }

                                        break;
                                    }
                                    //quiz!!!

                            }
                            if (type != MultimediaType.Unknown)
                            {
                                MultimediaListItem item = new MultimediaListItem(type, hintTitle, pageID, anno);
                                list.Add(item);
                            }
                        }
                    }
                }

            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
            return list;
        }
        #endregion
    }
    public class CustomAnnos
    {
        [XmlElement(Order = 1, ElementName = "Anno")]
        public List<Anno> Annos = new List<Anno>();


    }
}

