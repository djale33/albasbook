﻿namespace KlasimeBase
{
    public class SubTexture
    {
        public string Name { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public SubTexture(string name, int x, int y, int w, int h)
        {
            Name = name;
            X = x;
            Y = y;
            Width = w;
            Height = h;
        }
    }
}

