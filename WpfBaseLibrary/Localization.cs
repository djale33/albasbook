﻿using KlasimeBase.Log;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;


namespace KlasimeBase
{
    public static class Localization
    {
        public static string user="program";
        public static Hashtable htLocEN = new Hashtable();
        public static Hashtable htLocMK = new Hashtable();
        public static Hashtable htLocAL = new Hashtable();
        public enum Languages
        {
            English,
            Albenian,
            Macedonian
        }
        public static string GetLocalization(Type type, Languages language, string item)
        {
            
            try
            {
                
                string resourceID = "XBook2.loc.en.xml";
                switch (language)
                {
                    case Languages.English:
                        resourceID = "XBook2.loc.en.xml";
                        
                        break;
                    case Languages.Albenian:
                        resourceID = "XBook2.loc.al.xml";
                        
                        break;
                    case Languages.Macedonian:
                        resourceID = "XBook2.loc.mk.xml";

                        break;
                }

                return PrepareLocalization(type, resourceID, item);



            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }


            return "";
        }
        private static string PrepareLocalization(Type type, string resourceID,string item)
        {
            
            XmlDocument doc = new XmlDocument();
            string content;
            Assembly assembly = type.GetTypeInfo().Assembly;

            var allResources = assembly.GetManifestResourceNames();
            using (Stream sr = assembly.GetManifestResourceStream(resourceID))
            {
                using (StreamReader sr2 = new StreamReader(sr))
                {
                    content = sr2.ReadToEnd();

                    doc.LoadXml(content);
                }

            }
            if (resourceID.Contains(".en."))
            {
                if (htLocEN.Count > 0)
                {
                    if (htLocEN.ContainsKey(item))
                    {
                        return htLocEN[item].ToString();
                    }
                }
                else
                {
                    foreach (XmlNode nod in doc.SelectNodes("localization/word"))
                    {
                        if (htLocEN.ContainsKey(nod.Attributes["id"]) == false)
                        {
                            htLocEN.Add(nod.Attributes["id"].InnerText, nod.Attributes["value"].InnerText);
                        }
                    }

                    if (htLocEN.Count > 0)
                    {
                        if (htLocEN.ContainsKey(item))
                        {
                            return htLocEN[item].ToString();
                        }
                    }
                }
            }
            if (resourceID.Contains(".mk."))
            {
                if (htLocMK.Count > 0)
                {
                    if (htLocMK.ContainsKey(item))
                    {
                        return htLocMK[item].ToString();
                    }
                }
                else
                {
                    foreach (XmlNode nod in doc.SelectNodes("localization/word"))
                    {
                        if (htLocMK.ContainsKey(nod.Attributes["id"]) == false)
                        {
                            htLocMK.Add(nod.Attributes["id"].InnerText, nod.Attributes["value"].InnerText);
                        }
                    }

                    if (htLocMK.Count > 0)
                    {
                        if (htLocMK.ContainsKey(item))
                        {
                            return htLocMK[item].ToString();
                        }
                    }
                }
            }

            if (resourceID.Contains(".al."))
            {
                if (htLocAL.Count > 0)
                {
                    if (htLocAL.ContainsKey(item))
                    {
                        return htLocAL[item].ToString();
                    }
                }
                else
                {
                    foreach (XmlNode nod in doc.SelectNodes("localization/word"))
                    {
                        if (htLocAL.ContainsKey(nod.Attributes["id"]) == false)
                        {
                            htLocAL.Add(nod.Attributes["id"].InnerText, nod.Attributes["value"].InnerText);
                        }
                    }

                    if (htLocAL.Count > 0)
                    {
                        if (htLocAL.ContainsKey(item))
                        {
                            return htLocAL[item].ToString();
                        }
                    }
                }
            }
            return "";
        }
    }
}
