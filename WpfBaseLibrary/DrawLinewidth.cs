﻿//using Windows.ApplicationModel.Core;
//using Windows.Graphics.Display;
//using Windows.UI.ViewManagement;

namespace KlasimeBase
{
    public enum DrawLineWidth
    {
        Thin = 3,
        Medium = 6,
        Thick = 10
    }
    public enum DrawLineColor
    {
        LightPink,
        LightBlue,
        LightGreen
    }
}