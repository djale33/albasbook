﻿namespace KlasimeBase
{
    public class BookPageFiles
    {
        public string PageImage;
        public string PageSearchXML;
        public BookPageFiles(string pageImage,string pageSearchXML)
        {
            PageImage = pageImage;
            PageSearchXML = pageSearchXML;
        }
    }
}
