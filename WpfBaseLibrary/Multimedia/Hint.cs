﻿namespace KlasimeBase
{
    public class Hint
    {
        private int hintShapeColor;
        private int hintShapeColor2;
        private int hintShapeAlpha;
        private int hintW;
        private int hintH;
        private bool hintAuto;
        private int hintShapeType;
        private string text;

        public int HintShapeColor { get => hintShapeColor; set => hintShapeColor = value; }
        public int HintShapeColor2 { get => hintShapeColor2; set => hintShapeColor2 = value; }
        public int HintShapeAlpha { get => hintShapeAlpha; set => hintShapeAlpha = value; }
        public int HintW { get => hintW; set => hintW = value; }
        public int HintH { get => hintH; set => hintH = value; }
        public bool HintAuto { get => hintAuto; set => hintAuto = value; }
        public int HintShapeType { get => hintShapeType; set => hintShapeType = value; }
        public string Text { get => text; set => text = value; }
    }
}
