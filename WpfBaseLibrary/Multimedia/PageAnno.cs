﻿using System.Collections;
using System.Collections.Generic;

namespace KlasimeBase
{
    public class PageAnno
    {
        private int pageIndex;
        private PageInformation pageInformation;
        private List<Anno> annos = new List<Anno>();
        public Hashtable htDrawAnno = new Hashtable();

        public List<Anno> Annos { get => annos; set => annos = value; }
        public int PageIndex { get => pageIndex; set => pageIndex = value; }

        
        public PageInformation PageInformation { get => pageInformation; set => pageInformation = value; }
        
        //location from page
        public Anno FindAnnoByLocation(double X, double Y,double fscale)
        {
            
            foreach (DictionaryEntry tmpDE in htDrawAnno)
            {
                DrawAnno dw = (DrawAnno)tmpDE.Value;
            
                if (X/ fscale >= dw.rectX && X/ fscale <= dw.rectX + dw.irectWidth)
                {
                    if (Y/ fscale >= dw.irectY && Y/ fscale <= dw.irectY+dw.irectHeight)
                    {
                        string findAnno = tmpDE.Key.ToString();
                        Anno res= Annos.Find(x => x.AnnoId == findAnno);
                        return res;
                    }
                }
            }

            return null; 
        }

        //location from ruler
        public Anno FindAnnoByYLocation(double Y, double fscale)
        {
            foreach (DictionaryEntry tmpDE in htDrawAnno)
            {
                DrawAnno dw = (DrawAnno)tmpDE.Value;

                if (Y / fscale >= dw.irectY && Y / fscale <= dw.irectY + dw.irectHeight)
                {
                    string findAnno = tmpDE.Key.ToString();
                    Anno res = Annos.Find(x => x.AnnoId == findAnno);
                    return res;
                }
                
            }
            return null;
        }
    }
}
