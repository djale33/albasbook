﻿namespace KlasimeBase
{
    public class Location
    {
        private string tannoName ;
        private double x ;
        private double y;
        private double width;
        private double height;
        private double rotation;
        private bool reflection;
        private int reflectionType;
        private int reflectionAlpha;
        private double pageWidth;
        private double pageHeight;

        public string TannoName { get => tannoName; set => tannoName = value; }
        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }
        public double Width { get => width; set => width = value; }
        public double Height { get => height; set => height = value; }
        public double Rotation { get => rotation; set => rotation = value; }
        public bool Reflection { get => reflection; set => reflection = value; }
        public int ReflectionType { get => reflectionType; set => reflectionType = value; }
        public int ReflectionAlpha { get => reflectionAlpha; set => reflectionAlpha = value; }
        public double PageWidth { get => pageWidth; set => pageWidth = value; }
        public double PageHeight { get => pageHeight; set => pageHeight = value; }
    }
}
