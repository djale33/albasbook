﻿using System.Collections.Generic;

namespace KlasimeBase
{
    public class Action
    {
        private string triggerEventType;
        private string actionType;
        private bool autoPlay;
        private bool autoPlayAgain;
        private string soundStopTriggerEvt;
        private string cpName;
        private string className;
        private string audioURL;
        private string url;
        private string linkTarget;
        private List<Photo> photos = new List<Photo>();
        private int sliderType;
        private string caption;
        private string resourceContent;
        private bool reloadEveryTime;
        private bool drag;
        private bool hideTitle;
        private string titleText;
        private string titleSize;
        private string titleFont;
        private string bodyText;
        private string bodyAlign;
        private string bodySize;
        private string bodyFont;
        private string windowType;



        public string TriggerEventType { get => triggerEventType; set => triggerEventType = value; }
        public string ActionType { get => actionType; set => actionType = value; }
        public bool AutoPlay { get => autoPlay; set => autoPlay = value; }
        public bool AutoPlayAgain { get => autoPlayAgain; set => autoPlayAgain = value; }
        public string CpName { get => cpName; set => cpName = value; }
        public string ClassName { get => className; set => className = value; }
        public string AudioURL { get => audioURL; set => audioURL = value; }
        public string SoundStopTriggerEvt { get => soundStopTriggerEvt; set => soundStopTriggerEvt = value; }
        public string Url { get => url; set => url = value; }
        public List<Photo> Photos { get => photos; set => photos = value; }
        public string LinkTarget { get => linkTarget; set => linkTarget = value; }
        public int SliderType { get => sliderType; set => sliderType = value; }
        public string Caption { get => caption; set => caption = value; }
        public string ResourceContent { get => resourceContent; set => resourceContent = value; }
        public bool ReloadEveryTime { get => reloadEveryTime; set => reloadEveryTime = value; }
        public bool Drag { get => drag; set => drag = value; }
        public bool HideTitle { get => hideTitle; set => hideTitle = value; }
        public string TitleText { get => titleText; set => titleText = value; }
        public string TitleSize { get => titleSize; set => titleSize = value; }
        public string TitleFont { get => titleFont; set => titleFont = value; }
        public string BodyText { get => bodyText; set => bodyText = value; }
        public string BodyAlign { get => bodyAlign; set => bodyAlign = value; }
        public string BodySize { get => bodySize; set => bodySize = value; }
        public string BodyFont { get => bodyFont; set => bodyFont = value; }
        public string WindowType { get => windowType; set => windowType = value; }
    }
}
