﻿using KlasimeViews;

namespace KlasimeBase
{
    public class Anno
    {
        private string annotype;
        private string annoId ;
        private int alpha ;
        private Location location;
        private Hint hint;
        private Shadow shadow;
        private Action action;
        private string hotSpotsURL;
        private bool cutom = false;
        private int pageNumber;
        private MultimediaType multimediaTypeToAdd;

        public string HotSpotsURL { get => hotSpotsURL; set => hotSpotsURL = value; }
        public Shadow Shadow { get => shadow; set => shadow = value; }
        public Action Action { get => action; set => action = value; }
        public string Annotype { get => annotype; set => annotype = value; }
        public string AnnoId { get => annoId; set => annoId = value; }
        public int Alpha { get => alpha; set => alpha = value; }
        public Location Location { get => location; set => location = value; }
        public Hint Hint { get => hint; set => hint = value; }
        public bool Cutom { get => cutom; set => cutom = value; }
        
        public int PageNumber { get => pageNumber; set => pageNumber = value; }
        public MultimediaType MultimediaTypeToAdd { get => multimediaTypeToAdd; set => multimediaTypeToAdd = value; }
    }
}
