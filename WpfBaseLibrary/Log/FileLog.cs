﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Reflection;
using KlasimeBase.Interfaces;

namespace KlasimeBase.Log
{
    public class FileLog : ILogger
    {
        static string logFileName = "Klasime.log";
        
        public  void Log(MethodBase mb,Exception ee,string text="")
        {
            try
            {
                string ex_msg = "[" +  DateTime.Now.ToString() + " - " + PrepareMethodParams(mb) + "] " + PrareMessageFromException(ee) + Environment.NewLine + text ;
                
                Log(ex_msg);
                
            }
            catch (Exception )
            {
                
                throw;
            }


        }
        public  void Log(MethodBase mb, string msg)
        {
            try
            {
                string ex_msg = "[" + DateTime.Now.ToString() + " - " + mb.Name + "] " + msg;

                Log(ex_msg);

            }
            catch (Exception )
            {

                throw;
            }


        }
       
        private  string PrareMessageFromException(Exception ee)
        {
            string msg = ee.Message + Environment.NewLine;
            string stacktrace = ee.StackTrace + Environment.NewLine;




            stacktrace = ee.StackTrace + Environment.NewLine;

            if (ee.InnerException != null)
            {
                Exception ie = ee.InnerException;
                msg = msg + "InnerException:" + Environment.NewLine;
                stacktrace = stacktrace + "InnerException:" + Environment.NewLine;

                msg = msg + ie.Message + Environment.NewLine;
                stacktrace = stacktrace + ie.StackTrace + Environment.NewLine;

                if (ie.InnerException != null)
                {
                    Exception iie = ie.InnerException;
                    msg = msg + "InnerException:" + Environment.NewLine;
                    stacktrace = stacktrace + "InnerException:" + Environment.NewLine;

                    msg = msg + iie.Message + Environment.NewLine;
                    stacktrace = stacktrace + iie.StackTrace + Environment.NewLine;
                }
            }
            return msg +  stacktrace;
        }
        private  string PrepareMethodParams(MethodBase mb)
        {
            string res = mb.Name + " /";
            foreach (ParameterInfo item in mb.GetParameters())
            {
                res += item.Name + "/";
            }

            return res;
        }
         void Log(string message)
        {
            try
            {
                string path = Common.GetLocalPath();
                string fullPath = path + @"\" + logFileName;
                if (Directory.Exists(path) == true)
                {
                    // This text is added only once to the file.
                    if (!File.Exists(fullPath))
                    {
                        // Create a file to write to.
                        using (StreamWriter sw = File.CreateText(fullPath))
                        {
                            sw.WriteLine(message + Environment.NewLine);

                        }
                    }
                    else
                    {
                        using (StreamWriter sw = File.AppendText(fullPath))
                        {
                            sw.WriteLine(message + Environment.NewLine);

                        }
                    }
                }
            }
            catch (Exception )
            {

                throw;
            }


        }

        void ILogger.Log(string message)
        {
            try
            {
                string path = Common.GetLocalPath();
                string fullPath = path + @"\" + logFileName;
                if (Directory.Exists(path) == true)
                {
                    // This text is added only once to the file.
                    if (!File.Exists(fullPath))
                    {
                        // Create a file to write to.
                        using (StreamWriter sw = File.CreateText(fullPath))
                        {
                            sw.WriteLine(message + Environment.NewLine);

                        }
                    }
                    else
                    {
                        using (StreamWriter sw = File.AppendText(fullPath))
                        {
                            sw.WriteLine(message + Environment.NewLine);

                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
