﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace KlasimeBase.Interfaces
{
    public interface ILogger
    {
        void Log(string message);
        void Log(MethodBase mb, string msg);
        void Log(MethodBase mb, Exception ee, string text = "");
    }
}
