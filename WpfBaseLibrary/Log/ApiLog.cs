﻿using KlasimeBase.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace KlasimeBase.Log
{
    public enum Severity
    {
        Trace,
        Low,
        High,
        Critical
    }
    public static class ApiLog 
    {
        //static string logFileName = "Klasime.log";
       
        private static string PrapareMessageFromException(Exception ee)
        {
            string msg = ee.Message + Environment.NewLine;
            string stacktrace = ee.StackTrace + Environment.NewLine;




            stacktrace = ee.StackTrace + Environment.NewLine;

            if (ee.InnerException != null)
            {
                Exception ie = ee.InnerException;
                msg = msg + "InnerException:" + Environment.NewLine;
                stacktrace = stacktrace + "InnerException:" + Environment.NewLine;

                msg = msg + ie.Message + Environment.NewLine;
                stacktrace = stacktrace + ie.StackTrace + Environment.NewLine;

                if (ie.InnerException != null)
                {
                    Exception iie = ie.InnerException;
                    msg = msg + "InnerException:" + Environment.NewLine;
                    stacktrace = stacktrace + "InnerException:" + Environment.NewLine;

                    msg = msg + iie.Message + Environment.NewLine;
                    stacktrace = stacktrace + iie.StackTrace + Environment.NewLine;
                }
            }
            return msg + stacktrace;
        }
        private static string PrepareMethodParams(MethodBase mb)
        {
            string res = mb.Name + " /";
            if (mb != null)
            {
                foreach (ParameterInfo item in mb.GetParameters())
                {
                    res += item.Name + "/";
                }
            }

            return res;
        }
        //static
        public static void Log(string user, Severity sev,MethodBase mb, Exception ee, string text = "")
        {
            try
            {
                System.Net.Http.HttpClient httpClient = new System.Net.Http.HttpClient();

                LogClient lc = new LogClient(httpClient);
                LogItem li = new LogItem();
                li.User = user;
                li.Severity = (int)sev;
                li.Date = DateTime.Now;
                if (mb != null)
                {
                    
                    li.Method = mb.Name; //PrepareMethodParams(mb);
                    if(mb.DeclaringType != null)
                    {
                        li.Method = mb.DeclaringType.FullName;
                    }
                }
                li.Message = text;
                if (ee != null)
                {
                    li.Message += PrapareMessageFromException(ee);
                }
                lc.AddLogAsync(li);

                //LogSend(ex_msg);

            }
            finally
            {
            }


        }
       
      

    }
}
