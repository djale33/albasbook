﻿using KlasimeBase;
using KlasimeBase.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KlasimeViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MultimediaNavItem : ContentView
    {
        public static string user = "program";
        Anno myAnno;
        MultimediaType myType;
        Localization.Languages UserLang { get; set; }
        #region Custom Event

        public delegate void BookClickHandler(object sender, MultimediaEventArgs e);
        public event BookClickHandler OnMultimediaNavClick;



        private void OnBookClickEvent(object sender, MultimediaEventArgs e)
        {
            if (OnMultimediaNavClick != null)
            {
                OnMultimediaNavClick(sender, e);
            }
        }
        #endregion
        public MultimediaNavItem(Localization.Languages userLang)
        {
            InitializeComponent();
            UserLang = userLang;
        }
        public void SetTitle(string title)
        {
            try
            {
                lblTitle.Text = title;
            }
            catch (Exception ee)
            {

                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }


        }
        public void SetPageID(string pageID)
        {

            try
            {
                lblPageID.Text = pageID;

            }
            catch (Exception ee)
            {

                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
        public void SetType(MultimediaType type)
        {
            try
            {
                
                 // "Audio";
                myType = type;
                switch (type)
                {
                    case MultimediaType.Audio: lblType.Text = Localization.GetLocalization(GetType(), UserLang, "button_attach_audio"); break;
                    case MultimediaType.YouTube: lblType.Text = Localization.GetLocalization(GetType(), UserLang, "button_attach_youtube"); break;
                    case MultimediaType.Link: lblType.Text = Localization.GetLocalization(GetType(), UserLang, "button_attach_link"); break;
                    case MultimediaType.Note: lblType.Text = Localization.GetLocalization(GetType(), UserLang, "button_material_note"); break;
                    case MultimediaType.PhotoGalery: lblType.Text = Localization.GetLocalization(GetType(), UserLang, "button_attach_photo"); break;
                    case MultimediaType.Video: lblType.Text = Localization.GetLocalization(GetType(), UserLang, "button_attach_video"); break;
                    default: lblType.Text = "Unknown"; break;
                }
                
            }
            catch (Exception ee)
            {

                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }



        }
        public void SetAnno(Anno anno)
        {
            try
            {
                myAnno = anno;
            }
            catch (Exception ee)
            {

                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }



        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                MultimediaEventArgs b = new MultimediaEventArgs(myType, 400,300,myAnno,"");

                OnMultimediaNavClick(this, b);
            }
            catch (Exception ee)
            {
                ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
    }
}