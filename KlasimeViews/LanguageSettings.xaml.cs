﻿using KlasimeBase;
using KlasimeBase.Log;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KlasimeViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LanguageSettings : ContentView
    {
        
        #region Custom Event

        public delegate void LanguageSettingsHandler(object sender, LanguageSettingsEventArgs e);
        public event LanguageSettingsHandler OnLanguageChange;
        public int PageID { get; set; }
        Color selectedColor = Color.FromRgb(19, 107, 127);
        Color unselectedColor = Color.FromRgb(74, 74, 74);
        Localization.Languages selectedLanguage;
        private void OnLanguageSettingsEvent(object sender, LanguageSettingsEventArgs e)
        {
            if (OnLanguageChange != null)
            {
                OnLanguageChange(sender, e);
            }
        }
        #endregion

        CUser currentUser;
        public LanguageSettings()
        {
            
            InitializeComponent();
        }
        public LanguageSettings(CUser user)
        {
            currentUser = user;
            InitializeComponent();
            btnEng.BackgroundColor = selectedColor;
            btnAlb.BackgroundColor = selectedColor;
        }

        private void ContentView_Focused(object sender, FocusEventArgs e)
        {
          
        }
        public bool SetTitle(Localization.Languages lang)
        {
            selectedLanguage = lang;
            return true;
        }
        private void btnAccept_Clicked(object sender, EventArgs e)
        {
            try
            {
                LanguageSettingsEventArgs b = new LanguageSettingsEventArgs(selectedLanguage);

                OnLanguageChange(this, b);
            }
            catch (Exception ee)
            {
                ApiLog.Log(currentUser.username, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void btnAlb_Clicked(object sender, EventArgs e)
        {
            selectedLanguage = Localization.Languages.Albenian;
            btnEng.BackgroundColor = unselectedColor;
            btnAlb.BackgroundColor = selectedColor;
        }

        private void btnEng_Clicked(object sender, EventArgs e)
        {
            selectedLanguage = Localization.Languages.English;
            btnEng.BackgroundColor = selectedColor;
            btnAlb.BackgroundColor = unselectedColor;
        }
    }
}