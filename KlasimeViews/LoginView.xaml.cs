﻿using KlasimeBase.Log;
using Newtonsoft.Json;
using SharedKlasime;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XBook2;

namespace KlasimeViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : ContentView
    {
        public static string user = "program";
        HttpClient client;
        string urlPart = @"https://ti.portalishkollor.al";
        public LoginView()
        {
            InitializeComponent();
            client = new HttpClient();
        }
        private async void Button_Clicked(object sender, EventArgs e)
        {
            string test = LoginName.Text;
            try
            {
                var jsonString = await client.GetStringAsync(urlPart + "/api/authenticate?username=" + LoginName.Text + "&password=" + LoginPassword.Text);


                CUser user = JsonConvert.DeserializeObject<CUser>(jsonString);
                string loginToken = user.token;
                //WeatherForecast weatherForecast = JsonSerializer.Deserialize<WeatherForecast>(js);
                Task<CUser> task = requestUserProfile(loginToken);
                task.Wait();
                user = task.Result;
                user.token = loginToken;
                LoginStatus.TextColor = Color.Black;
                LoginStatus.Text = "Welcome " + user.firstname + " " + user.lastname;


                //Task<List<AlbasBook>> task1 = requestAvailableBooks(obj.token);
                //task1.Wait();
                //List<AlbasBook> userBooks = task1.Result;

                //Task<FileInfoList> task2 = downloadBook(obj.token, 45);
                //task2.Wait();
                //FileInfoList res = task2.Result;

                //await Navigation.PushAsync(new BookPage());
                //if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.Other)
                //{

                //todo: Navigation to Book Page, this page view havo to be in same library other included as reference !
                //Application.Current.MainPage = new NavigationPage(new BookPage(user));
                //}
                //else
                //{
                //    Application.Current.MainPage = new NavigationPage(new AppShell(user));
                //}

            }
            catch (Exception ee)
            {

               ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                LoginStatus.TextColor = Color.Red;
                LoginStatus.Text = ee.Message;
            }

        }
        private async Task<CUser> requestUserProfile(string token)
        {
            try
            {
                Task<string> task = client.GetStringAsync(urlPart + "/api/profile?token=" + token);
                task.Wait();
                string jsonString = task.Result.ToString();
                CUser obj = JsonConvert.DeserializeObject<CUser>(jsonString);
                return obj;
            }
            catch (Exception ee)
            {
               ApiLog.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                return new CUser();
            }

        }
      
       
    }
}