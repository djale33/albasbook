﻿using KlasimeBase;
using KlasimeBase.Log;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KlasimeViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchItem : ContentView
    {
        public static string user = "program";
        private int pageID;
        #region Custom Event

        public delegate void BookClickHandler(object sender, SearchEventArgs e);
        public event BookClickHandler OnSearchItemNavClick;
        public string StrLocation { get; set; }


        private void OnBookClickEvent(object sender, SearchEventArgs e)
        {
            if (OnSearchItemNavClick != null)
            {
                OnSearchItemNavClick(sender, e);
            }
        }
        #endregion
        public SearchItem()
        {
            InitializeComponent();
        }
        public void SetTitle(string title)
        {
            try
            {
                lblTitle.Text = title;
            }
            catch (Exception ee)
            {

                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }


        }
        public void SetLocation(string strLocation)
        {
            try
            {
                StrLocation = strLocation;
            }
            catch (Exception ee)
            {

                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }


        }
        public void SetPageID(string _pageID)
        {

            try
            {
                lblPageID.Text = _pageID;
                int.TryParse(_pageID, out pageID);

            }
            catch (Exception ee)
            {

                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }

        }
       
      
        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            try
            {
                SearchEventArgs b = new SearchEventArgs(pageID, "",StrLocation);

                OnSearchItemNavClick(this, b);
            }
            catch (Exception ee)
            {
                ApiLog.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }
    }
}