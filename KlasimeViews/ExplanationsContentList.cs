﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace KlasimeViews
{
    public partial class BookGalery
    {
      
        #region HELPER CLASS

        public class ExplanationsContentList
        {
            [JsonProperty("1.0.0")]
            public List<ExplanationsContent> _ExplanationsContent { get; set; }
        }

        #endregion


    }
}