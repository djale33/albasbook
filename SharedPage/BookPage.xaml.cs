﻿using SkiaSharp.Views.Forms;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using UWPBook;

namespace SharedPage
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BookPage : ContentView
    {
        SKBitmap monkeyBitmap;
        public string TitleText { get; set; }
        public static readonly BindableProperty TitleTextProperty =
            BindableProperty.Create(nameof(TitleText),
                typeof(string),
                typeof(BookPage),
                defaultValue: string.Empty,
                defaultBindingMode: BindingMode.OneWay
                );


        public BookPage()
        {
            InitializeComponent();
           
        }
        private void Canvas_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {

            monkeyBitmap = BitmapExtensions.LoadBitmapResource(GetType(), "UWPBook.pages.MonkeyFace.png");
            var scale = 1;
            using (SKCanvas canvas = new SKCanvas(monkeyBitmap))
            {
                //using (SKPaint paint = new SKPaint())
                //{
                //    paint.Style = SKPaintStyle.Stroke;
                //    paint.Color = SKColors.Black;
                //    paint.StrokeWidth = 24;
                //    paint.StrokeCap = SKStrokeCap.Round;

                //    using (SKPath path = new SKPath())
                //    {
                //        path.MoveTo(380, 390);
                //        path.CubicTo(560, 390, 560, 280, 500, 280);

                //        path.MoveTo(320, 390);
                //        path.CubicTo(140, 390, 140, 280, 200, 280);

                //        canvas.DrawPath(path, paint);
                //    }
                //}
                
            }
            // Create SKCanvasView to view result
            SKCanvasView canvasView = new SKCanvasView();
            canvasView.PaintSurface += OnCanvasViewPaintSurface;
            Content = canvasView;
        }
        void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;
            
            
            canvas.Clear();
           //canvas.Scale(2);
            canvas.DrawBitmap(monkeyBitmap, info.Rect, BitmapStretch.Uniform);
        }
    
        //async void OnButtonClick(object sender, EventArgs ags)
        //{
        //    await label.RelRotateTo(360, 1000);
        //}
        private void OnButtonClick(object sender, EventArgs ags)
        {
            try
            {
                SKImageInfo imageInfo = new SKImageInfo(300, 300);
                using (SKSurface surface = SKSurface.Create(imageInfo))
                {
                    SKCanvas canvas = surface.Canvas;
                    using (SKPaint paint = new SKPaint())
                    {
                        paint.Color = SKColors.Blue;
                        paint.StrokeWidth = 15;
                        paint.Style = SKPaintStyle.Stroke;
                        canvas.DrawCircle(50, 150, 30, paint);
                    }
                    
                }
            }
            catch (Exception)
            {

                throw;
            }
           
        }
    }
}