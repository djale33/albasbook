﻿using SkiaSharp;
using System.Collections.Generic;

namespace KlasimeBase
{
    public class MyLineList2
    {
        public SKPoint PointStart { get; set; }
        public SKPoint PointEnd { get; set; }
        public MyLineList2(SKPoint pointStart, SKPoint pointEnd)
        {
            PointStart = pointStart;
            PointEnd = pointEnd;
        }
    }
    public class MyLineList
    {
        public SKPoint PointStart { get; set; }
        public SKPoint PointEnd { get; set; }
        public DrawLineWidth customDrawLineWidth { get; set; }
        public DrawLineColor customDrawLineColor { get; set; }

        public SKPaint PaintLine { get; set; }

        List<SKPoint> listPoints = new List<SKPoint>();
        
        public List<SKPoint> ListPoints { get => listPoints; set => listPoints = value; }

        public MyLineList()
        {
         
        }        
        public MyLineList(SKPoint pointStart, SKPoint pointEnd)
        {
            PointStart = pointStart;
            PointEnd = pointEnd;

        }
        public MyLineList(SKPoint pointStart, SKPoint pointEnd, SKPaint paintLine)
        {
            PointStart = pointStart;
            PointEnd = pointEnd;
            PaintLine = paintLine;  
        }
        public MyLineList(SKPoint pointStart, SKPaint paintLine)
        {
            PointStart = pointStart;            
            PaintLine = paintLine;
            listPoints = new List<SKPoint>();
        }

        public MyLineList(SKPoint pointStart, SKPoint pointEnd, DrawLineWidth customDrawLineWidth, DrawLineColor customDrawLineColor)
        {
            PointStart = pointStart;
            PointEnd = pointEnd;
            this.customDrawLineWidth = customDrawLineWidth;
            this.customDrawLineColor = customDrawLineColor;
        }
       
    }

}