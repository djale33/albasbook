﻿using System.Collections.Generic;

namespace XBook2
{
    public partial  class Constants
    {
        public List<string> LeftMenuItems = new List<string>
        {
            "Skarko",
            "Kursori",
            "Libra",
            "Nenvizuises",
            "Fshiresi",
            "Lepsi",
            "Shenim",
            "Baskanfjitj",
            "Mjete",
            "Ceshtjet",
            "Provime"
        };
        public List<string> BottmMenuItems = new List<string>
        {
            "Previous",
            "Me pare",
            "Nadares",
            "Mbullini",
            "Albas",
            "Raft librash",
            "Konfiguriment",
            "Zvogeloni",
            "Next",            
        };
    }
}

