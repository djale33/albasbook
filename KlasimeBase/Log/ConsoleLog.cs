﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using KlasimeBase.Interfaces;

namespace KlasimeBase.Classes
{
    public class ConsoleLog : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine("["+ DateTime.Now.ToString() + "]: " + message);
        }

        public void Log(MethodBase mb, string msg)
        {
            throw new NotImplementedException();
        }

        public void Log(MethodBase mb, Exception ee, string text = "")
        {
            throw new NotImplementedException();
        }
    }
}
