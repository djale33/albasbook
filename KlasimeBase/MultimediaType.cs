﻿namespace KlasimeViews
{
    public enum MultimediaType
    {
        Unknown,
        PhotoGalery,
        Audio,
        Video,
        Note,
        YouTube,
        Link,
        File
    }
}