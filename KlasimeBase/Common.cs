﻿using System;

using System.IO;
using Xamarin.Forms;
using System.Net;
using System.Xml;
using System.Collections;
using KlasimeBase.Log;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms.PlatformConfiguration;

namespace KlasimeBase
{
    public static class Common
    {
        public static ApiLog logInstance =  ApiLog.GetInstance();
        public static string user = "Common";
        private static string ENCRYPTION_KEY = "klasaime_key";

        static string localPath;

        public static string LocalPath { get => localPath; set => localPath = value; }

        public static string RemoveVersionNumber(string path)
        {
            string res = "";
            try
            {
                var arr = path.Split('/');

                foreach (string item in arr)
                {
                    if (item.Split('.').Length != 3)
                    {
                        if (item != "")
                        {
                            res += "/" + item;
                        }
                    }
                }
            }
            catch (Exception ee)
            {

                throw;
            }

            return res;
        }
        public static string GetLocalPath()
        {
            string fileBasePath = "";

            #region prepare
            switch (Device.RuntimePlatform)
            {
                case "Android":
                    fileBasePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    break;
                case "iOS":
                    fileBasePath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    break;

                case "UWP":
                    fileBasePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    break;
                default:
                    fileBasePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    break;
            }


            #endregion
            return fileBasePath;
        }
        public static string GetExternalPath()
        {
            string fileBasePath = "";

            #region prepare
            switch (Device.RuntimePlatform)
            {
                case "Android":
                    fileBasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
                    break;
                case "iOS":
                    fileBasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
                    break;

                case "UWP":
                    fileBasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
                    break;
                default:
                    fileBasePath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
                    break;
            }


            #endregion
            return fileBasePath;
        }
        public static string GetPathSeparator()
        {
            string pathSeparator = "";
            #region prepare
            switch (Device.RuntimePlatform)
            {
                case "Android":
                    pathSeparator = @"/";
                    break;
                case "iOS":
                    pathSeparator = @"/";
                    break;

                case "UWP":
                    pathSeparator = @"\";
                    break;
                default:
                    pathSeparator = @"\";
                    break;
            }
            #endregion  
            return pathSeparator;
        }

        public static byte[] encodeByteArray(byte[] inputBuffer)
        {
            byte[] keysBuffer = System.Text.Encoding.ASCII.GetBytes(ENCRYPTION_KEY);
            byte[] output = new byte[inputBuffer.Length];

            int offset = 0;
            int inChar;
            int outChar;
            int bitMask;

            for (int i = 0; i < inputBuffer.Length; i++)
            {
                offset = i % keysBuffer.Length;
                inChar = inputBuffer[i];
                bitMask = keysBuffer[offset];
                outChar = bitMask ^ inChar;
                byte outc = Convert.ToByte(outChar);
                output[i] = outc;
            }

            return output;
        }
        public static byte[] codeByteArray(byte[] inputBuffer)
        {
            byte[] keysBuffer = System.Text.Encoding.ASCII.GetBytes(ENCRYPTION_KEY);
            byte[] output = new byte[inputBuffer.Length];

            int offset = 0;
            int inChar;
            int outChar;
            int bitMask;

            for (int i = 0; i < inputBuffer.Length; i++)
            {
                offset = i % keysBuffer.Length;
                inChar = inputBuffer[i];
                bitMask = keysBuffer[offset];
                outChar = bitMask ^ inChar;
                byte outc = Convert.ToByte(outChar);
                output[i] = outc;
            }

            return output;
        }

        public static void SetTitle(string title, Label control)
        {

            try
            {
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.LoadXml(title);
                    double leftMargin = double.Parse(doc.FirstChild.Attributes["LEFTMARGIN"].Value);
                    string alligment = doc.FirstChild.FirstChild.Attributes[0].Value;
                    switch (alligment)
                    {
                        case "LEFT":
                            control.HorizontalTextAlignment = TextAlignment.Start; break;
                        case "CENTER":
                            control.HorizontalTextAlignment = TextAlignment.Center; break;
                        case "Right":
                            control.HorizontalTextAlignment = TextAlignment.End; break;
                    }
                    Thickness th = new Thickness(leftMargin, 0, 0, 0);
                    control.Margin = th;

                    string font = doc.FirstChild.FirstChild.FirstChild.Attributes["FACE"].Value;
                    int SIZE = int.Parse(doc.FirstChild.FirstChild.FirstChild.Attributes["SIZE"].Value);
                    string COLOR = doc.FirstChild.FirstChild.FirstChild.Attributes["COLOR"].Value;
                    string LETTERSPACING = doc.FirstChild.FirstChild.FirstChild.Attributes["LETTERSPACING"].Value;
                    string KERNING = doc.FirstChild.FirstChild.FirstChild.Attributes["KERNING"].Value;
                    control.FontFamily = font;
                    control.FontSize = SIZE * 1.2;
                    control.TextColor = Color.FromHex(COLOR);
                    //
                    control.Text = doc.InnerText;
                }
                catch (Exception ee)
                {
                    control.Text = title;
                    logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                }


            }
            catch (System.Exception ee)
            {
                logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                string msg = ee.Message;
            }

        }
        public static void SetNote(string title, Label control)
        {

            XmlDocument doc = new XmlDocument();
            try
            {
                //try Note to parse
                title = "<root>" + title + "</root>";
                doc.LoadXml(title);
                foreach (XmlNode node in doc.ChildNodes)
                {
                    if (node.NodeType == XmlNodeType.Element)
                    {
                        control.Text += node.InnerText;
                        double leftMargin = 0;
                        if (node.FirstChild != null)
                        {
                            if (node.FirstChild.Attributes != null)
                            {
                                double.TryParse(node.FirstChild.Attributes["LEFTMARGIN"].Value, out leftMargin);

                                string alligment = node.FirstChild.FirstChild.Attributes[0].Value;
                                switch (alligment)
                                {
                                    case "LEFT":
                                        control.HorizontalTextAlignment = TextAlignment.Start; break;
                                    case "CENTER":
                                        control.HorizontalTextAlignment = TextAlignment.Center; break;
                                    case "Right":
                                        control.HorizontalTextAlignment = TextAlignment.End; break;
                                }
                                Thickness th = new Thickness(leftMargin, 0, 0, 0);
                                control.Margin = th;

                                string font = node.FirstChild.FirstChild.FirstChild.Attributes["FACE"].Value;
                                int SIZE = 19;
                                int.TryParse(node.FirstChild.FirstChild.FirstChild.Attributes["SIZE"].Value, out SIZE);
                                string COLOR = node.FirstChild.FirstChild.FirstChild.Attributes["COLOR"].Value;
                                string LETTERSPACING = node.FirstChild.FirstChild.FirstChild.Attributes["LETTERSPACING"].Value;
                                string KERNING = node.FirstChild.FirstChild.FirstChild.Attributes["KERNING"].Value;
                                control.FontFamily = font;
                                control.FontSize = SIZE * 1.2;
                                control.BackgroundColor = Color.FromHex(COLOR);
                            }
                        }
                        
                        //
                       
                    }
                }
            }
            catch (System.Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);                
            }

        }

        public static string GetTitle(string title)
        {
            string res = title;

            try
            {
                if (title.StartsWith("<"))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(title);
                    double leftMargin = double.Parse(doc.FirstChild.Attributes["LEFTMARGIN"].Value);
                    string alligment = doc.FirstChild.FirstChild.Attributes[0].Value;

                    Thickness th = new Thickness(leftMargin, 0, 0, 0);
                    // control.Margin = th;

                    string font = doc.FirstChild.FirstChild.FirstChild.Attributes["FACE"].Value;
                    int SIZE = int.Parse(doc.FirstChild.FirstChild.FirstChild.Attributes["SIZE"].Value);
                    string COLOR = doc.FirstChild.FirstChild.FirstChild.Attributes["COLOR"].Value;
                    string LETTERSPACING = doc.FirstChild.FirstChild.FirstChild.Attributes["LETTERSPACING"].Value;
                    string KERNING = doc.FirstChild.FirstChild.FirstChild.Attributes["KERNING"].Value;
                    //control.FontFamily = font;
                    //control.FontSize = SIZE;
                    //control.TextColor = Color.FromHex(COLOR);
                    //
                    res = doc.InnerText;
                }
                else
                {
                    res = title;
                }
            }
            catch (System.Exception ee)
            {
                logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                string msg = ee.Message;
            }
            return res;
        }
        //3840
        public static float CalculateDeltaF(int textLen)
        {
            float delta = 1.18f;
            Hashtable htCheck = new Hashtable();
            htCheck.Add(1.75f, new Measure(1, 7, 2.5f, 2.2f));
            htCheck.Add(1.38f, new Measure(8, 15, 2.1f, 1.4f));
            htCheck.Add(1.24f, new Measure(16, 24, 1.5f, 1.3f));
            htCheck.Add(1.18f, new Measure(25, 32, 1.4f, 1.26f));
            foreach (DictionaryEntry de in htCheck)
            {
                Measure m = (Measure)de.Value;
                if (textLen == m.from)
                {
                    return m.ValFrom;
                }
                if (textLen == m.to)
                {
                    return m.ValTo;
                }

                if (textLen > m.from && textLen < m.to)
                {
                    int step = m.to - m.from;
                    if (step > 0)
                    {
                        float fstep = (m.ValFrom - m.ValTo) / step;
                        int tmpSteps = m.to - textLen;
                        if (tmpSteps > 0)
                        {
                            float res = m.ValFrom - ((textLen - m.from) * fstep);
                            return res;
                        }
                    }
                }
            }


            return delta;
        }
        //1920
        public static float CalculateDeltaF2(int textLen)
        {
            float delta = 1.18f;
            Hashtable htCheck = new Hashtable();
            htCheck.Add(1.75f, new Measure(1, 7, 2.0f, 1.75f));
            htCheck.Add(1.38f, new Measure(8, 15, 1.74f, 1.38f));
            htCheck.Add(1.24f, new Measure(16, 24, 1.37f, 1.24f));
            htCheck.Add(1.18f, new Measure(25, 32, 1.23f, 1.18f));
            foreach (DictionaryEntry de in htCheck)
            {
                Measure m = (Measure)de.Value;
                if (textLen == m.from)
                {
                    return m.ValFrom;
                }
                if (textLen == m.to)
                {
                    return m.ValTo;
                }

                if (textLen > m.from && textLen < m.to)
                {
                    int step = m.to - m.from;
                    if (step > 0)
                    {
                        float fstep = (m.ValFrom - m.ValTo) / step;
                        int tmpSteps = m.to - textLen;
                        if (tmpSteps > 0)
                        {
                            float res = m.ValFrom - ((textLen - m.from) * fstep);
                            return res;
                        }
                    }
                }
            }


            return delta;
        }

        public static async Task OpenWebPage()
        {
            try
            {
                Uri uri = new Uri("http://www.portalishkollor.al/");
                await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
            }
        }
        public static async Task<bool> OpenWebPage(string strURL)
        {
            string url=strURL.Trim();
            try
            {
                Uri uri = new Uri(strURL);
                await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
                
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee,url);
                return false;
            }
            return true;
        }
    }
}
