﻿using KlasimeViews;
using SkiaSharp;
using System;
using XBook2;

namespace KlasimeBase
{
    public enum BookButtonAction
    {
        Activate,
        Download,
        Update,
        Open,
        Close,
        Remove
    }

    public class BookEventArgs : EventArgs
    {
        public AlbasBook AlbasBook { get; set; }
        public BookButtonAction Action { get; set; }
        public BookEventArgs(AlbasBook albasBook, BookButtonAction action)
        {
            try
            {
                AlbasBook = albasBook;
                Action = action;
            }
            catch (Exception ee)
            {

                string errMessage = ee.Message;
            }
           
        }
    }
    public class BookThumbEventArgs : EventArgs
    {
        public int PageId { get; set; }

        public BookThumbEventArgs(int pageID)
        {
            PageId = pageID;
        }
    }
    public class LanguageSettingsEventArgs : EventArgs
    {
        public Localization.Languages Language { get; set; }

        public LanguageSettingsEventArgs(Localization.Languages selectedLanguage)
        {
            Language = selectedLanguage;
        }
    }
    public class BookMultimediaNavEventArgs : EventArgs
    {
        public Anno MultimediaAnno { get; set; }

        public BookMultimediaNavEventArgs(Anno anno)
        {
            MultimediaAnno = anno;
        }
    }
    public class PageMouseMoveEventArgs : EventArgs
    {
        public PageType PageType { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public PageMouseMoveEventArgs(PageType myPageType, double x, double y,int w, int h)
        {
            PageType = myPageType;
            X = x;
            Y= y;
            Width = w;
            Height = h;
        }
    }
    public class BookDownloadEventArgs : EventArgs
    {
        public string FileName { get; set; }
        public int Total { get; set; }
        public double Progress { get; set; }
        public BookDownloadEventArgs(string fileName)
        {
            FileName = fileName;
        }
    }
    public class MultimediaEventArgs : EventArgs
    {
        public MultimediaType MultimediaType { get; private set; }
        public Anno FindAnno { get; private set; }
     
        public int ImageWidth { get; private set; }
        public int ImageHeight { get; private set; }
        public string VideoUrl { get; private set; }
        public bool AddNew { get;  set; }
        public MultimediaEventArgs(MultimediaType type, int imageWidth, int imageHeight, Anno findAnno, string videoUrl)
        {
            MultimediaType = type;
            FindAnno = findAnno;
            ImageWidth = imageWidth;
            ImageHeight = imageHeight;
            VideoUrl = videoUrl;
        }
    }
    public class ProgressEventArgs : EventArgs
    {
        public string Text { get; private set; }
        public ProgressEventArgs(string text)
        {
            Text = text;
        }
    }
    public class ProgressUpdateEventArgs : EventArgs
    {
        public string FileName { get; set; }
        public int DownloadCounter { get; private set; }
        public int TotalFilesToDownload { get; private set; }
        public ProgressUpdateEventArgs(int currentValue, int totalFiles, string fileName)
        {
            DownloadCounter = currentValue;
            TotalFilesToDownload = totalFiles;
            FileName = fileName;
        }
    }
    public class DownloadEventArgs : EventArgs
    {
        public bool FileSaved = false;
        public DownloadEventArgs(bool fileSaved)
        {
            FileSaved = fileSaved;
        }
    }
    public class MultimediaRulerEventArgs : EventArgs
    {
        public MultimediaType MultimediaType { get; private set; }
        public Anno FindAnno { get; private set; }
        public int ImageWidth { get; private set; }
        public int ImageHeight { get; private set; }
        public string VideoUrl { get; private set; }
        public double BaseProp { get; set; }
        public int Y { get; private set; }
        public double origY { get; set; }
        public double YProp { get; set; }
        public SKImageInfo imageInfo { get; set; }
        public MultimediaRulerEventArgs(MultimediaType type, Anno findAnno, int y)
        {
            MultimediaType = type;
            Y = y;
            FindAnno = findAnno;
            //ImageWidth = imageWidth;
            //ImageHeight = imageHeight;
            //VideoUrl = videoUrl;
        }
    }
    public class MultimediaRemoveEventArgs : EventArgs
    {
       
        public Anno FindAnno { get; private set; }
        
        public MultimediaRemoveEventArgs( Anno findAnno)
        {
            FindAnno = findAnno;
            
        }
    }

    public class SearchEventArgs : EventArgs
    {

        public int PageNum { get; private set; }
        public string SearchText { get; private set; }
        public string SearchTextLocation { get; private set; }
        public string VideoUrl { get; private set; }
        public SearchEventArgs(int pageNum, string searchText, string searchTextLocation)
        {


            PageNum = pageNum;
            SearchText = searchText;
            SearchTextLocation = searchTextLocation;
        }
    }
}