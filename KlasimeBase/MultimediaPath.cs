﻿namespace KlasimeViews
{
    public class MultimediaPath
    {
        public static string GetCustomMultimediaPath(MultimediaType multimediaType)
        {
            string path = "";
            switch (multimediaType)
            {
                case MultimediaType.Note:
                    path = "customNotes";
                    break;
                case MultimediaType.File:
                    path = "customFiles";
                    break;
                case MultimediaType.Link:
                    path = "customLinks";
                    break;
                case MultimediaType.Audio:
                    path = "customAudios";
                    break;
                case MultimediaType.Video:
                    path = "customVideos";
                    break;
                case MultimediaType.YouTube:
                    path = "customYoutoubes";
                    break;
                case MultimediaType.PhotoGalery:
                    path = "customPhotos";
                    break;
            }
            return path;
        }
        public static string GetActionType(MultimediaType multimediaType)
        {
            string path = "";
            switch (multimediaType)
            {
                case MultimediaType.Note:
                    path = "com.mobiano.flipbook.pageeditor.TAnnoActionShowInformation";
                    break;
                case MultimediaType.File:
                    path = "com.mobiano.flipbook.pageeditor.TAnnoActionFile";
                    break;
                case MultimediaType.Link:
                    path = "com.mobiano.flipbook.pageeditor.TAnnoActionOpenURL";
                    break;
                case MultimediaType.Audio:
                    path = "com.mobiano.flipbook.Action.TAnnoActionPlayAudio";
                    break;
                case MultimediaType.Video:
                    path = "com.mobiano.flipbook.pageeditor.TAnnoActionOpenWindow";
                    break;
                case MultimediaType.YouTube:
                    path = "com.mobiano.flipbook.pageeditor.TAnnoActionPlayVideo";
                    //nodAnno.Action.ResourceContent == null
                    //nodAnno.Action.WindowType == "TYPE_YOUTUBE"
                    break;
                case MultimediaType.PhotoGalery:
                    path = "com.mobiano.flipbook.pageeditor.TAnnoActionPhotoSlide";
                    break;
            }
            return path;
        }
    }
}