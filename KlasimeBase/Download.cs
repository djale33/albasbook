﻿using KlasimeBase.Log;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Net;
using System.IO;
using System.Text;
using Xamarin.Forms;
using System.Reflection;
using System.Threading.Tasks;
using System.Net.Http;
using KlasimeViews;
using XBook2;
//using System.Security.Policy;
using System.ComponentModel;
using System.Threading;

namespace KlasimeBase
{
    public  class Download : BaseLog
    {
        public static string user = "program";
        #region VARIABLES

        public static bool startDownload = false;
        public static int progressDownload = 0;
        static string localPath;
        string DownloadfileName = "";
       
        
        public static WebRequest request;

        #endregion

        #region Custom Event
        public event EventHandler OnFinishDownloadZipEvent;
        #endregion

        #region Custom Event
        public delegate void BookClickHandler(object sender, BookDownloadEventArgs e);
        public event BookClickHandler OnFileFinishEvent;
        public event BookClickHandler OnFileProgressEvent;

        private void OnFileDownloadFinishEvent(object sender, BookDownloadEventArgs e)
        {
            if (OnFileFinishEvent != null)
            {
                OnFileFinishEvent(sender, e);
            }
        }
        private void OnFileDownloadProgressEvent(object sender, BookDownloadEventArgs e)
        {
            if (OnFileProgressEvent != null)
            {
                OnFileProgressEvent(sender, e);
            }
        }
        #endregion

        #region PROPERTIES
        private bool Test { get; set; }
        public string BasePath { get; set; }
        public static string LocalPath { get => localPath; set => localPath = value; }
        #endregion

        #region CONSTRUCTOR
        public Download(string _basePath, string _localPath, string _urlPath, bool test = false)
        {
            LocalPath = _localPath;
            BasePath = _basePath;
            UrlPath = _urlPath;
            Test = test;
        }
        #endregion

        #region METHODS
        //public void GetZipFile(string urlPath)
        //{
        //    try
        //    {
        //        startDownload = true;
        //        Uri uri = new Uri(urlPath);
        //        request = WebRequest.Create(uri);

        //        request.BeginGetResponse(WebRequestCallbackZip, null);

        //    }
        //    catch (Exception ee)
        //    {
        //       FileLog.Log(MethodBase.GetCurrentMethod(), ee);

        //    }

        //}
        //public void WebRequestCallbackZip(IAsyncResult result)
        //{
        //    string fileName = "tmpFile.zip";
        //    fileName = Path.Combine(KlasimeBase.Common.GetLocalPath(), fileName);
        //    Device.BeginInvokeOnMainThread(() =>
        //    {
        //        try
        //        {

        //            Stream stream = request.EndGetResponse(result).GetResponseStream();

        //            if (File.Exists(fileName) == true)
        //            {
        //                File.Delete(fileName);
        //            }

        //            if (File.Exists(fileName) == false)
        //            {
        //                FileStream fs = new FileStream(fileName, FileMode.CreateNew);
        //                stream.CopyTo(fs);
        //                fs.Close();
        //                fs.Dispose();
        //            }

        //            if (File.Exists(fileName) == true)
        //            {
        //                //string MyFileNameFromZip = Path.Combine(KlasimeBase.Common.GetLocalPath(), "bukvar9.jpg");
        //                //if (File.Exists(MyFileNameFromZip) == false)
        //                //{
        //                ZipFile.ExtractToDirectory(fileName, KlasimeBase.Common.GetLocalPath() + localPath);
        //                //}
        //                //MyImage = SetImageSource(MyFileNameFromZip);

        //                if (File.Exists(fileName) == true)
        //                {
        //                    File.Delete(fileName);
        //                }

        //            }
        //            else
        //            {
        //                string Status = "image not exist: " + fileName;
        //            }
        //            progressDownload = 100;
        //            startDownload = false;
        //            var handler = OnFinishDownloadZipEvent;
        //            if (handler != null)
        //            {
        //                handler(this, EventArgs.Empty);
        //            }

        //        }
        //        catch (Exception ee)
        //        {
        //           FileLog.Log(MethodBase.GetCurrentMethod(), ee);
        //            progressDownload = 0;
        //            startDownload = false;

        //            if (File.Exists(fileName) == true)
        //            {
        //                File.Delete(fileName);
        //            }

        //            var handler = OnFinishDownloadZipEvent;
        //            if (handler != null)
        //            {
        //                handler(this, EventArgs.Empty);
        //            }
        //        }
        //    });

        //}

        public bool GetFileFromUrl(string urlPath)
        {
            bool res = true;
            try
            {
                startDownload = true;
                Uri uri = new Uri(urlPath);
                request = WebRequest.Create(uri);
                var arrwww = urlPath.Split('/');
                if (arrwww.Length > 1)
                {
                    DownloadfileName = arrwww[arrwww.Length - 1];
                    
                }
               request.BeginGetResponse(WebRequestCallbackZip2, null);

            }
            catch (Exception ee)
            {
               logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
                res= false;
            }

            return res;
        }
       
        public void WebRequestCallbackZip2(IAsyncResult result)
        {
           
           string fileName = Path.Combine(KlasimeBase.Common.GetLocalPath(), DownloadfileName);
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {

                    Stream stream = request.EndGetResponse(result).GetResponseStream();

                    if (File.Exists(fileName) == true)
                    {
                        File.Delete(fileName);
                    }

                    if (File.Exists(fileName) == false)
                    {
                        FileStream fs = new FileStream(fileName, FileMode.CreateNew);
                        stream.CopyTo(fs);
                        fs.Close();
                        fs.Dispose();
                    }

                    if (File.Exists(fileName) == true)
                    {
                        //decompile
                        byte[] source = File.ReadAllBytes(fileName);
                        //delete original after
                        if (File.Exists(fileName) == true)
                        {
                            File.Delete(fileName);
                        }

                        byte[] res = Common.encodeByteArray(source);
                        File.WriteAllBytes(fileName, res);

                       

                    }
                    else
                    {
                        string Status = "image not exist: " + fileName;
                    }
                    progressDownload = 100;
                    startDownload = false;
                    var handler = OnFinishDownloadZipEvent;
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }

                }
                catch (Exception exc)
                {
                    progressDownload = 0;
                    startDownload = false;
                   logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), exc);
                    if (File.Exists(fileName) == true)
                    {
                        File.Delete(fileName);
                    }

                    var handler = OnFinishDownloadZipEvent;
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }
                  
                }
            });
      
        }
        AutoResetEvent completed = new AutoResetEvent(false);
        static string UrlPath;
        WebClient client;
        public async Task<bool> getFileFromUrlAsync()
        {
            //string urlPath,string localPath

            try
            {
                Uri uri = new Uri(UrlPath);
                request = WebRequest.Create(uri);

                client = new WebClient();

                //release build
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                //if (File.Exists(localPath) == false)
                {
                    client.DownloadFileAsync(uri, LocalPath);
                    //client.DownloadFile(uri, LocalPath);

                    return true;
                }

                //completed.WaitOne();

            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }
            return false;
        }
        public async Task<bool> getFileFromUrlSingle()
        {
            //string urlPath,string localPath

            try
            {
                Uri uri = new Uri(UrlPath);
                request = WebRequest.Create(uri);

                client = new WebClient();

                //release build
                //client.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                //client.DownloadProgressChanged += Client_DownloadProgressChanged;
                //if (File.Exists(localPath) == false)
                {
                    //client.DownloadFileAsync(uri, LocalPath);
                    client.DownloadFile(uri, LocalPath);

                    return true;
                }

                //completed.WaitOne();

            }
            catch (Exception ee)
            {
                logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);

            }
            return false;
        }

        public event EventHandler<DownloadEventArgs> OnFileDownloaded;
        public async Task<int> getFileFromUrl2(string urlPath)
        {
            int res = 0;
            try 
            {
                Uri uri = new Uri(urlPath);
                request = WebRequest.Create(uri);
                var arrwww = urlPath.Split('/');
                if (arrwww.Length > 1)
                {
                    DownloadfileName = arrwww[arrwww.Length - 1];

                }
                string fileName1 = Path.Combine(KlasimeBase.Common.GetLocalPath(), DownloadfileName);

                WebClient client = new WebClient();

                //not work on windows home
                //release build
                //client.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                //client.DownloadProgressChanged += Client_DownloadProgressChanged;
                if (File.Exists(localPath) == false)
                {
                    client.DownloadFile(uri, localPath);
                    res = 1;
                }
                else
                {
                    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(),null, "localPath exist: " + localPath);
                }
                return res;
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);           
                return res;
            }

        }
        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                if (OnFileDownloaded != null)
                    OnFileDownloaded.Invoke(this, new DownloadEventArgs(false));
            }
            else
            {
                //if (OnFileDownloaded != null)
                //{
                //    OnFileDownloaded.Invoke(this, new DownloadEventArgs(true));
                //}
                BookDownloadEventArgs b = new BookDownloadEventArgs(sender.ToString());
                b.FileName = LocalPath;
                OnFileDownloadFinishEvent(null, b);
            }

            //try
            //{

            //    BookDownloadEventArgs b = new BookDownloadEventArgs(sender.ToString());

            //    OnFileDownloadFinishEvent(null, b);
            //}
            //catch (Exception ee)
            //{
            //    logInstance.Log(user, Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            //}
        }
        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            try
            {

                BookDownloadEventArgs b = new BookDownloadEventArgs(sender.ToString());
                b.Progress = (e.BytesReceived *100) / (e.TotalBytesToReceive );
                
                OnFileDownloadProgressEvent(null, b);
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        private void Client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            try
            {

                BookDownloadEventArgs b = new BookDownloadEventArgs(sender.ToString());

                OnFileDownloadFinishEvent(null, b);
            }
            catch (Exception ee)
            {
                logInstance.Log(user,Severity.Critical, MethodBase.GetCurrentMethod(), ee);
            }
        }

        async Task<Stream> GetStreamAsync(string uri) 
        { 
            TaskFactory factory = new TaskFactory(); 
            WebRequest request = WebRequest.Create(uri); 
            WebResponse response = await factory.FromAsync<WebResponse>(request.BeginGetResponse, request.EndGetResponse, null); 
            return response.GetResponseStream(); 
        }


        //public async Task<bool> GetStreamAsync2(string url)
        //{
        //    using (var client = new System.Net.Http.HttpClient()) // WebClient
        //    {
                
        //        var uri = new Uri(url);

        //        return await   client.DownloadFileTaskAsync(uri, localPath);
                
        //    }
        //}
        #endregion
    }
    //public static class HttpClientUtils
    //{
    //    public static string ENCRYPTION_KEY = "klasaime_key";

    //    #region Custom Event
    //    public delegate void BookClickHandler(object sender, BookDownloadEventArgs e);
    //    public static event BookClickHandler OnFileFinishEvent;

    //    private static void OnFileDownloadFinishEvent(object sender, BookDownloadEventArgs e)
    //    {
    //        if (OnFileFinishEvent != null)
    //        {
    //            OnFileFinishEvent(sender, e);
    //        }
    //    }
    //    #endregion
    //    private static byte[] encodeByteArray(byte[] inputBuffer, string key)
    //    {
    //        byte[] keysBuffer = System.Text.Encoding.ASCII.GetBytes(key);
    //        byte[] output = new byte[inputBuffer.Length];

    //        int offset = 0;
    //        int inChar;
    //        int outChar;
    //        int bitMask;

    //        for (int i = 0; i < inputBuffer.Length; i++)
    //        {
    //            offset = i % keysBuffer.Length;
    //            inChar = inputBuffer[i];
    //            bitMask = keysBuffer[offset];
    //            outChar = bitMask ^ inChar;
    //            byte outc = Convert.ToByte(outChar);
    //            output[i] = outc;
    //        }

    //        return output;
    //    }

    //    public static async Task<bool> DownloadFileTaskAsync(this HttpClient client, Uri uri, string FileName)
    //    {
    //        using (var s = await client.GetStreamAsync(uri))
    //        {
    //            //using (var fs = new FileStream(FileName, FileMode.CreateNew))
    //            {
    //                using (MemoryStream ms = new MemoryStream())
    //                {
                        
    //                    await s.CopyToAsync(ms);
    //                    byte[] res = encodeByteArray(ms.ToArray(), ENCRYPTION_KEY);
    //                    //await fs.CopyToAsync(res); //or ms decoded
    //                    File.WriteAllBytes(FileName, res);
    //                    //send event that file is done

    //                    //await s.CopyToAsync(fs);
    //                    try
    //                    {

    //                        BookDownloadEventArgs b = new BookDownloadEventArgs(FileName);

    //                        OnFileDownloadFinishEvent(null, b);
    //                    }
    //                    catch (Exception ee)
    //                    {
    //                        FileLog.Log(MethodBase.GetCurrentMethod(), ee);
    //                    }
    //                }
    //            }
    //        }
    //        return true;
    //    }
    //}
}
