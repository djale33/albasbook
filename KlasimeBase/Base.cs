﻿using KlasimeBase.Log;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Xml;

namespace KlasimeBase
{
    public static class Base 
    {
        public static ApiLog logInstance =  ApiLog.GetInstance();
        public static string user="";
        public static double SetDouble(XmlAttribute input)
        {
            double res = 0;
            if (input != null)
            {
                try
                {
                    res = Double.Parse(input.InnerText, CultureInfo.InvariantCulture);
                }
                catch (Exception ee)
                {
                    if (input != null)
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(),null, input.ToString() + ee );
                    }
                    else
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                    }
                }
            }
            return res;
        }
        public static double SetDouble(string input)
        {
            double res = 0;
            if (input != null)
            {
                try
                {
                    res = Double.Parse(input, CultureInfo.InvariantCulture);
                }
                catch (Exception ee)
                {
                    if (input != null)
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(),  ee,input);
                    }
                    else
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(),  ee);
                    }
                }
            }
            return res;
        }
        public static int SetInt32(XmlAttribute input)
        {
            int res = 0;
            if (input != null)
            {
                try
                {
                    if (input.InnerText != "NaN")
                    {
                        res = Int32.Parse(input.InnerText, CultureInfo.InvariantCulture);
                    }
                }
                catch (Exception ee)
                {
                    if (input != null)
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(),  ee, input.ToString());
                    }
                    else
                    {
                       logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                    }
                }
            }

            return res;
        }
        public static int SetInt32(XmlNode input)
        {
            int res = 0;
            if (input != null)
            {
                try
                {
                    res = Int32.Parse(input.InnerText, CultureInfo.InvariantCulture);
                }
                catch (Exception ee)
                {
                    if (input != null)
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee, input.ToString());
                    }
                    else
                    {
                       logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                    }
                }
            }

            return res;
        }
        public static int SetInt32(object input)
        {
            int res = 0;
            if (input != null)
            {
                try
                {
                    res = Convert.ToInt32(input, CultureInfo.InvariantCulture);
                }
                catch (Exception ee)
                {
                    if (input != null)
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee, input.ToString());
                    }
                    else
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                    }
                }
            }

            return res;
        }

       
        public static bool SetBool(XmlAttribute input)
        {
            bool res=false;
            if (input != null)
            {
                try
                {
                    res = bool.Parse(input.InnerText);
                }
                catch (Exception ee)
                {
                    if (input != null)
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee, input.ToString());
                    }
                    else
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                    }
                }
            }

            return res;
        }
        public static bool SetBool(XmlNode input)
        {
            bool res = false;
            if (input != null)
            {
                try
                {
                    res = bool.Parse(input.InnerText);
                }
                catch (Exception ee)
                {
                    if (input != null)
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee, input.ToString());
                    }
                    else
                    {
                        logInstance.Log(user,Severity.High, MethodBase.GetCurrentMethod(), ee);
                    }
                }
            }

            return res;
        }
        public static string PrepareJsonArray(string json)
        {
            string res = json.Remove(0, json.IndexOf("["));
            res = "{\"1.0.0\":" + res.Trim();
            return res;
        }

    }
}
