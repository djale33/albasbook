﻿using KlasimeBase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SharedKlasime
{
    public class CUser
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string photo { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public object contact_number { get; set; }
        public object address { get; set; }
        public List<object> schools { get; set; }
        public List<string> subjects { get; set; }
        [JsonProperty("token")]
        public string token { get; set; }
        public Localization.Languages currentLanguage { get; set; }
        public ColorTemlates currentColorTempalte { get; set; }
        public UserType currentUserType { get; set; }   
    }
}
