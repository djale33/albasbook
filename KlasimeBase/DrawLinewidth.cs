﻿//using Windows.ApplicationModel.Core;
//using Windows.Graphics.Display;
//using Windows.UI.ViewManagement;

namespace KlasimeBase
{
    public enum DrawLineWidth
    {
        Thin = 4,
        Medium = 8,
        Thick = 16
    }
    public enum DrawLineColor
    {
        LightPink,
        LightBlue,
        LightGreen
    }
}