﻿using KlasimeBase;
using SkiaSharp;

namespace KlasimeBase
{
    public class MyPenList
    {
        public DrawLineColor DrawLineColorEnum { get; set; }
        public DrawLineWidth MyDrawLineWidth { get; set; }      
        public SKPath sKPath { get; set; }
        
        public MyPenList(SKPath sKPath, DrawLineColor colorMyDrawLineColorColor, DrawLineWidth myDrawLineWidth)
        {
            this.sKPath = sKPath;
            DrawLineColorEnum = colorMyDrawLineColorColor;
            MyDrawLineWidth = myDrawLineWidth;
        }
    }
}