﻿using System;

namespace KlasimeBase
{
    public partial class BookPage
    {
        public interface IDownloader
        {
            void DownloadFile(string url, string folder);
            event EventHandler<DownloadEventArgs> OnFileDownloaded;
        }
      

    }
}

