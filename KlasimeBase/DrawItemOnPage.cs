﻿using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace KlasimeBase
{
    public class CDrawItemOnPage
    {
        #region VARIABLES
        int pageNumber;
        DrawType drawOnPagePenType;
        DrawLineWidth drawOnPagePenSize;
        SKColor drawOnPageColor;
        SKPoint pointStartLine;
        SKPoint pointEndLine;
        SKPoint pointStartPen;
        bool startPenDraw=false;
        int lineCounter = 0;
        int drawCounter = 0;
        DrawLineColor drawLineColor;
        public Dictionary<int,MyLineList> htLines = new Dictionary<int, MyLineList>();
        Dictionary<int, MyLineList> htDraws = new Dictionary<int, MyLineList>();
        List<SKPoint> listPoints = new List<SKPoint>();
        SKPaint paintLine;
        public List<MyPenList> completedDrawingPaths = new List<MyPenList>();
        public Dictionary<long, SKPath> inProgressPaths = new Dictionary<long, SKPath>();
        public List<SKPoint> myLocationList= new List<SKPoint>();
        public Hashtable htPenDraws = new Hashtable();
        #endregion

        #region CONSTRUCTOR
        public CDrawItemOnPage()
        {
         
        }
        public CDrawItemOnPage(int pageNumber, DrawType drawOnPagePenType, DrawLineWidth drawOnPagePenSize, SKColor drawOnPageColor, DrawLineColor idrawLineColor = default)
        {
            this.pageNumber = pageNumber;
            this.DrawOnPagePenType = drawOnPagePenType;
            this.DrawOnPagePenSize = drawOnPagePenSize;
            this.DrawOnPageColor = drawOnPageColor;
            this.DrawLineColor = idrawLineColor;
        }
        #endregion

        #region PROPERTIES
        public DrawType DrawOnPagePenType { get => drawOnPagePenType; set => drawOnPagePenType = value; }
        public DrawLineWidth DrawOnPagePenSize { get => drawOnPagePenSize; set => drawOnPagePenSize = value; }
        public SKColor DrawOnPageColor { get => drawOnPageColor; set => drawOnPageColor = value; }
        public SKPoint PointStartLine { get => pointStartLine; set => pointStartLine = value; }
        public SKPoint PointEndLine { get => pointEndLine; set => pointEndLine = value; }
        public int LineCounter { get => lineCounter; set => lineCounter = value; }
        public int DrawCounter { get => drawCounter; set => drawCounter = value; }
        public Dictionary<int, MyLineList> HtLines { get => htLines; set => htLines = value; }
        public Dictionary<int, MyLineList> HtDraws { get => htDraws; set => htDraws = value; }
        public int PageNumber { get => pageNumber; set => pageNumber = value; }
        public SKPoint PointStartPen { get => pointStartPen; set => pointStartPen = value; }
        public List<SKPoint> ListPoints { get => listPoints; set => listPoints = value; }
        public bool StartPenDraw { get => startPenDraw; set => startPenDraw = value; }
        public SKPaint PaintLine { get => paintLine; set => paintLine = value; }
        public DrawLineColor DrawLineColor { get => drawLineColor; set => drawLineColor = value; }

        #endregion

    }
   
}
