﻿namespace KlasimeBase
{
    public class Measure
    {
        public int from;
        public int to;
        public float ValFrom;
        public float ValTo;
        public Measure(int _from, int _to, float _ValFrom, float _ValTo)
        {
            from = _from;
            to = _to;
            ValFrom = _ValFrom;
            ValTo = _ValTo;
        }
    }
}
