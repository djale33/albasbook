﻿using System.Collections.Generic;

namespace XBook2
{
    public class AlbasBook
    {

        public int id { get; set; }
        public string grade { get; set; }
        public string subject { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string price { get; set; }
        public string pages { get; set; }
        public string editors { get; set; }
        public string large_photo_url { get; set; }
        public string medium_photo_url { get; set; }
        public string small_photo_url { get; set; }
        public int activated { get; set; }
        public string total_size { get; set; }
        public string last_version { get; set; }
        public List<string> all_versions { get; set; }
        public int amazon_hosted { get; set; }
        public string downloads { get; set; }


    }

}