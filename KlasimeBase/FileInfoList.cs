﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace XBook2
{
    public class FileInfoList
    {
        [JsonProperty("1.0.0")]
        public List<FileInfo> fileInfo { get; set; }
    }

}