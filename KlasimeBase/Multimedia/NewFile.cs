﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KlasimeBase.Multimedia
{
    public class NewFile
    {
        string name;
        string description;
        string fullPath;
        Stream stream;

        public string Name { get => name; set => name = value; }
        public string Description { get => description; set => description = value; }
        public string FullPath { get => fullPath; set => fullPath = value; }
        public Stream Stream { get => stream; set => stream = value; }
    }
}
