﻿namespace KlasimeBase
{
    public class Photo
    {
        private string title;
        private string description;
        private string url;

        public string Title { get => title; set => title = value; }
        public string Description { get => description; set => description = value; }
        public string Url { get => url; set => url = value; }
    }
}
