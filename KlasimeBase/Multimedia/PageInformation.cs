﻿namespace KlasimeBase
{
    public class PageInformation
    {
        private int pageColor;

        public int PageColor { get => pageColor; set => pageColor = value; }
    }
}
