﻿namespace KlasimeBase
{
    public class Shadow
    {
        private bool hasDropShadow ;
        private int shadowDistance;
        private int shadowAngle ;
        private int shadowColor ;
        private double shadowAlpha ;
        private int shadowBlurX ;
        private int shadowBlurY ;

        public bool HasDropShadow { get => hasDropShadow; set => hasDropShadow = value; }
        public int ShadowDistance { get => shadowDistance; set => shadowDistance = value; }
        public int ShadowAngle { get => shadowAngle; set => shadowAngle = value; }
        public int ShadowColor { get => shadowColor; set => shadowColor = value; }
        public double ShadowAlpha { get => shadowAlpha; set => shadowAlpha = value; }
        public int ShadowBlurX { get => shadowBlurX; set => shadowBlurX = value; }
        public int ShadowBlurY { get => shadowBlurY; set => shadowBlurY = value; }
    }
}
