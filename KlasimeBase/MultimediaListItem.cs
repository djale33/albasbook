﻿using KlasimeViews;
using System.Security.Cryptography.X509Certificates;

namespace KlasimeBase
{
    public class MultimediaListItemOLD
    {
        public MultimediaListItemOLD(MultimediaType type, string title, int pageID, Anno multimediaAnno)
        {
            this.type = type;
            Title = title;
            PageID = pageID;
            this.multimediaAnno = multimediaAnno;
        }

        public MultimediaType type { get; set; }
        public string Title { get; set; }
        public int PageID { get; set; }
        public Anno multimediaAnno { get; set; }
        
    }

    public class SearchListItem
    {
        public SearchListItem(string searchItem, int pageID,string strlocation)
        {
            this.SearchItem = searchItem;
            
            PageID = pageID;
            strLocation = strlocation;


        }

        
        public string SearchItem { get; set; }
        public int PageID { get; set; }
        public string strLocation { get; set; }

    }
}

