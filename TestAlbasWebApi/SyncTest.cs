using System.Text.Json;
using System.Web;
using WebApiAlbas.Helper;
using WebApiAlbas.Model.DTO;

namespace TestAlbasWebApi
{
    public class BookMultimediaTests
    {
        BookMultimedia bookMultimedia;
       

        [SetUp]
        public void Setup()
        {
            bookMultimedia = new BookMultimedia("c:\\albas\\sync\\books\\");
        }

        [Test]
        [TestCase(false,null, 41, @"c:\Users\darko\AppData\Roaming\klasime\book\41\", "Ahmet Gashi")]
        [TestCase(true, "ArnoldiTest2", 41,@"c:\Users\darko\AppData\Roaming\klasime\book\41\",null)]
        public void GetBookShares(bool teacherUser, string? teacherName, int bookID,  string bookPath, string? schoolName)
        {

            List<MultimediaItemDTO> multimediaItems = bookMultimedia.GetBookShares(teacherUser, teacherName, bookID, schoolName);
            
           
            bool res =true;
            
            foreach (var temp in multimediaItems) {
                string jsonPath = HttpUtility.UrlEncode(temp.Path);
                List<SyncModelDTO> oneShare = bookMultimedia.getMultimediaItems(bookID, jsonPath, temp.Teacher);
                res = downloadMultimediaFromTeacher(oneShare, bookPath);
            }                    
           
            if (res == false )
            {
                Assert.Fail();
            }
            else
                Assert.Pass();
        }
        //client side
        public bool downloadMultimediaFromTeacher(List<SyncModelDTO> multimediaItems,string bookPath)
        {
            bool res = true;
            try
            {
                foreach (SyncModelDTO onetem in multimediaItems)
                {                    
                    foreach (MultimediaItemDTO item in onetem.MultimediaItems)
                    {
                        File.WriteAllText(bookPath + @"\" + item.Name, item.MultimediaString);
                    }

                }
            }
            catch (Exception)
            {

                res = false;
            }
            return res;
        }

        [Test]
        [TestCase(41, @"c:\Users\darko\AppData\Roaming\klasime\book\41\", "AlketaK",true,"all",null,"new Content 31","Description for content 31")]
        [TestCase(41, @"c:\Users\darko\AppData\Roaming\klasime\book\41\", "AlketaK", true, "private",null, "new Content 32", "Description for content 32")]
        [TestCase(41, @"c:\Users\darko\AppData\Roaming\klasime\book\41\", "AlketaK", true, "schools","mySchool", "new Content 33", "Description for content 33")]
        [TestCase(41, @"c:\Users\darko\AppData\Roaming\klasime\book\41\", "AlketaK", false, "schools", "mySchool", "new Content 34", "Description for content 34, no files")]
        public void TeacherUploadMultimedia(int bookID,string bookPath,string proffesor, bool takeAll,string innerFolder,string? schoolName, string contentName, string contentDescription)
        {
            List<SyncModelDTO> files = prepareForUploadMultimediaItems(bookID, bookPath,takeAll);

            bool res = bookMultimedia.uploadMultimediaItems(files, bookID, proffesor,innerFolder, schoolName, contentName, contentDescription);
            
            if (res == false)
            {
                Assert.Fail();
            }
            else
                Assert.Pass();
        }
       
        //client side
        /// <summary>
        /// get pen, bookmarks, notes or just a description?
        /// </summary>
        /// <param name="bookID"></param>
        /// <param name="bookFolder"></param>
        /// <returns></returns>
        public List<SyncModelDTO> prepareForUploadMultimediaItems(int bookID, string bookFolder,bool takeAll)
        {
            List<SyncModelDTO> res = new List<SyncModelDTO>();

            if (takeAll)
            {


                if (Directory.Exists(bookFolder))
                {

                    //folder name is profesor
                    DirectoryInfo di = new DirectoryInfo(bookFolder);

                    SyncModelDTO onetem = new SyncModelDTO();
                    onetem.BookId = bookID;
                    onetem.Proffesor = di.Name;
                    List<MultimediaItemDTO> multimediaItems = new List<MultimediaItemDTO>();
                    foreach (FileInfo fi in di.GetFiles("custom*.*"))
                    {

                        string oneFile = File.ReadAllText(fi.FullName);
                        MultimediaItemDTO mi = new MultimediaItemDTO(fi.Name, oneFile);
                        multimediaItems.Add(mi);
                    }
                    onetem.MultimediaItems = multimediaItems;
                    if (onetem.MultimediaItems.Count > 0)
                        res.Add(onetem);

                }
                else
                {
                    //no book directory, doesnt mean that is error, just no multimedia for that book
                }
            }
            return res;

        }
    }
}